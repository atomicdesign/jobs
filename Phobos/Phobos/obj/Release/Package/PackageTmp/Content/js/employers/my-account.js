﻿$(function () {

    $('#jobs-applied-for-link').click(function (e) {
        e.preventDefault();

        $('#saved-jobs').hide();
        $('#jobs-applied-for').show();

        $('#saved-jobs-link').css('text-decoration', 'none');
        $('#jobs-applied-for-link').css('text-decoration', 'underline');
    });

    $('#saved-jobs-link').click(function (e) {
        e.preventDefault();

        $('#jobs-applied-for').hide();
        $('#saved-jobs').show();

        $('#saved-jobs-link').css('text-decoration', 'underline');
        $('#jobs-applied-for-link').css('text-decoration', 'none');
    });

});