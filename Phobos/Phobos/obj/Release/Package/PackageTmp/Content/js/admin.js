﻿$(function () {
    tinymce.init({
        selector: 'textarea.wysiwyg',
        height: 600,
        width: 800,
        menubar: false,
        plugins: [
                "advlist autolink link lists charmap hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                "table contextmenu paste textcolor"
        ],
        toolbar: "code undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons"

    });
});

var HBTemplates = {};
function LoadHBTemplate(templateID) { HBTemplates[templateID] = Handlebars.compile($('#' + templateID).html()); }