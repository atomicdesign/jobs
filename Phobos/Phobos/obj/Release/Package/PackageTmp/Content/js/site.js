﻿//One main loader function please. Break everything out into separate functions and call from here
$(function () {
	MinPageHeight(1200);
	//LoadWYSIWYGEditors();
	NavColor();

	$('#register-employer [type="submit"]').click(function (e) {
	    registerEmployer(e);
	});

	function registerEmployer(e) {
	    var $form = $('#register-employer form');

	    e.preventDefault();

	    if ($form.valid()) {
	        $(e.target).prop('disabled', true);

	        $.ajax({
	            url: $form.attr('action'),
	            type: $form.attr('method'),
	            data: $form.serialize(),
	            success: function (result) {
	                if (!result.url) {
	                    $('#register-employer')
                            .html($(result).html())
                            .find('[type="submit"]').click(function (e) {
                                registerEmployer(e);
                            });
	                }
	                else {
	                    window.location.href = result.url;
	                }
	            }
	        });
	    }

	    return false;
	}

	$('#register-jobseeker [type="submit"]').click(function (e) {
	    registerJobseeker(e);
	});

	function registerJobseeker(e) {
	    var $form = $('#register-jobseeker form');

	    e.preventDefault();

	    if ($form.valid()) {
	        $(e.target).prop('disabled', true);

	        $.ajax({
	            url: $form.attr('action'),
	            type: $form.attr('method'),
	            data: $form.serialize(),
	            success: function (result) {
	                if (!result.url) {
	                    $('#register-jobseeker')
                            .html($(result).html())
                            .find('[type="submit"]').click(function (e) {
                                registerJobseeker(e);
                            });
	                }
	                else {
	                    window.location.href = result.url;
	                }
	            }
	        });
	    }

	    return false;
	}

	$('.signin-btn').click(function (e) {
	    var $form = $('#LoginModal form'),
            $target = $(e.target);
	    e.preventDefault();

	    if ($form.valid()) {
	        $target.prop('disabled', true);

	        $.ajax({
	            url: $form.attr('action'),
	            type: $form.attr('method'),
	            data: $form.serialize(),
	            success: function (result) {
	                if (!result.url) {
	                    $('#login-success').text('The email or password provided is incorrect.');
	                    $target.prop('disabled', false);
	                }
	                else {
	                    var email = $('#UserName').val();
	                    var password = $('#Password').val();
	                    document.cookie = "email=" + email + "&password=" + password + "; path=/";
	                    if (localStorage.redirectJob && localStorage.redirectJob != null && localStorage.redirectJob != '') {
	                        var jobURL = localStorage.redirectJob;
	                        localStorage.clear("redirectJob");
	                        window.location.href = jobURL;
	                        return;
	                    }
	                    window.location.href = result.url;
	                }
	            }
	        });
	    }
	});

	$('#job-fair-register-btn').click(function () {
	    window.location.href = '/hireveterans/order-services';
	});

	$('#jobseeker-job-fair-register-btn').click(function () {
	    window.location.href = '/jobseeker/job-fair-registration';
	});

	$('#jobfair-registration-yes-btn').click(function () {
	    window.location.href = '/jobseeker/job-fair-registration-2';
	});

	$('#jobfair-registration-no-btn').click(function () {
	    window.location.href = '/page/transition-advice';
	});

	$('#job-fair-registration-continue-btn').click(function () {
	    window.location.href = '/';
	});
});


function MinPageHeight(x) {
    var page_height = parseInt($(document.body).height());
    if (page_height < x) {
        var diff = (x - page_height) + 75;
        $('.footer').css('margin-top', diff + 'px');
    }
}

function NavColor(){
    var navItem = $('.dropdown-toggle');
    var dropDown = $('.dropdown-menu');

    dropDown.hover(
      function () {
          $(this).parent(navItem).addClass('Darkest-Blue-bg');
      }, function () {
          $(this).parent(navItem).removeClass('Darkest-Blue-bg');
      }
    );
}


//function LoadWYSIWYGEditors(){
//    tinymce.init({
//        selector: 'textarea.wysiwyg',
//        height: 200,
//        menubar: false,
//        verify_html: false,
//        convert_urls: false,
//        forced_root_block: false,
//        doctype: "<!DOCTYPE html>",
//        plugins: [
//                "advlist lists charmap hr pagebreak spellchecker",
//                "searchreplace wordcount visualblocks visualchars code insertdatetime nonbreaking",
//                "table contextmenu paste textcolor"
//        ],
//        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor emoticons",
//        valid_elements: "strong/b,div[align],br,p,ul,li,ol,hr,blockquote"
//    });
//};

var HBTemplates = {};
function LoadHBTemplate(templateID) { HBTemplates[templateID] = Handlebars.compile($('#' + templateID).html()); }

function storeData(e) {
    if (e == "employer") {

        var email = $('#register-employer #Email').val()
        var password = $('#register-employer #Password').val()
        document.cookie = "email=" + email + "&password=" + password + "; path=/";
    }
    else if(e=="jobseeker") {
        var email = $('#register-jobseeker #Email').val()
        var password = $('#register-jobseeker #Password').val()
        document.cookie = "email=" + email + "&password=" + password + "; path=/";
    }
}