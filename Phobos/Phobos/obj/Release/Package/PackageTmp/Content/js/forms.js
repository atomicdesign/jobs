﻿$(function () {
    $('#form-entry > form').submit(SubmitForm);
});

function SubmitForm(e) {
    e.stopPropagation(); e.preventDefault();
    var action = $(this).attr('action');
    $('#form-entry div.form-error').remove();
    $.ajax({
        url: action,
        data: $(this).serialize(),
        type: 'POST',
        dataType: 'json',
        success: ProcessForm,
        complete: CompleteForm
    });
}

function ProcessForm(data) {
    if (data.success) {
        $('#form-entry').remove();
        $('#form-complete').show();
    } else {
        for (var key in data.errors) {
            $('#form-' + key).append('<div class="form-error">' + key + data.errors[key] + '</div>');
        }
    }
}

function CompleteForm() {
}