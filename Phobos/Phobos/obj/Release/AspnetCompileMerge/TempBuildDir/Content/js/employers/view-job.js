﻿$(function () {

    $('#email-job').click(function (e) {
        var $this = $(this),
            $popup = $('<div id="email-popup">' +
                        '<p>Please enter a comma-separated list of email addresses:</p>' +
                        '<input type="text" id="email-addresses" /><br><br>' +
                        '<input type="submit" id="send-email" /><a style="margin-left: 25px;" href="#" id="cancel-send-email">Cancel</a>' +
                      '</div>');

        e.preventDefault();

        $popup.find('#send-email').click(function () {

            $.post("/jobseeker/email-job", { emails: $('#send-mail').val() })
                .done(function () {
                    $popup
                     .hide()
                     .find('#send-email').val('');
                });
        });

        $popup.find('#cancel-send-email').click(function (e) {
            e.preventDefault();
            $popup.hide();
        });

        $popup.hide();
        $this.parent().append($popup);
        $popup.fadeIn('fast');
    });

    $(document).on('click', '#save-job', function (e) {
        var $this = $(this),
            count = 0;

        e.preventDefault();

        $this
            .text('.')
            .css('text-decoration', 'none');

        var timer = setInterval(function () {
            var length = $this.text().length;
            count++;

            if (length === 1) {
                $this.text('. .');
            }
            else if (length === 3) {
                $this.text('. . .');
            }
            else {
                $this.text('.');
            }

            if (count === 3) {
                clearInterval(timer);
                $this.hide().text('Job Saved').fadeIn();

                setTimeout(function () {
                    $this.fadeOut();
                }, 100);
            }
        }, 500);
        $.post('/jobseeker/save-job', { id: $('#save-job').data('jobId') })
            .done(function (data) {
   
                clearInterval(timer);
                new PNotify({
                    title: 'Job Saved!',
                    text: 'Job has been saved!',
                    type: 'success'
                });
                $('#savedjobns').html('This job is currently saved.<br /><a href="javascript:void(0)" id="removefromsaved">Remove from Saved Jobs</a>');
            });
    });

    $(document).on('click', '#removefromsaved', function (e) {
        e.preventDefault();

        $.post('/jobseeker/jobIdForSelection', { id: $('#jobIdForSelection').val() })
            .done(function (data) {
                var tempval = $('#jobIdForSelection').val();
                $('#savedjobns').html('<a href="javascript:void(0)" id="save-job" data-job-id="' + tempval + '">Save Job</a>');
            });
    });

    $('#apply-job').click(function (e) {
       
        e.preventDefault();

        $.post('/jobseeker/apply-job', { id: $('#apply-job').data('jobId')})
            .done(function (data) {
                console.log(data);
                e.preventDefault();
                $('#myCongModal').modal('show');
                var url = "http://" + $('#apply-job').data('url') + "/";
                var temp = new Date();
                var day = temp.getDate().toString().length == 1 ? '0' + temp.getDay().toString() : temp.getDay().toString();
                var month = (temp.getMonth() + 1).toString().length == 1 ? '0' + (temp.getMonth() + 1).toString() : (temp.getMonth() + 1).toString();
                var applied =  "You applied for this job on "  +  day + '/' + month  + '/' + temp.getFullYear();
                $('#buttonOfApply').html(applied);
               
            });
    });

    $('#btn-submit-email').click(function () {
        document.getElementById('appUrl').click();
    });

    $('#success-ok').click(function (e) {
        e.preventDefault();
        $('#success').hide();
    });
    

});

function ChangeClickCount() {
    $.get("../redircedCall?url=" + $('#appUrl').attr('href') + "&id=" +
        $('#appUrl').data('id'));
}