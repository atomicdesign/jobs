﻿$(function () {
    /*$('[data-type]').each(function (ix, el) {
        $(el).prepend('<a href="/admin/' + $(el).attr('data-type') + 'manager/edit/' + $(el).attr('data-id') + '" class="admin-frontend-link icon-cog icon-white"></a>').addClass('admin-frontend');
    });*/

    $('[data-type="PageContent"]').each(function (ix, el) {
        $(el)
            .wrapInner('<div class="admin-frontend-content" contenteditable="true"/>')
            .prepend('<div class="admin-frontend-links"><div class="btn-group">'
                        + '<a class="btn btn-primary btn-save"><i class="icon-cog icon-white"></i>Save</a>'
                        + '<a class="btn btn-source"><i class="icon-th"></i>Source</a>'
                        + '<a class="btn btn-full"><i class="icon-edit"></i>Full Editor</a>'
                   + '</div></div>')
            .addClass('admin-frontend');
    })
        .on('click', '.btn-save', SavePageContent)
        .on('click', '.btn-source', SourceMode)
        .on('click', '.btn-full', Popup);
    });

function SavePageContent(e) {
    e.stopPropagation(); e.preventDefault();
    var page = $(this).closest('[data-type="PageContent"]');
    if(!page.length) return;

    var pageID = page.attr('data-id');
    if(!pageID || pageID == '') return;

    var html = page.children('.admin-frontend-content').first().html();

    var adminPath = '/admin/PageManager/editAjax/' + pageID;
    $.ajax({
        url: adminPath,
        type: 'POST',
        data: { html: html },
        dataType: 'json',
        success: function (data) {
            alert(data.message);
        }
    });
    
}

function SourceMode(e) {
    e.stopPropagation(); e.preventDefault();
    var page = $(this).closest('[data-type="PageContent"]');
    if (!page.length) return;

    var pageID = page.attr('data-id');
    if (!pageID || pageID == '') return;

    var contentDiv = page.children('.admin-frontend-content').first();
    if (contentDiv.hasClass('source-view')) {
        var html = contentDiv.children('textarea').val();
        contentDiv.html(html).removeClass('source-view');
    } else {
        var html = contentDiv.html();
        contentDiv.html('<textarea>' + html + '</textarea>').addClass('source-view');
    }
}

function Popup(e) {
    e.stopPropagation(); e.preventDefault();
    var page = $(this).closest('[data-type="PageContent"]');
    if (!page.length) return;

    var pageID = page.attr('data-id');
    if (!pageID || pageID == '') return;

    var adminPath = '/admin/PageManager/Edit/' + pageID;
    window.open(adminPath, '_blank');
}