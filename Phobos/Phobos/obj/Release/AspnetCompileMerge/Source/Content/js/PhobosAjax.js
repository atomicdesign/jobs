﻿/**
 * jQuery CivilianJobsAjax plugin
 * This jQuery plugin overlays an image onto a page
 * @name CivilianJobsAjax.js
 * @author Nicholas Skvarla, Atomic Design (http://www.atomicdesign.net)
 * @version 1.0
 * @date February 29, 2012
 * @category jQuery plugin
 * @copyright (c) 2012 Nicholas Skvarla, Atomic Design (http://www.atomicdesign.net)
 * @license Creative Commons Attribution-NoDerivs 3.0 Unported License
 * @example Visit http://widgets.atomicdesign.net/doc/CivilianJobsAjax/1.0/ for more informations about this jQuery plugin
 */


(function ($) {
    var options;

    var methods = {
        init: function (options) {
            options = $.extend({}, $.fn.CivilianJobsAjax.defaults, options);
            $(options.container)
                .on('click', '.icon-plus', methods.add)
                .on('click', '.icon-edit', methods.edit)
                .on('click', '.icon-fire', methods.remove)
                .on('click', '.icon-thumbs-up', methods.save)
                .on('click', '.icon-thumbs-down', methods.list);

            options.templates.list = Handlebars.compile($(options.list_template).html());
            options.templates.edit = Handlebars.compile($(options.edit_template).html());

            this.data('ajaxoptions', options);
            methods.list();
        },
        ajax: function (action, callback, data) {
            $.ajax({
                url: this.options.api_point + action,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: callback
            });
        },
        add: function (e) {
            e.stopPropagation(); e.preventDefault();

            //.closest('[data-id]').attr('data-id')
        },
        edit: function (e) {
            e.stopPropagation(); e.preventDefault();
            methods.ajax(this.options.details_action, function (data) {
                if (data.success) $(this.options.container).find('tr[data-id="' + data.id + '"]').replaceWith(this.options.templates.edit(data.Item));
                else alert(data.message);
            }, { id: this.closest('[data-id]').attr('data-id') });
        },
        save: function (e) {
            e.stopPropagation(); e.preventDefault();
            methods.ajax(this.options.edit_action, function (data) {
                if (data.success) methods.list();
                else alert(data.message);
            }, { id: this.closest('[data-id]').attr('data-id') });
        },
        remove: function (e) {
            e.stopPropagation(); e.preventDefault();
            if (!confirm('Are you sure?')) return;
            methods.ajax(this.options.delete_action, function (data) {
                if (data.success) methods.list();
                else alert(data.message);
            }, { id: this.closest('[data-id]').attr('data-id') });
        },
        list: function () {
            methods.ajax(this.options.list_action, function (data) {
                if (data.success) $(this.options.container).html(this.options.templates.list(data));
            });
        }
    };

    $.fn.CivilianJobsAjax = function (method) {

        // Method calling logic
        if (methods[method]) {
            this.options = this.data('ajaxoptions');
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.CivilianJobsAjax');
        }

    };

    $.fn.CivilianJobsAjax.defaults = {
        container: ''
		, delete_icon: '.icon-fire'
		, save_icon: '.icon-thumbs-up'
		, revert_icon: '.icon-thumbs-down'
		, add_icon: '.icon-plus'
        , edit_icon: '.icon-edit'
        , api_point: ''
        , create_action: 'create'
        , delete_action: 'delete'
        , details_action: 'details'
        , list_action: 'list'
        , edit_action: 'edit'
        , list_template: ''
        , edit_template: ''
        , templates: {}
    };


})(jQuery);