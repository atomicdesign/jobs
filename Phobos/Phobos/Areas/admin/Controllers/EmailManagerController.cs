﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class EmailManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/EmailManager/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            ViewBag.message = "";
            return View();
        }

        public JsonResult List(string search="", string filter="")
        {
            var emails = from e in db.EmailQueue select e;
            if (search != String.Empty) emails = emails.Where(e => e.Subject.Contains(search));
            switch (filter)
            {
                case "Sent":
                    emails = emails.Where(e => e.Sent);
                    break;

                case "Errors":
                    emails = emails.Where(e => e.HasErrors);
                    break;

                case "Unsent":
                default:
                    emails = emails.Where(e =>!e.Sent && !e.HasErrors);
                    break;
            }
            return this.Json(new{ success = true, emails = emails.OrderByDescending(e=>e.Created).ToList()});
        }

        //
        // GET: /admin/EmailManager/Details/5
        public JsonResult Details(int id = 0)
        {
            EmailMessage emailmessage = db.EmailQueue.Find(id);
            if (emailmessage == null)
            {
                return this.Json(new { success = false });
            }
            return this.Json(new { success = true, email = emailmessage });
        }

        //
        // GET: /admin/EmailManager/Edit/5
        public JsonResult Resend(int id = 0)
        {
            EmailMessage emailmessage = db.EmailQueue.Find(id);
            if (emailmessage == null)
            {
                return this.Json(new { success = false });
            }
            emailmessage.HasErrors = false;
            emailmessage.ErrorMessage = "";
            emailmessage.Sent = false;
            emailmessage.Queued = true;
            db.Entry(emailmessage).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return this.Json(new { success = true });
        }

    }
}