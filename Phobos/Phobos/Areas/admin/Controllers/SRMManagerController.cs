﻿using CivilianJobs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Areas.admin.Controllers
{
    [Authorize]
    public class SRMManagerController : CivilianJobsAdminController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult updateSRM(incomeObj obj)
        {
            var temp = db.SrmDegrees.Where(x => x.SrmId == obj.Id).ToList();
            foreach (var item in temp)
            {
                db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
            }
            db.SaveChanges();

            foreach (var item in obj.spscIds)
            {
                var sps = new SrmDegrees { DegreeId = item, SrmId = obj.Id };
                db.SrmDegrees.Add(sps);
            }
            db.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }


        public ActionResult specialities(int srmId)
        {
            var specIds = db.SrmDegrees.Where(x => x.SrmId == srmId).Select(x=>x.DegreeId).ToList();
            var result = srmId == 0 ?
                db.UserDegrees.Where(x => x.Type == 5).ToList()
                : db.UserDegrees.Where(x => x.Type == 5 && specIds.Contains(x.Id)).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult manage(int Id, string type, string name)
        {
            if (Id == 0)
            {
                var ms = new UserDegree { Id = 0, Name = name, Type = type == "service" ? 3 : type == "rank" ? 4 : type == "speciality" ? 5 : 17 };
                db.UserDegrees.Add(ms);
                db.SaveChanges();
                return Json(new { Id = ms.Id, Name = ms.Name, type = type }, JsonRequestBehavior.AllowGet);
            }
            var mse = db.UserDegrees.First(x => x.Id == Id);
            mse.Name = name;
            db.Entry(mse).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Json(new { Id = mse.Id, Name = mse.Name, type = type }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult createLink(int mos, int rank, int service)
        {
            var item = new SRM
            {
                MosId = mos == 0 ? null : (int?)mos,
                RankId = rank == 0 ? null : (int?)rank,
                ServiceId = service
            };
            db.SRMs.Add(item);
            db.SaveChanges();
            var ug = db.UserDegrees.ToList();
            item.Mos = mos == 0 ? "All MOSs" : ug.First(x => x.Id == mos).Name;
            item.Rank = rank == 0 ? "All Ranks": ug.First(x => x.Id == rank).Name;
            item.Service = ug.First(x => x.Id == service).Name;
            return Json(item, JsonRequestBehavior.AllowGet);
        }
    }

    public class incomeObj
    {
        public int Id { get; set; }
        public List<int> spscIds { get; set; }
    }
}
