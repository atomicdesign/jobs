﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class AdminEmployerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/Employer/

        public ActionResult mtnsales()
        {
            ViewBag.Title = "MTN Sales";
            return View();
        }

        public ActionResult sales()
        {
            ViewBag.Title = "Sales";
            return View();
        }

        public ActionResult salesjobpostings()
        {
            ViewBag.Title = "Sales Job Postings";
            return View();
        }

        public ActionResult salesjobfairs()
        {
            ViewBag.Title = "Sales Job Fairs";
            return View();
        }

        public ActionResult webadsales()
        {
            ViewBag.Title = "Web Ad Sales";
            return View();
        }

        public ActionResult salesorderads()
        {
            ViewBag.Title = "Order Ads";
            return View();
        }

        public ActionResult saleswebusage()
        {
            ViewBag.Title = "Usage";
            return View();
        }

    }
}
