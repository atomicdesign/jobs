﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Text.RegularExpressions;
using CivilianJobs.Helpers;
using System.Net.Mail;

namespace CivilianJobs.Areas.admin.Controllers
{


    public class JobFairsManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/CalendarManager/
        public ActionResult Index()
        {
            ViewBag.message = "";
            CivilianJobsContext ctx = new CivilianJobsContext();
            
            var JobFairs = ctx.JobFairs.ToList();
            return View(JobFairs);

        }
        public ActionResult jobseekers(int id)
        {
            ViewBag.jobFairId = id;
            var jsIds = db.JobFairAppliers.Where(x => x.JobFairId == id).Select(x => x.UserId).ToList();
            var jobseekers = db.UserProfiles.Where(x => jsIds.Contains(x.UserProfileID));
            return View(jobseekers);
        }

        [HttpPost]
        public JsonResult sendMail(SendMailObject obj)
        {
            var jsIds = db.JobFairAppliers.Where(x => x.JobFairId == obj.Id).Select(x => x.UserId).ToList();
            var mail = new MailMessage
            {
                Body = obj.Body,
                Subject = obj.Subject
            };
            db.UserProfiles.Where(x => jsIds.Contains(x.UserProfileID)).ToList().
                ForEach(z =>
                {
                    mail.CC.Add(new MailAddress(z.UserName, z.FirstName));
                });

            AAEmailSender.Send(mail, "civilianjobs@gmail.com");
            return Json("", JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult add()
        {
            ViewBag.message = "";
            return View();
        }

        [HttpPost]
        public ActionResult add(int post = 0)
        {
            string sponsoredBy = Regex.Replace(Request.Form["sponsoredBy"], "<.*?>", string.Empty);
            string dayOfWeek = Regex.Replace(Request.Form["dayOfWeek"], "<.*?>", string.Empty);
            string month = Regex.Replace(Request.Form["month"], "<.*?>", string.Empty);
            string day = Regex.Replace(Request.Form["day"], "<.*?>", string.Empty);
            string year = Regex.Replace(Request.Form["year"], "<.*?>", string.Empty);
            string startTime = Regex.Replace(Request.Form["startHours"], "<.*?>", string.Empty) + ":" + Regex.Replace(Request.Form["startMins"], "<.*?>", string.Empty);
            string startTimeAMPM = Regex.Replace(Request.Form["startTimeAMPM"], "<.*?>", string.Empty);
            string endTime = Regex.Replace(Request.Form["endHours"], "<.*?>", string.Empty) + ":" + Regex.Replace(Request.Form["endMins"], "<.*?>", string.Empty);
            string endTimeAMPM = Regex.Replace(Request.Form["endTimeAMPM"], "<.*?>", string.Empty);

            string locationName = Regex.Replace(Request.Form["locationName"], "<.*?>", string.Empty);
            string locationAddress = Regex.Replace(Request.Form["locationAddress"], "<.*?>", string.Empty);
            string locationCity = Regex.Replace(Request.Form["locationCity"], "<.*?>", string.Empty);
            string locationState = Regex.Replace(Request.Form["locationState"], "<.*?>", string.Empty);

            string opportunity1 = Regex.Replace(Request.Form["opportunity1"], "<.*?>", string.Empty);
            string opportunity2 = Regex.Replace(Request.Form["opportunity2"], "<.*?>", string.Empty);
            string opportunity3 = Regex.Replace(Request.Form["opportunity3"], "<.*?>", string.Empty);
            string opportunity4 = Regex.Replace(Request.Form["opportunity4"], "<.*?>", string.Empty);
            string opportunity5 = Regex.Replace(Request.Form["opportunity5"], "<.*?>", string.Empty);
            string opportunity6 = Regex.Replace(Request.Form["opportunity6"], "<.*?>", string.Empty);
            string opportunity7 = Regex.Replace(Request.Form["opportunity7"], "<.*?>", string.Empty);
            string opportunity8 = Regex.Replace(Request.Form["opportunity8"], "<.*?>", string.Empty);
            string opportunity9 = Regex.Replace(Request.Form["opportunity9"], "<.*?>", string.Empty);
            string opportunity10 = Regex.Replace(Request.Form["opportunity10"], "<.*?>", string.Empty);


            string query = "INSERT INTO JobFairs (startTime, startTimeAMPM, endTime, endTimeAMPM, locationName, locationAddress, locationCity, locationState, locationZip, opportunity1, opportunity2, opportunity3, opportunity4, opportunity5, opportunity6, opportunity7, opportunity8, opportunity9, opportunity10, dayOfWeek, month, day, year, createDate, sponsoredBy) VALUES ('" +
                startTime + "', '" + Request.Form["startTimeAMPM"] + "', '" + endTime + "', '" +
                Request.Form["endTimeAMPM"] + "', '" + Request.Form["locationName"] + "', '" + Request.Form["locationAddress"] + "', '" + Request.Form["locationCity"] + "', '" + Request.Form["locationState"] + "', '" +
                Request.Form["locationZip"] + "', '" + Request.Form["opportunity1"] + "', '" + Request.Form["opportunity2"] + "', '" + Request.Form["opportunity3"] + "', '" + Request.Form["opportunity4"] + "', '" +
                Request.Form["opportunity5"] + "', '" + Request.Form["opportunity6"] + "', '" + Request.Form["opportunity7"] + "', '" + Request.Form["opportunity8"] + "', '" + Request.Form["opportunity9"] + "', '" +
                Request.Form["opportunity10"] + "', '" + Request.Form["dayOfWeek"] + "', '" + Request.Form["month"] + "', '" + Request.Form["day"] + "', '" + Request.Form["year"] + "', '" + DateTime.Now + "', '" + Request.Form["sponsoredBy"] + "')";

            CivilianJobsContext ctx = new CivilianJobsContext();
            ctx.Database.ExecuteSqlCommand(query);
            ctx.SaveChanges();

            ViewBag.message = "New Job Fair Added";

            return RedirectToAction("index");
        }

        public ActionResult delete(int id = 0)
        {
            CivilianJobsContext ctx = new CivilianJobsContext();

            ctx.Database.ExecuteSqlCommand("DELETE FROM JobFairs WHERE ID = " + id);
            ctx.SaveChanges();

            var JobFairs = ctx.JobFairs.ToList();
            return RedirectToAction("index");

        }

        public ActionResult edit(int id = 0)
        {
            CivilianJobsContext ctx = new CivilianJobsContext();

            var JobFairs = ctx.JobFairs.Where(m => m.ID == id).ToList();

            return View(JobFairs);
        }

        [HttpPost]
        public ActionResult edit()
        {
            int ID = Convert.ToInt32(Request.Form["ID"]);
            string sponsoredBy = Regex.Replace(Request.Form["sponsoredBy"], "<.*?>", string.Empty);
            string dayOfWeek = Regex.Replace(Request.Form["dayOfWeek"], "<.*?>", string.Empty);
            string month = Regex.Replace(Request.Form["month"], "<.*?>", string.Empty);
            string day = Regex.Replace(Request.Form["day"], "<.*?>", string.Empty);
            string year = Regex.Replace(Request.Form["year"], "<.*?>", string.Empty);
            string startTime = Regex.Replace(Request.Form["startHours"], "<.*?>", string.Empty) + ":" + Regex.Replace(Request.Form["startMins"], "<.*?>", string.Empty);
            string startTimeAMPM = Regex.Replace(Request.Form["startTimeAMPM"], "<.*?>", string.Empty);
            string endTime = Regex.Replace(Request.Form["endHours"], "<.*?>", string.Empty) + ":" + Regex.Replace(Request.Form["endMins"], "<.*?>", string.Empty);
            string endTimeAMPM = Regex.Replace(Request.Form["endTimeAMPM"], "<.*?>", string.Empty);

            string locationName = Regex.Replace(Request.Form["locationName"], "<.*?>", string.Empty);
            string locationAddress = Regex.Replace(Request.Form["locationAddress"], "<.*?>", string.Empty);
            string locationCity = Regex.Replace(Request.Form["locationCity"], "<.*?>", string.Empty);
            string locationState = Regex.Replace(Request.Form["locationState"], "<.*?>", string.Empty);

            string opportunity1 = Regex.Replace(Request.Form["opportunity1"], "<.*?>", string.Empty);
            string opportunity2 = Regex.Replace(Request.Form["opportunity2"], "<.*?>", string.Empty);
            string opportunity3 = Regex.Replace(Request.Form["opportunity3"], "<.*?>", string.Empty);
            string opportunity4 = Regex.Replace(Request.Form["opportunity4"], "<.*?>", string.Empty);
            string opportunity5 = Regex.Replace(Request.Form["opportunity5"], "<.*?>", string.Empty);
            string opportunity6 = Regex.Replace(Request.Form["opportunity6"], "<.*?>", string.Empty);
            string opportunity7 = Regex.Replace(Request.Form["opportunity7"], "<.*?>", string.Empty);
            string opportunity8 = Regex.Replace(Request.Form["opportunity8"], "<.*?>", string.Empty);
            string opportunity9 = Regex.Replace(Request.Form["opportunity9"], "<.*?>", string.Empty);
            string opportunity10 = Regex.Replace(Request.Form["opportunity10"], "<.*?>", string.Empty);

            CivilianJobsContext ctx = new CivilianJobsContext();
            string query = "UPDATE JobFairs SET sponsoredBy = '" + sponsoredBy + "', dayOfWeek = '" + dayOfWeek + "', month = '" + month + "', day = '" + day +
                "', year = '" + year + "', startTime = '" + startTime + "', startTimeAMPM = '" + startTimeAMPM + "', endTime = '" + endTime + "', endTimeAMPM = '" + endTimeAMPM +
                "', locationName = '" + locationName + "', locationAddress = '" + locationAddress + "', locationCity = '" + locationCity +
                "', locationState = '" + locationState + "', opportunity1 = '" + opportunity1 + "', opportunity2 = '" + opportunity2 + "', opportunity3 = '" + opportunity3 +
                "', opportunity4 = '" + opportunity4 + "', opportunity5 = '" + opportunity5 + "', opportunity6 = '" + opportunity6 + "', opportunity7 = '" + opportunity7 +
                "', opportunity8 = '" + opportunity8 + "', opportunity9 = '" + opportunity9 + "', opportunity10 = '" + opportunity10 + "' WHERE ID = " + ID;
            ctx.Database.ExecuteSqlCommand(query);
                
            ctx.SaveChanges();

            return RedirectToAction("index");
        }


    }

}
