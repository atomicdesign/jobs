﻿namespace CivilianJobs.Areas.admin.Controllers
{
    public class SendMailObject
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}