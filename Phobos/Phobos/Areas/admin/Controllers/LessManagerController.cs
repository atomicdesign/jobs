﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.IO;


namespace CivilianJobs.Areas.admin.Controllers
{
    public class LessManagerController : CivilianJobsAdminController
    {

        private void CacheStylesheet(DotLessStylesheet stylesheet)
        {
            HttpRuntime.Cache.Insert("DOTLESSSTYLESHEET_" + stylesheet.DotLessStylesheetID.ToString(), stylesheet, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0));
            HttpRuntime.Cache.Insert("DOTLESSVARIABLES_" + stylesheet.DotLessStylesheetID.ToString(), stylesheet.Variables, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0));
        }

        private DotLessStylesheet GetCachedStylesheet(int id)
        {
            DotLessStylesheet retval;
            if ((retval = (DotLessStylesheet)HttpRuntime.Cache.Get("DOTLESSSTYLESHEET_" + id.ToString())) == null)
            {
                retval = RefreshCachedStylesheet(id);
            }
            else
            {
                retval.Variables = (List<DotLessVariable>)HttpRuntime.Cache.Get("DOTLESSVARIABLES_" + id.ToString());
            }
            return retval;
        }

        private DotLessStylesheet RefreshCachedStylesheet(int id)
        {
            DotLessStylesheet retval = db.DotLessStylesheets.Find(id);
            CacheStylesheet(retval);
            return retval;
        }

        //
        // GET: /admin/LessManager/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            DotLessStylesheet stylesheet = RefreshCachedStylesheet(id);
            if (stylesheet == null) return Http404();
            return View(stylesheet);
        }

        [HttpPost]
        public JsonResult GetVariables(int id)
        {
            DotLessStylesheet stylesheet = GetCachedStylesheet(id);
            if (stylesheet == null) return JsonReply(false, "Invalid ID", id);
            return JsonReply(true, "", id, new { variables = stylesheet.Variables });
        }

        [HttpPost]
        public JsonResult AddVariable(DotLessVariable variable)
        {
            if (ModelState.IsValid)
            {
                db.DotLessVariables.Add(variable);
                db.SaveChanges();
                RefreshCachedStylesheet(variable.DotLessStylesheetID);
                return JsonReply(true, "", variable.DotLessVariableID);
            }
            return JsonReply(false, "Error Adding Variable", variable.DotLessVariableID, GetModelErrorMessages(ModelState));
        }

        [HttpPost]
        public JsonResult EditVariable(DotLessVariable variable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(variable).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                RefreshCachedStylesheet(variable.DotLessStylesheetID);
                return JsonReply(true, "", variable.DotLessVariableID);
            }
            return JsonReply(false, "Error Editing Variable", variable.DotLessVariableID, GetModelErrorMessages(ModelState));
        }

        public JsonResult GetStylesheetCode(int id)
        {
            DotLessStylesheet stylesheet = GetCachedStylesheet(id);
            if (stylesheet == null) return JsonReply(false, "Invalid ID", id);

            string str_LessCSS = "", str_CompiledCss = "";

            FileInfo LessCSS = new FileInfo(Server.MapPath(stylesheet.FullLessUrl));
            if (LessCSS.Exists)
            {
                using (FileStream fs = LessCSS.OpenRead())
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        str_LessCSS = sr.ReadToEnd();
                    }
                }
            }

            FileInfo CompiledCSS = new FileInfo(Server.MapPath(stylesheet.FullStylesheetURL));
            if (CompiledCSS.Exists)
            {
                using (FileStream fs = CompiledCSS.OpenRead())
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        str_CompiledCss = sr.ReadToEnd();
                    }
                }
            }

            return JsonReply(true, "", id, new { LessCSS = str_LessCSS, CompiledCSS = str_CompiledCss }); ;
        }




        [HttpPost]
        public JsonResult GetStylesheets()
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Server.MapPath(DotLessStylesheet.LessPath));
                List<string> all_stylesheets = (from fi in di.GetFiles("*.less") select fi.Name).ToList();

                List<DotLessStylesheet> known_stylesheets = db.DotLessStylesheets.OrderBy(e=>e.LessUrl).ToList();
                foreach (var x in known_stylesheets) x.Variables = null;
                var unknown_stylesheets = all_stylesheets.Except((from dls in known_stylesheets select dls.LessUrl).ToList()).OrderBy(e => e);

                return JsonReply(true, "", null, new { k_stylesheets = known_stylesheets, u_stylesheets = unknown_stylesheets });
            }
            catch (Exception err)
            {
                return JsonReply(false, err.ToString());
            }
        }

        [HttpPost]
        public JsonResult AddStylesheet(string LessUrl)
        {
            try
            {
                if (LessUrl.Trim() == String.Empty) throw new Exception("Invalid Filename");
                if (db.DotLessStylesheets.Any(e => e.LessUrl == LessUrl)) throw new Exception("Duplicate Entry");

                FileInfo fi = new FileInfo(Server.MapPath(DotLessStylesheet.LessPath + LessUrl));
                if (!fi.Exists) throw new Exception("File Not Exist");
                if (fi.Extension.ToLower() != ".less") throw new Exception("Must Be A .less File");
                
                DotLessStylesheet stylesheet = new DotLessStylesheet { LessUrl = LessUrl, StylesheetURL = System.IO.Path.GetFileNameWithoutExtension(LessUrl) + ".css" };
                db.DotLessStylesheets.Add(stylesheet);
                db.SaveChanges();
                stylesheet.GetVariables();
                db.SaveChanges();
                return JsonReply(true, "", LessUrl, new { stylesheet = stylesheet });
            }
            catch (Exception err)
            {
                return JsonReply(false, err.ToString(), LessUrl);
            }
        }

        [HttpPost]
        public JsonResult DeleteStylesheet(int DotLessStylesheetID)
        {
            DotLessStylesheet stylesheet = db.DotLessStylesheets.Find(DotLessStylesheetID);
            if (stylesheet != null)
            {
                db.DotLessStylesheets.Remove(stylesheet);
                db.SaveChanges();
                return JsonReply(true, "", DotLessStylesheetID);
            }
            return JsonReply(false, "Invalid ID", DotLessStylesheetID);;
        }

        [HttpPost]
        public JsonResult Compile(int id)
        {
            DotLessStylesheet stylesheet = GetCachedStylesheet(id);
            if (stylesheet == null) return JsonReply(false, "Invalid ID", id);
            return JsonReply(stylesheet.CompileStylesheet(), stylesheet.Errors, id );
        }


    }
}
