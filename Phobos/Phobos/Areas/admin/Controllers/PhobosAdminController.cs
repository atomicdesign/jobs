﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    [Authorize(Roles="Administrator")]
    public class CivilianJobsAdminController : CivilianJobs.Controllers.CivilianJobsBaseController
    {

        protected virtual void StatusCheck() 
        {
            
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            StatusCheck();
            ViewBag.Alerts = db.Alerts.Where(e => e.Dismissed == false).ToList();
        }

        protected void AddAlert(AlertLevel level, string message, bool autodismiss = false)
        {
            db.Alerts.Add(new AdminAlert { AlertLevel = level, AutoDismiss = autodismiss, Message = message, Created = DateTime.Now, Dismissed = false });
            db.SaveChanges();
        }
        protected JsonResult JsonReply(bool success, string message = "", object message_id = null, object data = null)
        {
            return this.Json(new { success = success, message = message, message_id = message_id, data = data });
        }
    }
}
