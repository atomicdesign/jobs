﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Text.RegularExpressions;


namespace CivilianJobs.Areas.admin.Controllers
{


    public class TransitionNewsController : CivilianJobsAdminController
    {
        //
        // GET: /admin/CalendarManager/
        public ActionResult Index()
        {
            ViewBag.message = "";
            CivilianJobsContext ctx = new CivilianJobsContext();
            var TransitionNewsArticles = ctx.TransitionNewsArticles.ToList();
            return View(TransitionNewsArticles);

        }

        public ActionResult add()
        {
            ViewBag.message = "";
            return View();
        }

        [HttpPost]
        public ActionResult add(int post = 0)
        {
            string title = Regex.Replace(Request.Form["title"], "<.*?>", string.Empty);
            string teasertext = Regex.Replace(Request.Form["teasertext"], "<.*?>", string.Empty);
            string content = Regex.Replace(Request.Form["content"], "<.*?>", string.Empty);

            CivilianJobsContext ctx = new CivilianJobsContext();
            ctx.Database.ExecuteSqlCommand("INSERT INTO TransitionNewsArticles (Date, Title, TeaserText, Content) VALUES ('"+
                DateTime.Now + "', '" + Request.Form["title"] + "', '" + Request.Form["teasertext"] + "', '"+Request.Form["content"]+"')");
            ctx.SaveChanges();

            ViewBag.message = "New Article Added";

            return RedirectToAction("index");
        }

        public ActionResult delete(int id = 0)
        {
            CivilianJobsContext ctx = new CivilianJobsContext();

            ctx.Database.ExecuteSqlCommand("DELETE FROM TransitionNewsArticles WHERE ID = " + id);
            ctx.SaveChanges();

            var TransitionNewsArticles = ctx.TransitionNewsArticles.ToList();
            return RedirectToAction("index");

        }

        public ActionResult edit(int id = 0)
        {
            CivilianJobsContext ctx = new CivilianJobsContext();

            var TransitionNewsArticle = ctx.TransitionNewsArticles.Where(m => m.ID == id).ToList();

            return View(TransitionNewsArticle);
        }

        [HttpPost]
        public ActionResult edit()
        {
            int ID = Convert.ToInt32(Request.Form["ID"]);
            string save = Request.Form["save"];
            string title = Regex.Replace(Request.Form["title"], "<.*?>", string.Empty);
            string teasertext = Regex.Replace(Request.Form["teasertext"], "<.*?>", string.Empty);
            string content = Regex.Replace(Request.Form["content"], "<.*?>", string.Empty);

            CivilianJobsContext ctx = new CivilianJobsContext();
            string query = "UPDATE TransitionNewsArticles SET Title = '" + Request.Form["title"] + "', TeaserText = '" + Request.Form["teasertext"] +
                "', Content = '" + Request.Form["content"] + "' WHERE ID = " + ID;
            ctx.Database.ExecuteSqlCommand(query);
                
            ctx.SaveChanges();

            return RedirectToAction("index");
        }


    }

}
