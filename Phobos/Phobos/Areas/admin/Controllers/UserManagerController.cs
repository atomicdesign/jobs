﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserManagerController : CivilianJobsAdminController
    {
        protected static List<string> AvailableRoles = new List<string>() { "Administrator" };

        public ActionResult Index()
        {
            List<string> availableUsernames = new List<string>();
            foreach (string role in AvailableRoles) availableUsernames.AddRange(Roles.GetUsersInRole(role));
            availableUsernames = availableUsernames.Distinct().ToList();
            return View(db.UserProfiles.Where(e => availableUsernames.Contains(e.UserName)).ToList());
        }

        public ActionResult Details(string username = "")
        {
            UserProfile userprofile = db.UserProfiles.Find(username);
            if (userprofile == null) return HttpNotFound();
            if (userprofile.UserName != User.Identity.Name) ViewBag.Roles = AvailableRoles;
            return View(userprofile);
        }

        private RoleChangeResult SetUserRole(string Username, string RoleName, bool adding)
        {
            RoleChangeResult retval = new RoleChangeResult();
            try
            {
                MembershipUser user = Membership.GetUser(Username, false);

                if (user == null || user.UserName == User.Identity.Name) throw new Exception("Invalid Username");
                if (!AvailableRoles.Contains(RoleName)) throw new Exception("Invalid Role");

                if (adding)
                {
                    if (Roles.IsUserInRole(Username, RoleName)) throw new Exception("User already in role.");
                    Roles.AddUserToRole(Username, RoleName);
                    retval.success = true;
                    retval.message = String.Format("User added to role {0}", RoleName);
                }
                else
                {
                    if (!Roles.IsUserInRole(Username, RoleName)) throw new Exception("User not in role.");
                    Roles.RemoveUserFromRole(Username, RoleName);
                    retval.success = true;
                    retval.message = String.Format("User removed from role {0}", RoleName);
                }
            }
            catch (Exception err)
            {
                retval.success = false;
                retval.message = err.Message;
            }
            return retval;
        }

        public JsonResult SetRole(string username, string rolename, bool adding)
        {
            return this.Json(SetUserRole(username, rolename, adding));
        }

        public JsonResult UnlockUser(string username)
        {
            MembershipUser user = Membership.GetUser(username, false);
            if (user == null) return JsonReply(false, "Invalid Username");
            user.UnlockUser();
            return JsonReply(true, "User unlocked");
        }

        public JsonResult ResetPassword(string username)
        {
            return this.Json(Helpers.CivilianJobsHelper.ResetUserPassword(username));
        }

        //
        // GET: /admin/UserManager/Edit/5

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.AvailableRoles = AvailableRoles;
        }

        protected internal class RoleChangeResult
        {
            public RoleChangeResult()
            {
                success = false;
                message = String.Empty;
            }
            public bool success { get; set; }
            public string message { get; set; }
        }
    }
}