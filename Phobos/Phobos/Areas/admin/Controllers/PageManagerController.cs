﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class PageManagerController : CivilianJobsAdminController
    {

        //
        // GET: /admin/PageManager/

        public ActionResult Index(string dir = "")
        {
            var pages = db.Pages.Include(p => p.Parent).Include(p => p.Template);
            return View(pages.ToList().OrderBy(p => p.FullSEOURL));
        }

        //
        // GET: /admin/PageManager/Create

        public ActionResult Create(int id = 0)
        {
            Page p = new Page();
            if (id > 0) p.ParentID = id;
            p.PrimaryNavID = db.NavBars.Where(e => e.Name == "Default").FirstOrDefault().NavBarID;
            p.TemplateID = db.Templates.Where(e => e.Name == "Static Page").FirstOrDefault().TemplateID;
            SetDropDowns(p);
            return View(p);
        }

        //
        // POST: /admin/PageManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Page page)
        {
            if (ModelState.IsValid)
            {
                if (!db.Pages.Any(e => e.SEOURL == page.SEOURL && e.Parent == page.Parent))
                {
                    db.Pages.Add(page);
                    db.SaveChanges();
                  
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("SEOURL", "This path is already taken");
                }
            }
            SetDropDowns(page);
            return View(page);
        }

        //
        // GET: /admin/PageManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return Http404();
            }
            SetDropDowns(page);
            return View(page);
        }

        //
        // POST: /admin/PageManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Page page)
        {
            if (ModelState.IsValid)
            {
                if (!db.Pages.Any(e => e.SEOURL == page.SEOURL && e.Parent == page.Parent && e.PageID != page.PageID))
                {
                    db.Entry(page).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("SEOURL", "This path is already taken");
                }
            }
            SetDropDowns(page);
            return View(page);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult EditAjax(int id, string html)
        {
            Page page = db.Pages.Find(id);
            if (page != null)
            {
                page.Content = html;
                db.Entry(page).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return this.Json(new { success = true, message = "Page Saved", referer = Request.UrlReferrer });
            }
            return this.Json(new { success = true, message = "Invalid Page", referer = Request.UrlReferrer });
        }

        //
        // GET: /admin/PageManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return Http404();
            }
            return View(page);
        }

        //
        // POST: /admin/PageManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Page page = db.Pages.Include("SubPages").FirstOrDefault(e => e.PageID == id);
            if (page == null) return Http404();
            page.DeleteSafe(db);
            db.SaveChanges();
            
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private void SetDropDowns(Page page)
        {
            ViewBag.ParentID = new SelectList(db.Pages, "PageID", "SEOURL", page.ParentID);
            ViewBag.TemplateID = new SelectList(AvailableTemplates, "TemplateID", "Name", page.TemplateID);
            ViewBag.PrimaryNavID = new SelectList(db.NavBars, "NavBarID", "Name", 1);
            ViewBag.SecondaryNavID = new SelectList(db.NavBars, "NavBarID", "Name", page.SecondaryNavID);
        }

        protected IEnumerable<Template> AvailableTemplates { get { return db.Templates.Where(e => e.UserSelectable); } }
    }
}