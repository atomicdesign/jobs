﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class FormManagerController : CivilianJobsAdminController
    {

        //
        // GET: /admin/FormManager/

        public ActionResult Index()
        {
            return View(db.Forms.ToList());
        }

        //
        // GET: /admin/FormManager/Details/5

        public ActionResult Fields(int id = 0)
        {
            Form form = db.Forms.Include("Fields").FirstOrDefault(e=>e.FormID == id);
            if (form == null)
            {
                return Http404();
            }
            return View(form);
        }

        //
        // GET: /admin/FormManager/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /admin/FormManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Form form)
        {
            if (ModelState.IsValid)
            {
                db.Forms.Add(form);
                db.SaveChanges();
                AddAlert(AlertLevel.SUCCESS, "Form created!", true);
                return RedirectToAction("Index");
            }

            return View(form);
        }

        //
        // GET: /admin/FormManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Form form = db.Forms.Find(id);
            if (form == null)
            {
                return Http404();
            }
            return View(form);
        }

        //
        // POST: /admin/FormManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Form form)
        {
            if (ModelState.IsValid)
            {
                db.Entry(form).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                AddAlert(AlertLevel.SUCCESS, "Form edited!", true);
                return RedirectToAction("Index");
            }
            return View(form);
        }

        //
        // GET: /admin/FormManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Form form = db.Forms.Find(id);
            if (form == null)
            {
                return Http404();
            }
            return View(form);
        }

        //
        // POST: /admin/FormManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Form form = db.Forms.Find(id);
            db.Forms.Remove(form);
            db.SaveChanges();
            AddAlert(AlertLevel.SUCCESS, "Form deleted!", true);
            return RedirectToAction("Index");
        }

        //
        // GET: /admin/FormField/Create

        public ActionResult CreateField(int id = 0)
        {
            FormField formfield = new FormField { FormID = id };
            LoadFieldTypes(formfield);
            return View(formfield);
        }

        //
        // POST: /admin/FormField/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateField(FormField formfield)
        {
            if (ModelState.IsValid)
            {
                db.FormFields.Add(formfield);
                db.SaveChanges();
                return RedirectToAction("Fields", new { id = formfield.FormID });
            }
            LoadFieldTypes(formfield);
            return View(formfield);
        }

        //
        // GET: /admin/FormField/Edit/5

        public ActionResult EditField(int id = 0)
        {
            FormField formfield = db.FormFields.Find(id);
            if (formfield == null)
            {
                return Http404();
            }
            LoadFieldTypes(formfield);
            return View(formfield);
        }

        //
        // POST: /admin/FormField/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditField(FormField formfield)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formfield).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Fields", new { id = formfield.FormID });
            }
            LoadFieldTypes(formfield);
            return View(formfield);
        }

        //
        // GET: /admin/FormField/Delete/5

        public ActionResult DeleteField(int id = 0)
        {
            FormField formfield = db.FormFields.Find(id);
            if (formfield == null)
            {
                return Http404();
            }
            return View(formfield);
        }

        //
        // POST: /admin/FormField/Delete/5

        [HttpPost, ActionName("DeleteField")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedField(int id)
        {
            FormField formfield = db.FormFields.Find(id);
            db.FormFields.Remove(formfield);
            db.SaveChanges();
            return RedirectToAction("Fields", new { id = formfield.FormID });
        }

        public ActionResult Options(int id = 0)
        {
            FormField formfield = db.FormFields.Find(id);
            if (formfield == null)
            {
                return Http404();
            }
            return View(formfield);
        }

        public ActionResult Submissions(int id = 0)
        {
            Form form = db.Forms.Find(id);
            if (form == null) return Http404();            
            return View(form);
        }

        [HttpPost]
        public JsonResult SubmissionList(int id = 0)
        {
            var subs = db.FormSubmissions.Where(e => e.FormID == id).OrderByDescending(e=>e.Timestamp).ToList();
            return this.Json(new { success = true, submissions = subs });
        }

        [HttpPost]
        public ActionResult SubmissionDetail(int id = 0)
        {
            var sub = db.FormSubmissions.Find(id);
            if (sub == null) return this.Json(new { });
            return Content(sub.XML);
        }

        [HttpPost]
        public JsonResult SubmissionDelete(int id = 0)
        {
            var sub = db.FormSubmissions.Find(id);
            if (sub == null) return this.Json(new { success=false });
            db.FormSubmissions.Remove(sub);
            db.SaveChanges();
            return this.Json(new { success = true });
        }

        private void LoadFieldTypes(FormField ff)
        {
            ViewBag.TypeVal = new SelectList(Enum.GetNames(typeof(FormFieldType)), ff.TypeVal);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}