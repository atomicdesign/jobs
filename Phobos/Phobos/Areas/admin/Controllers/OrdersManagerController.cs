﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Text.RegularExpressions;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs.Repositories;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class OrdersManagerController : CivilianJobsAdminController
    {
        public ActionResult Index()
        {
            var orders = new OrderRepository().Get();
            return View(orders);
        }

        public ActionResult Add()
        {
            return View(new JobFairRepository().Get());
        }

        [HttpPost]
        public ActionResult Add(Orders model, string username)
        {
            if (model.ResumeAccessViewsPerDay == 0)
            {
                model.ResumeAccessMonthsAccess = 0;
            }
            else if (model.ResumeAccessViewsPerDay == 200)
            {
                model.ResumeAccessMonthsAccess = model.ResumeAccessMonthsAccess200;
            }
            else if (model.ResumeAccessViewsPerDay == 400)
            {
                model.ResumeAccessMonthsAccess = model.ResumeAccessMonthsAccess400;
            }
            else if (model.ResumeAccessViewsPerDay == 600)
            {
                model.ResumeAccessMonthsAccess = model.ResumeAccessMonthsAccess600;
            }

            if (!ModelState.IsValid)
            {
                return View("order-services", new JobFairRepository().Get());
            }

            model.CreateDate = DateTime.Now;
            model.Active = true;
            new OrderRepository().Add(model, username);
            return RedirectToAction("index");
        }

        public ActionResult Edit(int id)
        {
            var order = new OrderRepository().Get(id);

            return View(order);
        }

        [HttpPost]
        public ActionResult Edit(Orders order)
        {
            var or = new OrderRepository();

            var currentOrder = or.Get(order.ID);
            currentOrder.Active = order.Active;

            new OrderRepository().Update(currentOrder);

            return RedirectToAction("index");
        }
    }
}
