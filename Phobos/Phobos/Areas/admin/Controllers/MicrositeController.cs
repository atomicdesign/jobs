﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Text.RegularExpressions;


namespace CivilianJobs.Areas.admin.Controllers
{


    public class MicrositeManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/CalendarManager/
        public ActionResult Index()
        {
            ViewBag.message = "";
            CivilianJobsContext ctx = new CivilianJobsContext();
            
            var PressReleases = ctx.Microsites.ToList();
            return View(PressReleases);

        }

        public ActionResult add()
        {
            ViewBag.message = "";
            return View();
        }

        [HttpPost]
        public ActionResult add(int post = 0)
        {
            string CompanyName = Regex.Replace(Request.Form["CompanyName"], "<.*?>", string.Empty);
            string CompanyURL = Regex.Replace(Request.Form["CompanyURL"], "<.*?>", string.Empty);
            string MissionStatement = Regex.Replace(Request.Form["MissionStatement"], "<.*?>", string.Empty);

            string Content = Regex.Replace(Request.Form["Content"], "<.*?>", string.Empty);
            string AdditionSectionTitle = Regex.Replace(Request.Form["AdditionSectionTitle"], "<.*?>", string.Empty);
            string AdditionSectionText = Regex.Replace(Request.Form["AdditionSectionText"], "<.*?>", string.Empty);
            string FacebookURL = Regex.Replace(Request.Form["FacebookURL"], "<.*?>", string.Empty);
            string TwitterURL = Regex.Replace(Request.Form["TwitterURL"], "<.*?>", string.Empty);

            string query = "INSERT INTO Microsites (CompanyName, CompanyURL, MissionStatement, Content, AdditionSectionTitle, AdditionSectionText, FacebookURL, TwitterURL) VALUES ('" +
                Request.Form["CompanyName"] + "', '" + Request.Form["CompanyURL"] + "', '" + Request.Form["MissionStatement"] + "', '" + Request.Form["MissionStatement"] + "', '" +
                Request.Form["Content"] + "', '" + Request.Form["AdditionSectionTitle"] + "', '" + Request.Form["AdditionSectionText"] + "', '" + Request.Form["FacebookURL"] + "', '" + Request.Form["TwitterURL"] + "')";

            CivilianJobsContext ctx = new CivilianJobsContext();
            ctx.Database.ExecuteSqlCommand("INSERT INTO Microsites (CompanyName, CompanyURL, MissionStatement, Content, AdditionSectionTitle, AdditionSectionText, FacebookURL, TwitterURL) VALUES ('" +
                Request.Form["CompanyName"] + "', '" + Request.Form["CompanyURL"] + "', '" + Request.Form["MissionStatement"] + "', '" +
                Request.Form["Content"] + "', '" + Request.Form["AdditionSectionTitle"] + "', '" + Request.Form["AdditionSectionText"] + "', '" + Request.Form["FacebookURL"] + "', '" + Request.Form["TwitterURL"] + "')");
            ctx.SaveChanges();

            ViewBag.message = "New Microsite Added";

            return RedirectToAction("index");
        }

        public ActionResult delete(int id = 0)
        {
            CivilianJobsContext ctx = new CivilianJobsContext();

            ctx.Database.ExecuteSqlCommand("DELETE FROM Microsites WHERE ID = " + id);
            ctx.SaveChanges();

            var Microsites = ctx.Microsites.ToList();
            return RedirectToAction("index");

        }

        public ActionResult edit(int id = 0)
        {
            CivilianJobsContext ctx = new CivilianJobsContext();

            var Microsites = ctx.Microsites.Where(m => m.ID == id).ToList();

            return View(Microsites);
        }

        [HttpPost]
        public ActionResult edit()
        {
            int ID = Convert.ToInt32(Request.Form["ID"]);
            string CompanyName = Regex.Replace(Request.Form["CompanyName"], "<.*?>", string.Empty);
            string CompanyURL = Regex.Replace(Request.Form["CompanyURL"], "<.*?>", string.Empty);
            string MissionStatement = Regex.Replace(Request.Form["MissionStatement"], "<.*?>", string.Empty);

            string Content = Regex.Replace(Request.Form["Content"], "<.*?>", string.Empty);
            string AdditionSectionTitle = Regex.Replace(Request.Form["AdditionSectionTitle"], "<.*?>", string.Empty);
            string AdditionSectionText = Regex.Replace(Request.Form["AdditionSectionText"], "<.*?>", string.Empty);
            string FacebookURL = Regex.Replace(Request.Form["FacebookURL"], "<.*?>", string.Empty);
            string TwitterURL = Regex.Replace(Request.Form["TwitterURL"], "<.*?>", string.Empty);

            CivilianJobsContext ctx = new CivilianJobsContext();
            string query = "UPDATE Microsites SET CompanyName = '" + Request.Form["CompanyName"] + "', CompanyURL = '" + Request.Form["CompanyURL"] +
                "', MissionStatement = '" + Request.Form["MissionStatement"] + "', Content = '" + Request.Form["Content"] +
                "', AdditionSectionTitle = '" + Request.Form["AdditionSectionTitle"] + "', AdditionSectionText = '" + Request.Form["AdditionSectionText"] +
                "', FacebookURL = '" + Request.Form["FacebookURL"] + "', TwitterURL = '" + Request.Form["TwitterURL"] + "' WHERE ID = " + ID;
            ctx.Database.ExecuteSqlCommand(query);
                
            ctx.SaveChanges();

            return RedirectToAction("index");
        }


    }

}
