﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;


namespace CivilianJobs.Areas.admin.Controllers
{


    public class CalendarManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/CalendarManager/
        public ActionResult Index()
        {
            ViewBag.GalleryID = new SelectList(db.Galleries, "GalleryID", "Name", 0);
            return View();
        }

        private DateTime ConvertUnixTimestampToDateTime(string unixTimeStamp)
        {
            System.DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(Convert.ToDouble(unixTimeStamp)).ToLocalTime();
            return dateTime;
        }

        [HttpGet]
        public JsonResult GetEvents(string start, string end)
        {
            DateTime startDT = ConvertUnixTimestampToDateTime(start);
            DateTime endDT = ConvertUnixTimestampToDateTime(end);

            List<Events> events = db.Events.Where(e => e.EventDate >= startDT && e.EventDate <= endDT).OrderBy(e => e.EventDate).ToList();

            return Json(new
            {
                success = true,
                events = events.Select(e => new
                {
                    id = e.EventID,
                    title = e.Heading,
                    start = e.EventDate.ToString("o"),
                    EventDesc = e.EventDesc,
                    EventContent = e.EventContent,
                    GalleryID = e.GalleryID
                }).ToArray(),
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateEvent(Events newevent)
        {
            if (newevent == null) { return this.Json(new { success = false, id = 0 }); }

            try
            {
                db.Events.Add(newevent);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false, id = 0 });
            }

            return this.Json(new
            {
                success = true,
                EventID = newevent.EventID,
                Heading = newevent.Heading,
                EventDate = newevent.EventDate.ToString("o"),
                EventDesc = newevent.EventDesc,
                EventContent = newevent.EventContent,
                GalleryID = newevent.GalleryID
            });
        }

        [HttpPost]
        public JsonResult UpdateEvent(Events eventchanged)
        {
            if (eventchanged == null) { return this.Json(new { success = false, id = 0 }); }

            try
            {
                db.Entry(eventchanged).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false, id = 0 });
            }

            return this.Json(new
            {
                success = true,
                EventID = eventchanged.EventID,
                Heading = eventchanged.Heading,
                EventDate = eventchanged.EventDate.ToString("o"),
                EventDesc = eventchanged.EventDesc,
                EventContent = eventchanged.EventContent,
                GalleryID = eventchanged.GalleryID
            });
        }


        [HttpPost]
        public JsonResult DeleteEvent(Events removeevent)
        {
            Events Event = db.Events.Find(removeevent.EventID);

            db.Events.Remove(Event);
            db.SaveChanges();

            return this.Json(new { success = true });
        }
    }

}
