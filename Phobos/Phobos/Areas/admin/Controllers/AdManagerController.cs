﻿using CivilianJobs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class AdManagerController : CivilianJobsAdminController
    {
        private string ImageFilePath = @"~/Content/upload/ad/";
        //
        // GET: /admin/AdManager/

        public ActionResult Index()
        {
            var ads = db.Ads;
            return View(ads.ToList().OrderBy(a => a.AdID));
        }

        //
        // GET: /admin/AdManager/Edit/5

        public ActionResult View(int id = 0)
        {
            Ad ad = db.Ads.Find(id);
            if (ad == null)
            {
                return Http404();
            }
            return View(ad);
        }

        //
        // GET: /admin/AdManager/Create

        public ActionResult Create(int id = 0)
        {
            Ad a = new Ad();
            return View(a);
        }

        //
        // POST: /admin/AdManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ad ad, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                if (!db.Ads.Any(e => e.PublishFor == ad.PublishFor && e.PublishPage == ad.PublishPage))
                {
                    ad.PublishStartDate = DateTime.Now;
                    ad.CreateDate = DateTime.Now;
                    ad.UpdateDate = DateTime.Now;
                    db.Ads.Add(ad);
                    db.SaveChanges();

                    if (Image != null && Image.ContentLength > 0)
                    {
                        try
                        {
                            string filename = Path.GetFileName(Image.FileName);
                            filename = ad.AdID + "_" + Regex.Replace(filename, @"[^a-zA-Z0-9\.-]", "_");
                            string directory = ImageFilePath + ad.AdID.ToString() + @"/";
                            string path = System.IO.Path.Combine(Server.MapPath(directory), filename);
                            FileInfo fi = new FileInfo(path);
                            fi.Directory.Create(); // If the directory already exists, this method does nothing.
                            if (fi.Exists) fi.Delete();
                            Image.SaveAs(path);
                            ad.ImageUrl = directory.Replace("~", "") + filename;
                            db.Entry(ad).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            AlertHandler.AddAlert("Error uploading image!", UserAlertLevel.ERROR);
                            ViewBag.UserAlerts = "Error uploading image!";
                        }
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("SEOURL", "This ad is already taken");
                    ViewBag.UserAlerts = "This ad is already taken";
                }
            }
            return View(ad);
        }

        //
        // GET: /admin/AdManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Ad ad = db.Ads.Find(id);
            if (ad == null)
            {
                return Http404();
            }
            return View(ad);
        }

        //
        // POST: /admin/AdManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ad ad, HttpPostedFileBase Image)
        {
            if (ModelState.IsValid)
            {
                //if (!db.Ads.Any(e => e.PublishTo == ad.PublishTo))
                //{
                    ad.UpdateDate = DateTime.Now;
                    db.Entry(ad).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    if (Image != null && Image.ContentLength > 0)
                    {
                        try
                        {
                            string filename = Path.GetFileName(Image.FileName);
                            filename = ad.AdID + "_" + Regex.Replace(filename, @"[^a-zA-Z0-9\.-]", "_");
                            string directory = ImageFilePath + ad.AdID.ToString() + @"/";
                            string path = System.IO.Path.Combine(Server.MapPath(directory), filename);
                            FileInfo fi = new FileInfo(path);
                            fi.Directory.Create(); // If the directory already exists, this method does nothing.
                            if (fi.Exists) fi.Delete();
                            Image.SaveAs(path);
                            ad.ImageUrl = directory.Replace("~", "") + filename;
                            db.Entry(ad).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            AlertHandler.AddAlert("Error uploading image!", UserAlertLevel.ERROR);
                            ViewBag.UserAlerts = "Error uploading image!";
                        }
                    }

                    return RedirectToAction("Index");
                //}
                //else
                //{
                //    ModelState.AddModelError("SEOURL", "This add is already taken");
                //}
            }
            return View(ad);
        }

        //
        // GET: /admin/AdManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Ad ad = db.Ads.Find(id);
            if (ad == null)
            {
                return Http404();
            }
            return View(ad);
        }

        //
        // POST: /admin/AdManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int AdID)
        {
            Ad ad = db.Ads.FirstOrDefault(a => a.AdID == AdID);
            if (ad == null) return Http404();
            db.Ads.Remove(ad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
