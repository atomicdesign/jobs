﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class MediaManagerController : CivilianJobsAdminController
    {
        protected static string UploadDir = "~/Content/upload/media";
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(string search = "")
        {
            DirectoryInfo di = new DirectoryInfo(Server.MapPath(UploadDir));
            FileInfo[] files = di.GetFiles("*" + search + "*");
            ArrayList li = new ArrayList();
            foreach (FileInfo fi in files)
            {
                li.Add(new { FileName = fi.Name, FileSize = fi.Length, Created = fi.CreationTime.ToShortDateString() });
            }
            return this.Json(new { files = li });
        }

        [HttpPost]
        public ActionResult UploadMedia(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    var filename = Path.GetFileName(file.FileName);
                    filename = Regex.Replace(filename, @"[^a-zA-Z0-9\.-]", "_");
                    var path = System.IO.Path.Combine(Server.MapPath(UploadDir), filename);

                    FileInfo fi = new FileInfo(path);
                    if (fi.Exists) fi.Delete();

                    file.SaveAs(path);
                    ViewBag.Message = "Success";
                }
                catch (Exception err)
                {
                    ViewBag.Message = "Error uploading file";
                    CivilianJobs.Helpers.LogHelper.Log(err);
                }
            }
            else
            {
                ViewBag.Message = "File Invalid";
            }

            return View();
        }

        [HttpPost]
        public JsonResult DeleteMedia(string file)
        {
            string message = "";
            try
            {
                var filename = Path.GetFileName(file);
                var path = System.IO.Path.Combine(Server.MapPath(UploadDir), filename);
                FileInfo fi = new FileInfo(path);
                if (fi.Exists)
                {
                    fi.Delete();
                    return this.Json(new { success = true });
                }
                message = "File Not Found";
            }
            catch (Exception err)
            {
                message = err.Message;
                CivilianJobs.Helpers.LogHelper.Log(err);
            }
            return this.Json(new { success = false, message = message });
        }

    }
}
