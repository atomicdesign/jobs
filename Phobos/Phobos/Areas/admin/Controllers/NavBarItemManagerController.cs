﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class NavBarItemManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/NavBarItemManager/

        public ActionResult Index(int id)
        {
            NavBar navbar = db.NavBars.Find(id);
            if (navbar == null)
            {
                return Http404();
            }
            return View(navbar);
        }

        public JsonResult List(int id)
        {
            var navbaritems = db.NavBarItems.Where(e => e.NavBarID == id);
            return this.Json(new
            {
                success = true,
                navbaritems = navbaritems.Select(n => new
                {
                    NavBarItemID = n.NavBarItemID,
                    Text = n.Text,
                    URL = n.URL,
                    OpenInNew = n.OpenInNew,
                    NavBarID = n.NavBarID,
                    Order = n.Order,
                    Items = n.Items
                }).ToList()
            });
        }

        //
        // GET: /admin/NavBarItemManager/Details/5

        public JsonResult Details(int id = 0)
        {
            //NavBarItem navbaritem = db.NavBarItems.Find(id);
            var navbaritem = db.NavBarItems.Where(e => e.NavBarItemID == id);//.SingleOrDefault();
            if (navbaritem == null)
            {
                return this.Json(new { success = false, message = "Could not find item" });
            }
            return this.Json(new
            {
                success = true,
                navbaritem = navbaritem.Select(n => new
                {
                    NavBarItemID = n.NavBarItemID,
                    Text = n.Text,
                    URL = n.URL,
                    OpenInNew = n.OpenInNew,
                    NavBarID = n.NavBarID,
                    Order = n.Order,
                    Items = n.Items
                }).SingleOrDefault()
            });
        }

        //
        // GET: /admin/NavBarItemManager/DetailsSub/5

        public JsonResult DetailsSub(int id = 0)
        {
            if (id < 0) //tertiary
            {
                id = id * -1;
                var subsubitem = db.NavBarSubSubItems.Where(e => e.NavBarSubSubItemID == id);
                if (subsubitem == null) { return this.Json(new { success = false, message = "Could not find item" }); }
                return this.Json(new
                {
                    success = true,
                    navbaritem = subsubitem.Select(n => new
            		{
                        NavBarSubItemID = (n.NavBarSubSubItemID * -1),
                        Text = n.Text,
                        URL = n.URL,
                        OpenInNew = n.OpenInNew,
                        Order = n.Order,
                        ParentID = n.ParentID
                    }).SingleOrDefault()
                }); 
            }
            else
            {
                var navbaritem = db.NavBarSubItems.Where(e => e.NavBarSubItemID == id);
                if (navbaritem == null) { return this.Json(new { success = false, message = "Could not find item" }); }
	            return this.Json(new
	            {
	                success = true,
	                navbaritem = navbaritem.Select(n => new
	                {
	                    NavBarSubItemID = n.NavBarSubItemID,
	                    Text = n.Text,
	                    URL = n.URL,
	                    OpenInNew = n.OpenInNew,
	                    Order = n.Order,
	                    ParentID = n.ParentID
	                }).SingleOrDefault()
	            });
        	}
        }

        //
        // POST: /admin/NavBarItemManager/Create

        [HttpPost]
        public JsonResult Create(NavBarItem navbaritem)
        {
            if (ModelState.IsValid)
            {
                db.NavBarItems.Add(navbaritem);
                db.SaveChanges();
                return this.Json(new { success = true, navbaritem = navbaritem });
            }
            return this.Json(new { success = false, message = GetModelErrorsAsString(ModelState) });
        }

        //
        // POST: /admin/NavBarItemManager/CreateSub

        [HttpPost]
        public JsonResult CreateSub(NavBarSubItem navbaritem)
        {
            if (ModelState.IsValid)
            {
                if (navbaritem.ParentID < 0) //tertiary
                {
                    NavBarSubSubItem subsubitem = new NavBarSubSubItem()
                    {
                        OpenInNew = navbaritem.OpenInNew,
                        Order = navbaritem.Order,
                        ParentID = (navbaritem.ParentID * -1),
                        Text = navbaritem.Text,
                        URL = navbaritem.URL
                    };
                    db.NavBarSubSubItems.Add(subsubitem);
                    db.SaveChanges();
                    return this.Json(new { success = true, navbaritem = subsubitem });
                }
                else
                {
	                db.NavBarSubItems.Add(navbaritem);
	                db.SaveChanges();
	                return this.Json(new { success = true, navbaritem = navbaritem });
	            }
                
            }
            return this.Json(new { success = false, message = GetModelErrorsAsString(ModelState) });
        }

        // POST: /admin/NavBarItemManager/Edit/5

        [HttpPost]
        public JsonResult Edit(NavBarItem navbaritem)
        {
            if (ModelState.IsValid)
            {
                NavBarItem current = db.NavBarItems.Find(navbaritem.NavBarItemID);
                db.Entry(current).CurrentValues.SetValues(navbaritem);
                db.SaveChanges();
                return this.Json(new { success = true, navbaritem = navbaritem });
            }
            return this.Json(new { success = false, message = GetModelErrorsAsString(ModelState) });
        }

        // POST: /admin/NavBarItemManager/EditSub/5

        [HttpPost]
        public JsonResult EditSub(NavBarSubItem navbaritem)
        {
            if (ModelState.IsValid)
            {
                if (navbaritem.NavBarSubItemID < 0) //tertiary
                {
                    NavBarSubSubItem subsubitem = new NavBarSubSubItem()
                    {
                        NavBarSubSubItemID = (navbaritem.NavBarSubItemID * -1),
                        OpenInNew = navbaritem.OpenInNew,
                        Order = navbaritem.Order,
                        ParentID = navbaritem.ParentID,
                        Text = navbaritem.Text,
                        URL = navbaritem.URL
                    };
                    db.Entry(subsubitem).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return this.Json(new { success = true, navbaritem = subsubitem });
                }
                else
	            {
	                NavBarSubItem current = db.NavBarSubItems.Find(navbaritem.NavBarSubItemID);
	                db.Entry(current).CurrentValues.SetValues(navbaritem);
	                db.SaveChanges();
	                return this.Json(new { success = true, navbaritem = navbaritem });
	            }
            }
            return this.Json(new { success = false, message = GetModelErrorsAsString(ModelState) });
        }


        //
        // POST: /admin/NavBarItemManager/Delete/5

        [HttpPost]
        public JsonResult Delete(int id)
        {
            NavBarItem navbaritem = db.NavBarItems.Find(id);
            db.NavBarItems.Remove(navbaritem);
            db.SaveChanges();
            return this.Json(new { success = true, navbaritem = navbaritem });
        }

        //
        // POST: /admin/NavBarItemManager/DeleteSub/5

        [HttpPost]
        public JsonResult DeleteSub(int id)
        {

            if (id < 0) //tertiary
            {
                NavBarSubSubItem subsubitem = db.NavBarSubSubItems.Find(id * -1);
                db.NavBarSubSubItems.Remove(subsubitem);
                db.SaveChanges();
                return this.Json(new { success = true, navbaritem = subsubitem });
            }
            
            NavBarSubItem navbaritem = db.NavBarSubItems.Find(id);
            db.NavBarSubItems.Remove(navbaritem);
            db.SaveChanges();
            return this.Json(new { success = true, navbaritem = navbaritem });
            
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}