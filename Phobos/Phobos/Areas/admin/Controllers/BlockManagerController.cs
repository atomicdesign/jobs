﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class BlockManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/BlockManager/

        public ActionResult Index()
        {
            return View(db.Blocks.ToList());
        }

        //
        // GET: /admin/BlockManager/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /admin/BlockManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Block block)
        {
            if (ModelState.IsValid)
            {
                db.Blocks.Add(block);
                db.SaveChanges();
                AddAlert(AlertLevel.SUCCESS, "Block created!", true);
                return RedirectToAction("Index");
            }

            return View(block);
        }

        //
        // GET: /admin/BlockManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Block block = db.Blocks.Find(id);
            if (block == null)
            {
                return Http404();
            }
            return View(block);
        }

        //
        // POST: /admin/BlockManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Block block)
        {
            if (ModelState.IsValid)
            {
                db.Entry(block).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                AddAlert(AlertLevel.SUCCESS, "Block edited!", true);
                return RedirectToAction("Index");
            }
            return View(block);
        }

        //
        // GET: /admin/BlockManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Block block = db.Blocks.Find(id);
            if (block == null)
            {
                return Http404();
            }
            return View(block);
        }

        //
        // POST: /admin/BlockManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Block block = db.Blocks.Find(id);
            db.Blocks.Remove(block);
            db.SaveChanges();
            AddAlert(AlertLevel.SUCCESS, "Block deleted!", true);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}