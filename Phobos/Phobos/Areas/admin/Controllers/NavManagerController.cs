﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class NavManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/NavManager/

        public ActionResult Index()
        {
            return View(db.NavBars.ToList());
        }

        //
        // GET: /admin/NavManager/Details/5

        public ActionResult Details(int id = 0)
        {
            NavBar navbar = db.NavBars.Find(id);
            if (navbar == null)
            {
                return Http404();
            }
            return View(navbar);
        }

        //
        // GET: /admin/NavManager/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /admin/NavManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NavBar navbar)
        {
            if (ModelState.IsValid)
            {
                db.NavBars.Add(navbar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(navbar);
        }

        //
        // GET: /admin/NavManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            NavBar navbar = db.NavBars.Find(id);
            if (navbar == null)
            {
                return Http404();
            }
            return View(navbar);
        }

        //
        // POST: /admin/NavManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NavBar navbar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(navbar).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(navbar);
        }

        //
        // GET: /admin/NavManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            NavBar navbar = db.NavBars.Find(id);
            if (navbar == null)
            {
                return Http404();
            }
            return View(navbar);
        }

        //
        // POST: /admin/NavManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NavBar navbar = db.NavBars.Find(id);
            db.NavBars.Remove(navbar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}