﻿using CivilianJobs.Helpers;
using CivilianJobs.Models.Jobs.Enums;
using CivilianJobs.Models.Jobs.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class DashboardController : CivilianJobsAdminController
    {
        //
        // GET: /admin/Dashboard/

        public ActionResult Index()
        {
            ViewBag.Employers = Roles.GetUsersInRole(CivilianJobsHelper.GetEnumDescription(JobsUserRole.Employer)).Count();
            ViewBag.JobSeekers = Roles.GetUsersInRole(CivilianJobsHelper.GetEnumDescription(JobsUserRole.JobSeeker)).Count();

            var jobPostings = new JobPostingRepository().GetTop100();
            ViewBag.JobPostings = new JobPostingRepository().GetCount();
            
            var jobApplications = new JobApplicationRepository().GetTop100();
            ViewBag.JobApplications = new JobApplicationRepository().GetCount();

            return View();
        }

        public ActionResult CMS()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "administrator", new { Area = "" });
        }

    }
}
