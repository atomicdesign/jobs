﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class FAQManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/FAQManager/

        public ActionResult Index()
        {
            ShowAlerts();
            return View(db.FAQs.ToList().OrderBy(x => x.Order).OrderBy(x => x.CategoryID));
        }

        //
        // GET: /admin/FAQManager/

        public ActionResult IndexCategory()
        {
            ShowAlerts();
            return View(db.FAQCategories.ToList().OrderBy(x => x.Name).OrderBy(x => x.Order));
        }

        //
        // GET: /admin/FAQManager/Create

        public ActionResult Create()
        {
            ViewBag.FAQCategoryID = new SelectList(db.FAQCategories, "FAQCategoryID", "Name");
            return View();
        }

        //
        // POST: /admin/FAQManager/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FAQ faq)
        {
            if (ModelState.IsValid)
            {
                db.FAQs.Add(faq);
                db.SaveChanges();
                AlertHandler.AddAlert("FAQ created!", UserAlertLevel.SUCCESS);
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.FAQCategories, "FAQCategoryID", "Name", faq.CategoryID);
            AlertHandler.AddAlert("Information is still needed!", UserAlertLevel.ERROR);
            ShowAlerts();
            return View(faq);
        }

        //
        // GET: /admin/FAQManager/CreateCategory

        public ActionResult CreateCategory()
        {
            return View();
        }

        //
        // POST: /admin/FAQManager/CreateCategory

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(FAQCategory faqcategory)
        {
            if (ModelState.IsValid)
            {
                db.FAQCategories.Add(faqcategory);
                db.SaveChanges();
                AlertHandler.AddAlert("FAQ Category created!", UserAlertLevel.SUCCESS);
                return RedirectToAction("IndexCategory");
            }
            AlertHandler.AddAlert("Information is still needed!", UserAlertLevel.ERROR);
            ShowAlerts();
            return View(faqcategory);
        }

        //
        // GET: /admin/FAQManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FAQ faq = db.FAQs.Find(id);
            if (faq == null)
            {
                return Http404();
            }
            ViewBag.FAQCategoryID = new SelectList(db.FAQCategories, "FAQCategoryID", "Name");
            return View(faq);
        }

        //
        // POST: /admin/FAQManager/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FAQ faq)
        {
            if (ModelState.IsValid)
            {
                db.Entry(faq).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                AlertHandler.AddAlert("FAQ edited!", UserAlertLevel.SUCCESS);
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.FAQCategories, "FAQCategoryID", "Name", faq.CategoryID);
            AlertHandler.AddAlert("Information is still needed!", UserAlertLevel.ERROR);
            ShowAlerts();
            return View(faq);
        }

        //
        // GET: /admin/FAQManager/EditCategory/5

        public ActionResult EditCategory(int id = 0)
        {
            FAQCategory faqcategory = db.FAQCategories.Find(id);
            if (faqcategory == null)
            {
                return Http404();
            }
            return View(faqcategory);
        }

        //
        // POST: /admin/FAQManager/EditCategory/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory(FAQCategory faqcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(faqcategory).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                AlertHandler.AddAlert("FAQ Category edited!", UserAlertLevel.SUCCESS);
                return RedirectToAction("IndexCategory");
            }
            AlertHandler.AddAlert("Information is still needed!", UserAlertLevel.ERROR);
            ShowAlerts();
            return View(faqcategory);
        }

        //
        // GET: /admin/FAQManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            FAQ faq = db.FAQs.Find(id);
            if (faq == null)
            {
                return Http404();
            }
            return View(faq);
        }

        //
        // POST: /admin/FAQManager/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FAQ faq = db.FAQs.Find(id);
            db.FAQs.Remove(faq);
            db.SaveChanges();
            AlertHandler.AddAlert("FAQ deleted!", UserAlertLevel.SUCCESS);
            return RedirectToAction("Index");
        }

        //
        // GET: /admin/FAQManager/DeleteCategory/5

        public ActionResult DeleteCategory(int id = 0)
        {
            FAQCategory faqcategory = db.FAQCategories.Find(id);
            if (faqcategory == null)
            {
                return Http404();
            }
            return View(faqcategory);
        }

        //
        // POST: /admin/FAQManager/Delete/5

        [HttpPost, ActionName("DeleteCategory")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCategoryConfirmed(int id)
        {
            FAQCategory faqcategory = db.FAQCategories.Find(id);
            db.FAQCategories.Remove(faqcategory);
            db.SaveChanges();
            AlertHandler.AddAlert("FAQ Category deleted!", UserAlertLevel.SUCCESS);
            return RedirectToAction("IndexCategory");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}