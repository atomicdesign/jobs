﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class SettingManagerController : CivilianJobsAdminController
    {
        //
        // GET: /admin/SettingManager/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult List(string search = "")
        {
            var settings = from s in db.Settings where (search == "" || s.Name.Contains(search)) orderby s.Name select s;
            return this.Json(new { success = true, settings = settings  });
        }

        //
        // GET: /admin/SettingManager/Create
        [HttpPost]
        public JsonResult Details(int id)
        {
            Setting s = db.Settings.Find(id);
            return this.Json(new { success = s != null, setting = s });
        }
        // POST: /admin/SettingManager/Create

        [HttpPost]
        public JsonResult Create(Setting setting)
        {
            if (ModelState.IsValid)
            {
                if(db.Settings.Any(e=>e.Name == setting.Name))
                {
                    return this.Json(new { success = false, message = "Duplicate Key" });
                }else{
                    db.Settings.Add(setting);
                    db.SaveChanges();
                    return this.Json(new { success = true, message = "Saved", setting = setting });
                }                
            }
            return this.Json(new { success = false, message = "Could Not Save" });
        }

        //
        // POST: /admin/SettingManager/Edit/5

        [HttpPost]
        public JsonResult Edit(Setting setting)
        {
            if (ModelState.IsValid)
            {
                if (db.Settings.Any(e => e.Name == setting.Name && e.SettingID != setting.SettingID)) 
                    return this.Json(new { success = false, message = "Duplicate Key" });

                var currentSetting = db.Settings.Find(setting.SettingID);
                db.Entry(currentSetting).CurrentValues.SetValues(setting);
                db.SaveChanges();
                return this.Json(new { success = true, setting = setting });
            }
            return this.Json(new { success = false, message = "Could Not Save" });
        }

        //
        // POST: /admin/SettingManager/Delete/5

        [HttpPost]
        public JsonResult Delete(int id)
        {
            Setting setting = db.Settings.Find(id);
            db.Settings.Remove(setting);
            db.SaveChanges();
            return this.Json(new { success = "true", message = "Deleted" });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}