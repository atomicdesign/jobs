﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.admin.Controllers
{
    public class FormFieldOptionController : CivilianJobsAdminController
    {
        

        //
        // GET: /admin/FormFieldOption/
        [HttpPost]
        public JsonResult List(int id)
        {
            var formfieldoptions = db.FormFieldOptions.Where(e => e.FormFieldID == id).OrderBy(e=>e.Order);
            return this.Json(new { success = true, formfieldoptions = formfieldoptions.ToList() });
        }

        //
        // GET: /admin/FormFieldOption/Details/5
        [HttpPost]
        public JsonResult Details(int id = 0)
        {
            FormFieldOption formfieldoption = db.FormFieldOptions.Find(id);
            if (formfieldoption == null)
            {
                return this.Json(new { success = false, message = "Could not find option" });
            }
            return this.Json(new { success = true, formfieldoption = formfieldoption });
        }

        //
        // POST: /admin/FormFieldOption/Create

        [HttpPost]
        public JsonResult Create(FormFieldOption formfieldoption)
        {
            if (ModelState.IsValid)
            {
                db.FormFieldOptions.Add(formfieldoption);
                db.SaveChanges();
                return this.Json(new { success = true, formfieldoption = formfieldoption });
            }
            return this.Json(new { success = false, message = GetModelErrorsAsString(ModelState) });
        }

        //
        // POST: /admin/FormFieldOption/Edit/5

        [HttpPost]
        public JsonResult Edit(FormFieldOption formfieldoption)
        {
            if (ModelState.IsValid)
            {
                FormFieldOption curFFO = db.FormFieldOptions.Find(formfieldoption.FormFieldOptionID);
                db.Entry(formfieldoption).CurrentValues.SetValues(formfieldoption);
                db.SaveChanges();
                return this.Json(new { success = true, formfieldoption = formfieldoption });
            }
            return this.Json(new { success = false, message = GetModelErrorsAsString(ModelState) });
        }

        //
        // POST: /admin/FormFieldOption/Delete/5

        [HttpPost]
        public JsonResult Delete(int id)
        {
            FormFieldOption formfieldoption = db.FormFieldOptions.Find(id);
            db.FormFieldOptions.Remove(formfieldoption);
            db.SaveChanges();
            return this.Json(new { success = true, formfieldoption = formfieldoption });
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}