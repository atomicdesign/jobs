﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using CivilianJobs.Models;
using System.IO;
using System.Text.RegularExpressions;


namespace CivilianJobs.Areas.admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class GalleriesManagerController : CivilianJobsAdminController
    {
        private string ImageFilePath = @"~/Content/upload/modules/galleries/";
        
        //
        // GET: /admin/GalleriesManager/

        public ActionResult Index()
        {
            return View(db.Galleries.ToList().OrderBy(e => e.Name));
        }

       
        //
        // GET: /admin/GalleriesManager/Create
        public ActionResult Create(int id = 0)
        {
            Gallery gallery = new Gallery();
            return View();
        }

        //
        // POST: /admin/GalleriesManager/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Gallery gallery)
        {
            if (ModelState.IsValid)
            {
                if (!db.Galleries.Any(e => e.SEOURL == gallery.SEOURL))
                {
                    gallery.Created = DateTime.Now;
                    db.Galleries.Add(gallery);
                    db.SaveChanges();
                    return RedirectToAction("Edit", new { id = gallery.GalleryID});
                }
                else
                {
                    ModelState.AddModelError("SEOURL", "This path is already taken");
                }
            }
            return View(gallery);
        }

        //
        // GET: /admin/GalleriesManager/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null) return Http404();
            return View(gallery);
        }

        //
        // POST: /admin/GalleriesManager/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Gallery gallery)
        {
            if (ModelState.IsValid)
            {
                if (!db.Galleries.Any(e => e.SEOURL == gallery.SEOURL && e.GalleryID != gallery.GalleryID))
                {
                    db.Entry(gallery).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("SEOURL", "This path is already taken");
                }
            }
            return View(gallery);
        }

        //
        // GET: /admin/GalleriesManager/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null) return Http404();
            return View(gallery);
        }

        //
        // POST: /admin/GalleriesManager/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null) return Http404();

            //delete image files
            DeleteItemImages(id);

            //delete directory
            if(gallery.GalleryItems != null && gallery.GalleryItems.Count > 0)
            {
                GalleryItem firstItem = gallery.GalleryItems.FirstOrDefault();
                if (firstItem.ImageUrl != null && firstItem.ImageUrl != "")
                {
                    DirectoryInfo di = new FileInfo(System.IO.Path.Combine(Server.MapPath(firstItem.ImageUrl))).Directory;
                    if (di.Exists) di.Delete();
                }
            }

            //delete gallery and its items
            gallery.DeleteSafe(db);
            
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        //
        // GET: /admin/GalleriesManager/Items
        public ActionResult Items(int id = 0)
        {
            Gallery gallery = db.Galleries.Find(id);
            if (gallery == null) return Http404();
            return View(gallery);
        }


        //
        // GET: /admin/GalleriesManager/CreateItem
        public ActionResult CreateItem(int id = 0)
        {
            GalleryItem galitem = new GalleryItem { GalleryID = id };
            return View(galitem);
        }

        //
        // POST: /admin/GalleriesManager/CreateItem
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateItem(GalleryItem galitem, HttpPostedFileBase ItemImage)
        {
            if (ModelState.IsValid)
            {
                db.GalleryItems.Add(galitem);
                db.SaveChanges();

                if (ItemImage != null && ItemImage.ContentLength > 0)
                {
                    try
                    {
                        string filename = Path.GetFileName(ItemImage.FileName);
                        filename = galitem.GalleryItemID + "_" + Regex.Replace(filename, @"[^a-zA-Z0-9\.-]", "_");
                        string directory = ImageFilePath + galitem.GalleryID.ToString() + @"/";
                        string path = System.IO.Path.Combine(Server.MapPath(directory), filename);
                        FileInfo fi = new FileInfo(path);
                        fi.Directory.Create(); // If the directory already exists, this method does nothing.
                        if (fi.Exists) fi.Delete();
                        ItemImage.SaveAs(path);
                        galitem.ImageUrl = directory.Replace("~", "") + filename;
                        db.Entry(galitem).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {
                        AlertHandler.AddAlert("Error uploading image!", UserAlertLevel.ERROR);
                    }
                }

                return RedirectToAction("Items", new { id = galitem.GalleryID });
            }

            return View(galitem);
        }

        //
        // GET: /admin/GalleriesManager/EditItem/5
        public ActionResult EditItem(int id = 0)
        {
            GalleryItem galitem = db.GalleryItems.Find(id);
            if (galitem == null) return Http404();
            return View(galitem);
        }

        //
        // POST: /admin/GalleriesManager/EditItem/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditItem(GalleryItem galitem, HttpPostedFileBase ItemImage)
        {
            if (ModelState.IsValid)
            {
                if (ItemImage != null && ItemImage.ContentLength > 0)
                {
                    try {
                        //delete old img
                        string deletefileName = Path.GetFileName(Request.Form["OldImage"]);
                        string deletefileDirectory = ImageFilePath + galitem.GalleryID.ToString() + @"/";
                        string deletefilePath = System.IO.Path.Combine(Server.MapPath(deletefileDirectory), deletefileName);
                        FileInfo deletefileInfo = new FileInfo(deletefilePath);
                        if (deletefileInfo.Exists) deletefileInfo.Delete();

                        //save new img
                        string filename = Path.GetFileName(ItemImage.FileName);
                        filename = galitem.GalleryItemID + "_" + Regex.Replace(filename, @"[^a-zA-Z0-9\.-]", "_");
                        string directory = ImageFilePath + galitem.GalleryID.ToString() + @"/";
                        string path = System.IO.Path.Combine(Server.MapPath(directory), filename);
                        FileInfo fi = new FileInfo(path);
                        fi.Directory.Create(); // If the directory already exists, this method does nothing.
                        if (fi.Exists) fi.Delete();
                        ItemImage.SaveAs(path);
                        galitem.ImageUrl = directory.Replace("~","") + filename;
                    }
                    catch (Exception err)
                    {
                        ViewBag.Message = "Error uploading file";
                        throw new Exception(err.Message);
                    }
                }

                db.Entry(galitem).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Items", new { id = galitem.GalleryID });
            }
            return View(galitem);
        }

        //
        // GET: /admin/GalleriesManager/DeleteItem/5
        public ActionResult DeleteItem(int id = 0)
        {
            GalleryItem galitem = db.GalleryItems.Find(id);
            if (galitem == null) return Http404();
            return View(galitem);
        }

        //
        // POST: /admin/GalleriesManager/DeleteItem/5

        [HttpPost, ActionName("DeleteItem")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedItem(int id)
        {
            GalleryItem galitem = db.GalleryItems.Find(id);

            if (galitem.ImageUrl != null && galitem.ImageUrl.Trim() != "")
            {
                string path = System.IO.Path.Combine(Server.MapPath(galitem.ImageUrl));
                FileInfo fi = new FileInfo(path);
                if (fi.Exists) fi.Delete();
            }

            db.GalleryItems.Remove(galitem);
            db.SaveChanges();

            return RedirectToAction("Items", new { id = galitem.GalleryID });
        }


        private void DeleteItemImages(int id)
        {
            Gallery gallery = db.Galleries.Find(id);
            foreach (GalleryItem galitem in gallery.GalleryItems)
            {
                if (galitem.ImageUrl != null && galitem.ImageUrl.Trim() != "")
                {
                    string path = System.IO.Path.Combine(Server.MapPath(galitem.ImageUrl));
                    FileInfo fi = new FileInfo(path);
                    if (fi.Exists) fi.Delete();
                }
            }
        }

    }
}
