﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.api.Controllers
{
    public class SettingController : CivilianJobsBaseApiController
    {

        public IEnumerable<Setting> GetSettings(string search = "")
        {
            return db.Settings.Where(e=>e.Name.ToLower().Contains(search)).AsEnumerable();
        }

        // GET api/Setting/5
        public Setting GetSetting(int id)
        {
            Setting setting = db.Settings.Find(id);
            if (setting == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return setting;
        }

        // PUT api/Setting/5
        public HttpResponseMessage PutSetting(int id, Setting setting)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != setting.SettingID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(setting).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Setting
        public HttpResponseMessage PostSetting(Setting setting)
        {
            if (ModelState.IsValid)
            {
                db.Settings.Add(setting);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, setting);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = setting.SettingID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Setting/5
        public HttpResponseMessage DeleteSetting(int id)
        {
            Setting setting = db.Settings.Find(id);
            if (setting == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Settings.Remove(setting);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, setting);
        }
    }
}