﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.api.Controllers
{
    [Authorize(Roles="Administrator")]
    public class AdminAlertController : ApiController
    {
        private CivilianJobsContext db = new CivilianJobsContext();

        // GET api/AdminAlert
        public IEnumerable<AdminAlert> GetAdminAlerts()
        {
            return db.Alerts.AsEnumerable();
        }

        // GET api/AdminAlert/5
        public AdminAlert GetAdminAlert(int id)
        {
            AdminAlert adminalert = db.Alerts.Find(id);
            if (adminalert == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return adminalert;
        }

        // PUT api/AdminAlert/5
        public HttpResponseMessage PutAdminAlert(int id, AdminAlert adminalert)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != adminalert.AdminAlertID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(adminalert).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/AdminAlert
        public HttpResponseMessage PostAdminAlert(AdminAlert adminalert)
        {
            if (ModelState.IsValid)
            {
                db.Alerts.Add(adminalert);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, adminalert);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = adminalert.AdminAlertID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/AdminAlert/5
        public HttpResponseMessage DeleteAdminAlert(int id)
        {
            AdminAlert adminalert = db.Alerts.Find(id);
            if (adminalert == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            adminalert.Dismissed = true;
            db.Entry(adminalert).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, adminalert);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}