﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CivilianJobs.Models;

namespace CivilianJobs.Areas.api.Controllers
{
    public class CivilianJobsBaseApiController : ApiController
    {
        protected CivilianJobsContext db = new CivilianJobsContext();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
