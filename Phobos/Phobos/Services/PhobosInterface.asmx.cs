﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;

namespace CivilianJobs.Areas.api.Controllers
{
    /// <summary>
    /// Summary description for CivilianJobsInterface
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CivilianJobsInterface : System.Web.Services.WebService
    {

        /* SETTING ADMIN PASSWORD */
        /*
         *  1) Change password ("butc1*2013") where it appears below to the new password.
         *  2) Uncomment the specified line in Controllers > AccountController.cs > Login(...)
         *  3) Run the project and navigate to /admin.
         *  4) Log in with the new password.
         *  5) Recomment out the line in Controllers > AccountController.cs > Login(...)
        */

        [WebMethod]
        public string ResetUser()
        {
            if (Membership.GetUser("admin@citrusweb.com") == null) Membership.CreateUser("admin@citrusweb.com", "c1vj0bz*2015", "admin@citrusweb.com");
            MembershipUser mu = Membership.GetUser("admin@citrusweb.com");
            string oldPass = mu.ResetPassword();
            mu.ChangePassword(oldPass, "c1vj0bz*2015");

            if (!Roles.RoleExists("Administrator")) Roles.CreateRole("Administrator");
            if (!Roles.IsUserInRole("admin@citrusweb.com", "Administrator")) Roles.AddUserToRole("admin@citrusweb.com", "Administrator");

            return "success";
        }

        [WebMethod]
        public string CreateTestUser()
        {
            if (Membership.GetUser("test@gmail.com") == null) Membership.CreateUser("test@gmail.com", "123456", "test@gmail.com");
            MembershipUser mu = Membership.GetUser("test@gmail.com");
            string oldPass = mu.ResetPassword();
            mu.ChangePassword(oldPass, "123456");

            if (!Roles.RoleExists("Job Seeker")) Roles.CreateRole("Job Seeker");
            if (!Roles.IsUserInRole("test@gmail.com", "Administrator")) Roles.AddUserToRole("test@gmail.com", "Job Seeker");


            if (Membership.GetUser("test2@gmail.com") == null) Membership.CreateUser("test2@gmail.com", "123456", "test2@gmail.com");
            MembershipUser mo2 = Membership.GetUser("test2@gmail.com");
            string oldPass2 = mo2.ResetPassword();
            mo2.ChangePassword(oldPass2, "123456");

            if (!Roles.RoleExists("Employer")) Roles.CreateRole("Employer");
            if (!Roles.IsUserInRole("test2@gmail.com", "Employer")) Roles.AddUserToRole("test2@gmail.com", "Employer");

            return "success";
        }
    }
}
