﻿var simple_motion_banner = function(name, width, speed, delay)
{
    //know thyself
    var myself = this;

    //set variables
    this.name = name;
    this.gallery = $('#' + name);
    this.width = width;
    this.speed = speed;
    this.delay = delay;
    this.item_class = 'div.simple-motion-banner-image';
    
    //get objects
    this.first_item = this.gallery.find(this.item_class).first();
    this.current_item = this.first_item;
    this.last_item = this.gallery.find(this.item_class).last();

    this.tick = function () {
        var old_item = myself.current_item;
        var new_item = myself.current_item.next(myself.item_class);
        if(!new_item.length) new_item = myself.first_item;
        
        old_item.animate({ left: -myself.width }, myself.speed);
        new_item.css('left', myself.width).animate({left: 0}, myself.speed);

        myself.current_item = new_item;

        setTimeout(myself.tick, myself.delay + myself.speed);
    };

    //initialize
    this.current_item.css('left', '0px');
    setTimeout(this.tick, myself.delay);
};

var motion_banners = [];
$(function () {
    console.log('starting motion banners');
    motion_banners['homepage-gallery'] = new simple_motion_banner('homepage-gallery', 680, 1000, 5000);
});