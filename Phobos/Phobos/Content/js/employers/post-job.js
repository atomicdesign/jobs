﻿$(function () {
  
    var $jobCat = $('#job-cat');
        $jobCatSelected = $('#job-cat-selected'),
        $selectedJobCats = $('#selected-job-cats');

    $jobCat.click(function (e) {
        var $target = $(e.target),
            index;

        if ($target.is('p')) {
            index = $jobCatSelected.find('p').length;

            if(index === -1){
                index = 0;
            }

            $jobCat.find($target).hide();

            debugger;

            $target.clone()
                .show()
                .wrap('<p>')
                .appendTo($jobCatSelected);

            $selectedJobCats
                .append('<input type="hidden" name="selectedJobCategories[' + index + '].ID" value="' + $target.data('categoryId') + '" data-category-id="' + $target.data('categoryId') + '" />')
                .append('<input type="hidden" name="selectedJobCategories[' + index + '].Name" value="' + $target.data('categoryName') + '" data-category-id="' + $target.data('categoryId') + '" />')
                .append('<input type="hidden" name="selectedJobCategories[' + index + '].ParentCategoryID" value="' + $target.data('categoryParentId') + '" data-category-id="' + $target.data('categoryId') + '" />');
        }
    });

    $jobCatSelected.click(function (e) {
        var $target = $(e.target),
            options;

        if ($target.is('p')) {
            $jobCatSelected.find($target).remove();
            $jobCat.find('[data-category-id=' + $target.data('categoryId') + ']').show();
            $selectedJobCats.find('[data-category-id=' + $target.data('categoryId') + ']').remove();
        }
    });

    // edit-job pre-selection of categories
    if ($('#JobPosting_ID').val()) {

        (function () {
            var $jobCategories = $('#pre-selected-job-cats span').each(function () {
                $jobCat.find('[data-category-id="' + $(this).data('categoryId') + '"]')
                    .trigger('click');
            });
        })();
    }
});