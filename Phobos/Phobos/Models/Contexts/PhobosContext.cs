﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models;
using CivilianJobs.Models.Mapping;
using CivilianJobs.Models.civjobs;

namespace CivilianJobs.Models
{
    public partial class CivilianJobsContext : DbContext
    {
        public CivilianJobsContext() : base("DefaultConnection") { }

        public DbSet<SRM> SRMs { get; set; }
        public DbSet<SrmDegrees> SrmDegrees { get; set; }
        public DbSet<JobFairAppliers> JobFairAppliers { get; set; }
        public DbSet<SavedResumes> SavedResumes { get; set; }
        public DbSet<PreOrder> PreOrders { get; set; }
        public DbSet<ProductDescription> ProductDescriptions { get; set; }

        public DbSet<ReferralNote> ReferralNotes { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserDegree> UserDegrees { get; set; }
        public DbSet<SystemLog> SystemLog { get; set; }
        public DbSet<AdminAlert> Alerts { get; set; }

        public DbSet<Template> Templates { get; set; }
        public DbSet<TemplateRegion> TemplateRegions { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<PageRegion> PageRegions { get; set; }
        public DbSet<Block> Blocks { get; set; }

        public DbSet<DotLessStylesheet> DotLessStylesheets { get; set; }
        public DbSet<DotLessVariable> DotLessVariables { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<Form> Forms { get; set; }
        public DbSet<FormField> FormFields { get; set; }
        public DbSet<FormFieldOption> FormFieldOptions { get; set; }
        public DbSet<FormSubmission> FormSubmissions { get; set; }

        public DbSet<NavBar> NavBars { get; set; }
        public DbSet<NavBarItem> NavBarItems { get; set; }
        public DbSet<NavBarSubItem> NavBarSubItems { get; set; }
        public DbSet<NavBarSubSubItem> NavBarSubSubItems { get; set; }

        public DbSet<EmailMessage> EmailQueue { get; set; }

        public DbSet<FAQCategory> FAQCategories { get; set; }
        public DbSet<FAQ> FAQs { get; set; }

        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<GalleryItem> GalleryItems { get; set; }

        public DbSet<SocialMedia> SocialMedia { get; set; }

        public DbSet<GeoName> GeoNames { get; set; }
        public DbSet<GeoNameCountry> GeoNameCountries { get; set; }
        public DbSet<GeoNamePostalCode> GeoNamePostalCodes { get; set; }

        public DbSet<Payment> Payments { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<ThreeOhOne> Redirects { get; set; }

        public DbSet<Events> Events { get; set; }

        public UserProfile LoadUser(string userName)
        {
            MembershipUser mu = Membership.GetUser(userName);

            if (mu != null)
            {
                UserProfile up = UserProfiles.FirstOrDefault(e => e.UserId == (Guid)mu.ProviderUserKey);

                if (up == null) //every user gets a profile
                {
                    up = new UserProfile();
                    up.UserName = mu.UserName;
                    up.Created = DateTime.Now;
                    UserProfiles.Add(up);
                    SaveChanges();
                }

                return up;
            }

            return null;
        }

        public DbSet<JobPostings> JobPostings { get; set; }
        public DbSet<JobCategories> JobCategories { get; set; }
        public DbSet<JobPostingCategory> JobPostingCategories { get; set; }
        public DbSet<JobApplication> JobApplications { get; set; }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<JobFairs> JobFairs { get; set; }
        public DbSet<TransitionNewsArticle> TransitionNewsArticles { get; set; }
        public DbSet<PressRelease> PressReleases { get; set; }
        public DbSet<Microsite> Microsites { get; set; }
        public DbSet<SavedJob> SavedJobs { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Orders> Orders { get; set; }


        public DbSet<ABAL> ABALs { get; set; }
        public DbSet<ActiveDuty> ActiveDuties { get; set; }
        public DbSet<Addresses> Addresses { get; set; }
        public DbSet<AllstateLead> AllstateLeads { get; set; }
        public DbSet<AppLog> AppLogs { get; set; }
        public DbSet<Avail> Avails { get; set; }

        public DbSet<Awards> Awardss { get; set; }
        public DbSet<NewDesi> NewDesis { get; set; }
       
        public DbSet<bmi> bmis { get; set; }
        public DbSet<CandCareerPref> CandCareerPrefs { get; set; }
        public DbSet<CandCert> CandCerts { get; set; }
        public DbSet<CandGeo> CandGeos { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<CandJobCategory> CandJobCategories { get; set; }
        public DbSet<CandLanguage> CandLanguages { get; set; }
        public DbSet<CandMilExp> CandMilExps { get; set; }
        public DbSet<CandPosition> CandPositions { get; set; }
        public DbSet<CandReason> CandReasons { get; set; }
        public DbSet<CandSchool> CandSchools { get; set; }
        public DbSet<CandSearchCriteriaLog> CandSearchCriteriaLogs { get; set; }
        public DbSet<CandSearchResultsLog> CandSearchResultsLogs { get; set; }
        public DbSet<CandSkill> CandSkills { get; set; }
        public DbSet<CandState> CandStates { get; set; }
        public DbSet<CandSurvey> CandSurveys { get; set; }
        public DbSet<CandWorkHistoryUpdateLog> CandWorkHistoryUpdateLogs { get; set; }
        public DbSet<CareerPref> CareerPrefs { get; set; }
        public DbSet<Certification> Certifications { get; set; }
        public DbSet<ClearanceReq> ClearanceReqs { get; set; }
        public DbSet<Clearance> Clearances { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientSurvey> ClientSurveys { get; set; }
        public DbSet<ClientSurveyReason> ClientSurveyReasons { get; set; }
        public DbSet<ClientType> ClientTypes { get; set; }
        public DbSet<CompensationType> CompensationTypes { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<DegreeReq> DegreeReqs { get; set; }
        public DbSet<Discharge> Discharges { get; set; }
        public DbSet<dtproperty> dtproperties { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<email_errors> email_errors { get; set; }
        public DbSet<email_sent> email_sent { get; set; }
        public DbSet<email_staging> email_staging { get; set; }
        public DbSet<EmailCampaignQuery> EmailCampaignQuerys { get; set; }
        public DbSet<EmployeeType> EmployeeTypes { get; set; }
        public DbSet<Ethnicity> Ethnicities { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Geo> Geos { get; set; }
        public DbSet<HearAbout> HearAbouts { get; set; }
        public DbSet<HighSchool> HighSchools { get; set; }
        public DbSet<HiringNeedList> HiringNeedLists { get; set; }
        public DbSet<HiringNeedsSurvey> HiringNeedsSurveys { get; set; }
        public DbSet<HiringNeedsSurveyNeed> HiringNeedsSurveyNeeds { get; set; }
        public DbSet<HN_JobCategoryXref> HN_JobCategoryXref { get; set; }
        public DbSet<JobCategory> JobCategory { get; set; }
        public DbSet<JobFair> JobFair { get; set; }
        public DbSet<JobFair2> JobFair2 { get; set; }
        public DbSet<JobFairAttendanceType> JobFairAttendanceTypes { get; set; }
        public DbSet<JobFairAttendee> JobFairAttendees { get; set; }
        public DbSet<JobFairCategory> JobFairCategories { get; set; }
        public DbSet<JobFairComparison> JobFairComparisons { get; set; }
        public DbSet<JobFairDisplaySize> JobFairDisplaySizes { get; set; }
        public DbSet<JobFairDoc> JobFairDocs { get; set; }
        public DbSet<JobFairDocType> JobFairDocTypes { get; set; }
        public DbSet<JobFairLocation> JobFairLocations { get; set; }
        public DbSet<JobFairRegistration> JobFairRegistrations { get; set; }
        public DbSet<JobFairRegistrationJob> JobFairRegistrationJobs { get; set; }
        public DbSet<JobFairRegistrationRep> JobFairRegistrationReps { get; set; }
        public DbSet<JobFairSkill> JobFairSkills { get; set; }
        public DbSet<JobFairStatu> JobFairStatus { get; set; }
        public DbSet<JobPosting> JobPosting { get; set; }
        public DbSet<JobPostingCandidate> JobPostingCandidates { get; set; }
        public DbSet<JobPostingCandidateStatu> JobPostingCandidateStatus { get; set; }
        public DbSet<JobPostingJobCategory> JobPostingJobCategories { get; set; }
        public DbSet<JobPostingStatu> JobPostingStatus { get; set; }
        public DbSet<JobSeekerOption> JobSeekerOptions { get; set; }
        public DbSet<JobSeekersDetail> JobSeekersDetails { get; set; }

        public DbSet<JobSeekerSurveyQuestion> JobSeekerSurveyQuestions { get; set; }
        public DbSet<JobSeekerSurveyResult> JobSeekerSurveyResults { get; set; }
        public DbSet<JobWrapStat> JobWrapStats { get; set; }
        public DbSet<LanguageProficiency> LanguageProficiencies { get; set; }
        public DbSet<LanguageReq> LanguageReqs { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Lead> Leads { get; set; }
        public DbSet<Leads_Backup> Leads_Backup { get; set; }
        public DbSet<LineItemDiscount> LineItemDiscounts { get; set; }
        public DbSet<Medal> Medals { get; set; }
        public DbSet<MilitaryExperience> MilitaryExperiences { get; set; }
        public DbSet<MonsterMail> MonsterMails { get; set; }
        public DbSet<MonsterMailJobURL> MonsterMailJobURLs { get; set; }
        public DbSet<MonsterMailMR> MonsterMailMRs { get; set; }
        public DbSet<MonsterMailUser> MonsterMailUsers { get; set; }
        public DbSet<MVE_Survey> MVE_Survey { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<OrderDetailJobPosting> OrderDetailJobPostings { get; set; }
        public DbSet<OrderStatu> OrderStatus { get; set; }
        public DbSet<Pay> Pays { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDiscount> ProductDiscounts { get; set; }
        public DbSet<ProductQty> ProductQties { get; set; }
        public DbSet<ProductTypeMaxDiscount> ProductTypeMaxDiscounts { get; set; }
        public DbSet<QualityRating> QualityRatings { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<ResumeViewLog> ResumeViewLogs { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<SecureResource> SecureResources { get; set; }
        public DbSet<SecureResourceType> SecureResourceTypes { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ShiftWork> ShiftWorks { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SpecialtyArea> SpecialtyAreas { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<tempcount> tempcounts { get; set; }
        public DbSet<Top40Survey> Top40Survey { get; set; }
        public DbSet<Travel> Travels { get; set; }
        public DbSet<TravelPct> TravelPcts { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<wcmga> wcmgas { get; set; }
        public DbSet<YearsOfService> YearsOfServices { get; set; }
        public DbSet<YesNo> YesNoes { get; set; }
        public DbSet<OrderPostingView> OrderPostingViews { get; set; }
        public DbSet<OrderResumeView> OrderResumeViews { get; set; }
        //public DbSet<Order1> Orders1 { get; set; }
        public DbSet<SecurityAccessView> SecurityAccessViews { get; set; }
        public DbSet<StateView> StateViews { get; set; }
        public DbSet<UserListView> UserListViews { get; set; }
        public DbSet<User1> Users1 { get; set; }
        public DbSet<viewABAL> viewABALs { get; set; }
        public DbSet<viewCandidateJobCategory> viewCandidateJobCategories { get; set; }
        public DbSet<viewCandidate> viewCandidates { get; set; }
        public DbSet<viewDirectHire> viewDirectHires { get; set; }
        public DbSet<viewJobCategory> viewJobCategories { get; set; }
        public DbSet<Ad> Ads { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Properties()
            .Where(x => x.GetCustomAttributes(false).OfType<NonUnicode>().Any())
            .Configure(c => c.IsUnicode(false));

            modelBuilder.Configurations.Add(new ABALMap());
            modelBuilder.Configurations.Add(new ActiveDutyMap());
            modelBuilder.Configurations.Add(new AddressMap());
            modelBuilder.Configurations.Add(new AllstateLeadMap());
            modelBuilder.Configurations.Add(new AppLogMap());
            modelBuilder.Configurations.Add(new AvailMap());
            modelBuilder.Configurations.Add(new bmiMap());
            modelBuilder.Configurations.Add(new CandCareerPrefMap());
            modelBuilder.Configurations.Add(new CandCertMap());
            modelBuilder.Configurations.Add(new CandGeoMap());
            modelBuilder.Configurations.Add(new CandidateMap());
            modelBuilder.Configurations.Add(new CandJobCategoryMap());
            modelBuilder.Configurations.Add(new CandLanguageMap());
            modelBuilder.Configurations.Add(new CandMilExpMap());
            modelBuilder.Configurations.Add(new CandPositionMap());
            modelBuilder.Configurations.Add(new CandReasonMap());
            modelBuilder.Configurations.Add(new CandSchoolMap());
            modelBuilder.Configurations.Add(new CandSearchCriteriaLogMap());
            modelBuilder.Configurations.Add(new CandSearchResultsLogMap());
            modelBuilder.Configurations.Add(new CandSkillMap());
            modelBuilder.Configurations.Add(new CandStateMap());
            modelBuilder.Configurations.Add(new CandSurveyMap());
            modelBuilder.Configurations.Add(new CandWorkHistoryUpdateLogMap());
            modelBuilder.Configurations.Add(new CareerPrefMap());
            modelBuilder.Configurations.Add(new CertificationMap());
            modelBuilder.Configurations.Add(new ClearanceReqMap());
            modelBuilder.Configurations.Add(new ClearanceMap());
            modelBuilder.Configurations.Add(new ClientMap());
            modelBuilder.Configurations.Add(new ClientSurveyMap());
            modelBuilder.Configurations.Add(new ClientSurveyReasonMap());
            modelBuilder.Configurations.Add(new ClientTypeMap());
            modelBuilder.Configurations.Add(new CompensationTypeMap());
            modelBuilder.Configurations.Add(new CouponMap());
            modelBuilder.Configurations.Add(new DegreeMap());
            modelBuilder.Configurations.Add(new DegreeReqMap());
            modelBuilder.Configurations.Add(new DischargeMap());
            modelBuilder.Configurations.Add(new dtpropertyMap());
            modelBuilder.Configurations.Add(new EmailMap());
            modelBuilder.Configurations.Add(new email_errorsMap());
            modelBuilder.Configurations.Add(new email_sentMap());
            modelBuilder.Configurations.Add(new email_stagingMap());
            modelBuilder.Configurations.Add(new EmailCampaignQueryMap());
            modelBuilder.Configurations.Add(new EmployeeTypeMap());
            modelBuilder.Configurations.Add(new EthnicityMap());
            modelBuilder.Configurations.Add(new ExperienceMap());
            modelBuilder.Configurations.Add(new GenderMap());
            modelBuilder.Configurations.Add(new GeoMap());
            modelBuilder.Configurations.Add(new HearAboutMap());
            modelBuilder.Configurations.Add(new HighSchoolMap());
            modelBuilder.Configurations.Add(new HiringNeedListMap());
            modelBuilder.Configurations.Add(new HiringNeedsSurveyMap());
            modelBuilder.Configurations.Add(new HiringNeedsSurveyNeedMap());
            modelBuilder.Configurations.Add(new HN_JobCategoryXrefMap());
            modelBuilder.Configurations.Add(new JobCategoryMap());
            modelBuilder.Configurations.Add(new JobFairMap());
            modelBuilder.Configurations.Add(new JobFair2Map());
            modelBuilder.Configurations.Add(new JobFairAttendanceTypeMap());
            modelBuilder.Configurations.Add(new JobFairAttendeeMap());
            modelBuilder.Configurations.Add(new JobFairCategoryMap());
            modelBuilder.Configurations.Add(new JobFairComparisonMap());
            modelBuilder.Configurations.Add(new JobFairDisplaySizeMap());
            modelBuilder.Configurations.Add(new JobFairDocMap());
            modelBuilder.Configurations.Add(new JobFairDocTypeMap());
            modelBuilder.Configurations.Add(new JobFairLocationMap());
            modelBuilder.Configurations.Add(new JobFairRegistrationMap());
            modelBuilder.Configurations.Add(new JobFairRegistrationJobMap());
            modelBuilder.Configurations.Add(new JobFairRegistrationRepMap());
            modelBuilder.Configurations.Add(new JobFairSkillMap());
            modelBuilder.Configurations.Add(new JobFairStatuMap());
            modelBuilder.Configurations.Add(new JobPostingMap());
            modelBuilder.Configurations.Add(new JobPostingCandidateMap());
            modelBuilder.Configurations.Add(new JobPostingCandidateStatuMap());
            modelBuilder.Configurations.Add(new JobPostingJobCategoryMap());
            modelBuilder.Configurations.Add(new JobPostingStatuMap());
            modelBuilder.Configurations.Add(new JobSeekerOptionMap());
            modelBuilder.Configurations.Add(new JobSeekerSurveyQuestionMap());
            modelBuilder.Configurations.Add(new JobSeekerSurveyResultMap());
            modelBuilder.Configurations.Add(new JobWrapStatMap());
            modelBuilder.Configurations.Add(new LanguageProficiencyMap());
            modelBuilder.Configurations.Add(new LanguageReqMap());
            modelBuilder.Configurations.Add(new LanguageMap());
            modelBuilder.Configurations.Add(new LeadMap());
            modelBuilder.Configurations.Add(new Leads_BackupMap());
            modelBuilder.Configurations.Add(new LineItemDiscountMap());
            modelBuilder.Configurations.Add(new MedalMap());
            modelBuilder.Configurations.Add(new MilitaryExperienceMap());
            modelBuilder.Configurations.Add(new MonsterMailMap());
            modelBuilder.Configurations.Add(new MonsterMailJobURLMap());
            modelBuilder.Configurations.Add(new MonsterMailMRMap());
            modelBuilder.Configurations.Add(new MonsterMailUserMap());
            modelBuilder.Configurations.Add(new MVE_SurveyMap());
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new OrderDetailMap());
            modelBuilder.Configurations.Add(new OrderDetailJobPostingMap());
            modelBuilder.Configurations.Add(new OrderStatuMap());
            modelBuilder.Configurations.Add(new PayMap());
            modelBuilder.Configurations.Add(new PermissionMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new ProductDiscountMap());
            modelBuilder.Configurations.Add(new ProductQtyMap());
            modelBuilder.Configurations.Add(new ProductTypeMaxDiscountMap());
            modelBuilder.Configurations.Add(new QualityRatingMap());
            modelBuilder.Configurations.Add(new RankMap());
            modelBuilder.Configurations.Add(new ReasonMap());
            modelBuilder.Configurations.Add(new RegionMap());
            modelBuilder.Configurations.Add(new ResumeViewLogMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new SchoolMap());
            modelBuilder.Configurations.Add(new SecureResourceMap());
            modelBuilder.Configurations.Add(new SecureResourceTypeMap());
            modelBuilder.Configurations.Add(new ServiceMap());
            modelBuilder.Configurations.Add(new ShiftWorkMap());
            modelBuilder.Configurations.Add(new SkillMap());
            modelBuilder.Configurations.Add(new SpecialtyAreaMap());
            modelBuilder.Configurations.Add(new StateMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new tempcountMap());
            modelBuilder.Configurations.Add(new Top40SurveyMap());
            modelBuilder.Configurations.Add(new TravelMap());
            modelBuilder.Configurations.Add(new TravelPctMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new wcmgaMap());
            modelBuilder.Configurations.Add(new YearsOfServiceMap());
            modelBuilder.Configurations.Add(new YesNoMap());
            modelBuilder.Configurations.Add(new OrderPostingViewMap());
            modelBuilder.Configurations.Add(new OrderResumeViewMap());
            //modelBuilder.Configurations.Add(new Order1Map());
            modelBuilder.Configurations.Add(new SecurityAccessViewMap());
            modelBuilder.Configurations.Add(new StateViewMap());
            modelBuilder.Configurations.Add(new UserListViewMap());
            modelBuilder.Configurations.Add(new User1Map());
            modelBuilder.Configurations.Add(new viewABALMap());
            modelBuilder.Configurations.Add(new viewCandidateJobCategoryMap());
            modelBuilder.Configurations.Add(new viewCandidateMap());
            modelBuilder.Configurations.Add(new viewDirectHireMap());
            modelBuilder.Configurations.Add(new viewJobCategoryMap());
            modelBuilder.Configurations.Add(new JobApplicationMap());
            modelBuilder.Configurations.Add(new UserDegreeMap());
            modelBuilder.Configurations.Add(new ProductDescriptionMap());
            modelBuilder.Configurations.Add(new PreOrdersMap());
            modelBuilder.Configurations.Add(new ReferralNoteMap());
            modelBuilder.Configurations.Add(new JobFairAppliersMap());
            modelBuilder.Configurations.Add(new SavedJobsMap());
            modelBuilder.Configurations.Add(new AwardsMap());
            modelBuilder.Configurations.Add(new SavedResumesMap());
        }

    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NonUnicode : Attribute
    {
    }
}