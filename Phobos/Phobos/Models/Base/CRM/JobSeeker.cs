﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Models.Base.CRM
{
    public class JobSeeker : BaseUser
    {
        [StringLength(256), DataType(DataType.PhoneNumber),  DisplayName("OCONUS Phone")]
        public string OCONUSphone { get; set; }
    }
}