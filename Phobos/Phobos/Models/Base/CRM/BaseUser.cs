﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Models.Base.CRM
{
    public class BaseUser
    {
        public Guid UserId { get; set; }

        [Required, StringLength(32), DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(32), DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        public string Address { get; set; }

        public string Address2 { get; set; }

        [Required, StringLength(32)]
        public string City { get; set; }

        [Required, StringLength(2), RegularExpression("[A-Za-z]{2}", ErrorMessage = "2 letter state abbreviations only")]
        public string State { get; set; }

        [Required, StringLength(32), RegularExpression(@"^\d{5}(-\d{4})?$", ErrorMessage = "Please enter a valid zip")]
        public string Zip { get; set; }

        [StringLength(32)]
        public string Country { get; set; }

        [Required, StringLength(32), DataType(DataType.PhoneNumber), DisplayName("Primary Phone")]
        public string PrimaryPhone { get; set; }

        [StringLength(32), DataType(DataType.PhoneNumber), DisplayName("Alternate Phone")]
        public string AlternatePhone { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Re-type E-mail")]
        [Compare("Email", ErrorMessage = "The Email fields do not match.")]
        public string EmailAgain { get; set; }

        [Required]
        [StringLength(256), DisplayName("Password")]
        public string Password { get; set; }

        [Required]
        [StringLength(256), DisplayName("Re-type Password")]
        [Compare("Password", ErrorMessage = "The Password fields do not match.")]
        public string PasswordAgain { get; set; }

        [Required, DisplayName("How did you hear about us")]
        public string HowHeardAbout { get; set; }
    }
}