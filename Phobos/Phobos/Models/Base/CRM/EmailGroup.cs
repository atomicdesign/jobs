﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using System.Runtime.Serialization.Json;

namespace CivilianJobs.Models
{
    public class EmailGroup
    {
        public int EmailGroupID { get; set; }

        [Required]
        [StringLength(55)]
        public string Name { get; set; }

        public string Description { get; set; }

        protected virtual ICollection<EmailContact> _Contacts { get; set; }
        public virtual ICollection<EmailContact> Contacts { get { return _Contacts ?? (_Contacts = new List<EmailContact>()); } set { _Contacts = value; } }

        public string JSON
        {
            get
            {
                DataContractJsonSerializer jsonout = new DataContractJsonSerializer(typeof(object));
                ArrayList li = new ArrayList();

                foreach (EmailContact contact in Contacts)
                {
                    li.Add(new { email = contact.EmailAddress, name = contact.EmailName });
                }

                string retval = String.Empty;

                using (MemoryStream ms = new MemoryStream())
                {
                    jsonout.WriteObject(ms, li);
                    retval = ms.ToString();
                }

                return retval;
            }
        }
    }

    public class EmailContact
    {
        public int EmailContactID { get; set; }

        [Required]
        [StringLength(255)]
        public string EmailAddress { get; set; }
        [StringLength(155)]
        public string EmailName { get; set; }

    }
}