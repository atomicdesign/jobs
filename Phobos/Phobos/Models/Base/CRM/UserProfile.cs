﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Helpers;

namespace CivilianJobs.Models
{
    public partial class UserProfile
    {
        public bool? IsActivated { get; set; }
        public string ActivationCode { get; set; }
        public int UserProfileID { get; set; }

        /*** PERSONAL INFO ***/
        public DateTime Created { get; set; }

        [StringLength(100)]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [StringLength(100)]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        public string FullName { get { return (FirstName ?? "") + " " + (LastName ?? ""); } }

        /*** ADDRESS ***/

        public string Address { get; set; }

        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(2)]
        [RegularExpression("[A-Za-z]{2}", ErrorMessage="2 letter state abbreviations only")]
        public string State { get; set; }

        [StringLength(10)]
        [RegularExpression("[0-9-]*", ErrorMessage="Numbers and dashes only")]
        public string Zip { get; set; }

        public string Country { get; set; }

        /*** CONTACT INFO ***/
        [Required, StringLength(32), DataType(DataType.PhoneNumber), DisplayName("Primary Phone")]
        public string PrimaryPhone { get; set; }

        [StringLength(32), DataType(DataType.PhoneNumber), DisplayName("Alternate Phone")]
        public string AlternatePhone { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /*** BEHIND THE SCENES ***/
        
        [Required]
        public Guid UserId { get; set; }

        [NotMapped]
        [HiddenInput]
        protected MembershipUser _User { get; set; }
        [NotMapped]
        [HiddenInput]
        public MembershipUser User
        {
            get
            {
                if (_User == null && UserId != null)
                {
                    _User = Membership.GetUser(UserId);
                }
                return _User;
            }
        }

        public string UserName
        {
            get
            {
                if (User != null) return User.UserName;
                return null;
            }
            set
            {
                if (value != null)
                {
                    _User = Membership.GetUser(value);
                    if(_User != null) UserId = (Guid)_User.ProviderUserKey;
                }
            }
        }

        /*** MISC ***/
        [DefaultValue(0)]
        public int Hax { get; set; }

        [Required]
        public string HowHeardAbout { get; set; }

        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        [DisplayName("Company Website")]
        public string CompanyWebsite { get; set; }

        public string WhichAccountExecutive { get; set; }

        public string OCONUSphone { get; set; }

        public Nullable<bool> RegCompleted { get; set; }
    }
}