﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models.Jobs.Enums;

namespace CivilianJobs.Models.Base.CRM
{
    public class Employer : BaseUser
    {
        [Required, DisplayName("Company Name")]
        public string CompanyName { get; set; }

        [DisplayName("Company Website")]
        public string CompanyWebsite { get; set; }

        [DisplayName("Which Account Executive are you working with?")]
        public string WhichAccountExecutive { get; set; }
    }
}