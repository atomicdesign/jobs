﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using dotless.Core;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using dotless.Core.configuration;

namespace CivilianJobs.Models
{

    public class DotLessTheme
    {
        public int DotLessThemeID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Author { get; set; }

        //add json object
    }

    public class DotLessLogger : dotless.Core.Loggers.ILogger
    {
        public void Debug(string message) { }
        public void Debug(string message, params object[] args) { }
        public void Error(string message) { }
        public void Error(string message, params object[] args) { }
        public void Info(string message) { }
        public void Info(string message, params object[] args) { }
        public void Log(dotless.Core.Loggers.LogLevel level, string message) { }
        public void Warn(string message) { }
        public void Warn(string message, params object[] args) { }
    }

    public class DotLessStylesheet
    {
        public static string StylesheetPath = "/Content/css/";
        public static string LessPath = "/Content/css/less/";

        public int DotLessStylesheetID { get; set; }

        [Required]
        [ReadOnly(true)]
        public string StylesheetURL { get; set; }

        public string FullStylesheetURL { get { return StylesheetPath + StylesheetURL; } }

        [Required]
        [ReadOnly(true)]
        public string LessUrl { get; set; }

        public string FullLessUrl { get { return LessPath + LessUrl; } }

        protected virtual ICollection<DotLessVariable> _Variables { get; set; }
        public virtual ICollection<DotLessVariable> Variables { get { return _Variables ?? (_Variables = new List<DotLessVariable>()); } set { _Variables = value; } }

        [NotMapped]
        public string Errors { get; private set; }
        public bool CompileStylesheet()
        {
            try
            {
                //make sure all the directories are there
                string inputFilename = HttpContext.Current.Server.MapPath(LessPath + LessUrl);
                string outputFilename = HttpContext.Current.Server.MapPath(StylesheetPath + StylesheetURL);
                Directory.CreateDirectory(outputFilename);

                //validate input file
                FileInfo input = new FileInfo(inputFilename);
                if (!input.Exists) throw new Exception("Invalid input file");

                FileInfo output = new FileInfo(outputFilename);

                //validate and/or create output file            
                //collapse variables
                StringBuilder sb = new StringBuilder();
                foreach (DotLessVariable var in Variables) sb.AppendLine(String.Format(@"@{0}: {1};", var.Name, var.Value));
                //append stylesheet
                using (FileStream fs = input.OpenRead()){
                using (StreamReader sr = new StreamReader(fs))
                {
                    sb.Append(sr.ReadToEnd());
                }
                }

                //write to file
                string outCss = Less.Parse(sb.ToString());
                if (outCss == String.Empty) throw new Exception("Output blank");
                if (!output.Exists) output.Create();
                using (FileStream fs = output.OpenWrite())
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(outCss);
                    }
                }

                return true;
                /*
                var config = new dotless.Core.configuration.DotlessConfiguration
                {
                    LogLevel = dotless.Core.Loggers.LogLevel.Error,
                    // Put your custom logger implementation here
                    Logger = typeof(dotless.Core.Loggers.AspResponseLogger)
                };

                var lessEngine = new EngineFactory(config).GetEngine();
                var outputCss = lessEngine.TransformToCss(sb.ToString(), outputFilename);
                 * */
                //return lessEngine.LastTransformationSuccessful;
            }
            catch (Exception err)
            {
                //Helpers.LogHelper.Log(err);
                Errors = err.Message;
            }
            return false;
        }

        public bool GetVariables()
        {
            try
            {
                //validate and open input file
                string inputFilename = HttpContext.Current.Server.MapPath(LessPath + LessUrl);
                FileInfo input = new FileInfo(inputFilename);
                if (!input.Exists) { Errors = "Invalid input file"; return false; }

                //look for variables
                string lessFile = "";
                using (StreamReader sr = new StreamReader(input.OpenRead()))
                {
                    lessFile = sr.ReadToEnd();
                }
                if (lessFile.Trim() == String.Empty) { Errors = "Empty input file"; return false; }

                MatchCollection matches = Regex.Matches(lessFile, @"@([a-zA-Z][a-zA-Z0-9]*)\s*:\s*(.*);");
                string tempName, tempValue;
                foreach (Match m in matches)
                {
                    try
                    {
                        if (m.Groups.Count == 3)
                        {
                            tempName = m.Groups[1].Value;
                            tempValue = m.Groups[2].Value;
                            if (!Variables.Any(e => e.Name == tempName)) Variables.Add(new DotLessVariable { Name = tempName, Value = tempValue });
                        }
                    }
                    catch (Exception err) { Helpers.LogHelper.Log(err.ToString()); }
                }

                return true;
                //put them in variables if they don't exist
            }
            catch (Exception err)
            {
                Helpers.LogHelper.Log(err);
                Errors = err.Message;
            }
            return false;
        }

        public bool ConsumeTheme(string JSON)
        {
            //for later :)

            return false;
        }
    }

    public class DotLessVariable
    {
        public int DotLessVariableID { get; set; }

        [ForeignKey("DotLessStylesheet")]
        public int DotLessStylesheetID { get; set; }
        [ScriptIgnore]
        public DotLessStylesheet DotLessStylesheet { get; set; }

        [Required]
        [StringLength(55)]
        [RegularExpression("[a-zA-Z][a-zA-Z0-9]*", ErrorMessage="Invalid Variable Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string Value { get; set; }
    }
}