﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace CivilianJobs.Models
{
    public enum FormFieldType
    {
        INPUT,
        SELECT,
        CHECKBOX,
        RADIO,
        TEXTAREA,
        COUNTRY_LIST,
        STATE_LIST,
        CONTENT_BLOCK,
        HONEYPOT
    }

    public class FormField
    {
        public int FormFieldID { get; set; }

        [Required]
        [RegularExpression("[a-zA-Z0-9]+", ErrorMessage = "Letters and Numbers only")]
        public string Name { get; set; }

        [AllowHtml]
        public string Label { get; set; }

        public string ToolTip { get; set; }

        public string TypeVal { get; set; }

        public FormFieldType TypeEnum
        {
            get
            {
                FormFieldType retval;
                Enum.TryParse<FormFieldType>(TypeVal, out retval);
                return retval;
            }
            set
            {
                TypeVal = value.ToString();
            }
        }
        [DefaultValue(false)]
        public bool Required { get; set; }

        [Required]
        [Range(0,100)]
        public int Order { get; set; }

        [ForeignKey("Form")]
        public int FormID { get; set; }
        [ScriptIgnore]
        public Form Form { get; set; }

        protected ICollection<FormFieldOption> _Options { get; set; }
        public ICollection<FormFieldOption> Options { get { return (_Options ?? (_Options = new List<FormFieldOption>())); } set { _Options = value; } }

        public bool IsValid(string input)
        {
            //Do Checky Stuff Here
            return true;
        }
    }
}