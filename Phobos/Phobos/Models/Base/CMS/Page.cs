﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using CivilianJobs.Helpers;
using CivilianJobs.Models;


namespace CivilianJobs.Models
{
    public class Page
    {
        public static List<SearchResult> Search(string query)
        {
            using (CivilianJobsContext db = new CivilianJobsContext())
            {
                List<Page> items = db.Pages.ToList();

                var results =
                    from item in items
                    let content = SearchResult.FormatContent(new string[]{item.Heading, item.Content})
                    let matches = SearchResult.QueryRegex(query).Matches(content)
                    where matches.Count > 0
                    select new SearchResult()
                    {
                        Excerpt = SearchResult.CreateExcerpt(content.Trim(), query),
                        Title = item.Title,
                        URL = "/page/" + item.SEOURL
                    };

                return results.ToList();
            }
        }


        [Key]
        public int PageID { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("URL Path")]
        [RegularExpression("[a-z][a-z0-9-]*", ErrorMessage = "The Path must begin with a letter and contain only lowercase letters, numbers, or dashes")]
        public string SEOURL { get; set; }

        public string CachedFullSEOURL { get; set; }
        public string FullSEOURL
        {
            get
            {
                if (Parent != null) return Parent.FullSEOURL + "/" + SEOURL;
                return SEOURL;
            }
        }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [Required]
        [MaxLength(100)]
        public string Heading { get; set; }

        [MaxLength(255)]
        [DisplayName("SEO Description")]
        public string MetaDescription { get; set; }

        [MaxLength(255)]
        [DisplayName("SEO Keywords")]
        public string MetaKeywords { get; set; }

        [Required]
        [AllowHtml]
        [DisplayName("HTML")]
        public string Content { get; set; }

        public IHtmlString Output(HtmlHelper htmlHelper)
        {
            return MvcHtmlString.Create(Regex.Replace(Content, @"\{\{(.+)\}\}", delegate(Match match)
            {
                if (match.Groups.Count > 0)
                {
                    Match functionCall = Regex.Match(match.Groups[1].Value, @"(.+)\((.*)\)");
                    if (functionCall.Groups.Count >= 2)
                    {
                        string functionName = functionCall.Groups[1].Value.Trim();
                        string functionVariables = functionCall.Groups[2].Value.Trim();

                        List<object> vararray = new List<object>();

                        if (functionVariables != String.Empty)
                        {
                            string[] variables = functionVariables.Split(',');
                            foreach (string variableUntrimmed in variables)
                            {
                                vararray.Add(variableUntrimmed.Trim().Replace("\"", ""));
                            }
                        }

                        return htmlHelper.RenderModule(functionName, vararray.ToArray()).ToString();
                    }
                }
                return String.Empty;
            }));
        }

        [ForeignKey("Parent")]
        public int? ParentID { get; set; }
        public virtual Page Parent { get; set; }

        [ForeignKey("Template")]
        public int TemplateID { get; set; }
        public virtual Template Template { get; set; }

        protected virtual ICollection<Page> _SubPages { get; set; }
        public virtual ICollection<Page> SubPages { get { return (_SubPages ?? (_SubPages = new List<Page>())); } set { _SubPages = value; } }

        [ForeignKey("SecondaryNav")]
        [DisplayName("Secondary Navigation")]
        public int? SecondaryNavID { get; set; }
        public virtual NavBar SecondaryNav { get; set; }

        [ForeignKey("PrimaryNav")]
        [DisplayName("Primary Navigation")]
        public int? PrimaryNavID { get; set; }
        public virtual NavBar PrimaryNav { get; set; }

        public void DeleteSafe(CivilianJobsContext db)
        {
            if (_SubPages != null)
            {
                var tempSubPages = _SubPages.ToList();
                foreach (Page p in tempSubPages)
                {
                    p.DeleteSafe(db);
                }
            }
            ParentID = null;
            db.Pages.Remove(this);
        }

    }

    [NotMapped]
    public class PageDisplayModel
    {
        public string Title { get; set; }

        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        [AllowHtml]
        public string Content { get; set; }

        public Template Template { get; set; }
        public NavBar SecondaryNav { get; set; }
        public NavBar PrimaryNav { get; set; }

        protected Stack<Breadcrumb> _bc { get; set; }
        public Stack<Breadcrumb> bc { get { return (_bc ?? (_bc = new Stack<Breadcrumb>())); } set { _bc = value; } }

        public string ActiveLink { get; set; }
    }
}