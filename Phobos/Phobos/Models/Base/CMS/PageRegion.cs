﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Script.Serialization;


namespace CivilianJobs.Models
{
    public class PageRegion
    {
        public int PageRegionID { get; set; }

        [ForeignKey("TemplateRegion")]
        public int TemplateRegionID { get; set; }
        public virtual TemplateRegion TemplateRegion { get; set; }

        [AllowHtml]
        public string HTML { get; set; }

        [ForeignKey("Page")]
        public int PageID { get; set; }
        [ScriptIgnore]
        public Page Page { get; set; }

        public PageRegionEditModel EditModel
        {
            get
            {
                return new PageRegionEditModel { HTML = HTML, Name = TemplateRegion.Name, PageRegionID = PageRegionID };
            }
        }
    }

    [NotMapped]
    public class PageRegionEditModel
    {
        public int PageRegionID { get; set; }

        public string Name { get; set; }
        
        [AllowHtml]
        public string HTML { get; set; }
    }
}