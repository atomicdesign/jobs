﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace CivilianJobs.Models
{
 
    public class NavBar
    {
        public NavBar EmployerLoggedIn()
        {
            this.Items = this.Items.Where(x => x.NavBarItemID != 26).ToList();
            return this;
        }
        public int NavBarID { get; set; }

        [Required]
        public string Name { get; set; }
        
        public string NameStripped { get { return Regex.Replace(Name, @"[^a-zA-Z0-9]", @"_"); } }

        [AllowHtml]
        public string Brand { get; set; }

        protected virtual ICollection<NavBarItem> _Items { get; set; }
        public virtual ICollection<NavBarItem> Items { get { return (_Items ?? (_Items = new List<NavBarItem>())); } set { _Items = value; } }

    }

   

    // PRIMARY //
    public class NavBarItem
    {
        public int NavBarItemID { get; set; }

        [Required]
        [AllowHtml]
        public string Text { get; set; }

        [Required]
        public string URL { get; set; }

        public int Offset { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool OpenInNew { get; set; }

        [ForeignKey("NavBar")]
        public int NavBarID { get; set; }
        [ScriptIgnore]
        public NavBar NavBar { get; set; }

        [Required]
        [Range(0, 100)]
        public int Order { get; set; }

        public string Color { get; set; }

        protected virtual ICollection<NavBarSubItem> _Items { get; set; }
        public virtual ICollection<NavBarSubItem> Items { get { return (_Items ?? (_Items = new List<NavBarSubItem>())); } set { _Items = value; } }

    }

    // SECONDARY //
    public class NavBarSubItem
    {
        public int NavBarSubItemID { get; set; }

        [Required]
        [AllowHtml]
        public string Text { get; set; }

        [Required]
        public string URL { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool OpenInNew { get; set; }

        [Required]
        [Range(0, 100)]
        public int Order { get; set; }

        [ForeignKey("Parent")]
        public int ParentID { get; set; }
        [ScriptIgnore]
        public NavBarItem Parent { get; set; }

        protected virtual ICollection<NavBarSubSubItem> _Items { get; set; }
        public virtual ICollection<NavBarSubSubItem> Items { get { return (_Items ?? (_Items = new List<NavBarSubSubItem>())); } set { _Items = value; } }

    }

    // TERTIARY //
    public class NavBarSubSubItem
    {
        public int NavBarSubSubItemID { get; set; }

        [Required]
        [AllowHtml]
        public string Text { get; set; }

        [Required]
        public string URL { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool OpenInNew { get; set; }

        [Required]
        [Range(0, 100)]
        public int Order { get; set; }

        [ForeignKey("Parent")]
        public int ParentID { get; set; }
        [ScriptIgnore]
        public NavBarSubItem Parent { get; set; }

    }
}