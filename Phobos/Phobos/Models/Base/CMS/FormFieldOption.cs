﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace CivilianJobs.Models
{
    public class FormFieldOption
    {
        [Key]
        public int FormFieldOptionID { get; set; }

        [ForeignKey("FormField")]
        public int FormFieldID { get; set; }
        [ScriptIgnore]
        public FormField FormField { get; set; }

        [Required]
        [Range(0,100)]
        public int Order { get; set; }

        [Required]
        public string Text { get; set; }
        [Required]
        public string Value { get; set; }

    }
}