﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using System.Web.Script.Serialization;

namespace CivilianJobs.Models
{
    public class TemplateRegion
    {
        public int TemplateRegionID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [ForeignKey("Template")]
        public int TemplateID { get; set; }
        [ScriptIgnore]
        public Template Template { get; set; }
    }
}