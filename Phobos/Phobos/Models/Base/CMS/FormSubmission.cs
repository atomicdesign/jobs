﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;


namespace CivilianJobs.Models
{
    public class FormSubmission
    {
        public int FormSubmissionID { get; set; }
        
        [ForeignKey("Form")]
        public int FormID { get; set; }
        [ScriptIgnore]
        public Form Form { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }
        public string TimestampReadable { get { return (Timestamp == null ? "" : Timestamp.ToString()); } }

        [Required]
        public string IP { get; set; }

        public string XML { get; set; } //rename at some point
    }

    [NotMapped]
    public class FormSubmissionHolder
    {

    }
}