﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;

namespace CivilianJobs.Models
{
    public class Form
    {
        public static List<SearchResult> Search(string query)
        {
            using (CivilianJobsContext db = new CivilianJobsContext())
            {

                List<Form> items = db.Forms.ToList();

                var results =
                    from item in items
                    let content = SearchResult.FormatContent(new string[] { item.Header, item.Content })
                    let matches = SearchResult.QueryRegex(query).Matches(content)
                    where matches.Count > 0
                    select new SearchResult()
                    {
                        Excerpt = SearchResult.CreateExcerpt(content.Trim(), query),
                        Title = item.Title,
                        URL = "/form/" + item.SEOURL
                    };

                return results.ToList();
            }
        }


        [Key]
        public int FormID { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("URL Path")]
        [RegularExpression("[a-z][a-z0-9-]+", ErrorMessage="Must begin with a letter and contain only lowercase letters, numbers, or dashes")]
        public string SEOURL { get; set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(255)]
        [DisplayName("SEO Description")]
        public string MetaDescription { get; set; }

        [MaxLength(255)]
        [DisplayName("SEO Keywords")]
        public string MetaKeywords { get; set; }

        public string EmailContact { get; set; }

        [Required]
        [AllowHtml]
        public string Content { get; set; }

        [AllowHtml]
        public string Header { get; set; }

        [Required]
        [AllowHtml]
        public string Confirmation { get; set; }

        protected virtual ICollection<FormField> _Fields { get; set; }
        public virtual ICollection<FormField> Fields { get { return (_Fields ?? (_Fields = new List<FormField>())); } set { _Fields = value; } }
    }
}