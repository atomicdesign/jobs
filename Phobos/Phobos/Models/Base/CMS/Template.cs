﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CivilianJobs.Models
{
    public class Template
    {
        public int TemplateID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string FileName { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool UserSelectable { get; set; }

        public string FullPath { get { return "~/Views/Templates/" + FileName + ".cshtml"; } }

        protected virtual ICollection<TemplateRegion> _Regions { get; set; }
        public virtual ICollection<TemplateRegion> Regions { get { return (_Regions ?? (_Regions = new List<TemplateRegion>())); } set { _Regions = value; } }
    }
}