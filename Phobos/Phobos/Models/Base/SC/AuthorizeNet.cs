﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Helpers;
using CivilianJobs.Helpers.AuthorizeNet;

namespace CivilianJobs.Models
{
    public class AuthorizeNetSilentPost
    {
        public int AuthorizeNetSilentPostID { get; set; }

        [Required]
        public DateTime Received { get; set; }
        [Required]
        public bool Processed { get; set; }

        public int? PaymentID { get; set; }
        public int? SubscriptionID { get; set; }

        public AuthorizeNetSilentPost()
        {
            Received = DateTime.Now;
            Processed = false;
        }


        public int x_response_code { get; set; }
        public int x_response_subcode { get; set; }
        public int x_response_reason_code { get; set; }
        [StringLength(255)]
        public string x_response_reason_text { get; set; }

        [StringLength(10)]
        public string x_auth_code { get; set; }

        public char x_avs_code { get; set; }

        [StringLength(255)]
        public string x_trans_id { get; set; }

        [StringLength(20)]
        public string x_invoice_num { get; set; }
        [StringLength(255)]
        public string x_description { get; set; }

        public decimal x_amount { get; set; }
        [StringLength(10)]
        public string x_method { get; set; }
        [StringLength(20)]
        public string x_type { get; set; }

        [StringLength(20)]
        public string x_cust_id { get; set; }
        [StringLength(50)]
        public string x_first_name { get; set; }
        [StringLength(50)]
        public string x_last_name { get; set; }
        [StringLength(50)]
        public string x_company { get; set; }
        [StringLength(60)]
        public string x_address { get; set; }
        [StringLength(40)]
        public string x_city { get; set; }
        [StringLength(40)]
        public string x_state { get; set; }
        [StringLength(20)]
        public string x_zip { get; set; }
        [StringLength(60)]
        public string x_country { get; set; }
        [StringLength(25)]
        public string x_phone { get; set; }
        [StringLength(25)]
        public string x_fax { get; set; }
        [StringLength(255)]
        public string x_email { get; set; }


        [StringLength(20)]
        public string x_ship_to_first_name { get; set; }
        [StringLength(20)]
        public string x_ship_to_last_name { get; set; }
        [StringLength(20)]
        public string x_ship_to_company { get; set; }
        [StringLength(20)]
        public string x_ship_to_address { get; set; }
        [StringLength(20)]
        public string x_ship_to_city { get; set; }
        [StringLength(20)]
        public string x_ship_to_state { get; set; }
        [StringLength(20)]
        public string x_ship_to_zip { get; set; }
        [StringLength(20)]
        public string x_ship_to_country { get; set; }
        [StringLength(20)]


        public decimal x_tax { get; set; }
        public decimal x_duty { get; set; }
        public decimal x_freight { get; set; }
        [StringLength(20)]
        public string x_tax_exempt { get; set; }
        [StringLength(25)]
        public string x_po_num { get; set; }

        [StringLength(40)]
        public string x_MD5_Hash { get; set; }

        public char x_cavv_response { get; set; }

        [StringLength(20)]
        public string x_test_request { get; set; }

        [StringLength(20)]
        public string x_subscription_id { get; set; }
        public int x_subscription_paynum { get; set; }
        [StringLength(20)]
        public string x_cim_profile_id { get; set; }

        public void ProcessPost()
        {
            using (CivilianJobsContext db = new CivilianJobsContext())
            {
                try
                {
                    if ((x_subscription_id ?? String.Empty) != String.Empty)
                    {
                        Subscription s = db.Subscriptions.FirstOrDefault(e => e.GatewaySubscriptionID == x_subscription_id);
                        if (s != null)
                        {
                            Payment p = new Payment
                            {
                                PaymentMethodType = x_method,
                                PaymentAmount = x_amount,
                                GatewayMessage = x_response_reason_text,
                                GatewayStatus = ((AuthorizeNetAimResponseCode)x_response_code).ToString(),
                                Processed = DateTime.Now,
                                Created = DateTime.Now,
                                Sent = DateTime.Now,
                                BillingFirstName = x_first_name,
                                BillingLastName = x_last_name,
                                BillingAddress = x_address,
                                BillingCity = x_city,
                                BillingState = x_state,
                                BillingZip = x_zip,
                                BillingCountry = x_country,
                                GatewayAuthorizationCode = x_response_code.ToString(),
                                GatewayTransactionId = x_trans_id,
                                CustomerId = x_cust_id,
                                TransactionSent = true,
                                TransactionProcessed = true,
                                TransactionSuccessful = (x_response_code == (int)AuthorizeNetAimResponseCode.APPROVED),
                                SubscriptionID = s.SubscriptionID
                            };
                            db.Payments.Add(p);
                            db.SaveChanges();

                            s.GatewaySubscriptionStatus = (p.TransactionSuccessful ? "ACTIVE" : "DEFAULT");
                            s.GatewaySubscriptionPaymentNumber = (short)x_subscription_paynum;
                            db.Entry(s).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        Payment p = db.Payments.FirstOrDefault(e => e.GatewayTransactionId == x_trans_id);
                        if (p != null)
                        {
                            p.GatewayMessage = x_response_reason_text;
                            p.GatewayStatus = ((AuthorizeNetAimResponseCode)x_response_code).ToString();
                            p.Processed = DateTime.Now;
                            p.TransactionProcessed = true;
                            p.GatewayAuthorizationCode = x_response_code.ToString();
                            p.TransactionSuccessful = (x_response_code == (int)AuthorizeNetAimResponseCode.APPROVED);

                            db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception err)
                {
                    LogHelper.Log(err);
                }
            }
        }

    }

}