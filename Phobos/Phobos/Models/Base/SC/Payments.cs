﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Helpers;

namespace CivilianJobs.Models
{
    public partial class Payment
    {
        [Key]
        public int PaymentID { get; set; }

        public Payment()
        {
            Created = DateTime.Now;
            TransactionProcessed = false;
            TransactionSuccessful = false;
        }

        [Required]
        public DateTime Created { get; set; }
        public DateTime Sent { get; set; }
        public DateTime Processed { get; set; }

        //Has been sent to gateway
        public bool TransactionSent { get; set; }
        //Has been processed by gateway (immediate or callback)
        public bool TransactionProcessed { get; set; }
        //Was successful
        public bool TransactionSuccessful { get; set; }

        //Human Readable Status
        public string TransactionStatusText
        {
            get
            {
                if (!TransactionSent) return "Unsent";
                if (!TransactionProcessed) return "Pending";
                if (!TransactionSuccessful) return "Failed";
                return "Success";
            }
        }

        //Info we get back from gateway

        [StringLength(55)]
        public string GatewayStatus { get; set; }
        [StringLength(255)]
        public string GatewayMessage { get; set; }
        [StringLength(255)]
        public string GatewayTransactionId { get; set; }
        [StringLength(255)]
        public string GatewayAuthorizationCode { get; set; }
        
        [Required]
        [StringLength(20)]
        public string PaymentMethodType { get; set; }
        [Required]
        [Range(0, 999999)]
        public decimal PaymentAmount { get; set; }

        [StringLength(20)]
        public string CustomerId { get; set; }

        [ForeignKey("Subscription")]
        public int? SubscriptionID { get; set; }
        public Subscription Subscription { get; set; }

        [StringLength(50)]
        public string BillingFirstName { get; set; }
        [StringLength(50)]
        public string BillingLastName { get; set; }
        [StringLength(60)]
        public string BillingAddress { get; set; }
        [StringLength(40)]
        public string BillingCity { get; set; }
        [StringLength(2)]
        public string BillingState { get; set; }
        [StringLength(20)]
        public string BillingZip { get; set; }
        [StringLength(60)]
        public string BillingCountry { get; set; }

        //CREDIT CARD
        [StringLength(5)]
        public string CreditCardType { get; set; }
        [StringLength(4)]
        public string CreditCardLast4 { get; set; }
        [StringLength(7)]
        public string CreditCardExpiration { get; set; }

        //AUTHORIZE.NET

        //PAYPAL

        //ECHECK

        //PO

        //???

    }

    public class Subscription
    {
        public int SubscriptionID { get; set; }

        [StringLength(20)]
        public string CustomerId { get; set; }
        [StringLength(20)]
        public string SubscriptionInvoiceNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string SubscriptionName { get; set; }
        [StringLength(255)]
        public string SubscriptionDescription { get; set; }

        [StringLength(13)]
        public string GatewaySubscriptionID { get; set; }
        [StringLength(50)]
        public string GatewaySubscriptionStatus { get; set; }
        public short GatewaySubscriptionPaymentNumber { get; set; }

        [StringLength(4)]
        public string CreditCardLast4 { get; set; }
        [StringLength(7)]
        public string ExpirationDate { get; set; }

        [StringLength(50)]
        public string BillingFirstName { get; set; }
        [StringLength(50)]
        public string BillingLastName { get; set; }
        [StringLength(60)]
        public string BillingAddress { get; set; }
        [StringLength(40)]
        public string BillingCity { get; set; }
        [StringLength(2)]
        public string BillingState { get; set; }
        [StringLength(20)]
        public string BillingZip { get; set; }
        [StringLength(60)]
        public string BillingCountry { get; set; }

        [Required]
        [Range(0, 999999)]
        public decimal SubscriptionAmount { get; set; }

        [Required]
        public DateTime SubscriptionStartDate { get; set; }

        //Number of months between payments
        [Required]
        [Range(1, 12)]
        public short SubscriptionMonthInterval { get; set; }

        // 9999 for infinite
        [Required]
        [Range(1, 9999)]
        public short SubscriptionNumOccurrences { get; set; }

        [Required]
        [Range(0, 99)]
        public short SubscriptionTrialOccurrences { get; set; }

        [Required]
        [Range(0, 999999)]
        public decimal SubscriptionTrialAmount { get; set; }

    }

}