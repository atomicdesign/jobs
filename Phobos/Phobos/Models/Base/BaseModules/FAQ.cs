﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace CivilianJobs.Models
{

    public class FAQCategory
    {
        public int FAQCategoryID { get; set; }

        [Required]
        [DefaultValue(0)]
        public int Order { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        protected virtual ICollection<FAQ> _FAQS { get; set; }
        public virtual ICollection<FAQ> FAQS { get { return _FAQS ?? (_FAQS = new List<FAQ>()); } set { _FAQS = value; } }
    }

    public class FAQ
    {
        public int FAQID { get; set; }

        [Required]
        [DefaultValue(0)]
        public int Order { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }
        [ScriptIgnore]
        public virtual FAQCategory Category { get; set; }

        [Required]
        [StringLength(255)]
        public string Question { get; set; }

        [Required]
        [AllowHtml]
        public string Answer { get; set; }

    }
}