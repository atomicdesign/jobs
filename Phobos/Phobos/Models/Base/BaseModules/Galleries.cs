﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using CivilianJobs.Modules;
using CivilianJobs.Helpers;
using System.Web.Script.Serialization;
using System.IO;
using System.Data;


namespace CivilianJobs.Models
{
    public class Gallery
    {
        public int GalleryID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        [StringLength(55)]
        [RegularExpression(@"[a-zA-Z0-9-]+", ErrorMessage="Letters and numbers only, no spaces")]
        public string SEOURL { get; set; }

        protected virtual ICollection<GalleryItem> _GalleryItems { get; set; }
        public virtual ICollection<GalleryItem> GalleryItems { get { return _GalleryItems ?? (_GalleryItems = new List<GalleryItem>()); } set { _GalleryItems = value; } }

        public void DeleteSafe(CivilianJobsContext db)
        {
            if (_GalleryItems != null)
            {
                var tempGalItems = _GalleryItems.ToList();
                foreach (GalleryItem galitem in tempGalItems)
                {
                    db.GalleryItems.Remove(galitem);
                }
            }
            db.Galleries.Remove(this);
        }
    
    }

    public class GalleryItem
    {
        public int GalleryItemID { get; set; }

        [ForeignKey("Gallery")]
        public int GalleryID { get; set; }
        [ScriptIgnore]
        public Gallery Gallery { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        [Required]
        [DefaultValue(0)]
        public short Order { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; }

        [HiddenInput]
        [DisplayName("Image")]
        public string ImageUrl { get; set; }
        [HiddenInput]
        public string ThumbUrl { get; set; }

        [StringLength(255)]
        public string LinkUrl { get; set; }

        public HttpPostedFileBase Image
        {
            set
            {
                if (value == null || GalleryItemID == 0) return;
                CivilianJobs.Helpers.MediaUploadResult result = CivilianJobs.Helpers.MediaHelper.AddImage(value, "~/Content/upload/gallery/" + GalleryItemID.ToString() + "/", true, 150, 150);
                if (result.Success) ImageUrl = result.FullPathWeb;
                if (result.ThumbSuccess) ThumbUrl = result.ThumbFullPathWeb;
            }
        }
    }
}