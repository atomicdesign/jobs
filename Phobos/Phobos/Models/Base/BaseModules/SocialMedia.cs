﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;

namespace CivilianJobs.Models
{
    public class SocialMedia
    {
        public int SocialMediaID { get; set; }

        [ReadOnly(true)]
        public string Name { get; set; }

        [HiddenInput]
        [Required]
        public string ModuleName { get; set; }

        public string UserInformation { get; set; }

        public string ImageUrl { get; set; }
    }
}