using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using CivilianJobs.Helpers;
using System.Text.RegularExpressions;

namespace CivilianJobs.Models
{
    [NotMapped]
    public class SearchResult
    {
        public string Content { get; set; }
        public string Excerpt { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }

        public static string FormatContent(string[] content)
        {
            return " " + String.Join(". ", content) + " ";
        }

        public static Regex QueryRegex(string query)
        {
            return new Regex(@"\b" + query + @"s?\b", RegexOptions.IgnoreCase);
        }
      

        public static string CreateExcerpt(string content, string query)
        {
            int maxlength = 400;

            //strip tags
            string excerpt = CivilianJobsHelper.StripTagsRegexCompiled(content);

            int indexOfQuery = excerpt.IndexOf(query, StringComparison.OrdinalIgnoreCase);

            //find base start and length
            int start = indexOfQuery > -1 ? indexOfQuery : 0;
            int length = start + maxlength > excerpt.Length ? excerpt.Length - start : maxlength;

            //create head, excerpt, tail
            string excerptHead = excerpt.Substring(0, start);
            string excerptTail = excerpt.Substring(start + length);
            excerpt = excerpt.Substring(start, length);

            //include beginning of sentence
            int sentenceStartIndex = excerptHead.LastIndexOf(".");
            if (sentenceStartIndex > -1 && sentenceStartIndex + 1 < excerptHead.Length)
            {
                excerpt = excerptHead.Substring(sentenceStartIndex + 1) + excerpt;
            }

            //include end of sentence
            int sentenceEndIndex = excerptTail.IndexOf(".");
            if (sentenceEndIndex > -1 && sentenceEndIndex + 1 < excerptTail.Length)
            {
                excerpt = excerpt + excerptTail.Substring(0, sentenceEndIndex + 1);
            }

            string taggedExcerpt = "";

            //find index of first instance
            indexOfQuery = excerpt.IndexOf(query, StringComparison.OrdinalIgnoreCase);

            string toBeTaggedExcerpt = "";
            string remainingExcerpt = excerpt;
            while (indexOfQuery > -1)
            {
                //cut string in two
                toBeTaggedExcerpt = remainingExcerpt.Substring(0, indexOfQuery + query.Length);
                remainingExcerpt = remainingExcerpt.Substring(indexOfQuery + query.Length);
                 
                //tag first half
                toBeTaggedExcerpt = toBeTaggedExcerpt.Insert(indexOfQuery, "<span class='searchquery'>");
                toBeTaggedExcerpt += "</span>";

                //append to tagged
                taggedExcerpt += toBeTaggedExcerpt;

                //get next index of query
                indexOfQuery = remainingExcerpt.IndexOf(query, StringComparison.OrdinalIgnoreCase);
            }
            //apend end of excerpt
            taggedExcerpt += remainingExcerpt;

            return taggedExcerpt;
        }
    }


}