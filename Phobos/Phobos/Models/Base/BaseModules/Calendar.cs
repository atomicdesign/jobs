using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;


namespace CivilianJobs.Models
{
    public class Events
    {
        public static List<SearchResult> Search(string query)
        {
            using (CivilianJobsContext db = new CivilianJobsContext())
            {

                List<Events> items = db.Events.ToList();

                var results =
                    from item in items
                    let content = SearchResult.FormatContent(new string[] { item.Heading, item.EventDesc, item.EventContent })
                    let matches = SearchResult.QueryRegex(query).Matches(content)
                    where matches.Count > 0
                    select new SearchResult()
                    {
                        Excerpt = SearchResult.CreateExcerpt(content.Trim(), query),
                        Title = item.Heading,
                        URL = "/calendar/event/" + item.EventID
                    };

                return results.ToList();
            }
        }


        [Key]
        public int EventID { get; set; }

        [Required, DisplayName("Date")]
        public DateTime EventDate { get; set; }

        [Required, DisplayName("Name")]
        public string Heading { get; set; }

        [DisplayName("Description")]
        public string EventDesc { get; set; }

        [AllowHtml, DisplayName("Content")]
        public string EventContent { get; set; }

        //added for the zoo
        [ForeignKey("Gallery"), DisplayName("Gallery")]
        public int? GalleryID { get; set; }
        //[ScriptIgnore]
        public virtual Gallery Gallery { get; set; }


    }
}