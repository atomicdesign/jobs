﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Models
{
    public class Ad
    {
        [Key]
        public int AdID { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Image")]
        public string ImageUrl { get; set; }
        [Required]
        [MaxLength(200)]
        public string Hyperlink { get; set; }
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(100)]
        [DisplayName("Publish For")]
        public string PublishFor { get; set; }
        [DisplayName("Publish Page")]
        public string PublishPage { get; set; }
        [DisplayName("Published On")]
        public DateTime PublishStartDate { get; set; }

        [DisplayName("Live Duration")]
        [DefaultValue(0)]
        public int LiveDuration { get; set; }

        [DisplayName("Pre-expire Notification")]
        [DefaultValue(0)]
        public int PreExpireNotification { get; set; }

        [MaxLength(100)]
        [DisplayName("Notification Email")]
        public string NotificationTo { get; set; }
        [MaxLength(100)]
        public string Subject { get; set; }
        [AllowHtml]
        public string Message { get; set; }
        [DefaultValue(0)]
        public int NotificationSent { get; set; }

        public HttpPostedFileBase Image
        {
            set
            {
                if (value == null || AdID == 0) return;
                CivilianJobs.Helpers.MediaUploadResult result = CivilianJobs.Helpers.MediaHelper.AddImage(value, "~/Content/upload/ad/" + AdID.ToString() + "/", true, 150, 150);
                if (result.Success) ImageUrl = result.FullPathWeb;
            }
        }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}