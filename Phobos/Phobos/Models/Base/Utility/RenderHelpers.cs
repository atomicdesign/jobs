﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CivilianJobs.Models
{
    [NotMapped]
    public class Breadcrumb
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }

    [NotMapped]
    public class CivilianJobsPuck
    {
        protected List<string> _Stylesheets { get; set; }
        public List<string> Stylesheets { get { return _Stylesheets ?? (_Stylesheets = new List<string>()); } set { _Stylesheets = value; } }

        protected List<string> _Scripts { get; set; }
        public List<string> Scripts { get { return _Scripts ?? (_Scripts = new List<string>()); } set { _Scripts = value; } }

        public CivilianJobsPuck()
        {
            _Stylesheets = new List<string>();
            _Scripts = new List<string>();
        }

        public IHtmlString RenderStyles()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string URL in _Stylesheets)
            {
                sb.Append(String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"/Content/css/{0}\"/>\r\n", URL));
            }
            return new MvcHtmlString(sb.ToString());
        }

        public IHtmlString RenderScripts()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string URL in _Scripts)
            {
                sb.Append(String.Format("<script type=\"text/javascript\" src=\"/Content/js/{0}\"></script>\r\n", URL));
            }
            return new MvcHtmlString(sb.ToString());
        }
    }
}