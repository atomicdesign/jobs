﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;

namespace CivilianJobs.Models
{
    public enum AlertLevel
    {
        INFO,
        NOTICE,
        SUCCESS,
        WARNING,
        ERROR,
        CRITICAL
    }

    public class AdminAlert
    {
        public int AdminAlertID { get; set; }

        [Required]
        public int AlertLevelInt { get; set; }
        public AlertLevel AlertLevel
        {
            get
            {
                return (AlertLevel)AlertLevelInt;
            }
            set
            {
                AlertLevelInt = (int)AlertLevel;
            }
        }

        [AllowHtml]
        [Required]
        public string Message { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Dismissed { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool AutoDismiss { get; set; }

        public string CSSClasses
        {
            get
            {
                string retval = "admin-alert admin-alert-" + AlertLevel.ToString();
                if (AutoDismiss) retval += " auto-dismiss";
                return retval;
            }
        }

        public IHtmlString HTML
        {
            get
            {
                string retval = "<div class=\"" + CSSClasses + "\">";
                retval += "<button type=\"button\" class=\"close\" data-id=\"" + AdminAlertID.ToString() + "\" data-dismiss=\"alert\">&times;</button>";
                retval += Message;
                retval += "</div>";
                return new MvcHtmlString(retval);
            }
        }
    }

    public enum UserAlertLevel
    {
        INFO,
        SUCCESS,
        ERROR
    }

    [NotMapped]
    public class UserAlert : IComparable
    {
        public UserAlert(string message, Models.UserAlertLevel alertLevel = Models.UserAlertLevel.INFO)
        {
            Message = message;
            AlertLevel = alertLevel;
        }

        public Models.UserAlertLevel AlertLevel { get; set; }

        [AllowHtml]
        public string Message { get; set; }

        public string RawHTML
        {
            get
            {
                return String.Format("<div class=\"alert alert-{0}\">{1}</div>", AlertLevel.ToString().ToLower(), Message);
            }
        }

        public IHtmlString HTML
        {
            get
            {
                return MvcHtmlString.Create(RawHTML);
            }
        }

        public int CompareTo(object obj)
        {
            if (obj.GetType() != typeof(UserAlert)) throw new System.ArgumentException();
            return (Message == ((UserAlert)obj).Message && AlertLevel == ((UserAlert)obj).AlertLevel ? 0 : 1);
        }
    }

    public class UserAlertHandler : IDisposable
    {
        public UserAlertHandler()
        {

        }

        public IHtmlString GetAlerts()
        {
            StringBuilder sb = new StringBuilder();

            Alerts.ForEach((alert) => { sb.AppendLine(alert.RawHTML); });
            Alerts.Clear(); //once we show them, destroy them

            return MvcHtmlString.Create(sb.ToString());
        }

        public void AddAlert(string message, UserAlertLevel alertlevel = UserAlertLevel.INFO)
        {
            Alerts.Add(new UserAlert(message, alertlevel));
        }

        public void Dispose()
        {
            HttpContext.Current.Session["Alerts"] = Alerts;
        }

        private List<UserAlert> _Alerts { get; set; }
        private List<UserAlert> Alerts
        {
            get
            {
                if (_Alerts == null)
                {
                    _Alerts = (List<UserAlert>)(HttpContext.Current.Session["Alerts"] ?? new List<UserAlert>());
                }
                return _Alerts;
            }
            set
            {
                _Alerts = value;
            }
        }
    }
}