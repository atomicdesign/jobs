﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data.Entity;
using System.Web.Mvc;
using System.IO;
using System.Runtime.Serialization.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using CivilianJobs.Helpers;

namespace CivilianJobs.Models
{
    public class EmailMessage
    {
        public int EmailMessageID { get; set; }

        public string To { get; set; } //make array
        public string From { get; set; }
        public string FromName { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }
        [AllowHtml]
        public string HTML { get; set; }
        public string PlainText { get; set; }

        public string AttachmentFilename1 { get; set; }
        public string AttachmentFilename2 { get; set; }
        public string AttachmentFilename3 { get; set; }
        public string AttachmentFilename4 { get; set; }

        public DateTime Created { get; set; }
        public string CreatedReadable { get { return Created.ToString(); } }

        public bool Queued { get; set; }
        public bool Sent { get; set; }
        public bool HasErrors { get; set; }
        public string ErrorMessage { get; set; }

        [NotMapped]
        public EmailServiceMessage EmailServiceMessage
        {
            get
            {
                var retval = new EmailServiceMessage
                {
                    html = HTML,
                    text = PlainText,
                    bcc_address = BCC,
                    subject = Subject,
                    from_email = From,
                    from_name = FromName,
                    to = new List<EmailServiceAddress>() { new EmailServiceAddress { email = To, name = To } },
                    attachments = new List<EmailServiceAttachment>()
                };

                if (AttachmentFilename1 != null && AttachmentFilename1 != String.Empty) retval.attachments.Add(new EmailServiceAttachment { filename = AttachmentFilename1 });
                if (AttachmentFilename2 != null && AttachmentFilename2 != String.Empty) retval.attachments.Add(new EmailServiceAttachment { filename = AttachmentFilename2 });
                if (AttachmentFilename3 != null && AttachmentFilename3 != String.Empty) retval.attachments.Add(new EmailServiceAttachment { filename = AttachmentFilename3 });
                if (AttachmentFilename4 != null && AttachmentFilename4 != String.Empty) retval.attachments.Add(new EmailServiceAttachment { filename = AttachmentFilename4 });

                return retval;
            }
        }
        public EmailMessage() : this("", "", "", "") { }
        public EmailMessage(string from, string to, string subject, string message, string newBCC = null)
        {
            To = to;
            HTML = message;
            From = from;
            FromName = from;
            Subject = subject;
            BCC = newBCC;
            Queued = true;
            Sent = false;
            Created = DateTime.UtcNow;
            HasErrors = false;
        }
    }
}