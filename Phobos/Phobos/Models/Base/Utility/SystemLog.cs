﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;

namespace CivilianJobs.Models
{
    public class SystemLog
    {
        public int SystemLogID { get; set; }

        public DateTime Created { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string HelpLink { get; set; }

        public Exception Exception
        {
            set
            {
                Created = DateTime.Now;
                Message = value.Message;
                StackTrace = value.StackTrace;
                HelpLink = value.HelpLink;

                if (value.GetType() == typeof(DbEntityValidationException)) //we get these a lot
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(value.Message);

                    DbEntityValidationException exc = (DbEntityValidationException)value;
                    foreach (DbEntityValidationResult x in exc.EntityValidationErrors)
                    {
                        foreach (DbValidationError y in x.ValidationErrors)
                        {
                            sb.AppendLine(y.ErrorMessage);
                        }
                    }
                    Message = sb.ToString();
                }
            }
        }
    }
}