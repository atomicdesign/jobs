﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CivilianJobs.Models
{
    public class Setting
    {
        public int SettingID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }

        public bool BoolValue
        {
            get
            {
                bool retval;
                return (bool.TryParse(Value, out retval) ? retval : false);
            }
        }

        public float FloatValue
        {
            get
            {
                float retval;
                return (Single.TryParse(Value, out retval) ? retval : 0.0f);
            }
        }

        public int IntValue
        {
            get
            {
                int retval;
                return (Int32.TryParse(Value, out retval) ? retval : 0);
            }
        }

        public string StringValue
        {
            get { return Value; }
        }
    }

    public class GlobalSettings : System.Configuration.ConfigurationSection
    {
        [ConfigurationProperty("PublicKey", IsRequired=true, DefaultValue=null)]
        public Guid PublicKey
        {
            get
            {
                return (Guid)this["PublicKey"];
            }
            set
            {
                this["PublicKey"] = value;
            }
        }

        [ConfigurationProperty("PrivateKey", IsRequired = true, DefaultValue = null)]
        public Guid PrivateKey
        {
            get
            {
                return (Guid)this["PrivateKey"];
            }
            set
            {
                this["PrivateKey"] = value;
            }
        }

        [ConfigurationProperty("LastKeyCheck", IsRequired = true, DefaultValue = null)]
        public DateTime LastKeyCheck
        {
            get
            {
                return (DateTime)this["LastKeyCheck"];
            }
            set
            {
                this["LastKeyCheck"] = value;
            }
        }
    }
}