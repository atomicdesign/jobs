﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;

namespace CivilianJobs.Models
{
    public class ThreeOhOne
    {
        public int ThreeOhOneID { get; set; }

        [StringLength(512)]
        public string InUrl { get; set; }
        [StringLength(512)]
        public string OutUrl { get; set; }

        public int TimesUsed { get; set; }
    }
}