﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;

namespace CivilianJobs.Models
{
    public class GeoName
    {
        [Key]
        public int geonameid { get; set; }//  : integer id of record in geonames database
        [StringLength(200)]
        public string name { get; set; }//     : name of geographical point (utf8) varchar(200)
        [StringLength(200)]
        public string asciiname { get; set; }//   : name of geographical point in plain ascii characters, varchar(200)
        public string alternatenames { get; set; }//   : alternatenames, comma separated varchar(5000)
        public decimal latitude { get; set; }//    : latitude in decimal degrees (wgs84)
        public decimal longitude { get; set; }//    : longitude in decimal degrees (wgs84)
        [StringLength(1)]
        public string feature_class { get; set; }//   : see http://www.geonames.org/export/codes.html, char(1)
        [StringLength(10)]
        public string feature_code { get; set; }//   : see http://www.geonames.org/export/codes.html, varchar(10)
        [StringLength(2)]
        public string country_code { get; set; }//  : ISO-3166 2-letter country code, 2 characters
        [StringLength(60)]
        public string cc2 { get; set; }//            : alternate country codes, comma separated, ISO-3166 2-letter country code, 60 characters
        [StringLength(20)]
        public string admin1_code { get; set; }//      : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
        [StringLength(80)]
        public string admin2_code { get; set; }//       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
        [StringLength(20)]
        public string admin3_code { get; set; }//      : code for third level administrative division, varchar(20)
        [StringLength(20)]
        public string admin4_code { get; set; }//       : code for fourth level administrative division, varchar(20)
        
        public int elevation { get; set; }//        : in meters, integer
        public string dem { get; set; }//               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
        [StringLength(40)]
        public string timezone { get; set; }//         : the timezone id (see file timeZone.txt) varchar(40)
        public DateTime modification_date { get; set; }// : date of last modification in yyyy-MM-dd format

        public string Consume
        {
            set
            {
                List<string> columns = new List<string>(value.Split('\t'));
                while (columns.Count < 19) columns.Add(""); //pad out
                int i = 0;
                int tempI;
                decimal tempD;
                DateTime tempDT;

                if (int.TryParse(columns[i++], out tempI)) geonameid = tempI;
                name = columns[i++];
                asciiname = columns[i++];
                alternatenames = columns[i++];
                if (decimal.TryParse(columns[i++], out tempD)) latitude = tempD;
                if (decimal.TryParse(columns[i++], out tempD)) longitude = tempD;
                feature_class = columns[i++];
                feature_code = columns[i++];
                country_code = columns[i++];
                cc2 = columns[i++];
                admin1_code = columns[i++];
                admin2_code = columns[i++];
                admin3_code = columns[i++];
                admin4_code = columns[i++];
                i++; //population
                if (int.TryParse(columns[i++], out tempI)) elevation = tempI;
                dem = columns[i++];
                timezone = columns[i++];
                if (DateTime.TryParse(columns[i++], out tempDT)) modification_date = tempDT;
            }
        }

        public string Expel
        {
            get
            {
                List<string> output = new List<string>();
                output.Add(geonameid.ToString());
                output.Add(name);
                output.Add(asciiname);
                output.Add(alternatenames);
                output.Add(latitude.ToString());
                output.Add(longitude.ToString());
                output.Add(feature_class);
                output.Add(feature_code);
                output.Add(country_code);
                output.Add(cc2);
                output.Add(admin1_code);
                output.Add(admin2_code);
                output.Add(admin3_code);
                output.Add(admin4_code);
                output.Add(String.Empty);
                output.Add(elevation.ToString());
                output.Add(dem);
                output.Add(timezone);
                output.Add(modification_date.ToString());

                return String.Join("\t", output.ToArray());
            }
        }
    }

    public class GeoNameEqualityComparer : IEqualityComparer<GeoName>
    {
        public bool Equals(GeoName o1, GeoName o2)
        {
            return (o1.asciiname == o2.asciiname && o1.admin1_code == o2.admin1_code && o1.country_code == o2.country_code);
        }


        public int GetHashCode(GeoName bx)
        {
            return (bx.asciiname + bx.admin1_code + bx.country_code).GetHashCode();
        }
    }

    public class GeoNameCountry
    {
        [Key]
        [StringLength(2)]
        public string ISO { get; set; }
        [StringLength(3)]
        public string ISO3 { get; set; }
        [StringLength(3)]
        public string ISO_Numeric { get; set; }
        [StringLength(2)]
        public string fips { get; set; }
        [StringLength(200)]
        public string Country { get; set; }
        [StringLength(200)]
        public string Capital { get; set; }
        public long Area { get; set; }
        public long Population { get; set; }
        [StringLength(2)]
        public string Continent { get; set; }
        [StringLength(5)]
        public string tld { get; set; }
        [StringLength(5)]
        public string CurrencyCode { get; set; }
        [StringLength(20)]
        public string CurrencyName { get; set; }
        [StringLength(20)]
        public string Phone { get; set; }
        public string Postal_Code_Format { get; set; }
        public string Postal_Code_Regex { get; set; }
        [StringLength(60)]
        public string Languages { get; set; }
        public int geonameid { get; set; }
        [StringLength(60)]
        public string neighbours { get; set; }
        [StringLength(2)]
        public string EquivalentFipsCode { get; set; }

        public string Consume
        {
            set
            {
                List<string> columns = new List<string>(value.Split('\t'));
                while (columns.Count < 19) columns.Add(""); //pad out
                int i = 0;
                int tempI;
                long tempL;
                
                ISO = columns[i++];
                ISO3 = columns[i++];
                ISO_Numeric = columns[i++];
                fips = columns[i++];
                Country = columns[i++];
                Capital = columns[i++];
                if (long.TryParse(columns[i++], out tempL)) Area = tempL;
                if (long.TryParse(columns[i++], out tempL)) Population = tempL;
                Continent = columns[i++];
                tld = columns[i++];
                CurrencyCode = columns[i++];
                CurrencyName = columns[i++];
                Phone = columns[i++];
                Postal_Code_Format = columns[i++];
                Postal_Code_Regex = columns[i++];
                Languages = columns[i++];
                if (int.TryParse(columns[i++], out tempI)) geonameid = tempI;
                neighbours = columns[i++];
                EquivalentFipsCode = columns[i++];
            }
        }

    }

    public class GeoNamePostalCode
    {
        public int GeoNamePostalCodeID { get; set; }

        [StringLength(20)]
        public string country_code { get; set; } // iso country code, 2 characters
        [StringLength(20)]
        public string postal_code { get; set; } // varchar(20)
        [StringLength(180)]
        public string place_name { get; set; } // varchar(180)
        [StringLength(100)]
        public string admin_name1 { get; set; } // 1. order subdivision (state) varchar(100)
        [StringLength(20)]
        public string admin_code1 { get; set; } // 1. order subdivision (state) varchar(20)
        [StringLength(100)]
        public string admin_name2 { get; set; } // 2. order subdivision (county/province) varchar(100)
        [StringLength(20)]
        public string admin_code2 { get; set; } // 2. order subdivision (county/province) varchar(20)
        [StringLength(100)]
        public string admin_name3 { get; set; } // 3. order subdivision (community) varchar(100)
        [StringLength(20)]
        public string admin_code3 { get; set; } // 3. order subdivision (community) varchar(20)
        public double latitude { get; set; } // estimated latitude (wgs84)
        public double longitude { get; set; } // estimated longitude (wgs84)
        public int accuracy { get; set; } // accuracy of lat/lng from 1=estimated to 6=centroid

        public string Consume
        {
            set
            {
                List<string> columns = new List<string>(value.Split('\t'));
                while (columns.Count < 19) columns.Add(""); //pad out
                int i = 0;
                int tempI;
                double tempD;

                country_code = columns[i++];
                postal_code = columns[i++];
                place_name = columns[i++];
                admin_name1 = columns[i++];
                admin_code1 = columns[i++];
                admin_name2 = columns[i++];
                admin_code2 = columns[i++];
                admin_name3 = columns[i++];
                admin_code3 = columns[i++];
                if (double.TryParse(columns[i++], out tempD)) latitude = tempD;
                if (double.TryParse(columns[i++], out tempD)) longitude = tempD;
                if (int.TryParse(columns[i++], out tempI)) accuracy = tempI;
            }
        }
    }
}