using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class GeoMap : EntityTypeConfiguration<Geo>
    {
        public GeoMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Geo1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Geo");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Geo1).HasColumnName("Geo");
        }
    }
}
