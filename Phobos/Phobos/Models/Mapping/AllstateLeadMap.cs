using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class AllstateLeadMap : EntityTypeConfiguration<AllstateLead>
    {
        public AllstateLeadMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.LeadSource)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AllstateLeads");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.HonorableDischarge).HasColumnName("HonorableDischarge");
            this.Property(t => t.LeadSource).HasColumnName("LeadSource");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
        }
    }
}
