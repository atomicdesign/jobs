using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class StateMap : EntityTypeConfiguration<State>
    {
        public StateMap()
        {
            // Primary Key
            this.HasKey(t => t.StateID);

            // Properties
            this.Property(t => t.StateID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StateName)
                .HasMaxLength(100);

            this.Property(t => t.ShortStateName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("States");
            this.Property(t => t.StateID).HasColumnName("StateID");
            this.Property(t => t.StateName).HasColumnName("StateName");
            this.Property(t => t.ShortStateName).HasColumnName("ShortStateName");
            this.Property(t => t.StateNumber).HasColumnName("StateNumber");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsState).HasColumnName("IsState");
            this.Property(t => t.States_StateID).HasColumnName("States_StateID");
        }
    }
}
