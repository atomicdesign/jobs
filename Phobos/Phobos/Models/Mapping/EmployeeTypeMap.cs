using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class EmployeeTypeMap : EntityTypeConfiguration<EmployeeType>
    {
        public EmployeeTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.EmployeeTypeID);

            // Properties
            this.Property(t => t.EmployeeTypeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.EmployeeType1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("EmployeeType");
            this.Property(t => t.EmployeeTypeID).HasColumnName("EmployeeTypeID");
            this.Property(t => t.EmployeeType1).HasColumnName("EmployeeType");
        }
    }
}
