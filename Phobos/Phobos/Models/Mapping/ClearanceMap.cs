using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ClearanceMap : EntityTypeConfiguration<Clearance>
    {
        public ClearanceMap()
        {
            // Primary Key
            this.HasKey(t => t.ClearanceID);

            // Properties
            this.Property(t => t.ClearanceID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Clearance1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Clearances");
            this.Property(t => t.ClearanceID).HasColumnName("ClearanceID");
            this.Property(t => t.Clearance1).HasColumnName("Clearance");
            this.Property(t => t.SpecialOrder).HasColumnName("SpecialOrder");
        }
    }
}
