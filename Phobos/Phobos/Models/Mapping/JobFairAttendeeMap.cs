using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairAttendeeMap : EntityTypeConfiguration<JobFairAttendee>
    {
        public JobFairAttendeeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.JobFairID, t.UserID });

            // Properties
            this.Property(t => t.JobFairID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("JobFairAttendees");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.UserID).HasColumnName("UserID");
        }
    }
}
