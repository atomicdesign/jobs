using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFair2Map : EntityTypeConfiguration<JobFair2>
    {
        public JobFair2Map()
        {
            // Primary Key
            this.HasKey(t => t.JobFairID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(255);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.State)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Zip)
                .IsFixedLength()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("JobFair2");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.DateStart).HasColumnName("DateStart");
            this.Property(t => t.DateEnd).HasColumnName("DateEnd");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.RosterDue).HasColumnName("RosterDue");
            this.Property(t => t.TablesAvailable).HasColumnName("TablesAvailable");
        }
    }
}
