using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class MonsterMailUserMap : EntityTypeConfiguration<MonsterMailUser>
    {
        public MonsterMailUserMap()
        {
            // Primary Key
            this.HasKey(t => new { t.userid, t.username });

            // Properties
            this.Property(t => t.userid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.username)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("MonsterMailUser");
            this.Property(t => t.userid).HasColumnName("userid");
            this.Property(t => t.username).HasColumnName("username");
            this.Property(t => t.active).HasColumnName("active");
        }
    }
}
