using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class TravelMap : EntityTypeConfiguration<Travel>
    {
        public TravelMap()
        {
            // Primary Key
            this.HasKey(t => t.TravelID);

            // Properties
            this.Property(t => t.TravelID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Travel1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Travel");
            this.Property(t => t.TravelID).HasColumnName("TravelID");
            this.Property(t => t.Travel1).HasColumnName("Travel");
        }
    }
}
