using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class HighSchoolMap : EntityTypeConfiguration<HighSchool>
    {
        public HighSchoolMap()
        {
            // Primary Key
            this.HasKey(t => t.HighSchoolID);

            // Properties
            this.Property(t => t.HighSchoolID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HighSchool1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HighSchool");
            this.Property(t => t.HighSchoolID).HasColumnName("HighSchoolID");
            this.Property(t => t.HighSchool1).HasColumnName("HighSchool");
        }
    }
}
