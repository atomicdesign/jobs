using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class viewCandidateMap : EntityTypeConfiguration<viewCandidate>
    {
        public viewCandidateMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserId, t.EmailAddress, t.ProfileComplete, t.InstructorDuty });

            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.EmailAddress)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Addr1)
                .HasMaxLength(100);

            this.Property(t => t.Addr2)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.State)
                .HasMaxLength(50);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.ServiceBranch)
                .HasMaxLength(50);

            this.Property(t => t.Rank)
                .HasMaxLength(10);

            this.Property(t => t.Specialties)
                .HasMaxLength(300);

            this.Property(t => t.Awards)
                .HasMaxLength(2000);

            this.Property(t => t.CityPreferences)
                .HasMaxLength(2000);

            this.Property(t => t.ProfileComplete)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.Certifications)
                .HasMaxLength(300);

            this.Property(t => t.InstructorDuty)
                .IsRequired()
                .HasMaxLength(1);

            this.Property(t => t.DischargeType)
                .HasMaxLength(50);

            this.Property(t => t.UndergradDegree)
                .HasMaxLength(100);

            this.Property(t => t.GraduateDegree)
                .HasMaxLength(100);

            this.Property(t => t.ForeignLanguage)
                .HasMaxLength(50);

            this.Property(t => t.Clearance)
                .HasMaxLength(50);

            this.Property(t => t.Cert1)
                .HasMaxLength(50);

            this.Property(t => t.Cert2)
                .HasMaxLength(50);

            this.Property(t => t.Cert3)
                .HasMaxLength(50);

            this.Property(t => t.Cert4)
                .HasMaxLength(50);

            this.Property(t => t.ReEnlistCode)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("viewCandidates");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            this.Property(t => t.Addr1).HasColumnName("Addr1");
            this.Property(t => t.Addr2).HasColumnName("Addr2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.ProfileActive).HasColumnName("ProfileActive");
            this.Property(t => t.ServiceBranch).HasColumnName("ServiceBranch");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.YearsOfService).HasColumnName("YearsOfService");
            this.Property(t => t.Specialties).HasColumnName("Specialties");
            this.Property(t => t.Awards).HasColumnName("Awards");
            this.Property(t => t.CityPreferences).HasColumnName("CityPreferences");
            this.Property(t => t.ProfileComplete).HasColumnName("ProfileComplete");
            this.Property(t => t.Certifications).HasColumnName("Certifications");
            this.Property(t => t.JobCategories).HasColumnName("JobCategories");
            this.Property(t => t.InstructorDuty).HasColumnName("InstructorDuty");
            this.Property(t => t.DischargeType).HasColumnName("DischargeType");
            this.Property(t => t.UndergradDegree).HasColumnName("UndergradDegree");
            this.Property(t => t.GraduateDegree).HasColumnName("GraduateDegree");
            this.Property(t => t.ForeignLanguage).HasColumnName("ForeignLanguage");
            this.Property(t => t.Clearance).HasColumnName("Clearance");
            this.Property(t => t.SepDate).HasColumnName("SepDate");
            this.Property(t => t.Cert1).HasColumnName("Cert1");
            this.Property(t => t.Cert2).HasColumnName("Cert2");
            this.Property(t => t.Cert3).HasColumnName("Cert3");
            this.Property(t => t.Cert4).HasColumnName("Cert4");
            this.Property(t => t.AvailDate).HasColumnName("AvailDate");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.WorkHistory).HasColumnName("WorkHistory");
            this.Property(t => t.EnlistDate).HasColumnName("EnlistDate");
            this.Property(t => t.ReEnlistCode).HasColumnName("ReEnlistCode");
            this.Property(t => t.DateProfileInactived).HasColumnName("DateProfileInactived");
        }
    }
}
