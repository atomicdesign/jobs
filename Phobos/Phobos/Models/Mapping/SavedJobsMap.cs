﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Mapping
{
    class SavedJobsMap : EntityTypeConfiguration<SavedJob>
    {
        public SavedJobsMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);
            this.ToTable("SavedJobs");
            this.HasRequired(t => t.JobPosting);
            this.HasRequired(t => t.UserProfile);
        }
    }
}
