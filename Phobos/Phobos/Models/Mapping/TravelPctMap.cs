using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class TravelPctMap : EntityTypeConfiguration<TravelPct>
    {
        public TravelPctMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TravelPct1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TravelPct");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.TravelPct1).HasColumnName("TravelPct");
        }
    }
}
