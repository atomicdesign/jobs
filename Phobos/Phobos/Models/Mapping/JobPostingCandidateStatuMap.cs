using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobPostingCandidateStatuMap : EntityTypeConfiguration<JobPostingCandidateStatu>
    {
        public JobPostingCandidateStatuMap()
        {
            // Primary Key
            this.HasKey(t => t.StatusID);

            // Properties
            this.Property(t => t.StatusID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Status)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("JobPostingCandidateStatus");
            this.Property(t => t.StatusID).HasColumnName("StatusID");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
