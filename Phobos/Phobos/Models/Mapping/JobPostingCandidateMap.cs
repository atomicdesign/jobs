using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobPostingCandidateMap : EntityTypeConfiguration<JobPostingCandidate>
    {
        public JobPostingCandidateMap()
        {
            // Primary Key
            this.HasKey(t => new { t.JobPostingID, t.UserID });

            // Properties
            this.Property(t => t.JobPostingID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("JobPostingCandidates");
            this.Property(t => t.JobPostingID).HasColumnName("JobPostingID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
        }
    }
}
