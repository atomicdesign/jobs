using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SchoolMap : EntityTypeConfiguration<School>
    {
        public SchoolMap()
        {
            // Primary Key
            this.HasKey(t => t.SchoolID);

            // Properties
            this.Property(t => t.SchoolID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SchoolName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Schools");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.SchoolName).HasColumnName("SchoolName");
        }
    }
}
