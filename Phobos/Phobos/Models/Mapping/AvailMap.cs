using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class AvailMap : EntityTypeConfiguration<Avail>
    {
        public AvailMap()
        {
            // Primary Key
            this.HasKey(t => t.AvailID);

            // Properties
            this.Property(t => t.AvailID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Avail1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Avail");
            this.Property(t => t.AvailID).HasColumnName("AvailID");
            this.Property(t => t.Avail1).HasColumnName("Avail");
        }
    }
}
