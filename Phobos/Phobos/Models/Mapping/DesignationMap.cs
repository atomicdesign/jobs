﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class DesignationMap : EntityTypeConfiguration<Designation>
    {
        public DesignationMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            this.Property(t => t.Designationees)
               .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Designate");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Designationees).HasColumnName("Designationees");
         }
    }
}