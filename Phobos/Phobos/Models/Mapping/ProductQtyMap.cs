using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ProductQtyMap : EntityTypeConfiguration<ProductQty>
    {
        public ProductQtyMap()
        {
            // Primary Key
            this.HasKey(t => t.ProductQtyId);

            // Properties
            this.Property(t => t.ProductType)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("ProductQty");
            this.Property(t => t.ProductQtyId).HasColumnName("ProductQtyId");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.Qty).HasColumnName("Qty");
        }
    }
}
