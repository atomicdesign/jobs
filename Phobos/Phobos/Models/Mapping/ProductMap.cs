using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            this.HasKey(t => t.ProductId);

            // Properties
            this.Property(t => t.ProductId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descr)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductType)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.UOM)
                .IsRequired()
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("Product");
            this.Property(t => t.ProductId).HasColumnName("ProductId");
            this.Property(t => t.Descr).HasColumnName("Descr");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.BaseRate).HasColumnName("BaseRate");
            this.Property(t => t.JobPostingTerm).HasColumnName("JobPostingTerm");
            this.Property(t => t.JobPostingActiveTerm).HasColumnName("JobPostingActiveTerm");
            this.Property(t => t.MaxResumeViews).HasColumnName("MaxResumeViews");
            this.Property(t => t.UOM).HasColumnName("UOM");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
