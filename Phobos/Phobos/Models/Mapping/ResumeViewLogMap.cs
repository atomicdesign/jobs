using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ResumeViewLogMap : EntityTypeConfiguration<ResumeViewLog>
    {
        public ResumeViewLogMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ClientUserid, t.CandidateUserid });

            // Properties
            this.Property(t => t.ClientUserid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CandidateUserid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ResumeViewLog");
            this.Property(t => t.ClientUserid).HasColumnName("ClientUserid");
            this.Property(t => t.CandidateUserid).HasColumnName("CandidateUserid");
            this.Property(t => t.DateViewed).HasColumnName("DateViewed");
        }
    }
}
