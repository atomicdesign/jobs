using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class HiringNeedsSurveyNeedMap : EntityTypeConfiguration<HiringNeedsSurveyNeed>
    {
        public HiringNeedsSurveyNeedMap()
        {
            // Primary Key
            this.HasKey(t => t.HiringNeedsSurveyNeedsID);

            // Properties
            // Table & Column Mappings
            this.ToTable("HiringNeedsSurveyNeeds");
            this.Property(t => t.HiringNeedsSurveyNeedsID).HasColumnName("HiringNeedsSurveyNeedsID");
            this.Property(t => t.HiringNeedsSurveyID).HasColumnName("HiringNeedsSurveyID");
            this.Property(t => t.HiringNeed).HasColumnName("HiringNeed");
        }
    }
}
