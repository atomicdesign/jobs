using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class MilitaryExperienceMap : EntityTypeConfiguration<MilitaryExperience>
    {
        public MilitaryExperienceMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descr)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("MilitaryExperience");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Descr).HasColumnName("Descr");
            this.Property(t => t.Officer).HasColumnName("Officer");
        }
    }
}
