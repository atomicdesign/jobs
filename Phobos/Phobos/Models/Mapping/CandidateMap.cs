using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandidateMap : EntityTypeConfiguration<Candidate>
    {
        public CandidateMap()
        {
            // Primary Key
            this.HasKey(t => t.UserId);

            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Cert1)
                .HasMaxLength(50);

            this.Property(t => t.Cert2)
                .HasMaxLength(50);

            this.Property(t => t.Cert3)
                .HasMaxLength(50);

            this.Property(t => t.Cert4)
                .HasMaxLength(50);

            this.Property(t => t.Specialty)
                .HasMaxLength(50);

            this.Property(t => t.AdminNotes)
                .HasMaxLength(2000);

            this.Property(t => t.BMI_Recruiter_Email)
                .HasMaxLength(100);

            this.Property(t => t.ReEnlistCode)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Candidates");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Recruiter).HasColumnName("Recruiter");
            this.Property(t => t.ServiceBranch).HasColumnName("ServiceBranch");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.YearsOfService).HasColumnName("YearsOfService");
            this.Property(t => t.SepDate).HasColumnName("SepDate");
            this.Property(t => t.CandSchool1).HasColumnName("CandSchool1");
            this.Property(t => t.CandSchool2).HasColumnName("CandSchool2");
            this.Property(t => t.CandSchool3).HasColumnName("CandSchool3");
            this.Property(t => t.HighSchool).HasColumnName("HighSchool");
            this.Property(t => t.Cert1).HasColumnName("Cert1");
            this.Property(t => t.Cert2).HasColumnName("Cert2");
            this.Property(t => t.Cert3).HasColumnName("Cert3");
            this.Property(t => t.Cert4).HasColumnName("Cert4");
            this.Property(t => t.AvailDate).HasColumnName("AvailDate");
            this.Property(t => t.PayExpectation).HasColumnName("PayExpectation");
            this.Property(t => t.Relo).HasColumnName("Relo");
            this.Property(t => t.DischargeType).HasColumnName("DischargeType");
            this.Property(t => t.Clearance).HasColumnName("Clearance");
            this.Property(t => t.MarketAsClearance).HasColumnName("MarketAsClearance");
            this.Property(t => t.Crime).HasColumnName("Crime");
            this.Property(t => t.MilitaryPosition1).HasColumnName("MilitaryPosition1");
            this.Property(t => t.MilitaryPosition2).HasColumnName("MilitaryPosition2");
            this.Property(t => t.MilitaryPosition3).HasColumnName("MilitaryPosition3");
            this.Property(t => t.CivilianPosition1).HasColumnName("CivilianPosition1");
            this.Property(t => t.CivilianPosition2).HasColumnName("CivilianPosition2");
            this.Property(t => t.CivilianPosition3).HasColumnName("CivilianPosition3");
            this.Property(t => t.PEI_ACAD).HasColumnName("PEI_ACAD");
            this.Property(t => t.PEI_ENL_COLLEGE).HasColumnName("PEI_ENL_COLLEGE");
            this.Property(t => t.PEI_CERT).HasColumnName("PEI_CERT");
            this.Property(t => t.PEI_ENL_SUPERMAN).HasColumnName("PEI_ENL_SUPERMAN");
            this.Property(t => t.PEI_CW).HasColumnName("PEI_CW");
            this.Property(t => t.PEI_RELO).HasColumnName("PEI_RELO");
            this.Property(t => t.PEI_MEDALS).HasColumnName("PEI_MEDALS");
            this.Property(t => t.PEI_LANGUAGES).HasColumnName("PEI_LANGUAGES");
            this.Property(t => t.PEI_CLEARANCE).HasColumnName("PEI_CLEARANCE");
            this.Property(t => t.InstructorDuty).HasColumnName("InstructorDuty");
            this.Property(t => t.bmi).HasColumnName("bmi");
            this.Property(t => t.PEI_INSTRUCTOR).HasColumnName("PEI_INSTRUCTOR");
            this.Property(t => t.PEI_MASTERTRAINER).HasColumnName("PEI_MASTERTRAINER");
            this.Property(t => t.ProfileActive).HasColumnName("ProfileActive");
            this.Property(t => t.MasterTrainer).HasColumnName("MasterTrainer");
            this.Property(t => t.Specialty).HasColumnName("Specialty");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.ReceivedSummaryPage).HasColumnName("ReceivedSummaryPage");
            this.Property(t => t.ReservedForBMI).HasColumnName("ReservedForBMI");
            this.Property(t => t.ReceivedEmailSurvey).HasColumnName("ReceivedEmailSurvey");
            this.Property(t => t.CandSchool4).HasColumnName("CandSchool4");
            this.Property(t => t.Ethnicity).HasColumnName("Ethnicity");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.AdminNotes).HasColumnName("AdminNotes");
            this.Property(t => t.WorkHistory).HasColumnName("WorkHistory");
            this.Property(t => t.ResumeExtracted).HasColumnName("ResumeExtracted");
            this.Property(t => t.MilitaryFriendlyColleges).HasColumnName("MilitaryFriendlyColleges");
            this.Property(t => t.BMI_Conversion_Date).HasColumnName("BMI_Conversion_Date");
            this.Property(t => t.BMI_CandidateGuid).HasColumnName("BMI_CandidateGuid");
            this.Property(t => t.BMI_Recruiter_Email).HasColumnName("BMI_Recruiter_Email");
            this.Property(t => t.ForeignLanguage).HasColumnName("ForeignLanguage");
            this.Property(t => t.ShiftWork).HasColumnName("ShiftWork");
            this.Property(t => t.TravelPct).HasColumnName("TravelPct");
            this.Property(t => t.DegreeUndergrad).HasColumnName("DegreeUndergrad");
            this.Property(t => t.DegreeGraduate).HasColumnName("DegreeGraduate");
            this.Property(t => t.EnlistDate).HasColumnName("EnlistDate");
            this.Property(t => t.ReEnlistCode).HasColumnName("ReEnlistCode");
            this.Property(t => t.DateProfileInactived).HasColumnName("DateProfileInactived");
            this.Property(t => t.Keywords).HasColumnName("Keywords");

            // Relationships
            this.HasMany(t => t.Medals)
                .WithMany(t => t.Candidates)
                .Map(m =>
                    {
                        m.ToTable("CandMedals");
                        m.MapLeftKey("UserID");
                        m.MapRightKey("MedalID");
                    });


        }
    }
}
