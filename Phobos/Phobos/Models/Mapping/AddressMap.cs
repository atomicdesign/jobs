using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class AddressMap : EntityTypeConfiguration<Addresses>
    {
        public AddressMap()
        {
            // Primary Key
            this.HasKey(t => t.UserID);

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Addr1)
                .HasMaxLength(100);

            this.Property(t => t.Addr2)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.State)
                .HasMaxLength(10);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.Country)
                .HasMaxLength(50);

            this.Property(t => t.Phone1)
                .HasMaxLength(20);

            this.Property(t => t.Phone2)
                .HasMaxLength(20);

            this.Property(t => t.OverseasPhone)
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("Addresses");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.Addr1).HasColumnName("Addr1");
            this.Property(t => t.Addr2).HasColumnName("Addr2");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.Phone1).HasColumnName("Phone1");
            this.Property(t => t.Phone2).HasColumnName("Phone2");
            this.Property(t => t.OverseasPhone).HasColumnName("OverseasPhone");
        }
    }
}
