using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class viewCandidateJobCategoryMap : EntityTypeConfiguration<viewCandidateJobCategory>
    {
        public viewCandidateJobCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.emailaddress);

            // Properties
            this.Property(t => t.emailaddress)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("viewCandidateJobCategories");
            this.Property(t => t.emailaddress).HasColumnName("emailaddress");
            this.Property(t => t.cats).HasColumnName("cats");
        }
    }
}
