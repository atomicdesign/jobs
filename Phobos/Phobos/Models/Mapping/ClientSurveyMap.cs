using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ClientSurveyMap : EntityTypeConfiguration<ClientSurvey>
    {
        public ClientSurveyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.JobFairID, t.FirstJobFair, t.HearAbout, t.Quality, t.RateCJN, t.Reason, t.Referrals, t.Quality2, t.CustomerService, t.UserId });

            // Properties
            this.Property(t => t.Company)
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .HasMaxLength(150);

            this.Property(t => t.JobFairID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Improve)
                .HasMaxLength(500);

            this.Property(t => t.Testimonial)
                .HasMaxLength(500);

            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ClientSurvey");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.FirstJobFair).HasColumnName("FirstJobFair");
            this.Property(t => t.HearAbout).HasColumnName("HearAbout");
            this.Property(t => t.Quality).HasColumnName("Quality");
            this.Property(t => t.RateCJN).HasColumnName("RateCJN");
            this.Property(t => t.Reason).HasColumnName("Reason");
            this.Property(t => t.Referrals).HasColumnName("Referrals");
            this.Property(t => t.Improve).HasColumnName("Improve");
            this.Property(t => t.Testimonial).HasColumnName("Testimonial");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.Quality2).HasColumnName("Quality2");
            this.Property(t => t.CustomerService).HasColumnName("CustomerService");
            this.Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
