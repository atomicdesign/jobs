﻿using CivilianJobs.Models.civjobs;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Mapping
{
    class AwardsMap : EntityTypeConfiguration<Awards>
    {
        public AwardsMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("Awards");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Award).HasColumnName("Award");
        }
    }
    }
