using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandSchoolMap : EntityTypeConfiguration<CandSchool>
    {
        public CandSchoolMap()
        {
            // Primary Key
            this.HasKey(t => t.CandSchoolID);

            // Properties
            this.Property(t => t.OtherSchoolName)
                .HasMaxLength(50);

            this.Property(t => t.Major)
                .HasMaxLength(50);

            this.Property(t => t.Activity1)
                .HasMaxLength(50);

            this.Property(t => t.Activity2)
                .HasMaxLength(50);

            this.Property(t => t.Activity3)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CandSchools");
            this.Property(t => t.CandSchoolID).HasColumnName("CandSchoolID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.OtherSchoolName).HasColumnName("OtherSchoolName");
            this.Property(t => t.Major).HasColumnName("Major");
            this.Property(t => t.GradYear).HasColumnName("GradYear");
            this.Property(t => t.Activity1).HasColumnName("Activity1");
            this.Property(t => t.Activity2).HasColumnName("Activity2");
            this.Property(t => t.Activity3).HasColumnName("Activity3");
            this.Property(t => t.DegreeID).HasColumnName("DegreeID");

            // Relationships
            this.HasOptional(t => t.School)
                .WithMany(t => t.CandSchools)
                .HasForeignKey(d => d.SchoolID);

        }
    }
}
