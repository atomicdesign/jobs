using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class EthnicityMap : EntityTypeConfiguration<Ethnicity>
    {
        public EthnicityMap()
        {
            // Primary Key
            this.HasKey(t => t.EthnicityID);

            // Properties
            this.Property(t => t.EthnicityID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Ethnicity1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Ethnicity");
            this.Property(t => t.EthnicityID).HasColumnName("EthnicityID");
            this.Property(t => t.Ethnicity1).HasColumnName("Ethnicity");
        }
    }
}
