using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobPostingMap : EntityTypeConfiguration<JobPosting>
    {
        public JobPostingMap()
        {
            // Primary Key
            this.HasKey(t => t.JobPostingID);

            // Properties
            this.Property(t => t.ContactCompany)
                .HasMaxLength(200);

            this.Property(t => t.ContactName)
                .HasMaxLength(50);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(20);

            this.Property(t => t.ContactFax)
                .HasMaxLength(20);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(200);

            this.Property(t => t.Title)
                .HasMaxLength(100);

            this.Property(t => t.CompensationBonus)
                .HasMaxLength(50);

            this.Property(t => t.JobPostingStatusCode)
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Compensation)
                .HasMaxLength(100);

            this.Property(t => t.Notes)
                .HasMaxLength(700);

            this.Property(t => t.JobWrapID)
                .HasMaxLength(50);

            this.Property(t => t.ApplicationURL)
                .HasMaxLength(500);

            this.Property(t => t.RequisitionId)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("JobPosting");
            this.Property(t => t.JobPostingID).HasColumnName("JobPostingID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.ContactCompany).HasColumnName("ContactCompany");
            this.Property(t => t.ContactName).HasColumnName("ContactName");
            this.Property(t => t.ContactPhone).HasColumnName("ContactPhone");
            this.Property(t => t.ContactFax).HasColumnName("ContactFax");
            this.Property(t => t.ContactEmail).HasColumnName("ContactEmail");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.EmployeeType).HasColumnName("EmployeeType");
            this.Property(t => t.Experience).HasColumnName("Experience");
            this.Property(t => t.Requirements).HasColumnName("Requirements");
            this.Property(t => t.Degree).HasColumnName("Degree");
            this.Property(t => t.Clearance).HasColumnName("Clearance");
            this.Property(t => t.ForeignLang).HasColumnName("ForeignLang");
            this.Property(t => t.CompensationMin).HasColumnName("CompensationMin");
            this.Property(t => t.CompensationMax).HasColumnName("CompensationMax");
            this.Property(t => t.CompensationPeriod).HasColumnName("CompensationPeriod");
            this.Property(t => t.CompensationBonus).HasColumnName("CompensationBonus");
            this.Property(t => t.Travel).HasColumnName("Travel");
            this.Property(t => t.StateID).HasColumnName("StateID");
            this.Property(t => t.OrderDetailId).HasColumnName("OrderDetailId");
            this.Property(t => t.JobPostingStatusCode).HasColumnName("JobPostingStatusCode");
            this.Property(t => t.ActivationDate).HasColumnName("ActivationDate");
            this.Property(t => t.ExpirationDate).HasColumnName("ExpirationDate");
            this.Property(t => t.ValidExpirationDate).HasColumnName("ValidExpirationDate");
            this.Property(t => t.HidePhone).HasColumnName("HidePhone");
            this.Property(t => t.HideFax).HasColumnName("HideFax");
            this.Property(t => t.HideEmail).HasColumnName("HideEmail");
            this.Property(t => t.Compensation).HasColumnName("Compensation");
            this.Property(t => t.EmailWhenAppliedTo).HasColumnName("EmailWhenAppliedTo");
            this.Property(t => t.HirenetID).HasColumnName("HirenetID");
            this.Property(t => t.ViewCount).HasColumnName("ViewCount");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.JobWrapped).HasColumnName("JobWrapped");
            this.Property(t => t.JobWrapID).HasColumnName("JobWrapID");
            this.Property(t => t.ApplicationURL).HasColumnName("ApplicationURL");
            this.Property(t => t.RequisitionId).HasColumnName("RequisitionId");
            this.Property(t => t.JobWrapDate).HasColumnName("JobWrapDate");
            
        }
    }
}
