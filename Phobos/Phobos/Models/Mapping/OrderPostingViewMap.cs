using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class OrderPostingViewMap : EntityTypeConfiguration<OrderPostingView>
    {
        public OrderPostingViewMap()
        {
            // Primary Key
            this.HasKey(t => new { t.OrderId, t.UserId, t.OrderDetailId, t.Qty });

            // Properties
            this.Property(t => t.OrderId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OrderDetailId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Qty)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("OrderPostingView");
            this.Property(t => t.OrderId).HasColumnName("OrderId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.OrderAppDate).HasColumnName("OrderAppDate");
            this.Property(t => t.OrderDetailId).HasColumnName("OrderDetailId");
            this.Property(t => t.Qty).HasColumnName("Qty");
            this.Property(t => t.PostingExpirationDate).HasColumnName("PostingExpirationDate");
        }
    }
}
