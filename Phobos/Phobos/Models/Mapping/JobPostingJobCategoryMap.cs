using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobPostingJobCategoryMap : EntityTypeConfiguration<JobPostingJobCategory>
    {
        public JobPostingJobCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.JobPostingID, t.JobCategoryID });

            // Properties
            this.Property(t => t.JobPostingID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.JobCategoryID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("JobPostingJobCategories");
            this.Property(t => t.JobPostingID).HasColumnName("JobPostingID");
            this.Property(t => t.JobCategoryID).HasColumnName("JobCategoryID");
        }
    }
}
