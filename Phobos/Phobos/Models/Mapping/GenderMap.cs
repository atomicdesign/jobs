using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class GenderMap : EntityTypeConfiguration<Gender>
    {
        public GenderMap()
        {
            // Primary Key
            this.HasKey(t => t.GenderID);

            // Properties
            this.Property(t => t.GenderID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Gender1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Gender");
            this.Property(t => t.GenderID).HasColumnName("GenderID");
            this.Property(t => t.Gender1).HasColumnName("Gender");
        }
    }
}
