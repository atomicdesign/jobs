using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class PayMap : EntityTypeConfiguration<Pay>
    {
        public PayMap()
        {
            // Primary Key
            this.HasKey(t => t.PayID);

            // Properties
            this.Property(t => t.PayID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Pay1)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Pay");
            this.Property(t => t.PayID).HasColumnName("PayID");
            this.Property(t => t.Pay1).HasColumnName("Pay");
        }
    }
}
