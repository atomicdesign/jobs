﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Mapping
{
    public class ProductDescriptionMap : EntityTypeConfiguration<ProductDescription>
    {
        public ProductDescriptionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table & Column Mappings
            this.ToTable("ProductDescriptions");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.package).HasColumnName("package");
            this.Property(t => t.price).HasColumnName("price");
            this.Property(t => t.quantity).HasColumnName("quantity");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.type).HasColumnName("type");
            this.Property(t => t.userId).HasColumnName("userId");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.state).HasColumnName("state");
        }
    }
}
