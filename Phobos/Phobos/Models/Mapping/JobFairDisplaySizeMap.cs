using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairDisplaySizeMap : EntityTypeConfiguration<JobFairDisplaySize>
    {
        public JobFairDisplaySizeMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.DisplaySize)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("JobFairDisplaySize");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.DisplaySize).HasColumnName("DisplaySize");
            this.Property(t => t.sort_order).HasColumnName("sort_order");
        }
    }
}
