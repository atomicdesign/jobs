using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandGeoMap : EntityTypeConfiguration<CandGeo>
    {
        public CandGeoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserId, t.GeoId });

            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.GeoId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandGeo");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.GeoId).HasColumnName("GeoId");
        }
    }
}
