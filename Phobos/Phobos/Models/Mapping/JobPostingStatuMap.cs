using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobPostingStatuMap : EntityTypeConfiguration<JobPostingStatu>
    {
        public JobPostingStatuMap()
        {
            // Primary Key
            this.HasKey(t => t.JobPostingStatusCode);

            // Properties
            this.Property(t => t.JobPostingStatusCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Descr)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("JobPostingStatus");
            this.Property(t => t.JobPostingStatusCode).HasColumnName("JobPostingStatusCode");
            this.Property(t => t.Descr).HasColumnName("Descr");
        }
    }
}
