using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandCareerPrefMap : EntityTypeConfiguration<CandCareerPref>
    {
        public CandCareerPrefMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserID, t.CareerPrefID });

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CareerPrefID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandCareerPrefs");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.CareerPrefID).HasColumnName("CareerPrefID");
        }
    }
}
