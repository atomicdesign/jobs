using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandSkillMap : EntityTypeConfiguration<CandSkill>
    {
        public CandSkillMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserID, t.SkillID });

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SkillID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandSkills");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.SkillID).HasColumnName("SkillID");
        }
    }
}
