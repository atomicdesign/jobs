using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class EmailCampaignQueryMap : EntityTypeConfiguration<EmailCampaignQuery>
    {
        public EmailCampaignQueryMap()
        {
            // Primary Key
            this.HasKey(t => t.QueryId);

            // Properties
            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.ProcedureName)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("EmailCampaignQuerys");
            this.Property(t => t.QueryId).HasColumnName("QueryId");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.ProcedureName).HasColumnName("ProcedureName");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
        }
    }
}
