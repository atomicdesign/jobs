using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class viewABALMap : EntityTypeConfiguration<viewABAL>
    {
        public viewABALMap()
        {
            // Primary Key
            this.HasKey(t => t.MasterfileGuid);

            // Properties
            this.Property(t => t.AR)
                .HasMaxLength(102);

            this.Property(t => t.Expires)
                .HasMaxLength(10);

            this.Property(t => t.State)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("viewABAL");
            this.Property(t => t.MasterfileGuid).HasColumnName("MasterfileGuid");
            this.Property(t => t.AR).HasColumnName("AR");
            this.Property(t => t.Expires).HasColumnName("Expires");
            this.Property(t => t.State).HasColumnName("State");
        }
    }
}
