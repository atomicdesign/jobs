﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Mapping
{
    public class JobApplicationMap : EntityTypeConfiguration<JobApplication>
    {
        public JobApplicationMap()
        {
            this.HasKey(x => x.ID);
            this.ToTable("JobApplications");
            this.Property(x=>x.UserProfileID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("UserProfileId");
            this.Property(x => x.JobPostingID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("JobPosting_ID");
            this.Property(x => x.ResumeID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None).HasColumnName("Resume_ID");
            this.Property(x => x.CreateDate).HasColumnName("CreateDate");
            this.Property(x => x.IsHidden).HasColumnName("IsHidden");
           
        }
    }
}