using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap()
        {
            // Primary Key
            this.HasKey(t => t.OrderId);

            // Properties
            this.Property(t => t.TransactionId)
                .HasMaxLength(50);

            this.Property(t => t.OrderStatusCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.PmtMethod)
                .HasMaxLength(2);

            this.Property(t => t.BillingContact)
                .HasMaxLength(255);

            this.Property(t => t.BillingTerms)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Order");
            this.Property(t => t.OrderId).HasColumnName("OrderId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.OrderDate).HasColumnName("OrderDate");
            this.Property(t => t.TransactionId).HasColumnName("TransactionId");
            this.Property(t => t.OrderAmt).HasColumnName("OrderAmt");
            this.Property(t => t.OrderStatusCode).HasColumnName("OrderStatusCode");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.PmtMethod).HasColumnName("PmtMethod");
            this.Property(t => t.CouponAmt).HasColumnName("CouponAmt");
            this.Property(t => t.OrderAppDate).HasColumnName("OrderAppDate");
            this.Property(t => t.LargeDiscountApproverUserId).HasColumnName("LargeDiscountApproverUserId");
            this.Property(t => t.LargeDiscountApproverRequired).HasColumnName("LargeDiscountApproverRequired");
            this.Property(t => t.AE_UserId).HasColumnName("AE_UserId");
            this.Property(t => t.CreditCardInfo).HasColumnName("CreditCardInfo");
            this.Property(t => t.ContactID).HasColumnName("ContactID");
            this.Property(t => t.AR1).HasColumnName("AR1");
            this.Property(t => t.AR1Pct).HasColumnName("AR1Pct");
            this.Property(t => t.AR2).HasColumnName("AR2");
            this.Property(t => t.AR2Pct).HasColumnName("AR2Pct");
            this.Property(t => t.AR3).HasColumnName("AR3");
            this.Property(t => t.AR3Pct).HasColumnName("AR3Pct");
            this.Property(t => t.CorpPct).HasColumnName("CorpPct");
            this.Property(t => t.BillingContact).HasColumnName("BillingContact");
            this.Property(t => t.BillingTermsId).HasColumnName("BillingTermsId");
            this.Property(t => t.BillingTerms).HasColumnName("BillingTerms");

            // Relationships
            this.HasRequired(t => t.Client)
                .WithMany(t => t.Orders)
                .HasForeignKey(d => d.UserId);

        }
    }
}
