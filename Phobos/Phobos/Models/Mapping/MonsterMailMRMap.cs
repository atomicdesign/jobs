using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class MonsterMailMRMap : EntityTypeConfiguration<MonsterMailMR>
    {
        public MonsterMailMRMap()
        {
            // Primary Key
            this.HasKey(t => new { t.id, t.email, t.datesent, t.userid });

            // Properties
            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.userid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MonsterMailMR");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.datesent).HasColumnName("datesent");
            this.Property(t => t.userid).HasColumnName("userid");
        }
    }
}
