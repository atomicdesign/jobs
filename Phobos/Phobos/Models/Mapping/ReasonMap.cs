using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ReasonMap : EntityTypeConfiguration<Reason>
    {
        public ReasonMap()
        {
            // Primary Key
            this.HasKey(t => t.ReasonID);

            // Properties
            this.Property(t => t.ReasonID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Reason1)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Reasons");
            this.Property(t => t.ReasonID).HasColumnName("ReasonID");
            this.Property(t => t.Reason1).HasColumnName("Reason");
        }
    }
}
