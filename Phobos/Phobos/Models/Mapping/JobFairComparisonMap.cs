using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairComparisonMap : EntityTypeConfiguration<JobFairComparison>
    {
        public JobFairComparisonMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairCompareId);

            // Properties
            this.Property(t => t.JobFairCompare)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("JobFairComparison");
            this.Property(t => t.JobFairCompareId).HasColumnName("JobFairCompareId");
            this.Property(t => t.JobFairCompare).HasColumnName("JobFairCompare");
        }
    }
}
