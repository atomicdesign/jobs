using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class wcmgaMap : EntityTypeConfiguration<wcmga>
    {
        public wcmgaMap()
        {
            // Primary Key
            this.HasKey(t => t.UserID);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(30);

            this.Property(t => t.LastName)
                .HasMaxLength(30);

            this.Property(t => t.Address)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.State)
                .HasMaxLength(10);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.Email)
                .HasMaxLength(200);

            this.Property(t => t.HomePhone)
                .HasMaxLength(20);

            this.Property(t => t.WorkPhone)
                .HasMaxLength(30);

            this.Property(t => t.CellPhone)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("wcmga");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.HomePhone).HasColumnName("HomePhone");
            this.Property(t => t.WorkPhone).HasColumnName("WorkPhone");
            this.Property(t => t.CellPhone).HasColumnName("CellPhone");
        }
    }
}
