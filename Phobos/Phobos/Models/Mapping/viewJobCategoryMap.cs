using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class viewJobCategoryMap : EntityTypeConfiguration<viewJobCategory>
    {
        public viewJobCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.JobCategoryID, t.JobCategory });

            // Properties
            this.Property(t => t.JobCategoryID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.JobCategory)
                .IsRequired()
                .HasMaxLength(103);

            // Table & Column Mappings
            this.ToTable("viewJobCategories");
            this.Property(t => t.JobCategoryID).HasColumnName("JobCategoryID");
            this.Property(t => t.JobCategory).HasColumnName("JobCategory");
        }
    }
}
