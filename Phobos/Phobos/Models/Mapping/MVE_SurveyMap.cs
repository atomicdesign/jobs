using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class MVE_SurveyMap : EntityTypeConfiguration<MVE_Survey>
    {
        public MVE_SurveyMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .HasMaxLength(50);

            this.Property(t => t.Company)
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Addr)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.State)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Zip)
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.Q1)
                .HasMaxLength(5000);

            this.Property(t => t.Q2)
                .HasMaxLength(5000);

            this.Property(t => t.Q3)
                .HasMaxLength(5000);

            this.Property(t => t.Q4)
                .HasMaxLength(5000);

            this.Property(t => t.Q5)
                .HasMaxLength(5000);

            this.Property(t => t.Q6)
                .HasMaxLength(5000);

            this.Property(t => t.Q7)
                .HasMaxLength(5000);

            this.Property(t => t.Q8)
                .HasMaxLength(5000);

            this.Property(t => t.Q9)
                .HasMaxLength(5000);

            this.Property(t => t.Q10)
                .HasMaxLength(5000);

            this.Property(t => t.Industry)
                .HasMaxLength(255);

            this.Property(t => t.EmployeeCount)
                .HasMaxLength(10);

            this.Property(t => t.EmployeeMilitaryPct)
                .HasMaxLength(10);

            this.Property(t => t.MilitaryHiresLastYear)
                .HasMaxLength(10);

            this.Property(t => t.CompanyWebsite)
                .HasMaxLength(200);

            this.Property(t => t.NewEmployeeMilitaryPct)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("MVE_Survey");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Addr).HasColumnName("Addr");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Q1).HasColumnName("Q1");
            this.Property(t => t.Q2).HasColumnName("Q2");
            this.Property(t => t.Q3).HasColumnName("Q3");
            this.Property(t => t.Q4).HasColumnName("Q4");
            this.Property(t => t.Q5).HasColumnName("Q5");
            this.Property(t => t.Q6).HasColumnName("Q6");
            this.Property(t => t.Q7).HasColumnName("Q7");
            this.Property(t => t.Q8).HasColumnName("Q8");
            this.Property(t => t.Q9).HasColumnName("Q9");
            this.Property(t => t.Q10).HasColumnName("Q10");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.Completed).HasColumnName("Completed");
            this.Property(t => t.Industry).HasColumnName("Industry");
            this.Property(t => t.EmployeeCount).HasColumnName("EmployeeCount");
            this.Property(t => t.EmployeeMilitaryPct).HasColumnName("EmployeeMilitaryPct");
            this.Property(t => t.MilitaryHiresLastYear).HasColumnName("MilitaryHiresLastYear");
            this.Property(t => t.CompanyWebsite).HasColumnName("CompanyWebsite");
            this.Property(t => t.NewEmployeeMilitaryPct).HasColumnName("NewEmployeeMilitaryPct");
        }
    }
}
