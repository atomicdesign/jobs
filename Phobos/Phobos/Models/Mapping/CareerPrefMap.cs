using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CareerPrefMap : EntityTypeConfiguration<CareerPref>
    {
        public CareerPrefMap()
        {
            // Primary Key
            this.HasKey(t => t.CareerPrefID);

            // Properties
            this.Property(t => t.CareerPrefID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CareerPref1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CareerPrefs");
            this.Property(t => t.CareerPrefID).HasColumnName("CareerPrefID");
            this.Property(t => t.CareerPref1).HasColumnName("CareerPref");
        }
    }
}
