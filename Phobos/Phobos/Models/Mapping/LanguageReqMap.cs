using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class LanguageReqMap : EntityTypeConfiguration<LanguageReq>
    {
        public LanguageReqMap()
        {
            // Primary Key
            this.HasKey(t => t.LanguageID);

            // Properties
            this.Property(t => t.LanguageID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Language)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LanguageReq");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.Language).HasColumnName("Language");
            this.Property(t => t.SpecialOrder).HasColumnName("SpecialOrder");
        }
    }
}
