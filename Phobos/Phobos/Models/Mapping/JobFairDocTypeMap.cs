using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairDocTypeMap : EntityTypeConfiguration<JobFairDocType>
    {
        public JobFairDocTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.DocTypeId);

            // Properties
            this.Property(t => t.DocType)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("JobFairDocType");
            this.Property(t => t.DocTypeId).HasColumnName("DocTypeId");
            this.Property(t => t.DocType).HasColumnName("DocType");
        }
    }
}
