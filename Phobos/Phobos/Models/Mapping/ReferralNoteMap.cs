﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Mapping
{
    public class ReferralNoteMap : EntityTypeConfiguration<ReferralNote>
    {
        public ReferralNoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            
            this.ToTable("ReferralNotes");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.senddate).HasColumnName("senddate");
            this.Property(t => t.sender).HasColumnName("sender");
            this.Property(t => t.receiver).HasColumnName("receiver");
        }
    }
}
