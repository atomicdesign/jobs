using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ActiveDutyMap : EntityTypeConfiguration<ActiveDuty>
    {
        public ActiveDutyMap()
        {
            // Primary Key
            this.HasKey(t => t.ActiveDutyID);

            // Properties
            this.Property(t => t.ActiveDutyID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ActiveDuty1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ActiveDuty");
            this.Property(t => t.ActiveDutyID).HasColumnName("ActiveDutyID");
            this.Property(t => t.ActiveDuty1).HasColumnName("ActiveDuty");
        }
    }
}
