using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairRegistrationMap : EntityTypeConfiguration<JobFairRegistration>
    {
        public JobFairRegistrationMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairRegistrationId);

            // Properties
            this.Property(t => t.HiringFor)
                .HasMaxLength(255);

            this.Property(t => t.AttendanceType)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.Status)
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.DisplaySize)
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("JobFairRegistration");
            this.Property(t => t.JobFairRegistrationId).HasColumnName("JobFairRegistrationId");
            this.Property(t => t.JobFairId).HasColumnName("JobFairId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.HiringFor).HasColumnName("HiringFor");
            this.Property(t => t.AttendanceType).HasColumnName("AttendanceType");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Comments).HasColumnName("Comments");
            this.Property(t => t.ElectricalOutlet).HasColumnName("ElectricalOutlet");
            this.Property(t => t.DisplaySize).HasColumnName("DisplaySize");
            this.Property(t => t.OrderId).HasColumnName("OrderId");
            this.Property(t => t.AttendanceMatchList).HasColumnName("AttendanceMatchList");
            this.Property(t => t.MatchListGeoPref).HasColumnName("MatchListGeoPref");
            this.Property(t => t.Brief).HasColumnName("Brief");
            this.Property(t => t.EmailAnnouncement).HasColumnName("EmailAnnouncement");
            this.Property(t => t.Sponsorship).HasColumnName("Sponsorship");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
        }
    }
}
