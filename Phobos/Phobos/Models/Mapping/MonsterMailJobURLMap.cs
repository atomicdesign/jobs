using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class MonsterMailJobURLMap : EntityTypeConfiguration<MonsterMailJobURL>
    {
        public MonsterMailJobURLMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.url1)
                .HasMaxLength(255);

            this.Property(t => t.url2)
                .HasMaxLength(255);

            this.Property(t => t.url3)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("MonsterMailJobURL");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.url1).HasColumnName("url1");
            this.Property(t => t.url2).HasColumnName("url2");
            this.Property(t => t.url3).HasColumnName("url3");
            this.Property(t => t.date_created).HasColumnName("date_created");
        }
    }
}
