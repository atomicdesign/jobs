﻿
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class NewDesiMap : EntityTypeConfiguration<NewDesi>
    {
        public NewDesiMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties        


            this.ToTable("NewDesis");
            this.Property(t => t.NewDesiField).HasColumnName("NewDesiField");
            this.Property(t => t.Checked).HasColumnName("Checked");
           
         }
    }
}