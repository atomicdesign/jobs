using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class email_errorsMap : EntityTypeConfiguration<email_errors>
    {
        public email_errorsMap()
        {
            // Primary Key
            this.HasKey(t => new { t.EmailID, t.From, t.To, t.Subject, t.Body, t.BodyFormat, t.rowguid });

            // Properties
            this.Property(t => t.From)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.To)
                .IsRequired();

            this.Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(1000);

            this.Property(t => t.Body)
                .IsRequired();

            this.Property(t => t.Attachment1_Filename)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("email_errors");
            this.Property(t => t.EmailID).HasColumnName("EmailID");
            this.Property(t => t.From).HasColumnName("From");
            this.Property(t => t.To).HasColumnName("To");
            this.Property(t => t.CC).HasColumnName("CC");
            this.Property(t => t.BCC).HasColumnName("BCC");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Body).HasColumnName("Body");
            this.Property(t => t.BodyFormat).HasColumnName("BodyFormat");
            this.Property(t => t.DateSent).HasColumnName("DateSent");
            this.Property(t => t.rowguid).HasColumnName("rowguid");
            this.Property(t => t.SendDate).HasColumnName("SendDate");
            this.Property(t => t.DateQueuedUp).HasColumnName("DateQueuedUp");
            this.Property(t => t.Attachment1_DocID).HasColumnName("Attachment1_DocID");
            this.Property(t => t.Attachment1_Filename).HasColumnName("Attachment1_Filename");
        }
    }
}
