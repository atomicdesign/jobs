using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandStateMap : EntityTypeConfiguration<CandState>
    {
        public CandStateMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserID, t.StateID });

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.StateID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandStates");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.StateID).HasColumnName("StateID");
        }
    }
}
