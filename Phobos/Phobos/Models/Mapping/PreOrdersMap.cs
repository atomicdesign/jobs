﻿using CivilianJobs.Models.civjobs;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Mapping
{
    public class PreOrdersMap : EntityTypeConfiguration<PreOrder>
    {
        public PreOrdersMap()
        {
            this.HasKey(t => t.Id);
            
            this.ToTable("PreOrders");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Orders).HasColumnName("Orders");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
        }
    }
}
