using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class OrderDetailMap : EntityTypeConfiguration<OrderDetail>
    {
        public OrderDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.OrderDetailId);

            // Properties
            this.Property(t => t.ProductType)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.UOM)
                .HasMaxLength(3);

            // Table & Column Mappings
            this.ToTable("OrderDetail");
            this.Property(t => t.OrderDetailId).HasColumnName("OrderDetailId");
            this.Property(t => t.OrderId).HasColumnName("OrderId");
            this.Property(t => t.ProductId).HasColumnName("ProductId");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.Qty).HasColumnName("Qty");
            this.Property(t => t.BaseRate).HasColumnName("BaseRate");
            this.Property(t => t.DiscountPct).HasColumnName("DiscountPct");
            this.Property(t => t.DiscountAmt).HasColumnName("DiscountAmt");
            this.Property(t => t.OrderDetailAmt).HasColumnName("OrderDetailAmt");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.UOM).HasColumnName("UOM");
            this.Property(t => t.LineItemDiscountPct).HasColumnName("LineItemDiscountPct");
            this.Property(t => t.LineItemDiscountAmt).HasColumnName("LineItemDiscountAmt");
            this.Property(t => t.LineItemTotal).HasColumnName("LineItemTotal");
            this.Property(t => t.PubId).HasColumnName("PubId");

            // Relationships
            this.HasRequired(t => t.Order)
                .WithMany(t => t.OrderDetails)
                .HasForeignKey(d => d.OrderId);

        }
    }
}
