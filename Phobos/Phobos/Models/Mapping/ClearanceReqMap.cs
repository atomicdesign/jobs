using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ClearanceReqMap : EntityTypeConfiguration<ClearanceReq>
    {
        public ClearanceReqMap()
        {
            // Primary Key
            this.HasKey(t => t.ClearanceReqID);

            // Properties
            this.Property(t => t.ClearanceReqID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ClearanceReq1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ClearanceReq");
            this.Property(t => t.ClearanceReqID).HasColumnName("ClearanceReqID");
            this.Property(t => t.ClearanceReq1).HasColumnName("ClearanceReq");
        }
    }
}
