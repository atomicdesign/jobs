using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class YesNoMap : EntityTypeConfiguration<YesNo>
    {
        public YesNoMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.valu)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("YesNo");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.valu).HasColumnName("valu");
        }
    }
}
