using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ClientMap : EntityTypeConfiguration<Client>
    {
        public ClientMap()
        {
            // Primary Key
            this.HasKey(t => t.UserID);

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CompanyName)
                .HasMaxLength(200);

            this.Property(t => t.ABAL_Notes)
                .HasMaxLength(250);

            this.Property(t => t.JobWrapURL)
                .HasMaxLength(1000);

            this.Property(t => t.JobWrapJobPostingUrl)
                .HasMaxLength(500);

            this.Property(t => t.JobWrapUrlFragment)
                .HasMaxLength(500);

            this.Property(t => t.CompanyWebsite)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Client");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.TotalResumeViews).HasColumnName("TotalResumeViews");
            this.Property(t => t.CurrentResumeViews).HasColumnName("CurrentResumeViews");
            this.Property(t => t.ResumeExpirationDate).HasColumnName("ResumeExpirationDate");
            this.Property(t => t.AE_UserId).HasColumnName("AE_UserId");
            this.Property(t => t.ABAL_ExpireDate).HasColumnName("ABAL_ExpireDate");
            this.Property(t => t.ABAL_Notes).HasColumnName("ABAL_Notes");
            this.Property(t => t.JobWrapURL).HasColumnName("JobWrapURL");
            this.Property(t => t.JobWrapJobPostingUrl).HasColumnName("JobWrapJobPostingUrl");
            this.Property(t => t.JobWrapUrlFragment).HasColumnName("JobWrapUrlFragment");
            this.Property(t => t.CRM_Lead_Created).HasColumnName("CRM_Lead_Created");
            this.Property(t => t.MasterfileGuid).HasColumnName("MasterfileGuid");
            this.Property(t => t.ClientType).HasColumnName("ClientType");
            this.Property(t => t.CompanyWebsite).HasColumnName("CompanyWebsite");
        }
    }
}
