﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobSeekersDetailMap : EntityTypeConfiguration<JobSeekersDetail>
    {
        JobSeekersDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
             
            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
           
                

             this.ToTable("JobSeekersDetails");
             this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Createdon).HasColumnName("Createdon");
            this.Property(t => t.MOSDD).HasColumnName("MOSDD");
            this.Property(t => t.HigherSchool_Deg).HasColumnName("HigherSchool_Deg");
            this.Property(t => t.UnderGrad_Deg).HasColumnName("UnderGrad_Deg");
            this.Property(t => t.Grad_Deg).HasColumnName("Grad_Deg");
            this.Ignore(t => t.JobFairs);
            this.Property(t => t.FurtherHelp).HasColumnName("FurtherHelp");
            this.Property(t => t.Language).HasColumnName("Language");
            this.Property(t => t.Service).HasColumnName("Service");

            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.InstructorDuty).HasColumnName("InstructorDuty");
            this.Property(t => t.Discharged).HasColumnName("Discharged");

            this.Property(t => t.CommissionDate).HasColumnName("CommissionDate");
            this.Property(t => t.SeperationDate).HasColumnName("SeperationDate");
            this.Property(t => t.AvailDate).HasColumnName("AvailDate");

            this.Property(t => t.ConvictedFelory).HasColumnName("ConvictedFelory");
            this.Property(t => t.SecurityClearence).HasColumnName("SecurityClearence");
            this.Property(t => t.MilitaryRelocation).HasColumnName("MilitaryRelocation");
            this.Property(t => t.YrsOfService).HasColumnName("YrsOfService");
           
        }
    }
}