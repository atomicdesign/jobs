using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairRegistrationJobMap : EntityTypeConfiguration<JobFairRegistrationJob>
    {
        public JobFairRegistrationJobMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.JobTitle)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("JobFairRegistrationJobs");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.JobFairRegistrationId).HasColumnName("JobFairRegistrationId");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.JobTitle).HasColumnName("JobTitle");
            this.Property(t => t.SkillId).HasColumnName("SkillId");
            this.Property(t => t.CategoryId).HasColumnName("CategoryId");
        }
    }
}
