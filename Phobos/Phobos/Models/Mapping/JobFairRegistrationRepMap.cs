using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairRegistrationRepMap : EntityTypeConfiguration<JobFairRegistrationRep>
    {
        public JobFairRegistrationRepMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.FirstName)
                .HasMaxLength(25);

            this.Property(t => t.LastName)
                .HasMaxLength(25);

            this.Property(t => t.MiddleName)
                .HasMaxLength(25);

            this.Property(t => t.Phone)
                .HasMaxLength(25);

            this.Property(t => t.DriversLicenseNumber)
                .HasMaxLength(25);

            this.Property(t => t.DriversLicenseState)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("JobFairRegistrationReps");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.JobFairRegistrationId).HasColumnName("JobFairRegistrationId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.DriversLicenseNumber).HasColumnName("DriversLicenseNumber");
            this.Property(t => t.DriversLicenseState).HasColumnName("DriversLicenseState");
            this.Property(t => t.DateOfBirth).HasColumnName("DateOfBirth");
        }
    }
}
