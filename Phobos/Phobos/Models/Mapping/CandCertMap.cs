using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandCertMap : EntityTypeConfiguration<CandCert>
    {
        public CandCertMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserId, t.CertId });

            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CertId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandCert");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.CertId).HasColumnName("CertId");
        }
    }
}
