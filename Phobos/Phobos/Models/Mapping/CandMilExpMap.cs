using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandMilExpMap : EntityTypeConfiguration<CandMilExp>
    {
        public CandMilExpMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserId, t.MilExpId });

            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.MilExpId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandMilExp");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.MilExpId).HasColumnName("MilExpId");
        }
    }
}
