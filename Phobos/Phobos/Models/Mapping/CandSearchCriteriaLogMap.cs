using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandSearchCriteriaLogMap : EntityTypeConfiguration<CandSearchCriteriaLog>
    {
        public CandSearchCriteriaLogMap()
        {
            // Primary Key
            this.HasKey(t => t.SearchID);

            // Properties
            this.Property(t => t.criteria_keyword)
                .HasMaxLength(100);

            this.Property(t => t.criteria_keyword2)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CandSearchCriteriaLog");
            this.Property(t => t.SearchID).HasColumnName("SearchID");
            this.Property(t => t.ClientUserID).HasColumnName("ClientUserID");
            this.Property(t => t.SearchDate).HasColumnName("SearchDate");
            this.Property(t => t.criteria_keyword).HasColumnName("criteria_keyword");
            this.Property(t => t.criteria_city1).HasColumnName("criteria_city1");
            this.Property(t => t.criteria_city2).HasColumnName("criteria_city2");
            this.Property(t => t.criteria_city3).HasColumnName("criteria_city3");
            this.Property(t => t.criteria_jobcat1).HasColumnName("criteria_jobcat1");
            this.Property(t => t.criteria_jobcat2).HasColumnName("criteria_jobcat2");
            this.Property(t => t.criteria_jobcat3).HasColumnName("criteria_jobcat3");
            this.Property(t => t.criteria_clearance).HasColumnName("criteria_clearance");
            this.Property(t => t.criteria_active_duty).HasColumnName("criteria_active_duty");
            this.Property(t => t.criteria_avail_date_range).HasColumnName("criteria_avail_date_range");
            this.Property(t => t.criteria_ethnicity).HasColumnName("criteria_ethnicity");
            this.Property(t => t.criteria_gender).HasColumnName("criteria_gender");
            this.Property(t => t.criteria_keyword2).HasColumnName("criteria_keyword2");
            this.Property(t => t.criteria_and_or).HasColumnName("criteria_and_or");
        }
    }
}
