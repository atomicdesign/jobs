using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ClientTypeMap : EntityTypeConfiguration<ClientType>
    {
        public ClientTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ClientTypeID);

            // Properties
            this.Property(t => t.ClientTypeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ClientType1)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ClientType");
            this.Property(t => t.ClientTypeID).HasColumnName("ClientTypeID");
            this.Property(t => t.ClientType1).HasColumnName("ClientType");
        }
    }
}
