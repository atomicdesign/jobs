using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobSeekerSurveyQuestionMap : EntityTypeConfiguration<JobSeekerSurveyQuestion>
    {
        public JobSeekerSurveyQuestionMap()
        {
            // Primary Key
            this.HasKey(t => t.question_number);

            // Properties
            this.Property(t => t.question_number)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.question)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("JobSeekerSurveyQuestions");
            this.Property(t => t.question_number).HasColumnName("question_number");
            this.Property(t => t.question).HasColumnName("question");
        }
    }
}
