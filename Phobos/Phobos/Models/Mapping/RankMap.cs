using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class RankMap : EntityTypeConfiguration<Rank>
    {
        public RankMap()
        {
            // Primary Key
            this.HasKey(t => t.RankID);

            // Properties
            this.Property(t => t.RankID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Rank1)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Ranks");
            this.Property(t => t.RankID).HasColumnName("RankID");
            this.Property(t => t.Rank1).HasColumnName("Rank");
        }
    }
}
