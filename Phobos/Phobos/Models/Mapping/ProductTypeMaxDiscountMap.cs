using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ProductTypeMaxDiscountMap : EntityTypeConfiguration<ProductTypeMaxDiscount>
    {
        public ProductTypeMaxDiscountMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.ProductType)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("ProductTypeMaxDiscount");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.ClientType).HasColumnName("ClientType");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.MaxDiscount).HasColumnName("MaxDiscount");
        }
    }
}
