using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CertificationMap : EntityTypeConfiguration<Certification>
    {
        public CertificationMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Descr)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Certifications");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Descr).HasColumnName("Descr");
        }
    }
}
