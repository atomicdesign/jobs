using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using CivilianJobs.Models;

namespace CivilianJobs.Models.Mapping
{
    public class JobCategoryMap : EntityTypeConfiguration<JobCategory>
    {
        public JobCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.JobCategoryID);

            // Properties
            this.Property(t => t.JobCategoryID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.JobCategory1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("JobCategory");
            this.Property(t => t.JobCategoryID).HasColumnName("JobCategoryID");
            this.Property(t => t.JobCategory1).HasColumnName("JobCategory");
            this.Property(t => t.SpecialtyArea).HasColumnName("SpecialtyArea");
        }
    }
}
