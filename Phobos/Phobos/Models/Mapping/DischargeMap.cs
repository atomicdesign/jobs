using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class DischargeMap : EntityTypeConfiguration<Discharge>
    {
        public DischargeMap()
        {
            // Primary Key
            this.HasKey(t => t.DischargeID);

            // Properties
            this.Property(t => t.DischargeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Discharge1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Discharge");
            this.Property(t => t.DischargeID).HasColumnName("DischargeID");
            this.Property(t => t.Discharge1).HasColumnName("Discharge");
        }
    }
}
