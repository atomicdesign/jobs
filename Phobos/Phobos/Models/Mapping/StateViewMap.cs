using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class StateViewMap : EntityTypeConfiguration<StateView>
    {
        public StateViewMap()
        {
            // Primary Key
            this.HasKey(t => t.StateID);

            // Properties
            this.Property(t => t.StateID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.State)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("StateView");
            this.Property(t => t.StateID).HasColumnName("StateID");
            this.Property(t => t.State).HasColumnName("State");
        }
    }
}
