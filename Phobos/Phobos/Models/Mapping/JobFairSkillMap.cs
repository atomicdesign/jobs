using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairSkillMap : EntityTypeConfiguration<JobFairSkill>
    {
        public JobFairSkillMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairSkillsID);

            // Properties
            // Table & Column Mappings
            this.ToTable("JobFairSkills");
            this.Property(t => t.JobFairSkillsID).HasColumnName("JobFairSkillsID");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.SkillID).HasColumnName("SkillID");
        }
    }
}
