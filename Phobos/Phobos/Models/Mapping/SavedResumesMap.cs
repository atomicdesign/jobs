using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SavedResumesMap : EntityTypeConfiguration<SavedResumes>
    {
        public SavedResumesMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);
            
            this.ToTable("SavedResumes");
        }
    }
}
