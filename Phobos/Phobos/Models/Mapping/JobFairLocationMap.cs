using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairLocationMap : EntityTypeConfiguration<JobFairLocation>
    {
        public JobFairLocationMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairLocationsID);

            // Properties
            // Table & Column Mappings
            this.ToTable("JobFairLocations");
            this.Property(t => t.JobFairLocationsID).HasColumnName("JobFairLocationsID");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.StateID).HasColumnName("StateID");
        }
    }
}
