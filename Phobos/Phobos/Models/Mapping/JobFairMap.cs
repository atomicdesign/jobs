using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairMap : EntityTypeConfiguration<JobFair>
    {
        public JobFairMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairID);

            // Properties
            this.Property(t => t.Location)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Sponsor)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.SponsorUrl)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("JobFair");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.Sponsor).HasColumnName("Sponsor");
            this.Property(t => t.Mil2Civ).HasColumnName("Mil2Civ");
            this.Property(t => t.SponsorUrl).HasColumnName("SponsorUrl");
        }
    }
}
