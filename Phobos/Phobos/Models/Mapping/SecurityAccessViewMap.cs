using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SecurityAccessViewMap : EntityTypeConfiguration<SecurityAccessView>
    {
        public SecurityAccessViewMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ResourceId, t.ResourceName, t.UserId, t.RoleId });

            // Properties
            this.Property(t => t.ResourceId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ResourceName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RoleId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("SecurityAccessView");
            this.Property(t => t.ResourceId).HasColumnName("ResourceId");
            this.Property(t => t.ResourceName).HasColumnName("ResourceName");
            this.Property(t => t.ResourceTypeId).HasColumnName("ResourceTypeId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
        }
    }
}
