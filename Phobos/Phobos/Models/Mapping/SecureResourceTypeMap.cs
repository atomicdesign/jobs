using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SecureResourceTypeMap : EntityTypeConfiguration<SecureResourceType>
    {
        public SecureResourceTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ResourceTypeId);

            // Properties
            this.Property(t => t.ResourceTypeName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SecureResourceType");
            this.Property(t => t.ResourceTypeId).HasColumnName("ResourceTypeId");
            this.Property(t => t.ResourceTypeName).HasColumnName("ResourceTypeName");
        }
    }
}
