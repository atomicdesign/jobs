using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ExperienceMap : EntityTypeConfiguration<Experience>
    {
        public ExperienceMap()
        {
            // Primary Key
            this.HasKey(t => t.ExperienceID);

            // Properties
            this.Property(t => t.ExperienceID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Experience1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Experience");
            this.Property(t => t.ExperienceID).HasColumnName("ExperienceID");
            this.Property(t => t.Experience1).HasColumnName("Experience");
        }
    }
}
