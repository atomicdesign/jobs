using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandWorkHistoryUpdateLogMap : EntityTypeConfiguration<CandWorkHistoryUpdateLog>
    {
        public CandWorkHistoryUpdateLogMap()
        {
            // Primary Key
            this.HasKey(t => t.email);

            // Properties
            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("CandWorkHistoryUpdateLog");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.date_created).HasColumnName("date_created");
            this.Property(t => t.date_workhistory_added).HasColumnName("date_workhistory_added");
        }
    }
}
