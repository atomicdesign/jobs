using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class PermissionMap : EntityTypeConfiguration<Permission>
    {
        public PermissionMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Permission");
            this.Property(t => t.PermissionId).HasColumnName("PermissionId");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.UserId).HasColumnName("UserId");

            // Relationships
            this.HasMany(t => t.SecureResources)
                .WithMany(t => t.Permissions)
                .Map(m =>
                    {
                        m.ToTable("SecureResourcePermission");
                        m.MapLeftKey("PermissionId");
                        m.MapRightKey("ResourceId");
                    });


        }
    }
}
