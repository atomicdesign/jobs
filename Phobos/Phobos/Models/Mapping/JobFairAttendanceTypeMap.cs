using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairAttendanceTypeMap : EntityTypeConfiguration<JobFairAttendanceType>
    {
        public JobFairAttendanceTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.descr)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("JobFairAttendanceType");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.descr).HasColumnName("descr");
        }
    }
}
