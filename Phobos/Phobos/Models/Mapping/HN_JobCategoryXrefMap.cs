using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class HN_JobCategoryXrefMap : EntityTypeConfiguration<HN_JobCategoryXref>
    {
        public HN_JobCategoryXrefMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CareerCode, t.JobCategoryId });

            // Properties
            this.Property(t => t.CareerCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.JobCategoryId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("HN_JobCategoryXref");
            this.Property(t => t.CareerCode).HasColumnName("CareerCode");
            this.Property(t => t.JobCategoryId).HasColumnName("JobCategoryId");
        }
    }
}
