using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandSurveyMap : EntityTypeConfiguration<CandSurvey>
    {
        public CandSurveyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.JobFairID, t.FirstJobFair, t.HearAbout, t.Quality, t.Satisfied, t.Compare, t.Recommend, t.Employed, t.Presentation });

            // Properties
            this.Property(t => t.JobFairID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Referrals)
                .HasMaxLength(500);

            this.Property(t => t.Improve)
                .HasMaxLength(500);

            this.Property(t => t.Testimonial)
                .HasMaxLength(500);

            this.Property(t => t.CandFName)
                .HasMaxLength(25);

            this.Property(t => t.CandLName)
                .HasMaxLength(25);

            this.Property(t => t.CandEmail)
                .HasMaxLength(200);

            this.Property(t => t.CandPhone)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CandSurvey");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.FirstJobFair).HasColumnName("FirstJobFair");
            this.Property(t => t.HearAbout).HasColumnName("HearAbout");
            this.Property(t => t.Quality).HasColumnName("Quality");
            this.Property(t => t.Satisfied).HasColumnName("Satisfied");
            this.Property(t => t.Compare).HasColumnName("Compare");
            this.Property(t => t.Recommend).HasColumnName("Recommend");
            this.Property(t => t.Referrals).HasColumnName("Referrals");
            this.Property(t => t.Employed).HasColumnName("Employed");
            this.Property(t => t.Presentation).HasColumnName("Presentation");
            this.Property(t => t.Improve).HasColumnName("Improve");
            this.Property(t => t.Testimonial).HasColumnName("Testimonial");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.CandFName).HasColumnName("CandFName");
            this.Property(t => t.CandLName).HasColumnName("CandLName");
            this.Property(t => t.CandEmail).HasColumnName("CandEmail");
            this.Property(t => t.CandPhone).HasColumnName("CandPhone");
        }
    }
}
