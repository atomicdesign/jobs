using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class HiringNeedListMap : EntityTypeConfiguration<HiringNeedList>
    {
        public HiringNeedListMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HiringNeed)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HiringNeedList");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.HiringNeed).HasColumnName("HiringNeed");
            this.Property(t => t.sort_order).HasColumnName("sort_order");
        }
    }
}
