using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class HearAboutMap : EntityTypeConfiguration<HearAbout>
    {
        public HearAboutMap()
        {
            // Primary Key
            this.HasKey(t => t.HearAboutID);

            // Properties
            this.Property(t => t.HearAboutID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.HearAbout1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HearAbout");
            this.Property(t => t.HearAboutID).HasColumnName("HearAboutID");
            this.Property(t => t.HearAbout1).HasColumnName("HearAbout");
        }
    }
}
