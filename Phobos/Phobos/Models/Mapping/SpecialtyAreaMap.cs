using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SpecialtyAreaMap : EntityTypeConfiguration<SpecialtyArea>
    {
        public SpecialtyAreaMap()
        {
            // Primary Key
            this.HasKey(t => t.SpecialtyAreaID);

            // Properties
            this.Property(t => t.SpecialtyAreaID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SpecialtyArea1)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SpecialtyArea");
            this.Property(t => t.SpecialtyAreaID).HasColumnName("SpecialtyAreaID");
            this.Property(t => t.SpecialtyArea1).HasColumnName("SpecialtyArea");
        }
    }
}
