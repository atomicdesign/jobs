using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairDocMap : EntityTypeConfiguration<JobFairDoc>
    {
        public JobFairDocMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairDocId);

            // Properties
            // Table & Column Mappings
            this.ToTable("JobFairDocs");
            this.Property(t => t.JobFairDocId).HasColumnName("JobFairDocId");
            this.Property(t => t.JobFairId).HasColumnName("JobFairId");
            this.Property(t => t.DocId).HasColumnName("DocId");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.DocTypeId).HasColumnName("DocTypeId");
        }
    }
}
