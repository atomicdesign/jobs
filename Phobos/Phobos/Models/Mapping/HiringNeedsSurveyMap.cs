using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class HiringNeedsSurveyMap : EntityTypeConfiguration<HiringNeedsSurvey>
    {
        public HiringNeedsSurveyMap()
        {
            // Primary Key
            this.HasKey(t => t.HiringNeedsSurveyID);

            // Properties
            this.Property(t => t.CompanyName)
                .HasMaxLength(100);

            this.Property(t => t.ContactName)
                .HasMaxLength(50);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(20);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("HiringNeedsSurvey");
            this.Property(t => t.HiringNeedsSurveyID).HasColumnName("HiringNeedsSurveyID");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.ContactName).HasColumnName("ContactName");
            this.Property(t => t.ContactPhone).HasColumnName("ContactPhone");
            this.Property(t => t.ContactEmail).HasColumnName("ContactEmail");
            this.Property(t => t.WantsJobFair).HasColumnName("WantsJobFair");
            this.Property(t => t.WantsCJN).HasColumnName("WantsCJN");
            this.Property(t => t.WantsDirectHire).HasColumnName("WantsDirectHire");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
            this.Property(t => t.WantsJobFairInvite).HasColumnName("WantsJobFairInvite");
            this.Property(t => t.WantsCJ).HasColumnName("WantsCJ");
            this.Property(t => t.WantsTargetHire).HasColumnName("WantsTargetHire");
            this.Property(t => t.WantsTemp2Perm).HasColumnName("WantsTemp2Perm");
            this.Property(t => t.WantsMVE).HasColumnName("WantsMVE");
        }
    }
}
