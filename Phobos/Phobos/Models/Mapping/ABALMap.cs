using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ABALMap : EntityTypeConfiguration<ABAL>
    {
        public ABALMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Notes)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("ABAL");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.MasterfileGuid).HasColumnName("MasterfileGuid");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.ExpirationDate).HasColumnName("ExpirationDate");
            this.Property(t => t.StateID).HasColumnName("StateID");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.LastUpdateUserID).HasColumnName("LastUpdateUserID");
            this.Property(t => t.LastUpdateDate).HasColumnName("LastUpdateDate");
            this.Property(t => t.CreateDate).HasColumnName("CreateDate");
        }
    }
}
