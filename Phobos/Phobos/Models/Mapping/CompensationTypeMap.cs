using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CompensationTypeMap : EntityTypeConfiguration<CompensationType>
    {
        public CompensationTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.CompensationTypeID);

            // Properties
            this.Property(t => t.CompensationType1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("CompensationType");
            this.Property(t => t.CompensationTypeID).HasColumnName("CompensationTypeID");
            this.Property(t => t.CompensationType1).HasColumnName("CompensationType");
        }
    }
}
