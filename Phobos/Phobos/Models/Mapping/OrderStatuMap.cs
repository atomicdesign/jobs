using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class OrderStatuMap : EntityTypeConfiguration<OrderStatu>
    {
        public OrderStatuMap()
        {
            // Primary Key
            this.HasKey(t => t.OrderStatusCode);

            // Properties
            this.Property(t => t.OrderStatusCode)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

            this.Property(t => t.Descr)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("OrderStatus");
            this.Property(t => t.OrderStatusCode).HasColumnName("OrderStatusCode");
            this.Property(t => t.Descr).HasColumnName("Descr");
        }
    }
}
