using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class LeadMap : EntityTypeConfiguration<Lead>
    {
        public LeadMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.fname)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.lname)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.phone)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Leads");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.client_userid).HasColumnName("client_userid");
            this.Property(t => t.fname).HasColumnName("fname");
            this.Property(t => t.lname).HasColumnName("lname");
            this.Property(t => t.email).HasColumnName("email");
            this.Property(t => t.phone).HasColumnName("phone");
            this.Property(t => t.branch).HasColumnName("branch");
            this.Property(t => t.rank).HasColumnName("rank");
            this.Property(t => t.region).HasColumnName("region");
            this.Property(t => t.date_created).HasColumnName("date_created");
        }
    }
}
