using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobWrapStatMap : EntityTypeConfiguration<JobWrapStat>
    {
        public JobWrapStatMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Status)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("JobWrapStats");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.DateWrapped).HasColumnName("DateWrapped");
            this.Property(t => t.JobsInFeed).HasColumnName("JobsInFeed");
            this.Property(t => t.JobsAdded).HasColumnName("JobsAdded");
            this.Property(t => t.JobsSkipped).HasColumnName("JobsSkipped");
            this.Property(t => t.JobsDuplicated).HasColumnName("JobsDuplicated");
            this.Property(t => t.JobsDeleted).HasColumnName("JobsDeleted");
            this.Property(t => t.JobsErrored).HasColumnName("JobsErrored");
            this.Property(t => t.Status).HasColumnName("Status");
        }
    }
}
