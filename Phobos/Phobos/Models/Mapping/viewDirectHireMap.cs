using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class viewDirectHireMap : EntityTypeConfiguration<viewDirectHire>
    {
        public viewDirectHireMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserId, t.Phone1, t.Phone2, t.OverseasPhone, t.Rank, t.Service, t.Skills, t.School, t.Major, t.YearsOfService, t.SepDate, t.Relo, t.AvailDate, t.Locations, t.Salary, t.Jobs, t.AdminNotes });

            // Properties
            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LastNameGuid)
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .HasMaxLength(512);

            this.Property(t => t.Phone1)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Phone2)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.OverseasPhone)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.State)
                .HasMaxLength(50);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.Rank)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Service)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Skills)
                .IsRequired();

            this.Property(t => t.School)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Major)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.YearsOfService)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.SepDate)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Relo)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.AvailDate)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Locations)
                .IsRequired();

            this.Property(t => t.Salary)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Jobs)
                .IsRequired();

            this.Property(t => t.Clearance)
                .HasMaxLength(50);

            this.Property(t => t.AdminNotes)
                .IsRequired()
                .HasMaxLength(2000);

            this.Property(t => t.Degree)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("viewDirectHire");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.LastNameGuid).HasColumnName("LastNameGuid");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone1).HasColumnName("Phone1");
            this.Property(t => t.Phone2).HasColumnName("Phone2");
            this.Property(t => t.OverseasPhone).HasColumnName("OverseasPhone");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.Service).HasColumnName("Service");
            this.Property(t => t.Skills).HasColumnName("Skills");
            this.Property(t => t.School).HasColumnName("School");
            this.Property(t => t.Major).HasColumnName("Major");
            this.Property(t => t.YearsOfService).HasColumnName("YearsOfService");
            this.Property(t => t.SepDate).HasColumnName("SepDate");
            this.Property(t => t.Relo).HasColumnName("Relo");
            this.Property(t => t.AvailDate).HasColumnName("AvailDate");
            this.Property(t => t.Locations).HasColumnName("Locations");
            this.Property(t => t.Salary).HasColumnName("Salary");
            this.Property(t => t.Jobs).HasColumnName("Jobs");
            this.Property(t => t.ClearanceGuid).HasColumnName("ClearanceGuid");
            this.Property(t => t.Clearance).HasColumnName("Clearance");
            this.Property(t => t.AdminNotes).HasColumnName("AdminNotes");
            this.Property(t => t.Degree).HasColumnName("Degree");
            this.Property(t => t.WorkHistoryGuid).HasColumnName("WorkHistoryGuid");
            this.Property(t => t.ReservedForBMI).HasColumnName("ReservedForBMI");
        }
    }
}
