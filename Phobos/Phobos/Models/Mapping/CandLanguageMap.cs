using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandLanguageMap : EntityTypeConfiguration<CandLanguage>
    {
        public CandLanguageMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CandLanguageID, t.UserID, t.LanguageID, t.ProficiencyID });

            // Properties
            this.Property(t => t.CandLanguageID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LanguageID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandLanguages");
            this.Property(t => t.CandLanguageID).HasColumnName("CandLanguageID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.ProficiencyID).HasColumnName("ProficiencyID");
        }
    }
}
