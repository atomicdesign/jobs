using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandSearchResultsLogMap : EntityTypeConfiguration<CandSearchResultsLog>
    {
        public CandSearchResultsLogMap()
        {
            // Primary Key
            this.HasKey(t => new { t.SearchID, t.CandidateUserID });

            // Properties
            this.Property(t => t.SearchID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CandidateUserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandSearchResultsLog");
            this.Property(t => t.SearchID).HasColumnName("SearchID");
            this.Property(t => t.CandidateUserID).HasColumnName("CandidateUserID");
        }
    }
}
