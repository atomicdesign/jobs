using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandReasonMap : EntityTypeConfiguration<CandReason>
    {
        public CandReasonMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserID, t.ReasonID });

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ReasonID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandReasons");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.ReasonID).HasColumnName("ReasonID");
        }
    }
}
