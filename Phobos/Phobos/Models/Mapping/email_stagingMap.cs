using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class email_stagingMap : EntityTypeConfiguration<email_staging>
    {
        public email_stagingMap()
        {
            // Primary Key
            this.HasKey(t => t.email);

            // Properties
            this.Property(t => t.email)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("email_staging");
            this.Property(t => t.email).HasColumnName("email");
        }
    }
}
