﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Mapping
{
    class JobFairAppliersMap : EntityTypeConfiguration<JobFairAppliers>
    {
        public JobFairAppliersMap()
        {
            this.HasKey(t => t.Id);
            this.ToTable("JobFairAppliers");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.JobFairId).HasColumnName("JobFairId");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.IsMailSent).HasColumnName("IsMailSent");
        }
    }
}