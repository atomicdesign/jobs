using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ServiceMap : EntityTypeConfiguration<Service>
    {
        public ServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.ServiceID);

            // Properties
            this.Property(t => t.ServiceID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Service1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Services");
            this.Property(t => t.ServiceID).HasColumnName("ServiceID");
            this.Property(t => t.Service1).HasColumnName("Service");
        }
    }
}
