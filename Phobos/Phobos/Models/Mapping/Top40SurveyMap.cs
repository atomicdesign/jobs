using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class Top40SurveyMap : EntityTypeConfiguration<Top40Survey>
    {
        public Top40SurveyMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(30);

            this.Property(t => t.Addr)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.State)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.Zip)
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.Branch)
                .HasMaxLength(20);

            this.Property(t => t.Rank)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(20);

            this.Property(t => t.HonorableDischarge)
                .HasMaxLength(3);

            this.Property(t => t.RefName)
                .HasMaxLength(50);

            this.Property(t => t.RefTitle)
                .HasMaxLength(50);

            this.Property(t => t.RefCompany)
                .HasMaxLength(50);

            this.Property(t => t.RefEmail)
                .HasMaxLength(50);

            this.Property(t => t.RefPhone)
                .HasMaxLength(20);

            this.Property(t => t.RefAddr)
                .HasMaxLength(50);

            this.Property(t => t.RefCity)
                .HasMaxLength(50);

            this.Property(t => t.RefState)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.RefZip)
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.NameAppears)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Top40Survey");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Addr).HasColumnName("Addr");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.Zip).HasColumnName("Zip");
            this.Property(t => t.Branch).HasColumnName("Branch");
            this.Property(t => t.Rank).HasColumnName("Rank");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.HonorableDischarge).HasColumnName("HonorableDischarge");
            this.Property(t => t.Q1).HasColumnName("Q1");
            this.Property(t => t.Q2).HasColumnName("Q2");
            this.Property(t => t.Q3).HasColumnName("Q3");
            this.Property(t => t.Q6).HasColumnName("Q6");
            this.Property(t => t.RefName).HasColumnName("RefName");
            this.Property(t => t.RefTitle).HasColumnName("RefTitle");
            this.Property(t => t.RefCompany).HasColumnName("RefCompany");
            this.Property(t => t.RefEmail).HasColumnName("RefEmail");
            this.Property(t => t.RefPhone).HasColumnName("RefPhone");
            this.Property(t => t.RefAddr).HasColumnName("RefAddr");
            this.Property(t => t.RefCity).HasColumnName("RefCity");
            this.Property(t => t.RefState).HasColumnName("RefState");
            this.Property(t => t.RefZip).HasColumnName("RefZip");
            this.Property(t => t.NameAppears).HasColumnName("NameAppears");
        }
    }
}
