using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ClientSurveyReasonMap : EntityTypeConfiguration<ClientSurveyReason>
    {
        public ClientSurveyReasonMap()
        {
            // Primary Key
            this.HasKey(t => t.ReasonID);

            // Properties
            this.Property(t => t.Reason)
                .IsRequired()
                .HasMaxLength(70);

            // Table & Column Mappings
            this.ToTable("ClientSurveyReason");
            this.Property(t => t.ReasonID).HasColumnName("ReasonID");
            this.Property(t => t.Reason).HasColumnName("Reason");
        }
    }
}
