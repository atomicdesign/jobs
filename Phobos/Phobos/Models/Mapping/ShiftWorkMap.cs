using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ShiftWorkMap : EntityTypeConfiguration<ShiftWork>
    {
        public ShiftWorkMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.descr)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ShiftWork");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.descr).HasColumnName("descr");
        }
    }
}
