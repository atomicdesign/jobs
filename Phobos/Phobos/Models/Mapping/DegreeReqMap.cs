using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class DegreeReqMap : EntityTypeConfiguration<DegreeReq>
    {
        public DegreeReqMap()
        {
            // Primary Key
            this.HasKey(t => t.DegreeReqID);

            // Properties
            this.Property(t => t.DegreeReqID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DegreeReq1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DegreeReq");
            this.Property(t => t.DegreeReqID).HasColumnName("DegreeReqID");
            this.Property(t => t.DegreeReq1).HasColumnName("DegreeReq");
        }
    }
}
