using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobSeekerSurveyResultMap : EntityTypeConfiguration<JobSeekerSurveyResult>
    {
        public JobSeekerSurveyResultMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            // Table & Column Mappings
            this.ToTable("JobSeekerSurveyResults");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Question).HasColumnName("Question");
            this.Property(t => t.Answer).HasColumnName("Answer");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
        }
    }
}
