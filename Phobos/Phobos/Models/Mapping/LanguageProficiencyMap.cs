using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class LanguageProficiencyMap : EntityTypeConfiguration<LanguageProficiency>
    {
        public LanguageProficiencyMap()
        {
            // Primary Key
            this.HasKey(t => t.ProficiencyID);

            // Properties
            this.Property(t => t.Proficiency)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LanguageProficiency");
            this.Property(t => t.ProficiencyID).HasColumnName("ProficiencyID");
            this.Property(t => t.Proficiency).HasColumnName("Proficiency");
        }
    }
}
