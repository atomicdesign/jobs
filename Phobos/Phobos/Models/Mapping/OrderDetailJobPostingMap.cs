using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class OrderDetailJobPostingMap : EntityTypeConfiguration<OrderDetailJobPosting>
    {
        public OrderDetailJobPostingMap()
        {
            // Primary Key
            this.HasKey(t => t.OrderDetailJobPosting1);

            // Properties
            // Table & Column Mappings
            this.ToTable("OrderDetailJobPosting");
            this.Property(t => t.OrderDetailJobPosting1).HasColumnName("OrderDetailJobPosting");
            this.Property(t => t.OrderDetailID).HasColumnName("OrderDetailID");
            this.Property(t => t.JobPostingID).HasColumnName("JobPostingID");
        }
    }
}
