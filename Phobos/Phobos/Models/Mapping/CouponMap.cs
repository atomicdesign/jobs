using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CouponMap : EntityTypeConfiguration<Coupon>
    {
        public CouponMap()
        {
            // Primary Key
            this.HasKey(t => t.CouponID);

            // Properties
            this.Property(t => t.CouponName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Coupon");
            this.Property(t => t.CouponID).HasColumnName("CouponID");
            this.Property(t => t.CouponName).HasColumnName("CouponName");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.Discount).HasColumnName("Discount");
        }
    }
}
