using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SecureResourceMap : EntityTypeConfiguration<SecureResource>
    {
        public SecureResourceMap()
        {
            // Primary Key
            this.HasKey(t => t.ResourceId);

            // Properties
            this.Property(t => t.ResourceName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SecureResource");
            this.Property(t => t.ResourceId).HasColumnName("ResourceId");
            this.Property(t => t.ResourceName).HasColumnName("ResourceName");
            this.Property(t => t.ResourceTypeId).HasColumnName("ResourceTypeId");
        }
    }
}
