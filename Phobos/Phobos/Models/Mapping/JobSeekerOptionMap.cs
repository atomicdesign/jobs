using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobSeekerOptionMap : EntityTypeConfiguration<JobSeekerOption>
    {
        public JobSeekerOptionMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.descr)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("JobSeekerOptions");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.question_number).HasColumnName("question_number");
            this.Property(t => t.descr).HasColumnName("descr");
        }
    }
}
