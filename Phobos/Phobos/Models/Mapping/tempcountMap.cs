using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class tempcountMap : EntityTypeConfiguration<tempcount>
    {
        public tempcountMap()
        {
            // Primary Key
            this.HasKey(t => t.CompanyName);

            // Properties
            this.Property(t => t.CompanyName)
                .IsRequired()
                .HasMaxLength(27);

            this.Property(t => t.JobPostingExpiration)
                .HasMaxLength(10);

            this.Property(t => t.DateWrapped)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("tempcounts");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.JobPostingExpiration).HasColumnName("JobPostingExpiration");
            this.Property(t => t.DateWrapped).HasColumnName("DateWrapped");
            this.Property(t => t.JobsInFeed).HasColumnName("JobsInFeed");
            this.Property(t => t.JobsAdded).HasColumnName("JobsAdded");
            this.Property(t => t.JobsSkipped).HasColumnName("JobsSkipped");
            this.Property(t => t.JobsWrapped).HasColumnName("JobsWrapped");
            this.Property(t => t.JobsDeleted).HasColumnName("JobsDeleted");
            this.Property(t => t.JobsDuplicated).HasColumnName("JobsDuplicated");
            this.Property(t => t.JobsErrored).HasColumnName("JobsErrored");
        }
    }
}
