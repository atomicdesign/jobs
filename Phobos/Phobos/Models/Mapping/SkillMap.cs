using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class SkillMap : EntityTypeConfiguration<Skill>
    {
        public SkillMap()
        {
            // Primary Key
            this.HasKey(t => t.SkillID);

            // Properties
            this.Property(t => t.SkillID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Skill1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Skills");
            this.Property(t => t.SkillID).HasColumnName("SkillID");
            this.Property(t => t.Skill1).HasColumnName("Skill");
        }
    }
}
