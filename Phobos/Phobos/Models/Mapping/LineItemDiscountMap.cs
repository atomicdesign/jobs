using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class LineItemDiscountMap : EntityTypeConfiguration<LineItemDiscount>
    {
        public LineItemDiscountMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.descr)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LineItemDiscounts");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.pct).HasColumnName("pct");
            this.Property(t => t.descr).HasColumnName("descr");
        }
    }
}
