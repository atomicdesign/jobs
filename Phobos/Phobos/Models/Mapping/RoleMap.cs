using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class RoleMap : EntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            // Primary Key
            this.HasKey(t => t.RoleId);

            // Properties
            this.Property(t => t.RoleName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Role");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.RoleName).HasColumnName("RoleName");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.DefaultRole).HasColumnName("DefaultRole");
            this.Property(t => t.Visible).HasColumnName("Visible");
            this.Property(t => t.ResourceId).HasColumnName("ResourceId");
        }
    }
}
