using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class MedalMap : EntityTypeConfiguration<Medal>
    {
        public MedalMap()
        {
            // Primary Key
            this.HasKey(t => t.MedalID);

            // Properties
            this.Property(t => t.MedalID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Medal1)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Medals");
            this.Property(t => t.MedalID).HasColumnName("MedalID");
            this.Property(t => t.Medal1).HasColumnName("Medal");
        }
    }
}
