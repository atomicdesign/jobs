using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandPositionMap : EntityTypeConfiguration<CandPosition>
    {
        public CandPositionMap()
        {
            // Primary Key
            this.HasKey(t => t.PositionID);

            // Properties
            this.Property(t => t.StartDate)
                .HasMaxLength(20);

            this.Property(t => t.EndDate)
                .HasMaxLength(20);

            this.Property(t => t.Company)
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .HasMaxLength(50);

            this.Property(t => t.Descr)
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("CandPositions");
            this.Property(t => t.PositionID).HasColumnName("PositionID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Descr).HasColumnName("Descr");
            this.Property(t => t.PositionNumber).HasColumnName("PositionNumber");
        }
    }
}
