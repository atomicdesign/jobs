using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class CandJobCategoryMap : EntityTypeConfiguration<CandJobCategory>
    {
        public CandJobCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.UserID, t.JobCategoryID });

            // Properties
            this.Property(t => t.UserID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.JobCategoryID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("CandJobCategories");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.JobCategoryID).HasColumnName("JobCategoryID");
        }
    }
}
