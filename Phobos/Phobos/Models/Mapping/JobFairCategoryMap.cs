using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class JobFairCategoryMap : EntityTypeConfiguration<JobFairCategory>
    {
        public JobFairCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.JobFairCategoryID);

            // Properties
            // Table & Column Mappings
            this.ToTable("JobFairCategories");
            this.Property(t => t.JobFairCategoryID).HasColumnName("JobFairCategoryID");
            this.Property(t => t.JobFairID).HasColumnName("JobFairID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.JobCategoryID).HasColumnName("JobCategoryID");
        }
    }
}
