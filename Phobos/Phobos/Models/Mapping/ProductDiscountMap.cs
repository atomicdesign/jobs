using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class ProductDiscountMap : EntityTypeConfiguration<ProductDiscount>
    {
        public ProductDiscountMap()
        {
            // Primary Key
            this.HasKey(t => t.DiscountId);

            // Properties
            this.Property(t => t.ProductType)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("ProductDiscount");
            this.Property(t => t.DiscountId).HasColumnName("DiscountId");
            this.Property(t => t.ProductId).HasColumnName("ProductId");
            this.Property(t => t.ProductType).HasColumnName("ProductType");
            this.Property(t => t.ProductQtyId).HasColumnName("ProductQtyId");
            this.Property(t => t.DiscountPct).HasColumnName("DiscountPct");

            // Relationships
            this.HasRequired(t => t.ProductQty)
                .WithMany(t => t.ProductDiscounts)
                .HasForeignKey(d => d.ProductQtyId);

        }
    }
}
