using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class QualityRatingMap : EntityTypeConfiguration<QualityRating>
    {
        public QualityRatingMap()
        {
            // Primary Key
            this.HasKey(t => t.QualityRatingId);

            // Properties
            this.Property(t => t.QualityRating1)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("QualityRating");
            this.Property(t => t.QualityRatingId).HasColumnName("QualityRatingId");
            this.Property(t => t.QualityRating1).HasColumnName("QualityRating");
        }
    }
}
