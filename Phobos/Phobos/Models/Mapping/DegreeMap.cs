using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class DegreeMap : EntityTypeConfiguration<Degree>
    {
        public DegreeMap()
        {
            // Primary Key
            this.HasKey(t => t.DegreeID);

            // Properties
            this.Property(t => t.DegreeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Degree1)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Degree");
            this.Property(t => t.DegreeID).HasColumnName("DegreeID");
            this.Property(t => t.Degree1).HasColumnName("Degree");
            this.Property(t => t.GraduateLevel).HasColumnName("GraduateLevel");
        }
    }
}
