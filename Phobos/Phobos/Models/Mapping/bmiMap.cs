using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class bmiMap : EntityTypeConfiguration<bmi>
    {
        public bmiMap()
        {
            // Primary Key
            this.HasKey(t => t.bmi_id);

            // Properties
            this.Property(t => t.bmi_value)
                .IsRequired()
                .HasMaxLength(30);

            // Table & Column Mappings
            this.ToTable("bmi");
            this.Property(t => t.bmi_id).HasColumnName("bmi_id");
            this.Property(t => t.bmi_value).HasColumnName("bmi_value");
        }
    }
}
