using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace CivilianJobs.Models.Mapping
{
    public class YearsOfServiceMap : EntityTypeConfiguration<YearsOfService>
    {
        public YearsOfServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.YearsID);

            // Properties
            this.Property(t => t.YearsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Years)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("YearsOfService");
            this.Property(t => t.YearsID).HasColumnName("YearsID");
            this.Property(t => t.Years).HasColumnName("Years");
        }
    }
}
