﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Jobs
{
    public class JobPostingsImport
    {
        public List<JobPostingImportModel> models { get; set; }
    }

    public  class JobPostingImportModel
    {
        public JobPostings Posting { get; set; }
        public List<int> CategoryIds { get; set; }
    }
}
