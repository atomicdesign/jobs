﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.ViewModels
{
    public class Cart
    {
        public Cart()
        {
            OrderDetails = new List<OrderDetail>();
        }
        [Key]
        public int CartId { get; set; }
        public uint Total { get; set; }
        public System.DateTime DateCreated { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
    }

    public class OrderDetail
    {
        public int  Id { get; set; }
        public string Description { get; set; }
        public uint Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public uint SubTotal { get; set; }
    }
}