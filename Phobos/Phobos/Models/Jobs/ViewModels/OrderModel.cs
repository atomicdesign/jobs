﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.ViewModels
{
    public class OrderModel
    {
        public int ID { get; set; }

        public DateTime CreateDate { get; set; }

        public bool Active { get; set; }

        public int JobPostingMonths { get; set; }

        public int ResumeAccessViewsPerDay { get; set; }

        public int ResumeAccessMonthsAccess { get; set; }
        public int ResumeAccessMonthsAccess200 { get; set; }
        public int ResumeAccessMonthsAccess400 { get; set; }
        public int ResumeAccessMonthsAccess600 { get; set; }

        public int CombinationJobPostingResumeAccess { get; set; }

        public string PrintAdvertising { get; set; }

        public int BannerAdvertising { get; set; }

        public bool PreSelectedResumes { get; set; }

        public bool PostJobsAtMilitaryBaseTransitionOffices { get; set; }

        public string JobFairPackage { get; set; }
        public bool JobFairPackage30DayResumeAccess { get; set; }
        public bool JobFairPackage30DayUnlimitedJobPosting { get; set; }
        public bool JobFairTargetedEmailCampaign { get; set; }
        public bool JobFair1PageCompanyBrief { get; set; }
        public bool JobFairMatchingProcess { get; set; }
        public bool JobFairCivilianJobNewsEighthPageAd { get; set; }
        public bool JobFairCivilianJobNewsQuarterPageAd { get; set; }
        public bool JobFairCivilianJobNewsHalfPageAd { get; set; }
        public bool JobFairCivilianJobNewsFullPageColorAd { get; set; }
        public string JobFairIds { get; set; }

        public float Total { get; set; }
    }
}