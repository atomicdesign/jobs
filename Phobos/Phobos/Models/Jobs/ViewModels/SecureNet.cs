﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.ViewModels
{

    /// <summary>
    /// Credit Card Specific Data. 
    /// </summary>
    public class Card
    {
        #region Properties

        /// <summary>
        /// Data that has been read from the card's magnetic stripe.
        /// </summary>
        [JsonProperty("trackData")]
        public String TrackData { get; set; }

        /// <summary>
        /// Credit card account number.
        /// </summary>
        [JsonProperty("number")]
        public String Number { get; set; }

        /// <summary>
        /// Card security code.
        /// </summary>
        [JsonProperty("cvv")]
        public String Cvv { get; set; }

        /// <summary>
        /// Expiration date for the card. Format: MM/YYYY.
        /// </summary>
        [JsonProperty("expirationDate")]
        public string ExpirationDate { get; set; }

        /// <summary>
        /// Unaltered KSN Number from PIN pad. (Debit only.)
        /// </summary>
        [JsonProperty("ksn")]
        public String Ksn { get; set; }

        /// <summary>
        /// Pinblock obtained from the PIN pad. (Debit only.)
        /// </summary>
        [JsonProperty("pinBlock")]
        public String PinBlock { get; set; }

        /// <summary>
        /// First name of the account holder.
        /// </summary>
        [JsonProperty("firstName")]
        public String FirstName { get; set; }

        /// <summary>
        /// Last name of the account holder.
        /// </summary>
        [JsonProperty("lastName")]
        public String LastName { get; set; }

        /// <summary>
        /// Billing address of the account holder.
        /// </summary>
        [JsonProperty("address")]
        public Address Address { get; set; }

        /// <summary>
        /// Image of the signature of the cardholder as completed at the time of the transaction.
        /// </summary>
        [JsonProperty("signature")]
        public byte[] Signature { get; set; }

        #endregion
    }

    /// <summary>
    /// Contains information related to the integration.
    /// </summary>
    public class DeveloperApplication
    {
        #region Properties

        /// <summary>
        /// Developer ID of integrator as assigned by SecureNet.
        /// </summary>
        [JsonProperty("developerId")]
        public int DeveloperId { get; set; }

        /// <summary>
        /// Version number of the integrator's application.
        /// </summary>
        [JsonProperty("version")]
        public String Version { get; set; }

        #endregion
    }

    /// <summary>
    /// Account holder address details.
    /// </summary>
    public class Address
    {
        #region Properties

        /// <summary>
        /// Street address.
        /// </summary>
        [JsonProperty("line1")]
        public string Line1 { get; set; }

        /// <summary>
        /// City where address is located.
        /// </summary>
        [JsonProperty("city")]

        public string City { get; set; }

        /// <summary>
        /// State where address is located; valid values are 2-character state abbreviations.
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }

        /// <summary>
        /// 5- or 9-digit zip code.
        /// </summary>
        [JsonProperty("zip")]
        public string Zip { get; set; }

        /// <summary>
        /// Country name. Defaults to US.
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        #endregion
    }

    public class ChargeRequest
    {
        /// <summary>
        /// Amount to charge the account.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Credit-card-specific data. Required for credit card charges.
        /// </summary>
        public Card Card { get; set; }

        /// <summary>
        /// Contains developer Id and version information related to the integration.
        /// </summary>
        public DeveloperApplication DeveloperApplication { get; set; }

        /// <summary>
        /// Indicates whether the card object is to be added to the vault to be stored as a token.
        /// </summary>
        public bool AddToVault { get; set; }

        /// <summary>
        /// Indicates whether the card object is to be added to the vault to be stored as a token even if the attempted authorization is declined.
        /// </summary>
        public bool AddToVaultOnFailure { get; set; }

        /// <summary>
        /// Typically used as an optional field for pin debit processing. The value of cashBackAmount indicates the amount in cash to be given back to the customer during card processing.
        /// </summary>
        public decimal CashBackAmount { get; set; }

        /// <summary>
        /// Indicates whether it is permissible to authorize less than the total balance available on a prepaid card.
        /// </summary>
        public bool AllowPartialChanges { get; set; }

        /// <summary>
        /// Additional data to assist in reporting, ecommerce or moto transactions, and level 2 or level 3 processing. Includes user-defined fields and invoice-related information.
        /// </summary>
        public ExtendedInformation ExtendedInformation { get; set; }

        /// <summary>
        /// Indicates how checks for duplicate transactions should behave. Duplicates are evaluated on the basis of amount, card number, and order ID; these evaluation criteria can be 
        /// extended to also include customer ID, invoice number, or a user-defined field. Valid values for this parameter are:
        /// 0 - No duplicate check 
        /// 1 - Exception code is returned in case of duplicate
        /// 2 - Previously existing transaction is returned in case of duplicate
        /// 3 - Check is performed as above but without using order ID, and exception code is returned in case of duplicate 
        /// The transactionDuplicateCheckIndicator parameter must be enabled in the Virtual Terminal under Tools->Duplicate Transactions. Duplicates are checked only for APPROVED transactions.
        /// </summary>
        public int TransactionDuplicateCheckIndicator { get; set; }

        /// <summary>
        /// Client-generated unique ID for each transaction, used as a way to prevent the processing of duplicate transactions. The orderId must be unique to the merchant's SecureNet ID; 
        /// however, uniqueness is only evaluated for APPROVED transactions and only for the last 30 days. If a transaction is declined, the corresponding orderId may be used again.
        /// The orderId is limited to 25 characters; e.g., “CUSTOMERID MMddyyyyHHmmss”.
        /// </summary>
        public string OrderId { get; set; }
    }

    /// <summary>
    /// Additional data to assist in reporting, ecommerce or moto transactions, and level 2 or level 3 processing.
    /// </summary>
    public class ExtendedInformation
    {
        #region Properties

        /// <summary>
        ///  Type of goods that are being purchased. Valid values are DIGITAL or PHYSICAL. 
        /// </summary>
        [JsonProperty("typeOfGoods")]
        public String TypeOfGoods { get; set; }

        /// Device code of the device where the transaction originated.
        /// </summary>
        [JsonProperty("deviceCode")]
        public String DeviceCode { get; set; }

        /// <summary>
        /// Entry source of the transaction.
        /// </summary>
        [JsonProperty("entrySource")]
        public String EntrySource { get; set; }

        /// <summary>
        /// Notes associated with the transaction.
        /// </summary>
        [JsonProperty("notes")]
        public String Notes { get; set; }

        /// <summary>
        /// Invoice number.
        /// </summary>
        [JsonProperty("invoiceNumber")]
        public String InvoiceNumber { get; set; }

        /// <summary>
        /// Invoice description.
        /// </summary>
        [JsonProperty("invoiceDescription")]
        public String InvoiceDescription { get; set; }

        #endregion
    }

    public class SecureNetResponse
    {
        #region Properties

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }

        [JsonProperty("responseCode")]
        public int ResponseCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("responseDateTime")]
        public string ResponseDateTime { get; set; }

        [JsonProperty("rawRequest")]
        public object RawRequest { get; set; }

        [JsonProperty("rawResponse")]
        public object RawResponse { get; set; }

        [JsonProperty("jsonRequest")]
        public object JsonRequest { get; set; }

        [JsonProperty("transaction")]
        public Transaction Transaction { get; set; }

        #endregion
    }

    public class Transaction
    {
        #region Properties

        [JsonProperty("secureNetId")]
        public int SecureNetId { get; set; }

        [JsonProperty("transactionType")]
        public string TransactionType { get; set; }

        [JsonProperty("orderId")]
        public string OrderId { get; set; }

        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("authorizationCode")]
        public string AuthorizationCode { get; set; }

        [JsonProperty("authorizedAmount")]
        public double AuthorizedAmount { get; set; }

        [JsonProperty("allowedPartialCharges")]
        public bool AllowedPartialCharges { get; set; }

        [JsonProperty("paymentTypeCode")]
        public string PaymentTypeCode { get; set; }

        [JsonProperty("paymentTypeResult")]
        public string PaymentTypeResult { get; set; }

        [JsonProperty("level2Valid")]
        public bool Level2Valid { get; set; }

        [JsonProperty("level3Valid")]
        public bool Level3Valid { get; set; }

        [JsonProperty("transactionData")]
        public TransactionData TransactionData { get; set; }

        [JsonProperty("settlementData")]
        public object SettlementData { get; set; }

        [JsonProperty("vaultData")]
        public object VaultData { get; set; }

        [JsonProperty("creditCardType")]
        public string CreditCardType { get; set; }

        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty("avsCode")]
        public string AvsCode { get; set; }

        [JsonProperty("avsResult")]
        public string AvsResult { get; set; }

        [JsonProperty("cardHolder_FirstName")]
        public string CardHolderFirstName { get; set; }

        [JsonProperty("cardHolder_LastName")]
        public string CardHolderLastName { get; set; }

        [JsonProperty("expirationDate")]
        public string ExpirationDate { get; set; }

        [JsonProperty("billAddress")]
        public Address BillAddress { get; set; }

        [JsonProperty("email")]
        public object Email { get; set; }

        [JsonProperty("phone")]
        public object Phone { get; set; }

        [JsonProperty("company")]
        public object Company { get; set; }

        [JsonProperty("cardCodeCode")]
        public string CardCodeCode { get; set; }

        [JsonProperty("cardCodeResult")]
        public string CardCodeResult { get; set; }

        [JsonProperty("accountName")]
        public object AccountName { get; set; }

        [JsonProperty("accountType")]
        public object AccountType { get; set; }

        [JsonProperty("accountNumber")]
        public object AccountNumber { get; set; }

        [JsonProperty("checkNumber")]
        public object CheckNumber { get; set; }

        [JsonProperty("traceNumber")]
        public object TraceNumber { get; set; }

        [JsonProperty("surchargeAmount")]
        public double SurchargeAmount { get; set; }

        [JsonProperty("cashbackAmount")]
        public double CashbackAmount { get; set; }

        [JsonProperty("fnsNumber")]
        public object FnsNumber { get; set; }

        [JsonProperty("voucherNumber")]
        public object VoucherNumber { get; set; }

        [JsonProperty("fleetCardInfo")]
        public object FleetCardInfo { get; set; }

        [JsonProperty("gratuity")]
        public double Gratuity { get; set; }

        [JsonProperty("industrySpecificData")]
        public string IndustrySpecificData { get; set; }

        [JsonProperty("marketSpecificData")]
        public string MarketSpecificData { get; set; }

        [JsonProperty("networkCode")]
        public string NetworkCode { get; set; }

        [JsonProperty("additionalAmount")]
        public double AdditionalAmount { get; set; }

        [JsonProperty("additionalData1")]
        public object AdditionalData1 { get; set; }

        [JsonProperty("additionalData2")]
        public object AdditionalData2 { get; set; }

        [JsonProperty("additionalData3")]
        public object AdditionalData3 { get; set; }

        [JsonProperty("additionalData4")]
        public string AdditionalData4 { get; set; }

        [JsonProperty("additionalData5")]
        public string AdditionalData5 { get; set; }

        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("imageResult")]
        public object ImageResult { get; set; }

        #endregion
    }

    public class TransactionData
    {
        #region Properties

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        #endregion
    }
}