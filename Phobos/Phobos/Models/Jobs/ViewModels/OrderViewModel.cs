﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.ViewModels
{
    public class OrderViewModel
    {
        public Card Card { get; set; }

        public Address Address { get; set; }

        public string ExpirationMonth { get; set; }

        public string ExpirationYear { get; set; }
    }
}