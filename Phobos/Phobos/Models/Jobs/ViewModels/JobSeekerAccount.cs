﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs.Enums;

namespace CivilianJobs.Models.Jobs.ViewModels
{
    public class JobSeekerAccount
    {
        public UserProfile User { get; set; } 

        public IEnumerable<JobApplication> JobApplications { get; set; }

        public IEnumerable<SavedJob> SavedJobs { get; set; }

        public IEnumerable<Document> Documents { get; set; }
    }
}