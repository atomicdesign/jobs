﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs.Enums;

namespace CivilianJobs.Models.Jobs.ViewModels
{
    public class PostJob
    {
        public JobPostings JobPosting { get; set; }

        public IEnumerable<JobCategories> JobCategories { get; set; }

        public SelectList EmployeeTypes { get; set; }

        public SelectList ExperienceTypes { get; set; }

        public SelectList DegreeTypes { get; set; }

        public SelectList TravelTypes { get; set; }

        public SelectList SecurityTypes { get; set; }

        public SelectList ForeignLanguageTypes { get; set; }

        public SelectList LocationTypes { get; set; }
    }
}