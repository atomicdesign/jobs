﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Jobs.ViewModels
{
    public class detailedOrder
    {
        public string package1 { get; set; }
        public string title1 { get; set; }
        public int quantity1 { get; set; }
        public decimal price1 { get; set; }
        public string type1 { get; set; }
    }
    public class detOrders
    {
        public Orders order { get; set; }
        public List<detailedOrder> data { get; set; }
    }
}
