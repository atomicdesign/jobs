﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Enums
{
    /// <summary>
    /// Note that the descriptions are intended to match the membership system's role names.
    /// </summary>
    public enum JobsUserRole
    {
        [Description("Employer")]
        Employer = 1,

        [Description("Job Seeker")]
        JobSeeker = 2,

        [Description("Account Representative")]
        AccountRepresentative = 3
    }

    public enum EmployeeType 
    {
        [Description("Full Time")]
        FullTime = 1,

        [Description("Part Time")]
        PartTime = 2,

        [Description("Contract")]
        Contract = 3,

        [Description("Intern")]
        Intern = 4
    }

    public enum ExperienceRequired
    {
        [Description("None")]
        None = 1,

        [Description("College")]
        College = 2,

        [Description("Less than 1 Year")]
        LessThanOneYear = 3,

        [Description("At least 1 Year")]
        OnePlusYears = 4,

        [Description("At least 3 Years")]
        ThreePlusYears = 5,

        [Description("More than 5 Years")]
        FivePlusYears = 6
    }

    public enum DegreeRequired
    {
        [Description("None")]
        None = 1,

        [Description("High School")]
        HighSchool = 2,

        [Description("Less than 1 Year")]
        LessThanOneYear = 3,

        [Description("2yr College Degree")]
        TwoYearCollege = 4,

        [Description("4yr College Degree")]
        FourYearCollege = 5,

        [Description("Graduate Degree")]
        GraduateDegree = 6
    }

    public enum TravelRequired
    {
        [Description("None")]
        None = 1,

        [Description("Negligible")]
        Negligible = 2,

        [Description("Up to 25%")]
        percent25 = 3,

        [Description("Up to 50%")]
        percent50 = 4,

        [Description("More than 50%")]
        MoreThan50 = 5
    }

    public enum SecurityClearance
    {
        [Description("Not Required")]
        NotRequired = 1,

        [Description("Confidential or higher")]
        ConfidentialOrHigher = 2,

        [Description("Secret or higher")]
        SecretOrHigher = 3,

        [Description("Top Secret or higher")]
        TopSecretOrHigher = 4,

        [Description("Lifestyle Polygraph")]
        LifestylePolygraph = 5
    }

    public enum ForeignLanguage
    {
        [Description("Not Required")]
        NonRequired = 0,
        [Description("Not Required")]
        NotRequired = 1,

        [Description("Arabic")]
        Arabic = 2,

        [Description("Chinese")]
        Chinese = 3,

        [Description("Farsi")]
        Farsi = 4,

        [Description("French")]
        French = 5,

        [Description("German")]
        German = 6,

        [Description("Italian")]
        Italian = 7,

        [Description("Japanese")]
        Japanese = 8,

        [Description("Korean")]
        Korean = 9,

        [Description("Other")]
        Other = 10,

        [Description("Portuguese")]
        Portuguese = 11,

        [Description("Russian")]
        Russian = 12,

        [Description("Spanish")]
        Spanish = 13,

        [Description("Tagalog")]
        Tagalog = 14,

        [Description("Urdu")]
        Urdu = 15,
    }

    public enum JobLocation
    {

        [Description("AK")]
        AK = 269,

        [Description("AK Anchorage")]
        AKAnchorage = 7,

        [Description("AK Fairbanks")]
        AKFairbanks = 8,

        [Description("AK Juneau")]
        AKJuneau = 10,

        [Description("AK Nome")]
        AKNome = 9,

        [Description("AK Southwest")]
        AKSouthwest = 11,

        [Description("AL")]
        AL = 268,

        [Description("AL Anniston")]
        ALAnniston = 1,

        [Description("AL Birmingham")]
        ALBirmingham = 2,

        [Description("AL Huntsville")]
        ALHuntsville = 5,

        [Description("AL Mobile")]
        ALMobile = 3,

        [Description("AL Montgomery")]
        ALMontgomery = 4,

        [Description("AL Tuscaloosa")]
        ALTuscaloosa = 6,

        [Description("Anywhere")]
        Anywhere = 314,

        [Description("AR")]
        AR = 271,

        [Description("AR Eastern")]
        AREastern = 16,

        [Description("AR Little Rock")]
        ARLittleRock = 17,

        [Description("AR Western")]
        ARWestern = 18,

        [Description("AZ")]
        AZ = 270,

        [Description("AZ Flagstaff")]
        AZFlagstaff = 12,

        [Description("AZ Phoenix")]
        AZPhoenix = 13,

        [Description("AZ Tucson")]
        AZTucson = 14,

        [Description("AZ Yuma")]
        AZYuma = 15,

        [Description("CA")]
        CA = 272,

        [Description("CA Anaheim")]
        CAAnaheim = 19,

        [Description("CA Central Coast")]
        CACentralCoast = 20,

        [Description("CA Central Valley")]
        CACentralValley = 21,

        [Description("CA Eureka")]
        CAEureka = 22,

        [Description("CA Long Beach")]
        CALongBeach = 23,

        [Description("CA Los Angeles")]
        CALosAngeles = 24,

        [Description("CA Marin/North Bay")]
        CAMarinNorthBay = 25,

        [Description("CA Oakland")]
        CAOakland = 26,

        [Description("CA Orange County")]
        CAOrangeCounty = 27,

        [Description("CA Riverside")]
        CARiverside = 28,

        [Description("CA Sacramento")]
        CASacramento = 29,

        [Description("CA San Bernardino")]
        CASanBernardino = 30,

        [Description("CA San Diego")]
        CASanDiego = 31,

        [Description("CA San Francisco")]
        CASanFrancisco = 32,

        [Description("CA Santa Barbara")]
        CASantaBarbara = 33,

        [Description("CA Silicon Valley")]
        CASiliconValley = 34,

        [Description("CN")]
        CN = 316,

        [Description("CN Calgary")]
        CNCalgary = 317,

        [Description("CN Halifax")]
        CNHalifax = 318,

        [Description("CN Montreal")]
        CNMontreal = 319,

        [Description("CN Ottawa")]
        CNOttawa = 320,

        [Description("CN Toronto")]
        CNToronto = 321,

        [Description("CN Vancouver")]
        CNVancouver = 322,

        [Description("CO")]
        CO = 273,

        [Description("CO Boulder")]
        COBoulder = 37,

        [Description("CO Colorado Springs")]
        COColoradoSprings = 38,

        [Description("CO Denver")]
        CODenver = 39,

        [Description("CO Grand Junction")]
        COGrandJunction = 41,

        [Description("CO Ventura County")]
        COVenturaCounty = 36,

        [Description("CT")]
        CT = 274,

        [Description("CT Bridgeport")]
        CTBridgeport = 42,

        [Description("CT Hartford")]
        CTHartford = 43,

        [Description("CT New London")]
        CTNewLondon = 45,

        [Description("CT Stamford")]
        CTStamford = 46,

        [Description("CT New Haven")]
        CTNewHaven = 44,

        [Description("DC")]
        DC = 48,

        [Description("DE")]
        DE = 47,

        [Description("FL")]
        FL = 275,

        [Description("FL Clearwater")]
        FLClearwater = 313,

        [Description("FL Daytona")]
        FLDaytona = 50,

        [Description("FL Ft. Lauderdale")]
        FLFtLauderdale = 51,

        [Description("FL Ft. Myers")]
        FLFtMyers = 52,

        [Description("FL Jacksonville")]
        FLJacksonville = 53,

        [Description("FL Melbourne")]
        FLMelbourne = 54,

        [Description("FL Miami")]
        FLMiami = 55,

        [Description("FL Orlando")]
        FLOrlando = 56,

        [Description("FL Pensacola")]
        FLPensacola = 57,

        [Description("FL Sarasota")]
        FLSarasota = 58,

        [Description("FL St. Petersburg")]
        FLStPetersburg = 59,

        [Description("FL Tallahassee")]
        FLTallahassee = 60,

        [Description("FL Tampa")]
        FLTampa = 61,

        [Description("FL West Palm Beach")]
        FLWestPalmBeach = 62,

        [Description("GA")]
        GA = 276,

        [Description("GA Albany")]
        GAAlbany = 315,

        [Description("GA Athens")]
        GAAthens = 67,

        [Description("GA Atlanta")]
        GAAtlanta = 63,

        [Description("GA Augusta")]
        GAAugusta = 66,

        [Description("GA Columbus")]
        GAColumbus = 69,

        [Description("GA Savannah")]
        GASavannah = 68,

        [Description("HI")]
        HI = 70,

        [Description("IA")]
        IA = 280,

        [Description("IA Cedar Rapids")]
        IACedarRapids = 91,

        [Description("IA Davenport")]
        IADavenport = 93,

        [Description("IA Des Moines")]
        IADesMoines = 92,

        [Description("IA Dubuque")]
        IADubuque = 94,

        [Description("IA Sioux City")]
        IASiouxCity = 95,

        [Description("ID")]
        ID = 277,

        [Description("ID Boise")]
        IDBoise = 71,

        [Description("ID Twin Falls")]
        IDTwinFalls = 72,

        [Description("IL")]
        IL = 278,

        [Description("IL Bloomington")]
        ILBloomington = 74,

        [Description("IL Chicago")]
        ILChicago = 75,

        [Description("IL Northern")]
        ILNorthern = 73,

        [Description("IL Quincy")]
        ILQuincy = 80,

        [Description("IL Rockford")]
        ILRockford = 81,

        [Description("IL Southern")]
        ILSouthern = 82,

        [Description("IL Springfield")]
        ILSpringfield = 83,

        [Description("IN")]
        IN = 279,

        [Description("IN Evansville")]
        INEvansville = 84,

        [Description("IN Fort Wayne")]
        INFortWayne = 85,

        [Description("IN Gary")]
        INGary = 86,

        [Description("IN Indianapolis")]
        INIndianapolis = 87,

        [Description("IN Lafayette")]
        INLafayette = 88,

        [Description("IN South Bend")]
        INSouthBend = 89,

        [Description("IN Terre Haute")]
        INTerreHaute = 90,

        [Description("KS")]
        KS = 281,

        [Description("KS Kansas City")]
        KSKansasCity = 96,

        [Description("KS Overland Park")]
        KSOverlandPark = 97,

        [Description("KS Topeka")]
        KSTopeka = 98,

        [Description("KS Wichita")]
        KSWichita = 99,

        [Description("KY")]
        KY = 282,

        [Description("KY Lexington")]
        KYLexington = 101,

        [Description("KY Louisville")]
        KYLouisville = 102,

        [Description("KY Northern Kentucky")]
        KYNorthernKentucky = 103,

        [Description("KY Paducah")]
        KYPaducah = 100,

        [Description("LA")]
        LA = 283,

        [Description("LA Alexandria")]
        LAAlexandria = 104,

        [Description("LA Baton Rouge")]
        LABatonRouge = 105,

        [Description("LA Lafayette")]
        LALafayette = 106,

        [Description("LA New Orleans")]
        LANewOrleans = 107,

        [Description("LA Northern")]
        LANorthern = 108,

        [Description("MA")]
        MA = 286,

        [Description("MA Boston")]
        MABoston = 119,

        [Description("MA Cape Cod")]
        MACapeCod = 122,

        [Description("MA Framingham")]
        MAFramingham = 123,

        [Description("MA Springfield")]
        MASpringfield = 124,

        [Description("MD")]
        MD = 285,

        [Description("MD Anne Arundel County")]
        MDAnneArundelCounty = 112,

        [Description("MD Baltimore")]
        MDBaltimore = 113,

        [Description("MD Columbia")]
        MDColumbia = 114,

        [Description("MD Frederick/Hagerstown")]
        MDFrederickHagerstown = 115,

        [Description("MD Montgomery County")]
        MDMontgomeryCounty = 116,

        [Description("MD Prince George's County")]
        MDPrinceGeorgesCounty = 117,

        [Description("MD Salisbury")]
        MDSalisbury = 118,

        [Description("ME")]
        ME = 284,

        [Description("ME Augusta")]
        MEAugusta = 109,

        [Description("ME Bangor")]
        MEBangor = 110,

        [Description("ME Portland")]
        MEPortland = 111,

        [Description("MI")]
        MI = 287,

        [Description("MI Ann Arbor")]
        MIAnnArbor = 125,

        [Description("MI Detroit")]
        MIDetroit = 126,

        [Description("MI Flint")]
        MIFlint = 127,

        [Description("MI Grand Rapids")]
        MIGrandRapids = 128,

        [Description("MI Kalamazoo")]
        MIKalamazoo = 129,

        [Description("MI Lansing")]
        MILansing = 130,

        [Description("MN")]
        MN = 288,

        [Description("MN Duluth")]
        MNDuluth = 134,

        [Description("MN Minneapolis")]
        MNMinneapolis = 133,

        [Description("MN Northern")]
        MNNorthern = 131,

        [Description("MN Rochester")]
        MNRochester = 132,

        [Description("MN St. Paul")]
        MNStPaul = 135,

        [Description("MO")]
        MO = 290,

        [Description("MO Jefferson City")]
        MOJeffersonCity = 139,

        [Description("MO Kansas City")]
        MOKansasCity = 140,

        [Description("MO Northeastern")]
        MONortheastern = 141,

        [Description("MO Quincy")]
        MOQuincy = 142,

        [Description("MO Southeastern")]
        MOSoutheastern = 143,

        [Description("MO Springfield/Joplin")]
        MOSpringfieldJoplin = 144,

        [Description("MO St. Louis")]
        MOStLouis = 145,

        [Description("MS")]
        MS = 289,

        [Description("MS Central")]
        MSCentral = 136,

        [Description("MS Northern")]
        MSNorthern = 137,

        [Description("MS Southern")]
        MSSouthern = 138,

        [Description("MT")]
        MT = 291,

        [Description("MT Billings")]
        MTBillings = 146,

        [Description("MT Great Falls")]
        MTGreatFalls = 147,

        [Description("MT Helena")]
        MTHelena = 148,

        [Description("MT Missoula")]
        MTMissoula = 149,

        [Description("National")]
        National = 999,

        [Description("NC")]
        NC = 297,

        [Description("NC Asheville")]
        NCAsheville = 177,

        [Description("NC Charlotte")]
        NCCharlotte = 173,

        [Description("NC Greensboro")]
        NCGreensboro = 175,

        [Description("NC Greenville")]
        NCGreenville = 174,

        [Description("NC Raleigh/Durham")]
        NCRaleighDurham = 176,

        [Description("NCWilmington")]
        NCWilmington = 178,

        [Description("NC Winston Salem")]
        NCWinstonSalem = 179,

        [Description("ND")]
        ND = 298,

        [Description("ND Central")]
        NDCentral = 180,

        [Description("ND Eastern")]
        NDEastern = 181,

        [Description("ND Western")]
        NDWestern = 182,

        [Description("NE")]
        NE = 292,

        [Description("NE Lincoln")]
        NELincoln = 150,

        [Description("NE North Platte")]
        NENorthPlatte = 152,

        [Description("NE Omaha")]
        NEOmaha = 151,

        [Description("NH")]
        NH = 155,

        [Description("NJ")]
        NJ = 294,

        [Description("NJ Central")]
        NJCentral = 158,

        [Description("NJ Northern")]
        NJNorthern = 159,

        [Description("NJ Southern")]
        NJSouthern = 160,

        [Description("NM")]
        NM = 295,

        [Description("NM Albuquerque")]
        NMAlbuquerque = 161,

        [Description("NM Santa Fe")]
        NMSantaFe = 162,

        [Description("NV")]
        NV = 293,

        [Description("NV Las Vegas")]
        NVLasVegas = 153,

        [Description("NV Reno")]
        NVReno = 154,

        [Description("NY")]
        NY = 296,

        [Description("NY Albany")]
        NYAlbany = 163,

        [Description("NY Binghamton")]
        NYBinghamton = 164,

        [Description("NY Buffalo")]
        NYBuffalo = 165,

        [Description("NY Long Island")]
        NYLongIsland = 166,

        [Description("NY New York City")]
        NYNewYorkCity = 167,

        [Description("NY Newburgh")]
        NYNewburgh = 323,

        [Description("NY Northern")]
        NYNorthern = 168,

        [Description("NY Rochester")]
        NYRochester = 169,

        [Description("NY Syracuse")]
        NYSyracuse = 170,

        [Description("NY Utica")]
        NYUtica = 171,

        [Description("NY Westchester")]
        NYWestchester = 172,

        [Description("OH")]
        OH = 299,

        [Description("OH Akron")]
        OHAkron = 183,

        [Description("OH Cincinnati")]
        OHCincinnati = 184,

        [Description("OH Cleveland")]
        OHCleveland = 185,

        [Description("OH Columbus")]
        OHColumbus = 186,

        [Description("OH Dayton")]
        OHDayton = 187,

        [Description("OH Middletown")]
        OHMiddletown = 188,

        [Description("OH Northwest")]
        OHNorthwest = 189,

        [Description("OH Toledo")]
        OHToledo = 190,

        [Description("OH Youngstown")]
        OHYoungstown = 191,

        [Description("OK")]
        OK = 300,

        [Description("OK Oklahoma City")]
        OKOklahomaCity = 192,

        [Description("OK Tulsa")]
        OKTulsa = 193,

        [Description("OR")]
        OR = 301,

        [Description("OR Central")]
        ORCentral = 194,

        [Description("OR Portland")]
        ORPortland = 195,

        [Description("OR Salem")]
        ORSalem = 196,

        [Description("OR Southern")]
        ORSouthern = 197,

        [Description("Overseas")]
        Overseas = 267,

        [Description("PA")]
        PA = 302,

        [Description("PA Allentown")]
        PAAllentown = 198,

        [Description("PAErie")]
        PAErie = 199,

        [Description("PA Harrisburg")]
        PAHarrisburg = 200,

        [Description("PA Johnstown")]
        PAJohnstown = 201,

        [Description("PA Philadelphia")]
        PAPhiladelphia = 202,

        [Description("PA Pittsburgh")]
        PAPittsburgh = 203,

        [Description("PA Reading")]
        PAReading = 204,

        [Description("PA State College")]
        PAStateCollege = 205,

        [Description("PA Wilkes Barre")]
        PAWilkesBarre = 206,

        [Description("PA York/Lancaster")]
        PAYorkLancaster = 207,

        [Description("PR San Juan")]
        PRSanJuan = 208,

        [Description("RI")]
        RI = 209,

        [Description("RI Providence")]
        RIProvidence = 210,

        [Description("SC")]
        SC = 303,

        [Description("SC Charleston")]
        SCCharleston = 214,

        [Description("SC Columbia")]
        SCColumbia = 211,

        [Description("SC Greenville")]
        SCGreenville = 213,

        [Description("SC Myrtle Beach")]
        SCMyrtleBeach = 212,

        [Description("SD")]
        SD = 215,

        [Description("SD Rapid City")]
        SDRapidCity = 217,

        [Description("SD Sioux Falls")]
        SDSiouxFalls = 216,

        [Description("TN")]
        TN = 304,

        [Description("TN Chattanooga")]
        TNChattanooga = 218,

        [Description("TN Jackson")]
        TNJackson = 219,

        [Description("TN Knoxville")]
        TNKnoxville = 220,

        [Description("TN Memphis")]
        TNMemphis = 221,

        [Description("TN Nashville")]
        TNNashville = 222,

        [Description("TX")]
        TX = 305,

        [Description("TX Abilene/Odessa")]
        TXAbileneOdessa = 223,

        [Description("TX Amarillo/Lubbock")]
        TXAmarilloLubbock = 224,

        [Description("TX Austin")]
        TXAustin = 225,

        [Description("TX Beaumont")]
        TXBeaumont = 227,

        [Description("TX Corpus Christi")]
        TXCorpusChristi = 232,

        [Description("TX Dallas")]
        TXDallas = 226,

        [Description("TX El Paso")]
        TXElPaso = 228,

        [Description("TX Fort Worth")]
        TXFortWorth = 229,

        [Description("TX Houston")]
        TXHouston = 230,

        [Description("TX San Antonio")]
        TXSanAntonio = 231,

        [Description("TX Waco")]
        TXWaco = 233,

        [Description("TX Wichita Falls")]
        TXWichitaFalls = 234,

        [Description("UT")]
        UT = 306,

        [Description("UT Provo")]
        UTProvo = 235,

        [Description("UT Salt Lake City")]
        UTSaltLakeCity = 236,

        [Description("UT St. George")]
        UTStGeorge = 237,

        [Description("VA")]
        VA = 308,

        [Description("VA Alexandria")]
        VAAlexandria = 243,

        [Description("VA Arlington")]
        VAArlington = 246,

        [Description("VA Charlottesville")]
        VACharlottesville = 244,

        [Description("VA Fairfax")]
        VAFairfax = 245,

        [Description("VA Norfolk")]
        VANorfolk = 247,

        [Description("VA Northern")]
        VANorthern = 248,

        [Description("VA Richmond")]
        VARichmond = 249,

        [Description("VA Roanoke")]
        VARoanoke = 250,

        [Description("VA Southwest")]
        VASouthwest = 251,

        [Description("VA Vienna")]
        VAVienna = 252,

        [Description("VI St. Croix")]
        VIStCroix = 240,

        [Description("VI St. John")]
        VIStJohn = 241,

        [Description("VI St. Thomas")]
        VIStThomas = 242,

        [Description("VT")]
        VT = 307,

        [Description("VT Northern")]
        VTNorthern = 238,

        [Description("VT Southern")]
        VTSouthern = 239,

        [Description("WA")]
        WA = 309,

        [Description("WA Bellevue")]
        WABellevue = 253,

        [Description("WA Seattle")]
        WASeattle = 256,

        [Description("WA Spokane")]
        WASpokane = 255,

        [Description("WA Tacoma")]
        WATacoma = 257,

        [Description("WA Yakima")]
        WAYakima = 254,

        [Description("WI")]
        WI = 311,

        [Description("WI Eau Claire")]
        WIEauClaire = 260,

        [Description("WI Green Bay")]
        WIGreenBay = 261,

        [Description("WI Madison")]
        WIMadison = 262,

        [Description("WI Milwaukee")]
        WIMilwaukee = 263,

        [Description("WI Northern")]
        WINorthern = 264,

        [Description("WV")]
        WV = 310,

        [Description("WV Northern")]
        WVNorthern = 258,

        [Description("WV Southern")]
        WVSouthern = 259,

        [Description("WY")]
        WY = 312,

        [Description("WY Casper")]
        WYCasper = 265,

        [Description("WY Cheyenne")]
        WYCheyenne = 266,

    }

    public enum HowHeardAbout
    {

        [Description("ACAPTAP")]
        ACAPTAP = 1,

        [Description("BaseNewspaper")]
        BaseNewspaper= 2,

        [Description("BradleyMorris")]
        BradleyMorris= 3,

        [Description("CivilianJobNews")]
        CivilianJobNews= 4,

        [Description("ContactedByPhone")]
        ContactedByPhone= 5,

        [Description("CorporateGray")]
        CorporateGray= 6,

        [Description("Email")]
        Email = 7,

        [Description("InternetSearchEngine")]
        InternetSearchEngine = 8,

        [Description("JobFair")]
        JobFair = 9,

        [Description("LocalMarketingRep")]
        LocalMarketingRep= 10,

        [Description("MailerDirectMail")]
        MailerDirectMail= 11,

        [Description("MilitaryAssociation")]
        MilitaryAssociation= 12,

        [Description("MilitaryHousing")]
        MilitaryHousing= 13,

        [Description("MilitaryTimes")]
        MilitaryTimes= 14,

        [Description("Radio")]
        Radio= 15,

        [Description("Referral")]
        Referral = 16,

        [Description("TradeJournal")]
        TradeJournal = 17,

        [Description("TradeMagazine")]
        TradeMagazine= 18,

    }

}


