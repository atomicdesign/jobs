﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs
{
    public  class  JobPostingForUI
    {
        public string Location { get; set; }
        public string Url { get; set; }
        public string Company { get; set; }
        public string Compensation { get; set; }
        public string ExpirationDate { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }

    }
    public class JobPostingResult
    {
        public List<JobPostingForUI> objects { get; set; }
        public int count { get; set; }
    }
    public class ResumeResult
    {
        public List<Resume> objects { get; set; }
        public int count { get; set; }
    }
    public class ResumeSearchResult
    {
        public List<JobSeekersDetail> objects { get; set; }

        //public Resume Resume { get; set; }
        public int count { get; set; }

        public JobSeekersDetail jobseeker { get; set; }
    }

}