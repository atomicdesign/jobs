﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class JobApplication
    {
        public int ID { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsHidden { get; set; }

        public JobPostings JobPosting { get; set; }

        public UserProfile UserProfile { get; set; }

       public JobSeekersDetail Resume { get; set; }
       public int? JobPostingID { get; set; }

       public int UserProfileID { get; set; }

       public int? ResumeID { get; set; }
        public bool? Starred { get; set; }
        public int? StatusId { get; set; }
        [NotMapped]
        public string DateString { get; set; }
    }
}