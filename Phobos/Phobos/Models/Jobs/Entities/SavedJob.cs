﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class SavedJob
    {
        public int ID { get; set; }

        public JobPostings JobPosting { get; set; }

        public UserProfile UserProfile { get; set; }
    }
}