﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class TransitionNewsArticle
    {
        public int ID { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }

        public string TeaserText { get; set; }

        public string Content { get; set; }
        public string Img { get; set; }
    }
}