﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class JobCategories
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int? ParentCategoryID { get; set; }
    }
}