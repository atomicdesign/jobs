﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class PressRelease
    {
        public int ID { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; }

        public string TeaserText { get; set; }

        public string Content { get; set; }

        //1 = Employer, 2 = Job Seeker, 3 = Both
        //public int type { get; set; }
    }
}