﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class Document
    {
        public int ID { get; set; }

        public UserProfile UserProfile { get; set; }

        public string FileName { get; set; }

        public string Description { get; set; }
    }
}