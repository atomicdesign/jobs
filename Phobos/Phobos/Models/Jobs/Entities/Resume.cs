﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class Resume
    {
        public int ID { get; set; }

        public UserProfile UserProfile { get; set; }

        public string MostRecentJobTitle { get; set; }

        public string DesiredPay { get; set; }

        public string MilitarySpecialty1 { get; set; }
        public string MilitarySpecialty2 { get; set; }
        public string MilitarySpecialty3 { get; set; }   
    }
}