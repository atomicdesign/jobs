﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class JobFairs
    {
        public int ID { get; set; }

        public DateTime createDate { get; set; }

        [Required]
        public string sponsoredBy { get; set; }

        [Required]
        public string dayOfWeek { get; set; }

        [Required]
        public string month { get; set; }

        [Required]
        public string day { get; set; }

        [Required]
        public string year { get; set; }

        [Required]
        public string startTime { get; set; }

        [Required]
        public string startTimeAMPM { get; set; }

        [Required]
        public string endTime { get; set; }

        [Required]
        public string endTimeAMPM { get; set; }

        [Required]
        public string locationName { get; set; }

        [Required]
        public string locationAddress { get; set; }

        [Required]
        public string locationCity { get; set; }

        [Required]
        public string locationState { get; set; }

        [Required]
        public string locationZip { get; set; }

        public string opportunity1 { get; set; }

        public string opportunity2 { get; set; }

        public string opportunity3 { get; set; }

        public string opportunity4 { get; set; }

        public string opportunity5 { get; set; }

        public string opportunity6 { get; set; }

        public string opportunity7 { get; set; }

        public string opportunity8 { get; set; }

        public string opportunity9 { get; set; }

        public string opportunity10 { get; set; }
    }
}