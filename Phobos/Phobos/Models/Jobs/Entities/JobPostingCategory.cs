﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class JobPostingCategory
    {
        public int ID { get; set; }

        public JobPostings JobPosting { get; set; }

        public JobCategories JobCategory { get; set; }
    }
}