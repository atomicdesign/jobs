﻿using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.Jobs.Entities
{
    public class Microsite
    {
        public int ID { get; set; }

        public string CompanyName { get; set; }

        public string MissionStatement { get; set; }

        public string Content { get; set; }

        public string AdditionSectionTitle { get; set; }

        public string AdditionSectionText { get; set; }

        public string FacebookURL { get; set; }

        public string TwitterURL { get; set; }

        public string CompanyURL { get; set; }

    }
}