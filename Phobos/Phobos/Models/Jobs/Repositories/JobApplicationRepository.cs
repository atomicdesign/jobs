﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class JobApplicationRepository : IRepository<JobApplication>
    {
        public int Add(JobApplication entity)
        {
            throw new NotImplementedException(); 
        }

        public int Add(string username, int jobPostingId, bool starred=false)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var jobApplication = new JobApplication();

                try
                {
                    var userprof = ctx.UserProfiles.First(x => x.UserName == username);
                    jobApplication.CreateDate = DateTime.Now;
                    jobApplication.JobPostingID = ctx.JobPostings.First(x => x.ID == jobPostingId).ID;
                    var resume = ctx.JobSeekersDetails.FirstOrDefault(x => x.UserId == userprof.UserId);
                    jobApplication.ResumeID = resume == null ? null : (int?)resume.Id;
                    jobApplication.UserProfileID = userprof.UserProfileID;
                    jobApplication.Starred = (bool?)starred;
                    jobApplication.StatusId = 0;
                    ctx.JobApplications.Add(jobApplication);
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }

                    throw dbEx;
                }

                return jobApplication.ID;
            }
        }

        public void Update(JobApplication entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public IEnumerable<JobApplication> Get()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobApplications.ToList();
            }
        }

        public IEnumerable<JobApplication> GetTop100()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobApplications.OrderByDescending(x=>x.CreateDate).ToList();
            }
        }

        public int GetCount()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobApplications.Count();
            }
        }

        public JobApplication Get(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobApplications.First(x => x.ID == id);
            }
        }

        public JobApplication Get(string username, int jobPostingId)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobApplications.FirstOrDefault(x => x.UserProfile.UserName == username && x.JobPosting.ID == jobPostingId);
            }
        }

        public IEnumerable<JobApplication> GetByApplicant(string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobApplications.Include(x => x.JobPosting).Where(x => x.UserProfile.UserName == username).ToList();
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobApplications.Remove(ctx.JobApplications.First(x => x.ID == id));
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
