﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public interface IRepository<T>
    {
        int Add(T t);
        void Update(T t);
        IEnumerable<T> Get();
        T Get(int id);
        bool Delete(int id);
    }
}
