﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class DocumentRepository : IRepository<Document>
    {
        public int Add(Document entity, string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                try
                {
                    entity.UserProfile = ctx.UserProfiles.First(x => x.UserName == username);

                    ctx.Documents.Add(entity);
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }

                    throw dbEx;
                }

                return entity.ID;
            }
        }

        public int Add(Document document)
        {
            throw new NotImplementedException();
        }


        public void Update(Document document)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Document> Get(string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.Documents.Where(x => x.UserProfile.UserName == username).ToList();
            }
        }

        public IEnumerable<Document> Get()
        {
            throw new NotImplementedException();
        }


        public Document Get(int id)
        {
            throw new NotImplementedException();

        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
