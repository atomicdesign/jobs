﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class SavedJobRepository : IRepository<SavedJob>
    {
        public int Add(SavedJob entity)
        {
            throw new NotImplementedException(); 
        }

        public int Add(string username, int jobPostingId)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var savedJob = new SavedJob();

                try
                {
                    savedJob.JobPosting = ctx.JobPostings.First(x => x.ID == jobPostingId);
                    savedJob.UserProfile = ctx.UserProfiles.First(x => x.UserName == username);

                    ctx.SavedJobs.Add(savedJob);
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }

                    throw dbEx;
                }

                return savedJob.ID;
            }
        }
        public IEnumerable<SavedJob> Get()
        {
            throw new Exception();
        }

        public void Update(SavedJob savedJob)
        {
            throw new NotImplementedException();
        }

        public IQueryable<SavedJob> Get(string key1, string key2)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.SavedJobs;
            }

        }

        public SavedJob Get(int id)
        {
            throw new NotImplementedException();

        }
        public List<int> GetIds(int userId)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.SavedJobs.Where(x => x.UserProfile.UserProfileID == userId).Select(x => x.JobPosting.ID).ToList();
            }
        }

        public SavedJob Get(string username, int jobPostingId)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.SavedJobs.FirstOrDefault(x => x.UserProfile.UserName == username && x.JobPosting.ID == jobPostingId);
            }
        }

        public IEnumerable<SavedJob> Get(string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.SavedJobs.Include(x => x.JobPosting).Where(x => x.UserProfile.UserName == username).ToList();
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var item = ctx.SavedJobs.Find(id);
                ctx.Entry(item).State = EntityState.Deleted;
                ctx.SaveChanges();
                return true;
            }
        }

        public bool Delete( string username, int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var item = ctx.SavedJobs.FirstOrDefault(x => x.JobPosting.ID == id && x.UserProfile.UserName == username);
                ctx.Entry(item).State = EntityState.Deleted;
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
