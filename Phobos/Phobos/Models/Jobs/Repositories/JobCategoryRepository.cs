﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class JobCategoryRepository : IRepository<JobCategories>
    {
        public int Add(JobCategories entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobCategories.Add(entity);
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public void Update(JobCategories entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public IEnumerable<JobCategories> Get()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobCategories.ToList();
            }
        }

        public JobCategories Get(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobCategories.First(x => x.ID == id);
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobCategories.Remove(ctx.JobCategories.First(x => x.ID == id));
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
