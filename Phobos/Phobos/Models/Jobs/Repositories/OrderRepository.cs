﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class OrderRepository : IRepository<Orders>
    {
        public int Add(Orders order)
        {
            throw new NotImplementedException();
        }

        public int Add(Orders entity, string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                entity.UserProfile = ctx.UserProfiles.First(x => x.UserName == username);

                ctx.Orders.Add(entity);
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public void Update(Orders entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public IEnumerable<Orders> Get()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.Orders.Include(x => x.UserProfile).ToList();
            }
        }

        public Orders Get(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.Orders.Include(x => x.UserProfile).First(x => x.ID == id);
            }
        }

        public Orders Get(string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.Orders.FirstOrDefault(x => x.UserProfile.UserName == username);
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Orders.Remove(ctx.Orders.First(x => x.ID == id));
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
