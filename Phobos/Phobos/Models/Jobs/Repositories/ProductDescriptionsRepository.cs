﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class ProductDescriptionsRepository
    {
        public void create(ProductDescription desc)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.ProductDescriptions.Add(desc);
                ctx.SaveChanges();
            }
        }

        public IEnumerable<ProductDescription> Get(int userId)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.ProductDescriptions.Where(x => x.userId == userId).ToList();
            }
        }
    }
}
