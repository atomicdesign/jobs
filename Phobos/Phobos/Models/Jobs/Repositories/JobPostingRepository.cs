﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;
using CivilianJobs.Helpers;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class JobPostingRepository : IRepository<JobPostings>
    {
        public int Add(JobPostings entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobPostings.Add(entity);
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public int Add(JobPostings entity, IEnumerable<JobCategories> jobCategories)
        {
            using (var ctx = new CivilianJobsContext())
            {
                entity.CreateDate = DateTime.Now;

                ctx.JobPostings.Add(entity);
                ctx.SaveChanges();

                jobCategories.ToList().ForEach(x => ctx.JobPostingCategories.Add(new JobPostingCategory(){ JobPosting = entity, JobCategory = ctx.JobCategories.First(jc => jc.ID == x.ID) }));
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public void Update(JobPostings entity)
        {
            throw new NotImplementedException();
        }

        public void Update(JobPostings entity, IEnumerable<JobCategories> jobCategories)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;

                // clear the current categories
                ctx.JobPostingCategories.Where(x => x.JobPosting.ID == entity.ID).ToList().ForEach(x => ctx.JobPostingCategories.Remove(x));

                // add the new
                jobCategories.ToList().ForEach(x => ctx.JobPostingCategories.Add(new JobPostingCategory() { JobPosting = entity, JobCategory = ctx.JobCategories.First(jc => jc.ID == x.ID) }));
                ctx.SaveChanges();
            }
        }

        public IEnumerable<JobPostings> Get()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobPostings.ToList();
            }
        }
        public int GetCount()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobPostings.Count();
            }
        }
        public IEnumerable<JobPostings> GetTop100()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobPostings.OrderByDescending(x=>x.CreateDate).Take(100).ToList();
            }
        }

        public JobPostingResult Get(int index, int count, int sorting, HttpRequestBase request, UrlHelper url, string title, string keyword, string location, string company, string postingDate, bool partTime, bool fullTime, bool contract, bool internship, bool temp, bool other, List<int> savedIds =null)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var result = new JobPostingResult();
                result.objects = new List<JobPostingForUI>();
                bool startSavedLoad = savedIds == null;
                savedIds = savedIds == null ? new List<int>() : savedIds;
                var objs = ctx.JobPostings.AsQueryable();
                switch (sorting)
                {
                    case 0:
                        objs = objs.OrderByDescending(x => x.CreateDate);
                        break;
                    case 1:
                        objs = objs.OrderBy(x => x.JobLocation);
                        break;
                    default:
                        break;
                }
                DateTime? date = string.IsNullOrEmpty(postingDate) ? null : (DateTime?)DateTime.Parse(postingDate);
                var t = Enums.JobLocation.AK.GetType();
                var dictReturn = Enum.GetValues(typeof(Enums.JobLocation))
               .Cast<Enums.JobLocation>()
               .ToDictionary(q => (int)q, q => q.ToString());

                var dics = dictReturn.Where(x => string.IsNullOrEmpty(location) || x.Value.ToUpper().StartsWith(location.ToUpper().Replace(" ", string.Empty))).
                    Select(x => x.Key).ToList();

                objs = objs.Where(x => (string.IsNullOrEmpty(title) || x.Title != null && x.Title.ToLower().Contains(title.ToLower())) &&
                (string.IsNullOrEmpty(company) || x.Company != null && x.Company.ToLower().Contains(company.ToLower())) &&
                (date == null || x.CreateDate == date.Value) &&
                (string.IsNullOrEmpty(location) || dics.Contains(x.JobLocation)) &&
                (string.IsNullOrEmpty(keyword) ||
               x.Title != null && x.Title.ToLower().Contains(keyword.ToLower()) ||
                x.Description != null && x.Description.ToLower().Contains(keyword.ToLower()) ||
                x.JobRequireDescription != null && x.JobRequireDescription.ToLower().Contains(keyword.ToLower()))
                && ((fullTime && x.EmployeeType == 1)
                || (partTime && x.EmployeeType == 2)
                || (contract && x.EmployeeType == 3)
                || (internship && x.EmployeeType == 4)
                || (temp && x.EmployeeType == 5)
                || (other && x.EmployeeType == 6))
                && (startSavedLoad || savedIds.Contains(x.ID)));
                result.count = objs.Count();
                var objects = objs.Skip((index - 1) * count).Take(count).ToList();
                foreach (var item in objects)
                {
                    result.objects.Add(new JobPostingForUI
                    {
                        Title = item.Title,
                        Id = item.ID,
                        Compensation = item.Compensation,
                        ExpirationDate = item.ExpirationDate.ToShortDateString(),
                        Location = CivilianJobsHelper.GetEnumDescription((CivilianJobs.Models.Jobs.Enums.JobLocation)item.JobLocation),
                        Company = item.Company,
                        Url = "<a id=\"" + item.ID + "\" class=\"p-c\" href =\"javascript:void(0)\" onclick=\"OpenJobPageGlobal(\'" +
                        url.Action("job", "JobSeeker", new { id = item.ID }, request.Url.Scheme)
                        + "\')\"> " + item.Title + "</a>"
                    });
                }
                return result;
            }
        }
        public IEnumerable<JobPostings> Get(string createdBy)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var result =  ctx.JobPostings
                            .Include(x => x.Applications)
                            .Include(x => x.Categories)
                            .Where(x => x.CreatedBy == createdBy).ToList();
                result.ForEach(x=>
                {
                    x.StatusName = x.Status == 0 ? "Open" : (x.Status == 1 ? "Paused" : "Closed");
                    x.ExpDays = (x.ExpirationDate - DateTime.Now).Days;
                    if(x.ExpDays <= 0)
                    {
                        x.StatusName = "Expired";
                        x.ExpDays = 0;
                        x.ViewCount = x.ViewCount == null ? 0 : x.ViewCount.Value;
                        }
                });
                return result;
            }
        }

        public IEnumerable<JobPostings> GetLatest()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobPostings.OrderByDescending(x=>x.CreateDate).Take(5).ToList();
            }
        }

        public JobPostings Get(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobPostings
                            .Include(x => x.Applications)
                            .Include(x => x.Categories.Select(y => y.JobCategory))
                            .First(x => x.ID == id);
            }
        }

        public void UpdateViewsCount(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var jb = ctx.JobPostings.First(x => x.ID == id);
                jb.ViewCount = jb.ViewCount == null ? (int?)1 : jb.ViewCount + 1;
                ctx.Entry(jb).State = EntityState.Modified;
                ctx.SaveChanges(); 
            }
        }


        public JobPostings GetWithApplicants(int id, string createdBy)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobPostings
                            .Include(x => x.Applications.Select(y => y.UserProfile))
                            .Include(x => x.Applications.Select(y => y.Resume))
                            .Include(x => x.Categories)
                            .First(x => x.ID == id && x.CreatedBy == createdBy);
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobPostings.Remove(ctx.JobPostings.First(x => x.ID == id));
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
