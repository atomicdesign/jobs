﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class JobFairRepository : IRepository<JobFairs>
    {
        public int Add(JobFairs entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobFairs.Add(entity);
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public void Update(JobFairs entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public IEnumerable<JobFairs> Get()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobFairs.ToList();
            }
        }

        public JobFairs Get(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.JobFairs.First(x => x.ID == id);
            }
        }

        public bool RegisterUser(int userId, int jobFairId, bool MailSent)
        {
            try
            {
                using (var ctx = new CivilianJobsContext())
                {
                    ctx.JobFairAppliers.Add(new JobFairAppliers
                    {
                        Date = DateTime.Now,
                        IsMailSent = MailSent,
                        JobFairId = jobFairId,
                        UserId = userId
                    });
                    ctx.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.JobFairs.Remove(ctx.JobFairs.First(x => x.ID == id));
                ctx.SaveChanges();

                return true;
            }
        }
    }
}
