﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Providers.Entities;
using System.Data.Entity;

namespace CivilianJobs.Models.Jobs.Repositories
{
    public class ResumeRepository : IRepository<Resume>
    {
        public int Add(Resume entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Resumes.Add(entity);
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public int Add(Resume entity, string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                entity.UserProfile = ctx.UserProfiles.First(x => x.UserName == username);

                ctx.Resumes.Add(entity);
                ctx.SaveChanges();

                return entity.ID;
            }
        }

        public void Update(Resume entity)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
            }
        }

        public IEnumerable<Resume> Get()
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.Resumes.Include(x => x.UserProfile).Take(10).ToList();
            }
        }
        public ResumeResult Get(int index, int count, int sorting, System.Web.HttpRequestBase request, System.Web.Mvc.UrlHelper url)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var result = new ResumeResult();
                result.objects = new List<Resume>();
                result.count = ctx.Resumes.Count();
                var poss = ctx.SpecialtyAreas.ToList();
                var objs = ctx.Resumes.Include(x => x.UserProfile).AsQueryable();
                switch (sorting)
                {
                    case 0:
                        objs = objs.OrderByDescending(x => x.UserProfile.Created);
                        break;
                    default:
                        break;
                }
                result.objects = objs.Skip((index - 1) * count).Take(count).ToList();
                foreach (var item in result.objects)
                {
                    if (item.MilitarySpecialty1 == null)
                        item.MilitarySpecialty1 = "";
                    else
                    {
                        var posId = short.Parse(item.MilitarySpecialty1);
                        item.MilitarySpecialty1 = poss.First(x => x.SpecialtyAreaID == posId).SpecialtyArea1;
                    }
                    if (item.MilitarySpecialty2 == null)
                        item.MilitarySpecialty2 = "";
                    else
                    {
                        var posId = short.Parse(item.MilitarySpecialty2);
                        item.MilitarySpecialty2 = poss.First(x => x.SpecialtyAreaID == posId).SpecialtyArea1;
                    }
                    if (item.MilitarySpecialty3 == null)
                        item.MilitarySpecialty3 = "";
                    else
                    {
                        var posId = short.Parse(item.MilitarySpecialty3);
                        item.MilitarySpecialty3 = poss.First(x => x.SpecialtyAreaID == posId).SpecialtyArea1;
                    }
                }
                return result;
            }
        }


        public Resume Get(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                return ctx.Resumes.First(x => x.ID == id);
            }
        }

        public Resume Get(string username)
        {
            using (var ctx = new CivilianJobsContext())
            {
                var resume =  ctx.Resumes.FirstOrDefault(x => x.UserProfile.UserName == username);
                resume.UserProfile = ctx.UserProfiles.First(x => x.UserName == username);
                return resume;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new CivilianJobsContext())
            {
                ctx.Resumes.Remove(ctx.Resumes.First(x => x.ID == id));
                ctx.SaveChanges();

                return true;
            }
        }

        public ResumeSearchResult Get(int index, int count, int sorting, string SearchKeywords)
        {
            //Get Search parameters
            //Bro You Fucked our codes!!!
            List<string> GEtAttributes = new List<string>();
            GEtAttributes = SearchKeywords.Split(',').ToList();
            var Searchkeyword = GEtAttributes[0].ToString() != "" ? GEtAttributes[0].ToString() : "";
            var SecurityClearence = GEtAttributes[1].ToString() != "" ? Convert.ToInt32(GEtAttributes[1]) : 0;
            var rank = GEtAttributes[2].ToString() != "" ? Convert.ToInt32(GEtAttributes[2]) : 0;
            var YrsOfService = GEtAttributes[3].ToString() != "" ? Convert.ToInt32(GEtAttributes[3]) : 0;
            var Language = GEtAttributes[4].ToString() != "" ? Convert.ToInt32(GEtAttributes[4]) : 0;
            var JobLocation = GEtAttributes[5].ToString() != "" ? GEtAttributes[5].ToString() : "";
            var ActiveDuty = GEtAttributes[6].ToString() != "" ? GEtAttributes[6].ToString() : "";
            var StartDateChecker = GEtAttributes[7].ToString() != "";
            var EndDateChecker = GEtAttributes[8].ToString() != "";
            var StartDate = GEtAttributes[7].ToString() != "" ? Convert.ToDateTime(GEtAttributes[7]) : DateTime.MinValue;
            var EndDate = GEtAttributes[8].ToString() != "" ? Convert.ToDateTime(GEtAttributes[8]) : DateTime.MaxValue;
            var JobCategory = GEtAttributes[9].ToString() != "" ? GEtAttributes[9].ToString() : "";

            using (var ctx = new CivilianJobsContext())
            {
                var result = new ResumeSearchResult();
                var locattion = ctx.UserDegrees.Where(x => x.Type == 9 && x.Name == JobLocation).FirstOrDefault();
                if (locattion != null)
                {
                    JobLocation = locattion.Id.ToString();
                }
                var category = ctx.UserDegrees.Where(x => x.Type == 16 && x.Name == JobCategory).FirstOrDefault();
                if (category != null)
                {
                    JobCategory = category.Id.ToString();
                }

                var obj = new List<JobSeekersDetail>();
                var k =SearchKeywords.Replace(",","");
                if (string.IsNullOrEmpty(k))
                {
                     obj = ctx.JobSeekersDetails.ToList();
                }
                else
                {
                   obj = (from js in ctx.JobSeekersDetails.AsQueryable()
                               where (Searchkeyword == "" || (ctx.UserProfiles.FirstOrDefault(x=>x.UserId == js.UserId).FirstName + " " + ctx.UserProfiles.FirstOrDefault(x => x.UserId == js.UserId).LastName).ToLower().Contains(Searchkeyword.ToLower())) &&
                               (SecurityClearence <= 0 || js.SecurityClearence == SecurityClearence) &&
                               (rank <= 0 || js.Rank == rank) &&
                               (YrsOfService <= 0 || js.YrsOfService == YrsOfService) &&
                               (Language <= 0 || js.Language == Language) &&
                               (ActiveDuty == "" || js.DescriptionJob == ActiveDuty) &&
                                (JobLocation == "" || js.location.ToLower().Contains(JobLocation.ToLower())) &&
                                 (JobCategory == "" || js.AvailableCategories.ToLower().Contains(JobCategory.ToLower())) &&
                               (string.IsNullOrEmpty(js.AvailDate.ToString()) || !StartDateChecker || js.AvailDate > StartDate)
                               && (string.IsNullOrEmpty(js.AvailDate.ToString()) || !EndDateChecker || js.AvailDate < EndDate)
                               select js).ToList();
                }
                result.count = obj.Count();
                result.objects = obj.Skip((index - 1) * count).Take(count).ToList();
                switch (sorting)
                {
                    case 0:
                        result.objects = result.objects.OrderByDescending(x => x.Createdon).ToList();
                        break;
                    default:
                        break;
                }
                foreach (var item in result.objects)
                {
                    if (item.SalaryRange > 0)
                    {
                        item.SalaryRangeValue = ctx.UserDegrees.Where(x => x.Id == item.SalaryRange).FirstOrDefault().Name;
                    }
                    else
                    {
                        item.SalaryRangeValue = "";
                    }
                    if (item.UserId != null)
                    {
                        var user = ctx.UserProfiles.Where(x => x.UserId == item.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            item.UserName = user.FirstName + " " + user.LastName;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.AvailableSpecialities))
                    {
                        foreach (var sp in item.AvailableSpecialities.Split(',').Take(3))
                        {
                            var spid = Convert.ToInt32(sp);
                            var spl = ctx.UserDegrees.Where(x => x.Id == spid).FirstOrDefault();
                            if (spl != null)
                            {
                                item.SpecialityValue = item.SpecialityValue + "<p class='p-d'>" + spl.Name + "</p>";
                            }
                        }
                        item.SpecialityValue.TrimEnd(',');
                    }
                    else
                    {
                        item.SpecialityValue = "<p class='p-d'>NO Military Specialities.</p>";
                    }
                    if (!string.IsNullOrEmpty(item.location))
                    {
                        foreach (var sp in item.location.Split(',').Take(3))
                        {
                            var spid = Convert.ToInt32(sp);
                            var spl = ctx.UserDegrees.Where(x => x.Id == spid).FirstOrDefault();
                            if (spl != null)
                            {
                                item.LocationValue = item.LocationValue + "<p class='p-d'>" + spl.Name + "</p>";
                            }
                        }
                        item.LocationValue.TrimEnd(',');
                    }
                    else
                    {
                        item.LocationValue = "<p class='p-d'>NO Location.</p>";
                    }
                }
                return result;
            }
        }


        public ResumeSearchResult GetSaved(int index, int count, int sorting, string SearchKeywords, int profileId)
        {

            //Get Search parameters
            //Bro You Fucked our codes!!!
            List<string> GEtAttributes = new List<string>();
            GEtAttributes = SearchKeywords.Split(',').ToList();
            var Searchkeyword = GEtAttributes[0].ToString() != "" ? GEtAttributes[0].ToString() : "";
            var SecurityClearence = GEtAttributes[1].ToString() != "" ? Convert.ToInt32(GEtAttributes[1]) : 0;
            var rank = GEtAttributes[2].ToString() != "" ? Convert.ToInt32(GEtAttributes[2]) : 0;
            var YrsOfService = GEtAttributes[3].ToString() != "" ? Convert.ToInt32(GEtAttributes[3]) : 0;
            var Language = GEtAttributes[4].ToString() != "" ? Convert.ToInt32(GEtAttributes[4]) : 0;
            var JobLocation = GEtAttributes[5].ToString() != "" ? GEtAttributes[5].ToString() : "";
            var ActiveDuty = GEtAttributes[6].ToString() != "" ? GEtAttributes[6].ToString() : "";
            var StartDateChecker = GEtAttributes[7].ToString() != "";
            var EndDateChecker = GEtAttributes[8].ToString() != "";
            var StartDate = GEtAttributes[7].ToString() != "" ? Convert.ToDateTime(GEtAttributes[7]) : DateTime.MinValue;
            var EndDate = GEtAttributes[8].ToString() != "" ? Convert.ToDateTime(GEtAttributes[8]) : DateTime.MaxValue;
            var JobCategory = GEtAttributes[9].ToString() != "" ? GEtAttributes[9].ToString() : "";

            using (var ctx = new CivilianJobsContext())
            {
                var saved = ctx.SavedResumes.Where(x => x.EmployerId == profileId).Select(x => x.ResumeId).ToList();
                var result = new ResumeSearchResult();
                var locattion = ctx.UserDegrees.Where(x => x.Type == 9 && x.Name == JobLocation).FirstOrDefault();
                if (locattion != null)
                {
                    JobLocation = locattion.Id.ToString();
                }
                var category = ctx.UserDegrees.Where(x => x.Type == 16 && x.Name == JobCategory).FirstOrDefault();
                if (category != null)
                {
                    JobCategory = category.Id.ToString();
                }

                var obj = new List<JobSeekersDetail>();
                var k = SearchKeywords.Replace(",", "");
                if (string.IsNullOrEmpty(k))
                {
                    obj = ctx.JobSeekersDetails.Where(x=> saved.Contains(x.Id)).ToList();
                }
                else
                {
                    obj = (from js in ctx.JobSeekersDetails.AsQueryable()
                           where (saved.Contains(js.Id)) && (Searchkeyword == "" || (ctx.UserProfiles.FirstOrDefault(x => x.UserId == js.UserId).FirstName + " " + ctx.UserProfiles.FirstOrDefault(x => x.UserId == js.UserId).LastName).ToLower().Contains(Searchkeyword.ToLower())) &&
                           (SecurityClearence <= 0 || js.SecurityClearence == SecurityClearence) &&
                           (rank <= 0 || js.Rank == rank) &&
                           (YrsOfService <= 0 || js.YrsOfService == YrsOfService) &&
                           (Language <= 0 || js.Language == Language) &&
                           (ActiveDuty == "" || js.DescriptionJob == ActiveDuty) &&
                            (JobLocation == "" || js.location.ToLower().Contains(JobLocation.ToLower())) &&
                             (JobCategory == "" || js.AvailableCategories.ToLower().Contains(JobCategory.ToLower())) &&
                           (string.IsNullOrEmpty(js.AvailDate.ToString()) || !StartDateChecker || js.AvailDate > StartDate)
                           && (string.IsNullOrEmpty(js.AvailDate.ToString()) || !EndDateChecker || js.AvailDate < EndDate)
                           select js).ToList();
                }
                result.count = obj.Count();
                result.objects = obj.Skip((index - 1) * count).Take(count).ToList();
                switch (sorting)
                {
                    case 0:
                        result.objects = result.objects.OrderByDescending(x => x.Createdon).ToList();
                        break;
                    default:
                        break;
                }
                foreach (var item in result.objects)
                {
                    if (item.SalaryRange > 0)
                    {
                        item.SalaryRangeValue = ctx.UserDegrees.Where(x => x.Id == item.SalaryRange).FirstOrDefault().Name;
                    }
                    else
                    {
                        item.SalaryRangeValue = "";
                    }
                    if (item.UserId != null)
                    {
                        var user = ctx.UserProfiles.Where(x => x.UserId == item.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            item.UserName = user.FirstName + " " + user.LastName;
                        }
                    }
                    if (!string.IsNullOrEmpty(item.AvailableSpecialities))
                    {
                        foreach (var sp in item.AvailableSpecialities.Split(',').Take(3))
                        {
                            var spid = Convert.ToInt32(sp);
                            var spl = ctx.UserDegrees.Where(x => x.Id == spid).FirstOrDefault();
                            if (spl != null)
                            {
                                item.SpecialityValue = item.SpecialityValue + "<p class='p-d'>" + spl.Name + "</p>";
                            }
                        }
                        item.SpecialityValue.TrimEnd(',');
                    }
                    else
                    {
                        item.SpecialityValue = "<p class='p-d'>NO Military Specialities.</p>";
                    }
                    if (!string.IsNullOrEmpty(item.location))
                    {
                        foreach (var sp in item.location.Split(',').Take(3))
                        {
                            var spid = Convert.ToInt32(sp);
                            var spl = ctx.UserDegrees.Where(x => x.Id == spid).FirstOrDefault();
                            if (spl != null)
                            {
                                item.LocationValue = item.LocationValue + "<p class='p-d'>" + spl.Name + "</p>";
                            }
                        }
                        item.LocationValue.TrimEnd(',');
                    }
                    else
                    {
                        item.LocationValue = "<p class='p-d'>NO Location.</p>";
                    }
                }
                return result;
            }
        }

        public List<UserDegree> GetLocations()
        {
            var result = new List<UserDegree>();
            using (var ctx = new CivilianJobsContext())
            {
                result = ctx.UserDegrees.Where(x => x.Type == 9).ToList();
            }
            return result;
        }
        public List<UserDegree> GetCategories()
        {
            var result = new List<UserDegree>();
            using (var ctx = new CivilianJobsContext())
            {
                result = ctx.UserDegrees.Where(x => x.Type == 16).ToList();
            }
            return result;
        }
    }
}
