using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandSkill
    {
        public int UserID { get; set; }
        public short SkillID { get; set; }
    }
}
