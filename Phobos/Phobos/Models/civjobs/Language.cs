using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Language
    {
        public short LanguageID { get; set; }
        public string LanguageName { get; set; }
    }
}
