using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandWorkHistoryUpdateLog
    {
        public string email { get; set; }
        public System.DateTime date_created { get; set; }
        public Nullable<System.DateTime> date_workhistory_added { get; set; }
    }
}
