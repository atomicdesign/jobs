using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Certification
    {
        public int id { get; set; }
        public string Descr { get; set; }
    }
}
