using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class MonsterMailUser
    {
        public int userid { get; set; }
        public string username { get; set; }
        public Nullable<byte> active { get; set; }
    }
}
