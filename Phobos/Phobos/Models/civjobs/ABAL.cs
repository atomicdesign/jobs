using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ABAL
    {
        public System.Guid ID { get; set; }
        public System.Guid MasterfileGuid { get; set; }
        public int UserID { get; set; }
        public System.DateTime ExpirationDate { get; set; }
        public int StateID { get; set; }
        public string Notes { get; set; }
        public int LastUpdateUserID { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
