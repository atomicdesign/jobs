using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Candidate
    {
        public Candidate()
        {
            this.Medals = new List<Medal>();
        }

        public int UserId { get; set; }
        public Nullable<int> Recruiter { get; set; }
        public Nullable<short> ServiceBranch { get; set; }
        public Nullable<short> Rank { get; set; }
        public Nullable<short> YearsOfService { get; set; }
        public Nullable<System.DateTime> SepDate { get; set; }
        public Nullable<int> CandSchool1 { get; set; }
        public Nullable<int> CandSchool2 { get; set; }
        public Nullable<int> CandSchool3 { get; set; }
        public Nullable<short> HighSchool { get; set; }
        public string Cert1 { get; set; }
        public string Cert2 { get; set; }
        public string Cert3 { get; set; }
        public string Cert4 { get; set; }
        public Nullable<System.DateTime> AvailDate { get; set; }
        public Nullable<short> PayExpectation { get; set; }
        public Nullable<byte> Relo { get; set; }
        public Nullable<short> DischargeType { get; set; }
        public Nullable<short> Clearance { get; set; }
        public Nullable<short> MarketAsClearance { get; set; }
        public Nullable<byte> Crime { get; set; }
        public Nullable<int> MilitaryPosition1 { get; set; }
        public Nullable<int> MilitaryPosition2 { get; set; }
        public Nullable<int> MilitaryPosition3 { get; set; }
        public Nullable<int> CivilianPosition1 { get; set; }
        public Nullable<int> CivilianPosition2 { get; set; }
        public Nullable<int> CivilianPosition3 { get; set; }
        public Nullable<byte> PEI_ACAD { get; set; }
        public Nullable<byte> PEI_ENL_COLLEGE { get; set; }
        public Nullable<byte> PEI_CERT { get; set; }
        public Nullable<byte> PEI_ENL_SUPERMAN { get; set; }
        public Nullable<byte> PEI_CW { get; set; }
        public Nullable<byte> PEI_RELO { get; set; }
        public Nullable<byte> PEI_MEDALS { get; set; }
        public Nullable<byte> PEI_LANGUAGES { get; set; }
        public Nullable<byte> PEI_CLEARANCE { get; set; }
        public Nullable<byte> InstructorDuty { get; set; }
        public Nullable<byte> bmi { get; set; }
        public Nullable<byte> PEI_INSTRUCTOR { get; set; }
        public Nullable<byte> PEI_MASTERTRAINER { get; set; }
        public Nullable<byte> ProfileActive { get; set; }
        public Nullable<short> MasterTrainer { get; set; }
        public string Specialty { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<byte> ReceivedSummaryPage { get; set; }
        public Nullable<byte> ReservedForBMI { get; set; }
        public Nullable<byte> ReceivedEmailSurvey { get; set; }
        public Nullable<int> CandSchool4 { get; set; }
        public Nullable<short> Ethnicity { get; set; }
        public Nullable<short> Gender { get; set; }
        public string AdminNotes { get; set; }
        public string WorkHistory { get; set; }
        public Nullable<byte> ResumeExtracted { get; set; }
        public Nullable<byte> MilitaryFriendlyColleges { get; set; }
        public Nullable<System.DateTime> BMI_Conversion_Date { get; set; }
        public Nullable<System.Guid> BMI_CandidateGuid { get; set; }
        public string BMI_Recruiter_Email { get; set; }
        public Nullable<short> ForeignLanguage { get; set; }
        public Nullable<short> ShiftWork { get; set; }
        public Nullable<short> TravelPct { get; set; }
        public Nullable<short> DegreeUndergrad { get; set; }
        public Nullable<short> DegreeGraduate { get; set; }
        public Nullable<System.DateTime> EnlistDate { get; set; }
        public string ReEnlistCode { get; set; }
        public Nullable<System.DateTime> DateProfileInactived { get; set; }
        public string Keywords { get; set; }
        public virtual ICollection<Medal> Medals { get; set; }
    }
}
