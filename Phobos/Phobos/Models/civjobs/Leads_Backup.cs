using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Leads_Backup
    {
        public System.Guid id { get; set; }
        public int client_userid { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public short branch { get; set; }
        public short rank { get; set; }
        public Nullable<short> region { get; set; }
    }
}
