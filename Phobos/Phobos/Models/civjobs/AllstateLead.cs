using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class AllstateLead
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> StateId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<bool> HonorableDischarge { get; set; }
        public string LeadSource { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
    }
}
