using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Ethnicity
    {
        public short EthnicityID { get; set; }
        public string Ethnicity1 { get; set; }
    }
}
