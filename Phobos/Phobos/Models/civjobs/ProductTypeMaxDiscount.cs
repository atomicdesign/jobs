using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ProductTypeMaxDiscount
    {
        public int id { get; set; }
        public short ClientType { get; set; }
        public string ProductType { get; set; }
        public Nullable<decimal> MaxDiscount { get; set; }
    }
}
