using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandState
    {
        public int UserID { get; set; }
        public short StateID { get; set; }
    }
}
