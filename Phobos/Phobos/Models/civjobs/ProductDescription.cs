﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models
{
    public class ProductDescription
    {
        public int Id { get; set; }
        public string package { get; set; }
        public string title { get; set; }
        public int  quantity { get; set; }
        public decimal price { get; set; }
        public int userId { get; set;}
        public string type { get; set; }
        public DateTime date { get; set; }
        public int state { get; set; }
    }
}
