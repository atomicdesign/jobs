using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CompensationType
    {
        public byte CompensationTypeID { get; set; }
        public string CompensationType1 { get; set; }
    }
}
