using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandMilExp
    {
        public int UserId { get; set; }
        public int MilExpId { get; set; }
    }
}
