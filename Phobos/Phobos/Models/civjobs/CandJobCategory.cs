using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandJobCategory
    {
        public int UserID { get; set; }
        public short JobCategoryID { get; set; }
    }
}
