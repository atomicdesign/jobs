using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class HiringNeedList
    {
        public int id { get; set; }
        public string HiringNeed { get; set; }
        public Nullable<short> sort_order { get; set; }
    }
}
