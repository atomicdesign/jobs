using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class HighSchool
    {
        public short HighSchoolID { get; set; }
        public string HighSchool1 { get; set; }
    }
}
