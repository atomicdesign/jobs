using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobWrapStat
    {
        public int id { get; set; }
        public int UserId { get; set; }
        public System.DateTime DateWrapped { get; set; }
        public short JobsInFeed { get; set; }
        public short JobsAdded { get; set; }
        public short JobsSkipped { get; set; }
        public Nullable<short> JobsDuplicated { get; set; }
        public Nullable<short> JobsDeleted { get; set; }
        public Nullable<short> JobsErrored { get; set; }
        public string Status { get; set; }
    }
}
