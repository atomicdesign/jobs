using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class AppLog
    {
        public int id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string msg { get; set; }
    }
}
