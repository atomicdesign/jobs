using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandPosition
    {
        public int PositionID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public string Descr { get; set; }
        public Nullable<byte> PositionNumber { get; set; }
    }
}
