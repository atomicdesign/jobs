using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Medal
    {
        public Medal()
        {
            this.Candidates = new List<Candidate>();
        }

        public short MedalID { get; set; }
        public string Medal1 { get; set; }
        public virtual ICollection<Candidate> Candidates { get; set; }
    }
}
