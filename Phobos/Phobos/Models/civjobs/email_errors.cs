using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class email_errors
    {
        public System.Guid EmailID { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public byte BodyFormat { get; set; }
        public Nullable<System.DateTime> DateSent { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<System.DateTime> SendDate { get; set; }
        public Nullable<System.DateTime> DateQueuedUp { get; set; }
        public Nullable<System.Guid> Attachment1_DocID { get; set; }
        public string Attachment1_Filename { get; set; }
    }
}
