using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class YearsOfService
    {
        public short YearsID { get; set; }
        public string Years { get; set; }
    }
}
