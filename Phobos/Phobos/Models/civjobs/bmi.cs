using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class bmi
    {
        public byte bmi_id { get; set; }
        public string bmi_value { get; set; }
    }
}
