using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class EmailCampaignQuery
    {
        public int QueryId { get; set; }
        public string Description { get; set; }
        public string ProcedureName { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }
}
