using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandReason
    {
        public int UserID { get; set; }
        public short ReasonID { get; set; }
    }
}
