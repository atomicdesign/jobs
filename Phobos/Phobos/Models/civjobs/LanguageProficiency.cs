using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class LanguageProficiency
    {
        public byte ProficiencyID { get; set; }
        public string Proficiency { get; set; }
    }
}
