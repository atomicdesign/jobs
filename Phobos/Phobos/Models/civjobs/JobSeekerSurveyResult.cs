using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobSeekerSurveyResult
    {
        public int id { get; set; }
        public int UserId { get; set; }
        public short Question { get; set; }
        public short Answer { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
    }
}
