using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobPostingCandidateStatu
    {
        public short StatusID { get; set; }
        public string Status { get; set; }
    }
}
