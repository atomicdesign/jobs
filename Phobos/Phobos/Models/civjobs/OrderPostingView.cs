using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class OrderPostingView
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public Nullable<System.DateTime> OrderAppDate { get; set; }
        public int OrderDetailId { get; set; }
        public short Qty { get; set; }
        public Nullable<System.DateTime> PostingExpirationDate { get; set; }
    }
}
