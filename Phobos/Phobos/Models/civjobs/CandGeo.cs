using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandGeo
    {
        public int UserId { get; set; }
        public int GeoId { get; set; }
    }
}
