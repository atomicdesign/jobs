using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ClearanceReq
    {
        public short ClearanceReqID { get; set; }
        public string ClearanceReq1 { get; set; }
    }
}
