using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ClientSurvey
    {
        public string Company { get; set; }
        public string Email { get; set; }
        public int JobFairID { get; set; }
        public byte FirstJobFair { get; set; }
        public byte HearAbout { get; set; }
        public byte Quality { get; set; }
        public byte RateCJN { get; set; }
        public byte Reason { get; set; }
        public byte Referrals { get; set; }
        public string Improve { get; set; }
        public string Testimonial { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public byte Quality2 { get; set; }
        public byte CustomerService { get; set; }
        public int UserId { get; set; }
    }
}
