using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairComparison
    {
        public byte JobFairCompareId { get; set; }
        public string JobFairCompare { get; set; }
    }
}
