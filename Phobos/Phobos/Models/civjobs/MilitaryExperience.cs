using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class MilitaryExperience
    {
        public int id { get; set; }
        public string Descr { get; set; }
        public bool Officer { get; set; }
    }
}
