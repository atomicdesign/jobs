using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class MVE_Survey
    {
        public System.Guid id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Addr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Q1 { get; set; }
        public string Q2 { get; set; }
        public string Q3 { get; set; }
        public string Q4 { get; set; }
        public string Q5 { get; set; }
        public string Q6 { get; set; }
        public string Q7 { get; set; }
        public string Q8 { get; set; }
        public string Q9 { get; set; }
        public string Q10 { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<bool> Completed { get; set; }
        public string Industry { get; set; }
        public string EmployeeCount { get; set; }
        public string EmployeeMilitaryPct { get; set; }
        public string MilitaryHiresLastYear { get; set; }
        public string CompanyWebsite { get; set; }
        public string NewEmployeeMilitaryPct { get; set; }
    }
}
