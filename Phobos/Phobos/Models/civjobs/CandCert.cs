using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandCert
    {
        public int UserId { get; set; }
        public int CertId { get; set; }
    }
}
