using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class HiringNeedsSurvey
    {
        public int HiringNeedsSurveyID { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public Nullable<byte> WantsJobFair { get; set; }
        public Nullable<byte> WantsCJN { get; set; }
        public Nullable<byte> WantsDirectHire { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<byte> WantsJobFairInvite { get; set; }
        public Nullable<byte> WantsCJ { get; set; }
        public Nullable<byte> WantsTargetHire { get; set; }
        public Nullable<byte> WantsTemp2Perm { get; set; }
        public Nullable<byte> WantsMVE { get; set; }
    }
}
