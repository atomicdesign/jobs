using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class viewCandidate
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public Nullable<byte> ProfileActive { get; set; }
        public string ServiceBranch { get; set; }
        public string Rank { get; set; }
        public Nullable<short> YearsOfService { get; set; }
        public string Specialties { get; set; }
        public string Awards { get; set; }
        public string CityPreferences { get; set; }
        public string ProfileComplete { get; set; }
        public string Certifications { get; set; }
        public string JobCategories { get; set; }
        public string InstructorDuty { get; set; }
        public string DischargeType { get; set; }
        public string UndergradDegree { get; set; }
        public string GraduateDegree { get; set; }
        public string ForeignLanguage { get; set; }
        public string Clearance { get; set; }
        public Nullable<System.DateTime> SepDate { get; set; }
        public string Cert1 { get; set; }
        public string Cert2 { get; set; }
        public string Cert3 { get; set; }
        public string Cert4 { get; set; }
        public Nullable<System.DateTime> AvailDate { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string WorkHistory { get; set; }
        public Nullable<System.DateTime> EnlistDate { get; set; }
        public string ReEnlistCode { get; set; }
        public Nullable<System.DateTime> DateProfileInactived { get; set; }
    }
}
