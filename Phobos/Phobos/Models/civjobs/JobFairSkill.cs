using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairSkill
    {
        public int JobFairSkillsID { get; set; }
        public int JobFairID { get; set; }
        public int UserID { get; set; }
        public short SkillID { get; set; }
    }
}
