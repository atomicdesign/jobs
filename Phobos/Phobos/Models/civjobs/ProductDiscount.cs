using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ProductDiscount
    {
        public short DiscountId { get; set; }
        public short ProductId { get; set; }
        public string ProductType { get; set; }
        public byte ProductQtyId { get; set; }
        public decimal DiscountPct { get; set; }
        public virtual ProductQty ProductQty { get; set; }
    }
}
