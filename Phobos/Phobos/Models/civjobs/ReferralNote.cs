﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models
{
    public class ReferralNote
    {
        public int Id { get; set; }
        public int sender { get; set; }
        public string receiver { get; set; }
        public DateTime senddate { get; set; }
    }

    public class detNotes
    {
        public List<ReferralNote> data { get; set; }
    }
}
