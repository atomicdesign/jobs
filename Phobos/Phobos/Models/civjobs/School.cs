using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class School
    {
        public School()
        {
            this.CandSchools = new List<CandSchool>();
        }

        public short SchoolID { get; set; }
        public string SchoolName { get; set; }
        public virtual ICollection<CandSchool> CandSchools { get; set; }
    }
}
