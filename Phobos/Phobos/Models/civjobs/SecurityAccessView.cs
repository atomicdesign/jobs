using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class SecurityAccessView
    {
        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
        public Nullable<int> ResourceTypeId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
