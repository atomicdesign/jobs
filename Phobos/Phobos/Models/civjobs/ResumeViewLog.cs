using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ResumeViewLog
    {
        public int ClientUserid { get; set; }
        public int CandidateUserid { get; set; }
        public System.DateTime DateViewed { get; set; }
    }
}
