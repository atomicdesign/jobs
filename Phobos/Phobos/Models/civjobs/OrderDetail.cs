using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public short ProductId { get; set; }
        public string ProductType { get; set; }
        public short Qty { get; set; }
        public decimal BaseRate { get; set; }
        public decimal DiscountPct { get; set; }
        public decimal DiscountAmt { get; set; }
        public decimal OrderDetailAmt { get; set; }
        public Nullable<int> JobFairID { get; set; }
        public string UOM { get; set; }
        public Nullable<decimal> LineItemDiscountPct { get; set; }
        public Nullable<decimal> LineItemDiscountAmt { get; set; }
        public Nullable<decimal> LineItemTotal { get; set; }
        public Nullable<int> PubId { get; set; }
        public virtual Order Order { get; set; }
    }
}
