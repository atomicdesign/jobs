using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class SecureResource
    {
        public SecureResource()
        {
            this.Permissions = new List<Permission>();
        }

        public int ResourceId { get; set; }
        public string ResourceName { get; set; }
        public Nullable<int> ResourceTypeId { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
