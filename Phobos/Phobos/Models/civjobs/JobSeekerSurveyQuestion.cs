using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobSeekerSurveyQuestion
    {
        public int question_number { get; set; }
        public string question { get; set; }
    }
}
