﻿using CivilianJobs.Models.Jobs.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models
{
    public class JobFairAppliers
    {
        public int Id { get; set; }
        public bool IsMailSent { get; set; }
        public int UserId { get; set; }
        public int JobFairId { get; set; }
        public DateTime Date { get; set; }
    }
}
