using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Addresses
    {
        public int UserID { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string OverseasPhone { get; set; }
    }
}
