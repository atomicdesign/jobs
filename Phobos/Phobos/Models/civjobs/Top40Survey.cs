using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Top40Survey
    {
        public int id { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public string Name { get; set; }
        public string Addr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Branch { get; set; }
        public string Rank { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string HonorableDischarge { get; set; }
        public string Q1 { get; set; }
        public string Q2 { get; set; }
        public string Q3 { get; set; }
        public string Q6 { get; set; }
        public string RefName { get; set; }
        public string RefTitle { get; set; }
        public string RefCompany { get; set; }
        public string RefEmail { get; set; }
        public string RefPhone { get; set; }
        public string RefAddr { get; set; }
        public string RefCity { get; set; }
        public string RefState { get; set; }
        public string RefZip { get; set; }
        public string NameAppears { get; set; }
    }
}
