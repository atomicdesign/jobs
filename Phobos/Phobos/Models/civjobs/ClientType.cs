using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ClientType
    {
        public short ClientTypeID { get; set; }
        public string ClientType1 { get; set; }
    }
}
