using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class SpecialtyArea
    {
        public short SpecialtyAreaID { get; set; }
        public string SpecialtyArea1 { get; set; }
    }
}
