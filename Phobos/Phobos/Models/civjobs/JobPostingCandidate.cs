using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobPostingCandidate
    {
        public int JobPostingID { get; set; }
        public int UserID { get; set; }
        public byte Status { get; set; }
        public System.DateTime CreateDate { get; set; }
    }
}
