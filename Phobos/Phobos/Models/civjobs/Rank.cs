using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Rank
    {
        public short RankID { get; set; }
        public string Rank1 { get; set; }
    }
}
