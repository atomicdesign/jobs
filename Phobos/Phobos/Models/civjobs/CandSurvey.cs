using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandSurvey
    {
        public int JobFairID { get; set; }
        public byte FirstJobFair { get; set; }
        public byte HearAbout { get; set; }
        public byte Quality { get; set; }
        public byte Satisfied { get; set; }
        public byte Compare { get; set; }
        public byte Recommend { get; set; }
        public string Referrals { get; set; }
        public byte Employed { get; set; }
        public byte Presentation { get; set; }
        public string Improve { get; set; }
        public string Testimonial { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CandFName { get; set; }
        public string CandLName { get; set; }
        public string CandEmail { get; set; }
        public string CandPhone { get; set; }
    }
}
