using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class SecureResourceType
    {
        public int ResourceTypeId { get; set; }
        public string ResourceTypeName { get; set; }
    }
}
