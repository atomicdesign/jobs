using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairRegistrationJob
    {
        public int id { get; set; }
        public System.Guid JobFairRegistrationId { get; set; }
        public short StateId { get; set; }
        public string JobTitle { get; set; }
        public Nullable<short> SkillId { get; set; }
        public Nullable<short> CategoryId { get; set; }
    }
}
