using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairRegistration
    {
        public System.Guid JobFairRegistrationId { get; set; }
        public int JobFairId { get; set; }
        public int UserId { get; set; }
        public string HiringFor { get; set; }
        public string AttendanceType { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> ElectricalOutlet { get; set; }
        public string DisplaySize { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<bool> AttendanceMatchList { get; set; }
        public Nullable<bool> MatchListGeoPref { get; set; }
        public Nullable<bool> Brief { get; set; }
        public Nullable<bool> EmailAnnouncement { get; set; }
        public Nullable<bool> Sponsorship { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
    }
}
