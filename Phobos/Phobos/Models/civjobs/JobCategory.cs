using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobCategory
    {
        public short JobCategoryID { get; set; }
        public string JobCategory1 { get; set; }
        public Nullable<short> SpecialtyArea { get; set; }
    }
}
