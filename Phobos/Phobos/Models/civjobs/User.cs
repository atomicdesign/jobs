using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public bool Active { get; set; }
        public Nullable<short> HearAbout { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public Nullable<int> ReferringUserId { get; set; }
        public Nullable<System.Guid> guid { get; set; }
    }
}
