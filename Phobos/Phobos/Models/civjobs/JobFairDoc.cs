using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairDoc
    {
        public int JobFairDocId { get; set; }
        public int JobFairId { get; set; }
        public int DocId { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<byte> DocTypeId { get; set; }
    }
}
