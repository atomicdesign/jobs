using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class UserListView
    {
        public int UserId { get; set; }
        public string EmailAddress { get; set; }
        public bool Active { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
