using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class MonsterMailJobURL
    {
        public System.Guid id { get; set; }
        public string url1 { get; set; }
        public string url2 { get; set; }
        public string url3 { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
    }
}
