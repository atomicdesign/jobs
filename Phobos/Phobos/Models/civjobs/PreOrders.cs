﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models.civjobs
{
    public class PreOrder
    {
        public int Id { get; set; }
        public string Orders { get; set; }
        public int? UserId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
