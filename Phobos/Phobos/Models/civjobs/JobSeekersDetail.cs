﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Helpers;
using CivilianJobs.Models.civjobs;

namespace CivilianJobs.Models
{
    public partial class JobSeekersDetail
    {

        //Contact Page Fields
        public JobSeekersDetail()
        {
            _userProfile = new UserProfile();
            NewD = new List<NewDesi>();
            awardss = new List<Awards>();
            regions = new List<chbox>();
          //  _UserDegree = new List<UserDegree>();
        }
        [NotMapped]
        public UserProfile _userProfile { get; set; }
       //  [NotMapped]
     //   public UserDegree _UserDegree { get; set; }
        public int Id { get; set; }
        public Guid UserId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Createdon { get; set; }

        public int HigherSchool_Deg { get; set; }
        public int UnderGrad_Deg { get; set; }
        public int Grad_Deg { get; set; }


        public bool FurtherHelp { get; set; }
        public int Language { get; set; }
         [Required]
        public int? Service { get; set; }

        public int Rank { get; set; }

        [NotMapped]
        public int JobFairs { get; set; }
        public int? MOSDD { get; set; }
        [Required]
        public int InstructorDuty { get; set; }
         [Required]
        public int Discharged { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CommissionDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SeperationDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? AvailDate { get; set; }

        [Required]
        public int ConvictedFelory { get; set; }
         [Required]
        public int SecurityClearence { get; set; }
        [Required]
        public int MilitaryRelocation { get; set; }
        [Required]
        public int YrsOfService { get; set; }
        public string AvailableSpecialities { get; set; }
        public string MilitaryAwards { get; set; }
        public string Designation { get; set; }


        public string location { get; set; }
        public string regionso { get; set; }
        public int? SalaryRange { get; set; }
        public int? ShiftWork { get; set; }
        public int? Travel { get; set; }

        public string AvailableCategories { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? currentstartdate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? currentenddate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PreStartDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PreEndDate { get; set; }
        public string CompanyUnit { get; set; }

        public string MOS { get; set; }

        public string DescriptionJob { get; set; }

        public string PreCompanyUnit { get; set; }

        public int? ViewCount { get; set; }

        public string PreMOS { get; set; }

        public string PreDescriptionJob { get; set; }
        [DataType(DataType.MultilineText)]
        public string ExperienceWorkHistory { get; set; }
        [NotMapped ]
        public List<Awards> awardss { get; set; }
         [NotMapped]
        public List<NewDesi> NewD { get; set; }
         [NotMapped]
          public string SalaryRangeValue { get; set; }
         [NotMapped]
         public string LocationValue { get; set; }
         [NotMapped]
         public string CategoryValue { get; set; }
         [NotMapped]
         public string SpecialityValue { get; set; }
         [NotMapped]
         public string UserName { get; set; }
      
        public int? Ethnicity { get; set; }
        public int? Gender { get; set; }
        [NotMapped]
        public List<chbox> regions { get; set; }

    }

    public class chbox
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
   
}