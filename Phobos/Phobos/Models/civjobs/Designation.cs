﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models
{
    public class Designation
    {

        public int Id { get; set; }
        public string Designationees { get; set; }
        [NotMapped]
        public bool Checked { get; set; }
    }
}