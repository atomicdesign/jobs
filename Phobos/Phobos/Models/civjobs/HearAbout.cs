using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class HearAbout
    {
        public short HearAboutID { get; set; }
        public string HearAbout1 { get; set; }
    }
}
