﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace CivilianJobs.Models
{
    public class SRM
    {
        public int Id { get; set; }
        public  int ServiceId { get; set; }
        public Nullable<int> RankId { get; set; }
        public Nullable<int> MosId { get; set; }

        [NotMapped]
        public string Service { get; set; }
        [NotMapped]
        public string Rank { get; set; }
        [NotMapped]
        public string Mos { get; set; }
    }

    public class SrmDegrees
    {
        public int Id { get; set; }
        public int SrmId { get; set; }
        public int DegreeId { get; set; }
    }
}
