using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobPostingStatu
    {
        public string JobPostingStatusCode { get; set; }
        public string Descr { get; set; }
    }
}
