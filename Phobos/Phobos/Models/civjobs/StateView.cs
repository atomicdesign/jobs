using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class StateView
    {
        public int StateID { get; set; }
        public string State { get; set; }
    }
}
