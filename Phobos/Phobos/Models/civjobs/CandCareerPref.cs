using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandCareerPref
    {
        public int UserID { get; set; }
        public short CareerPrefID { get; set; }
    }
}
