using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class OrderStatu
    {
        public string OrderStatusCode { get; set; }
        public string Descr { get; set; }
    }
}
