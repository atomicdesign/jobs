using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class HiringNeedsSurveyNeed
    {
        public int HiringNeedsSurveyNeedsID { get; set; }
        public int HiringNeedsSurveyID { get; set; }
        public int HiringNeed { get; set; }
    }
}
