using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class LanguageReq
    {
        public short LanguageID { get; set; }
        public string Language { get; set; }
        public Nullable<short> SpecialOrder { get; set; }
    }
}
