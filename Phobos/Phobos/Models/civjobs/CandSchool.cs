using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandSchool
    {
        public int CandSchoolID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<short> SchoolID { get; set; }
        public string OtherSchoolName { get; set; }
        public string Major { get; set; }
        public Nullable<short> GradYear { get; set; }
        public string Activity1 { get; set; }
        public string Activity2 { get; set; }
        public string Activity3 { get; set; }
        public Nullable<short> DegreeID { get; set; }
        public virtual School School { get; set; }
    }
}
