﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CivilianJobs.Models.civjobs
{
    public class Awards
    {
       
        public int Id { get; set; }
        public string Award { get; set; }
        [NotMapped]
        public bool Checked { get; set; }
    }
}