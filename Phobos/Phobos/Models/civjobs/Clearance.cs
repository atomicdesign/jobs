using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Clearance
    {
        public short ClearanceID { get; set; }
        public string Clearance1 { get; set; }
        public Nullable<short> SpecialOrder { get; set; }
    }
}
