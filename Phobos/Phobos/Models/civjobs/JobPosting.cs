using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobPosting
    {
        public int JobPostingID { get; set; }
        public int UserID { get; set; }
        public string ContactCompany { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactEmail { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<short> EmployeeType { get; set; }
        public Nullable<short> Experience { get; set; }
        public string Requirements { get; set; }
        public Nullable<short> Degree { get; set; }
        public Nullable<short> Clearance { get; set; }
        public Nullable<short> ForeignLang { get; set; }
        public Nullable<decimal> CompensationMin { get; set; }
        public Nullable<decimal> CompensationMax { get; set; }
        public Nullable<byte> CompensationPeriod { get; set; }
        public string CompensationBonus { get; set; }
        public Nullable<short> Travel { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<int> OrderDetailId { get; set; }
        public string JobPostingStatusCode { get; set; }
        public Nullable<System.DateTime> ActivationDate { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public System.DateTime ValidExpirationDate { get; set; }
        public Nullable<bool> HidePhone { get; set; }
        public Nullable<bool> HideFax { get; set; }
        public Nullable<bool> HideEmail { get; set; }
        public string Compensation { get; set; }
        public Nullable<bool> EmailWhenAppliedTo { get; set; }
        public Nullable<int> HirenetID { get; set; }
        public Nullable<int> ViewCount { get; set; }
        public string Notes { get; set; }
        public bool JobWrapped { get; set; }
        public string JobWrapID { get; set; }
        public string ApplicationURL { get; set; }
        public string RequisitionId { get; set; }
        public Nullable<System.DateTime> JobWrapDate { get; set; }
    }
}
