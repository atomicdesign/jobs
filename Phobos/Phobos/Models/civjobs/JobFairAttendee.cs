using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairAttendee
    {
        public int JobFairID { get; set; }
        public int UserID { get; set; }
    }
}
