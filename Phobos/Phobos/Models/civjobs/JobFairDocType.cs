using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairDocType
    {
        public byte DocTypeId { get; set; }
        public string DocType { get; set; }
    }
}
