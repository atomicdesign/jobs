using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class HN_JobCategoryXref
    {
        public string CareerCode { get; set; }
        public short JobCategoryId { get; set; }
    }
}
