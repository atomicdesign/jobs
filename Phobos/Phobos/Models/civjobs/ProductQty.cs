using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ProductQty
    {
        public ProductQty()
        {
            this.ProductDiscounts = new List<ProductDiscount>();
        }

        public byte ProductQtyId { get; set; }
        public string ProductType { get; set; }
        public short Qty { get; set; }
        public virtual ICollection<ProductDiscount> ProductDiscounts { get; set; }
    }
}
