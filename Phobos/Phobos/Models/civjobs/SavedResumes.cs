using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class SavedResumes
    {
        public int Id { get; set; }
        public int ResumeId { get; set; }
        public int EmployerId { get; set; }
    }
}
