using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public short SortOrder { get; set; }
        public bool DefaultRole { get; set; }
        public bool Visible { get; set; }
        public Nullable<int> ResourceId { get; set; }
    }
}
