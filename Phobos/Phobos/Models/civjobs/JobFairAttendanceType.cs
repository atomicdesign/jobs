using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairAttendanceType
    {
        public string id { get; set; }
        public string descr { get; set; }
    }
}
