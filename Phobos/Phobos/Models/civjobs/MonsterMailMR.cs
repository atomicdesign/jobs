using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class MonsterMailMR
    {
        public System.Guid id { get; set; }
        public string email { get; set; }
        public System.DateTime datesent { get; set; }
        public int userid { get; set; }
    }
}
