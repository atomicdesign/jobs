using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Coupon
    {
        public int CouponID { get; set; }
        public string CouponName { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<float> Discount { get; set; }
    }
}
