using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairDisplaySize
    {
        public string id { get; set; }
        public string DisplaySize { get; set; }
        public short sort_order { get; set; }
    }
}
