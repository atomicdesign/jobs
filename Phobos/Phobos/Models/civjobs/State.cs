using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class State
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public string ShortStateName { get; set; }
        public Nullable<short> StateNumber { get; set; }
        public short SortOrder { get; set; }
        public bool IsState { get; set; }
        public Nullable<int> States_StateID { get; set; }
    }
}
