using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class ClientSurveyReason
    {
        public byte ReasonID { get; set; }
        public string Reason { get; set; }
    }
}
