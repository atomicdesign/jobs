using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class QualityRating
    {
        public byte QualityRatingId { get; set; }
        public string QualityRating1 { get; set; }
    }
}
