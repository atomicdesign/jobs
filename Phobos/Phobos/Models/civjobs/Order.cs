using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Order
    {
        public Order()
        {
            this.OrderDetails = new List<OrderDetail>();
        }

        public int OrderId { get; set; }
        public int UserId { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string TransactionId { get; set; }
        public Nullable<decimal> OrderAmt { get; set; }
        public string OrderStatusCode { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string PmtMethod { get; set; }
        public Nullable<decimal> CouponAmt { get; set; }
        public Nullable<System.DateTime> OrderAppDate { get; set; }
        public Nullable<int> LargeDiscountApproverUserId { get; set; }
        public Nullable<bool> LargeDiscountApproverRequired { get; set; }
        public Nullable<int> AE_UserId { get; set; }
        public string CreditCardInfo { get; set; }
        public Nullable<System.Guid> ContactID { get; set; }
        public Nullable<int> AR1 { get; set; }
        public Nullable<double> AR1Pct { get; set; }
        public Nullable<int> AR2 { get; set; }
        public Nullable<double> AR2Pct { get; set; }
        public Nullable<int> AR3 { get; set; }
        public Nullable<double> AR3Pct { get; set; }
        public Nullable<double> CorpPct { get; set; }
        public string BillingContact { get; set; }
        public Nullable<short> BillingTermsId { get; set; }
        public string BillingTerms { get; set; }
        public virtual Client Client { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
