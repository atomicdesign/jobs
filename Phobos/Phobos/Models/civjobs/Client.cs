using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Client
    {
        public Client()
        {
            this.Orders = new List<Order>();
        }

        public int UserID { get; set; }
        public string CompanyName { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<short> TotalResumeViews { get; set; }
        public Nullable<short> CurrentResumeViews { get; set; }
        public Nullable<System.DateTime> ResumeExpirationDate { get; set; }
        public Nullable<int> AE_UserId { get; set; }
        public Nullable<System.DateTime> ABAL_ExpireDate { get; set; }
        public string ABAL_Notes { get; set; }
        public string JobWrapURL { get; set; }
        public string JobWrapJobPostingUrl { get; set; }
        public string JobWrapUrlFragment { get; set; }
        public Nullable<bool> CRM_Lead_Created { get; set; }
        public Nullable<System.Guid> MasterfileGuid { get; set; }
        public Nullable<short> ClientType { get; set; }
        public string CompanyWebsite { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
