using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Permission
    {
        public Permission()
        {
            this.SecureResources = new List<SecureResource>();
        }

        public int PermissionId { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public virtual ICollection<SecureResource> SecureResources { get; set; }
    }
}
