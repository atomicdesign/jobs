using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class viewABAL
    {
        public System.Guid MasterfileGuid { get; set; }
        public string AR { get; set; }
        public string Expires { get; set; }
        public string State { get; set; }
    }
}
