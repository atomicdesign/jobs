using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairCategory
    {
        public int JobFairCategoryID { get; set; }
        public Nullable<int> JobFairID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<short> JobCategoryID { get; set; }
    }
}
