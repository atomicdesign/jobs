using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandSearchResultsLog
    {
        public int SearchID { get; set; }
        public int CandidateUserID { get; set; }
    }
}
