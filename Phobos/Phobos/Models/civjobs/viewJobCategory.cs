using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class viewJobCategory
    {
        public short JobCategoryID { get; set; }
        public string JobCategory { get; set; }
    }
}
