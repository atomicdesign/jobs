using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class DegreeReq
    {
        public short DegreeReqID { get; set; }
        public string DegreeReq1 { get; set; }
    }
}
