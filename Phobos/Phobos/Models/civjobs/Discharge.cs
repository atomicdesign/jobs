using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Discharge
    {
        public short DischargeID { get; set; }
        public string Discharge1 { get; set; }
    }
}
