using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobPostingJobCategory
    {
        public int JobPostingID { get; set; }
        public short JobCategoryID { get; set; }
    }
}
