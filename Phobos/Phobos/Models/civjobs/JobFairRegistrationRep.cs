using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairRegistrationRep
    {
        public int id { get; set; }
        public System.Guid JobFairRegistrationId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string DriversLicenseNumber { get; set; }
        public string DriversLicenseState { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
    }
}
