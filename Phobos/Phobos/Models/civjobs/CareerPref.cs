using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CareerPref
    {
        public short CareerPrefID { get; set; }
        public string CareerPref1 { get; set; }
    }
}
