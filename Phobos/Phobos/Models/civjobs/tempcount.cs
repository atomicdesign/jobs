using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class tempcount
    {
        public string CompanyName { get; set; }
        public string JobPostingExpiration { get; set; }
        public string DateWrapped { get; set; }
        public Nullable<short> JobsInFeed { get; set; }
        public Nullable<short> JobsAdded { get; set; }
        public Nullable<short> JobsSkipped { get; set; }
        public Nullable<int> JobsWrapped { get; set; }
        public Nullable<short> JobsDeleted { get; set; }
        public Nullable<short> JobsDuplicated { get; set; }
        public Nullable<short> JobsErrored { get; set; }
    }
}
