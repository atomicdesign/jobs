using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class viewDirectHire
    {
        public int UserId { get; set; }
        public string LastNameGuid { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string OverseasPhone { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Rank { get; set; }
        public string Service { get; set; }
        public string Skills { get; set; }
        public string School { get; set; }
        public string Major { get; set; }
        public string YearsOfService { get; set; }
        public string SepDate { get; set; }
        public string Relo { get; set; }
        public string AvailDate { get; set; }
        public string Locations { get; set; }
        public string Salary { get; set; }
        public string Jobs { get; set; }
        public Nullable<short> ClearanceGuid { get; set; }
        public string Clearance { get; set; }
        public string AdminNotes { get; set; }
        public string Degree { get; set; }
        public string WorkHistoryGuid { get; set; }
        public Nullable<byte> ReservedForBMI { get; set; }
    }
}
