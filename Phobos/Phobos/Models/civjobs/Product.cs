using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Product
    {
        public short ProductId { get; set; }
        public string Descr { get; set; }
        public string ProductType { get; set; }
        public decimal BaseRate { get; set; }
        public Nullable<short> JobPostingTerm { get; set; }
        public Nullable<short> JobPostingActiveTerm { get; set; }
        public Nullable<short> MaxResumeViews { get; set; }
        public string UOM { get; set; }
        public bool IsActive { get; set; }
    }
}
