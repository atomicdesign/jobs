using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class EmployeeType
    {
        public short EmployeeTypeID { get; set; }
        public string EmployeeType1 { get; set; }
    }
}
