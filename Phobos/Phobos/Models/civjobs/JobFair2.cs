using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFair2
    {
        public int JobFairID { get; set; }
        public System.DateTime DateStart { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public Nullable<System.DateTime> RosterDue { get; set; }
        public Nullable<int> TablesAvailable { get; set; }
    }
}
