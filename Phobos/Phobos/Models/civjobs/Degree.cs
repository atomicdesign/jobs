using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class Degree
    {
        public short DegreeID { get; set; }
        public string Degree1 { get; set; }
        public Nullable<bool> GraduateLevel { get; set; }
    }
}
