using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class LineItemDiscount
    {
        public int id { get; set; }
        public float pct { get; set; }
        public string descr { get; set; }
    }
}
