using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFairLocation
    {
        public int JobFairLocationsID { get; set; }
        public Nullable<int> JobFairID { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<short> StateID { get; set; }
    }
}
