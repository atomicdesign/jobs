using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobSeekerOption
    {
        public int id { get; set; }
        public int question_number { get; set; }
        public string descr { get; set; }
    }
}
