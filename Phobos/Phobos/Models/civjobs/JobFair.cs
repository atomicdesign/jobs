using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class JobFair
    {
        public int JobFairID { get; set; }
        public System.DateTime Date { get; set; }
        public string Location { get; set; }
        public string Sponsor { get; set; }
        public Nullable<byte> Mil2Civ { get; set; }
        public string SponsorUrl { get; set; }
    }
}
