using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandLanguage
    {
        public int CandLanguageID { get; set; }
        public int UserID { get; set; }
        public short LanguageID { get; set; }
        public byte ProficiencyID { get; set; }
    }
}
