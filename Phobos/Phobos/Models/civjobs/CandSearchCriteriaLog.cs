using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class CandSearchCriteriaLog
    {
        public int SearchID { get; set; }
        public int ClientUserID { get; set; }
        public Nullable<System.DateTime> SearchDate { get; set; }
        public string criteria_keyword { get; set; }
        public Nullable<int> criteria_city1 { get; set; }
        public Nullable<int> criteria_city2 { get; set; }
        public Nullable<int> criteria_city3 { get; set; }
        public Nullable<int> criteria_jobcat1 { get; set; }
        public Nullable<int> criteria_jobcat2 { get; set; }
        public Nullable<int> criteria_jobcat3 { get; set; }
        public Nullable<short> criteria_clearance { get; set; }
        public Nullable<short> criteria_active_duty { get; set; }
        public Nullable<short> criteria_avail_date_range { get; set; }
        public Nullable<short> criteria_ethnicity { get; set; }
        public Nullable<short> criteria_gender { get; set; }
        public string criteria_keyword2 { get; set; }
        public Nullable<int> criteria_and_or { get; set; }
    }
}
