using System;
using System.Collections.Generic;

namespace CivilianJobs.Models
{
    public partial class OrderDetailJobPosting
    {
        public int OrderDetailJobPosting1 { get; set; }
        public Nullable<int> OrderDetailID { get; set; }
        public Nullable<int> JobPostingID { get; set; }
    }
}
