﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using CivilianJobs.Models;
using CivilianJobs.Helpers;
using System.Data.Entity;

namespace CivilianJobs
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public partial class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();
            if (exc.GetType() == typeof(System.Web.HttpException))
            {
                HttpException hte = (HttpException)exc;
                switch (hte.GetHttpCode())
                {
                    case 404:
                        using (CivilianJobs.Models.CivilianJobsContext db = new Models.CivilianJobsContext())
                        {
                            var outUrl = db.Redirects.FirstOrDefault(et => et.InUrl == Request.Path);
                            if (outUrl != null)
                            {
                                try
                                {
                                    outUrl.TimesUsed++;
                                    db.Entry(outUrl).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                                catch (Exception err)
                                {
                                    CivilianJobs.Helpers.LogHelper.Log(err);
                                }

                                if (outUrl.OutUrl != String.Empty)
                                {
                                    Response.Clear();
                                    Response.StatusCode = 301;
                                    Response.RedirectLocation = outUrl.OutUrl;
                                    Response.End();
                                }
                                return;
                            }
                            else
                            {
                                try
                                {
                                    db.Redirects.Add(new Models.ThreeOhOne { InUrl = Request.Path, TimesUsed = 0, OutUrl = "" });
                                    db.SaveChanges();
                                }
                                catch (Exception err)
                                {
                                    LogHelper.Log(err);
                                }
                            }
                        }
                        break;
                    default:
                        LogHelper.Log(exc);
                        break;
                }
            }
            else
            {
                LogHelper.Log(exc);
            }
        }
    }
}