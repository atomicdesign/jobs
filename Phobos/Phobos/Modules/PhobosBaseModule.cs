﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Models;
using CivilianJobs.Helpers;

namespace CivilianJobs.Modules
{
    [NotMapped]
    public class CivilianJobsBaseModule
    {
        protected HtmlHelper htmlHelper;
        protected CivilianJobsContext db;
        protected static string ModuleViewPath = "~/Views/Modules/";
        protected ControllerContext _context;
        protected ViewDataDictionary _vdd;
        protected CivilianJobsPuck _puck;

        public CivilianJobsBaseModule() { db = new CivilianJobsContext();  }

        protected virtual void Initialize(HtmlHelper newHelper, params object[] p)
        {
            htmlHelper = newHelper;
            _context = new ControllerContext(htmlHelper.ViewContext.RequestContext, htmlHelper.ViewContext.Controller);
            _vdd = new ViewDataDictionary(htmlHelper.ViewData);
            _puck = (CivilianJobsPuck)(newHelper.ViewBag.CivilianJobsPuck);
        }

        public virtual string RenderModule(HtmlHelper newHelper, params object[] p)
        {
            //Initialize(newHelper, p);
            return String.Empty;
        }

        public string RenderView(string viewName, object model)
        {
            string retval = String.Empty;
            try
            {                
                using (StringWriter sw = new StringWriter())
                {
                    _vdd.Model = model;
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(_context, viewName);
                    ViewContext viewContext = new ViewContext(_context, viewResult.View, _vdd, _context.Controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    retval = sw.ToString();
                }
            }
            catch (Exception err) { LogHelper.Log(err);  }
            return retval;
        }

        public static CivilianJobsBaseModule GetModule(string FullClassPath)
        {
            try
            {
                Type t = Type.GetType(FullClassPath);
                return (CivilianJobsBaseModule)Activator.CreateInstance(t);
            }
            catch (Exception) { }
            return null;
        }

        public static CivilianJobsBaseModule GetModule(string FullClassPath, string ValidateInheritanceType)
        {
            try
            {
                Type t = Type.GetType(FullClassPath);
                Type t2 = Type.GetType(ValidateInheritanceType);
                if(t2.IsAssignableFrom(t)) return (CivilianJobsBaseModule)Activator.CreateInstance(t);
            }
            catch (Exception) { }
            return null;
        }

    }
}