﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Models;
using CivilianJobs.Helpers;

namespace CivilianJobs.Modules
{
    public class CivilianJobsSimpleMotionBanner : CivilianJobsSlider
    {
        

        public CivilianJobsSimpleMotionBanner()
            : base()
        {
            
        }

        public override string RenderModule(HtmlHelper newHelper, params object[] p)
        {
            if (p.Length == 0) return String.Empty;
            Initialize(newHelper, p);
            
            _puck.Scripts.Add("SimpleMotionBanner.js");
            _puck.Stylesheets.Add("SimpleMotionBanner.css");

            if (_gallery != null)
            {
                return RenderView(ModuleViewPath + "_SimpleMotionBanner.cshtml", _gallery);
            }
            return String.Empty;
        }
    }
}