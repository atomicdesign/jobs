﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Models;
using CivilianJobs.Helpers;

namespace CivilianJobs.Modules
{
    public class FacebookLike : SocialMediaPluginBase
    {
        internal static class _args
        {
            public static int URL = 0;
            public static int LAYOUT = 1;
            public static int WIDTH = 2;
            public static int FONT = 3;
            public static int COLOR_SCHEME = 4;
            public static int VERB = 5;
        }

        private List<string> _layouts = new List<string> { "standard", "button_count", "box_count" };
        private List<string> _fonts = new List<string> { "arial", "lucida grande", "segoe ui", "tahoma", "trebuchet ms", "verdana" };
        private List<string> _colorschemes = new List<string> { "light", "dark" };
        private List<string> _verbs = new List<string> { "like", "recommend" };

        private string _url = "";
        private string _layout = "standard";
        private int _width = 400;
        private string _font = "arial";
        private string _colorscheme = "light";
        private string _verb = "like";

        protected override void Initialize(HtmlHelper newHelper, params object[] p)
        {
            base.Initialize(newHelper, p);
            //GetSocialMediaInfo("FacebookLike");
            //Don't need it for like
            if (p.Length > _args.URL) _url = (string)p[_args.URL];
            if (_url.Trim() == String.Empty) _url = newHelper.ViewContext.HttpContext.Request.Path;

            if (p.Length > _args.LAYOUT && _layouts.Contains((string)p[_args.LAYOUT])) _layout = (string)p[_args.LAYOUT];
            if (p.Length > _args.WIDTH) _width = (int)p[_args.WIDTH];
            if (p.Length > _args.FONT && _layouts.Contains((string)p[_args.FONT])) _layout = (string)p[_args.FONT];
            if (p.Length > _args.COLOR_SCHEME && _colorschemes.Contains((string)p[_args.COLOR_SCHEME])) _colorscheme = (string)p[_args.COLOR_SCHEME];
            if (p.Length > _args.VERB && _verbs.Contains((string)p[_args.VERB])) _verb = (string)p[_args.VERB];
        }

        public override string RenderModule(HtmlHelper newHelper, params object[] p)
        {
            Initialize(newHelper, p);
            return String.Format("<div class=\"fb-like\" data-href=\"{0}\" data-send=\"false\" data-width=\"{1}\" data-show-faces=\"false\" data-font=\"{2}\" data-colorscheme=\"{3}\" data-action=\"{4}\" data-layout=\"{5}\"></div>", _url, _width, _font, _verb, _layout);
        }
    }
}