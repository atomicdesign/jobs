﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Models;
using CivilianJobs.Helpers;

namespace CivilianJobs.Modules
{
    public class SocialMediaPluginBase : CivilianJobsBaseModule
    {
        protected SocialMedia _SocialMediaInfo;
        protected void GetSocialMediaInfo(string ModuleName){ _SocialMediaInfo = db.SocialMedia.FirstOrDefault(e => e.ModuleName == ModuleName); }
        protected IHtmlString GetIcon() { return MvcHtmlString.Create((_SocialMediaInfo != null ? String.Format("<div class=\"social-media\"><img src=\"{0}\"/></div>", _SocialMediaInfo.ImageUrl) : "")); }
    }

}