﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.Entity;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CivilianJobs.Models;
using CivilianJobs.Helpers;

namespace CivilianJobs.Modules
{
    public class CivilianJobsSlider : CivilianJobsBaseModule
    {
        protected Models.Gallery _gallery;
        protected string _galleryName = "";
        protected string _domID = "";
        protected int _maxItems = 100;


        internal class _args
        {
            public static int GALLERY_NAME = 0;
            public static int DOM_ID = 1;
            public static int MAX_ITEMS = 2;
        }

        public CivilianJobsSlider() : base() { }

        protected override void Initialize(HtmlHelper newHelper, params object[] p)
        {
 	        base.Initialize(newHelper, p);
            if (p.Length > _args.GALLERY_NAME) _galleryName = (string)p[_args.GALLERY_NAME];
            if (p.Length > _args.DOM_ID) _domID = (string)p[_args.DOM_ID];
            if (p.Length > _args.MAX_ITEMS && !int.TryParse((string)p[_args.MAX_ITEMS], out _maxItems)) _maxItems = 100;

            _gallery = db.Galleries.FirstOrDefault(e => e.Name == _galleryName);

            _vdd["GALLERY_NAME"] = _galleryName;
            _vdd["DOM_ID"] = _domID;
            _vdd["MAX_ITEMS"] = _maxItems;
        }
        
    }
}