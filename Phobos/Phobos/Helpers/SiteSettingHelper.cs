﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CivilianJobs.Models;
using System.Linq.Expressions;
using System.Web.Caching;

namespace CivilianJobs.Helpers
{
    public static class SiteSettingHelper
    {
        public const int SettingCacheLifetimeHours = 1;

        public static Dictionary<string, string> SiteSettings
        {
            get
            {
                if (HttpContext.Current == null) //Testing
                {
                    return GetFreshSettings();
                }
                else
                {
                    Dictionary<string, string> retval = (Dictionary<string, string>)(HttpContext.Current.Cache.Get("SiteSettings"));
                    if (retval == null) retval = ReloadSettings();
                    return retval;
                }
            }
        }

        public static void SettingCacheItemUpdateCallback(string key, CacheItemUpdateReason reason, out Object expensiveObject, out CacheDependency dependency, out DateTime absoluteExpiration, out TimeSpan slidingExpiration)
        {
            expensiveObject = GetFreshSettings();
            dependency = null;
            absoluteExpiration = DateTime.UtcNow.AddHours(SettingCacheLifetimeHours);
            slidingExpiration = Cache.NoSlidingExpiration;
        }

        public static Dictionary<string, string> ReloadSettings()
        {
            HttpContext.Current.Cache.Remove("SiteSettings");
            Dictionary<string, string> retval = GetFreshSettings();
            HttpContext.Current.Cache.Insert("SiteSettings", retval, null, DateTime.UtcNow.AddHours(SettingCacheLifetimeHours), System.Web.Caching.Cache.NoSlidingExpiration);
            return retval;
        }

        public static Dictionary<string, string> GetFreshSettings()
        {
            Dictionary<string, string> Settings = new Dictionary<string, string>();
            using (CivilianJobsContext db = new CivilianJobsContext())
            {
                db.Settings.ToList().ForEach((setting) => { Settings[setting.Name] = setting.Value; });
            }
            return Settings;
        }

        public static string GetSetting(string Key, string DefaultValue = null)
        {
            return (SiteSettings.ContainsKey(Key) ? SiteSettings[Key] : DefaultValue);
        }
    }
}