﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CivilianJobs.Models;
using CivilianJobs.Modules;
using MaxMind.GeoIP;
using System.Linq.Expressions;
using System.Web.Caching;
using System.Globalization;
using System.Web.Routing;

namespace CivilianJobs.Helpers
{

    public class LegacyUrlRoute : RouteBase
        {
            public override RouteData GetRouteData(HttpContextBase httpContext)
            {
                const string status = "301 Moved Permanently";
                bool redirect = false;
                var request = httpContext.Request;
                var response = httpContext.Response;
                var legacyUrl = request.Url.PathAndQuery.ToString().ToLower().Substring(1);
                var newUrl = "";
                var animalID = request.QueryString.Count != 0 ? request.QueryString["aid"] : "";
                var date = request.QueryString.Count != 0 ? request.QueryString["date"] : "";

                switch (legacyUrl)
                {
                    case "default.aspx":
                        newUrl = "/"; redirect = true; break;
					//ZOO
                    /*case "hours.aspx":
                    case "military-rates.aspx":
                    case "regular-rates.aspx":
                    case "group-rates.aspx":
                    case "school-rates.aspx":
                        newUrl = "/page/hours-rates"; redirect = true; break;
                    case "zooteens.aspx":
                        newUrl = "/page/zoo-teens"; redirect = true; break;
                    case "search.aspx":
                        newUrl = "/search/"; redirect = true; break;
                    case "sitemap.aspx":
                        newUrl = "/sitemap"; redirect = true; break;
                    /*case "directory.aspx":
                        using (PhobosContext db = new PhobosContext())
                        {
                            Animal animal = db.Animals.FirstOrDefault();
                            newUrl = animal.URL; redirect = true; 
                        }
                        break;*/
                    default:
						//ZOO
                        /*if (legacyUrl.Contains("directory.aspx") && animalID != "")
                        {
                            using (PhobosContext db = new PhobosContext())
                            {
                                Animal animal = db.Animals.Find(int.Parse(animalID));
                                if (animal != null)
                                {
                                    newUrl = animal.URL;
                                }
                            }
                            redirect = true;
                        }
                        else if (legacyUrl.Contains("daily-events.aspx") && date != "")
                        {
                            newUrl = "/calendar/date/" + date.Replace("/", "-");
                            redirect = true;
                        }
                        else*/
						if (legacyUrl.Contains(".aspx"))
                        {
                            legacyUrl = legacyUrl.Replace(".aspx", "");

                            using(PhobosContext db = new PhobosContext())
                            {
                                List<Page> pages = db.Pages.Where(e => e.SEOURL == legacyUrl).ToList();
                                if (pages.Count > 0)
                                {
                                    newUrl = "/page/" + legacyUrl;
                                }
                                else
                                {
                                    List<Form> forms = db.Forms.Where(e => e.SEOURL == legacyUrl).ToList();
                                    if (forms.Count > 0)
                                    {
                                        newUrl = "/form/" + legacyUrl;
                                    }
                                }
                            }
                            redirect = true;
                        }
                        break;
                }

                if (redirect)
                {
                    response.Status = status;
                    response.RedirectLocation = newUrl;
                    response.End();
                }

                return null;
            }

            public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
            {
                return null;
            }
        }

}