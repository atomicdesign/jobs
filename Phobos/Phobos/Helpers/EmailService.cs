﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data.Entity;
using System.Web.Mvc;
using System.IO;
using System.Runtime.Serialization.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations.Resources;
using CivilianJobs.Models;

namespace CivilianJobs.Helpers
{
    [NotMapped]
    public class EmailServicePacket
    {
        public string key { get; set; }
        public EmailServiceMessage message { get; set; }
    }

    [NotMapped]
    public class EmailServiceMessage
    {
        public string html { get; set; }
        public string text { get; set; }
        public string subject { get; set; }
        public string from_email { get; set; }
        public string from_name { get; set; }
        public string bcc_address { get; set; }
        public List<EmailServiceAddress> to { get; set; }

        public bool important { get { return false; } }
        public bool track_opens { get { return false; } }
        public bool track_clicks { get { return false; } }
        public bool auto_text { get { return true; } }
        public bool auto_html { get { return true; } }
        public bool inline_css { get { return false; } }
        public List<EmailServiceAttachment> attachments { get; set; }
    }

    [NotMapped]
    public class EmailServiceAddress
    {
        public string email { get; set; }
        public string name { get; set; }
    }

    [NotMapped]
    public class EmailServiceAttachment
    {
        public string type { get; set; }
        public string name { get; set; }
        public string content { get; set; }

        public string filename
        {
            set
            {
                if (!System.IO.Path.IsPathRooted(value)) value = HttpContext.Current.Server.MapPath(value);
                FileInfo fi = new FileInfo(value);
                if (fi.Exists)
                {
                    name = fi.Name;
                    string mime;
                    if (!EmailService.MimeTypes.TryGetValue(fi.Extension.ToUpper(), out mime)) mime = "binary/octet-stream";
                    type = mime;
                    content = Convert.ToBase64String(File.ReadAllBytes(fi.FullName));
                }
            }
        }
    }

    [NotMapped]
    public class EmailServiceSendResponse
    {
        public string email { get; set; }
        public string status { get; set; }
        public string _id { get; set; }
    }

    [NotMapped]
    public class EmailServiceSendError
    {
        public string code { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string message { get; set; }
    }

    [NotMapped]
    public class EmailService
    {
        public static Dictionary<string, string> MimeTypes = new Dictionary<string, string>{
            { ".PDF", "application/pdf" },
            { ".JPG", "image/jpeg" },
            { ".JPEG", "image/jpeg" },
            { ".PNG", "image/png" },
            { ".GIF", "image/gif" },
            { ".TIFF", "image/tiff" },
            { ".TIF", "image/tiff" }
        };

        private string _apiKey = "";
        private readonly string _apiHost = "https://mandrillapp.com/api/1.0/";
        private readonly string _apiFormat = ".json";
        private readonly string _sendMethod = "messages/send";

        public EmailService()
        {
            _apiKey = SiteSettingHelper.GetSetting("Mail API Key", String.Empty);
        }

        public bool SendOne(EmailMessage msg)
        {
            if (_apiKey == null || _apiKey == String.Empty) return false;
            try
            {
                EmailServicePacket pkt = new EmailServicePacket { key = _apiKey, message = msg.EmailServiceMessage };
                DataContractJsonSerializer jsonout = new DataContractJsonSerializer(typeof(EmailServicePacket));

                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(_apiHost + _sendMethod + _apiFormat);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/json";
                webRequest.KeepAlive = true;

                jsonout.WriteObject(webRequest.GetRequestStream(), pkt);

                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

                if (webResponse.StatusCode != HttpStatusCode.OK) throw new Exception(webResponse.StatusCode.ToString() + " response code");

                //DataContractJsonSerializer jsonin = new DataContractJsonSerializer(typeof(EmailServiceSendResponse[]));

                msg.Queued = false;
                msg.Sent = true;
                msg.HasErrors = false;
                msg.ErrorMessage = null;
                return true;
            }
            catch (Exception err)
            {
                Helpers.LogHelper.Log(err);
                msg.Queued = false;
                msg.Sent = false;
                msg.HasErrors = true;
                msg.ErrorMessage = err.ToString();
            }
            return false;
        }

        public bool SendBatch()
        {
            try
            {
                using (CivilianJobsContext db = new CivilianJobsContext())
                {
                    List<EmailMessage> messages = db.EmailQueue.Where(e => !(e.Sent || e.HasErrors)).ToList();
                    foreach (EmailMessage msg in messages)
                    {
                        SendOne(msg);
                        db.Entry(msg).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception err)
            {
                Helpers.LogHelper.Log(err);
            }
            return false;
        }
    }
}