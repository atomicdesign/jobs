﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using CivilianJobs.Models;

namespace CivilianJobs.Helpers
{

    public static class GooglePlaceResultStatus
    {
        public static readonly string OK = "OK";
        public static readonly string UNKNOWN_ERROR = "UNKNOWN_ERROR";
        public static readonly string ZERO_RESULTS = "ZERO_RESULTS";
        public static readonly string OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";
        public static readonly string REQUEST_DENIED = "REQUEST_DENIED";
        public static readonly string INVALID_REQUEST = "INVALID_REQUEST";
        public static readonly string NOT_FOUND = "NOT_FOUND";
    }

    public static class GooglePlaces
    {
        private static string APIUrl = "https://maps.googleapis.com/maps/api/place/";
        private static string GEOUrl = "http://maps.googleapis.com/maps/api/geocode/json?";
        private static string TZUrl = "https://maps.googleapis.com/maps/api/timezone/json?";

        public static string APIKey = "AIzaSyCkG9Il0qrbi20QyWoGMGP7r2YWGrNFsM8";

        public static List<string> SupportedGooglePlaceTypes = new List<string>()
        {
            "accounting","airport","amusement_park","aquarium","art_gallery","atm","bakery","bank","bar","beauty_salon","bicycle_store","book_store","bowling_alley","bus_station","cafe","campground","car_dealer","car_rental","car_repair","car_wash","casino","cemetery","church","city_hall","clothing_store","convenience_store","courthouse","dentist","department_store","doctor","electrician","electronics_store","embassy","establishment","finance","fire_station","florist","food","funeral_home","furniture_store","gas_station","general_contractor","grocery_or_supermarket","gym","hair_care","hardware_store","health","hindu_temple","home_goods_store","hospital","insurance_agency","jewelry_store","laundry","lawyer","library","liquor_store","local_government_office","locksmith","lodging","meal_delivery","meal_takeaway","mosque","movie_rental","movie_theater","moving_company","museum","night_club","painter","park","parking","pet_store","pharmacy","physiotherapist","place_of_worship","plumber","police","post_office","real_estate_agency","restaurant","roofing_contractor","rv_park","school","shoe_store","shopping_mall","spa","stadium","storage","store","subway_station","synagogue","taxi_stand","train_station","travel_agency","university","veterinary_care","zoo","administrative_area_level_1","administrative_area_level_2","administrative_area_level_3","colloquial_area","country","floor","geocode","intersection","locality","natural_feature","neighborhood","political","point_of_interest","post_box","postal_code","postal_code_prefix","postal_town","premise","room","route","street_address","street_number","sublocality","sublocality_level_4","sublocality_level_5","sublocality_level_3","sublocality_level_2","sublocality_level_1","subpremise","transit_station"
        };

        private static string URLParam(string key, string val)
        {
            if(val == null || val == String.Empty) return "";

            return HttpUtility.UrlEncode(key).Replace("+", "%20") + "=" + HttpUtility.UrlEncode(val).Replace("+", "%20");
        }

        public static string CleanGooglePlaceTypes(string inTypes)
        {
            if (inTypes == null || inTypes == String.Empty) return "";
            List<string> allTypes = new List<string>(inTypes.Split('|'));
            return String.Join("|", allTypes.Intersect(SupportedGooglePlaceTypes).ToArray());            
        }

        private static string URLParams(Dictionary<string, string> values)
        {
            bool first = false;
            StringBuilder sb = new StringBuilder();
            foreach(string key in values.Keys)
            {
                if(first){ first = false; } else{ sb.Append("&"); }
                sb.Append(URLParam(key, values[key]));
            }
            return sb.ToString();
        }

        public static GoogleSearchResult GetPlacesSearch(GoogleSearchCoords coords, string GooglePlaceTypes, string GooglePlaceKeywords)
        {
            return GetPlacesSearch(coords.lat, coords.lng, GooglePlaceTypes, GooglePlaceKeywords);
        }

        public static GoogleSearchResult GetPlacesSearch(double latitude, double longitude, string GooglePlaceTypes, string GooglePlaceKeywords)
        {
            Dictionary<string, string> values = new Dictionary<string,string>();
            values.Add("key", APIKey);
            values.Add("query", GooglePlaceKeywords);
            values.Add("sensor", "false");
            values.Add("location", latitude.ToString() + "," + longitude.ToString());
            values.Add("types", GooglePlaceTypes);
            values.Add("radius", "50000");

            string URL = APIUrl + "textsearch/json?" + URLParams(values);
            return JsonGetResults(URL);
        }

        public static GoogleSearchResult GetPlacesSearchNext(string PageToken)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("key", APIKey);
            values.Add("pagetoken", PageToken);
            values.Add("sensor", "false");
            string URL = APIUrl + "textsearch/json?" + URLParams(values);
            return JsonGetResults(URL);
        }

        public static GoogleSearchResult GetPlaceDetails(string id)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("key", APIKey);
            values.Add("reference", id);
            values.Add("sensor", "false");
            string URL = APIUrl + "details/json?" + URLParams(values);
            return JsonGetResults(URL);
        }

        private static GoogleSearchResult JsonGetResults(string URL)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(URL);
                webRequest.Method = "GET";
                webRequest.ContentType = "text/json";
                webRequest.KeepAlive = true;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode != HttpStatusCode.OK) throw new Exception(webResponse.StatusCode.ToString() + " response code");

                DataContractJsonSerializer jsonin = new DataContractJsonSerializer(typeof(GoogleSearchResult));
                return (GoogleSearchResult)jsonin.ReadObject(webResponse.GetResponseStream());
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
            }
            return null;
        }

        public static GoogleGeocodingResponse FindLocation(string location)
        {
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("address", location);
                values.Add("sensor", "false");
                string URL = GEOUrl + URLParams(values);

                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(URL);
                webRequest.Method = "GET";
                webRequest.ContentType = "text/json";
                webRequest.KeepAlive = true;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode != HttpStatusCode.OK) throw new Exception(webResponse.StatusCode.ToString() + " response code");

                DataContractJsonSerializer jsonin = new DataContractJsonSerializer(typeof(GoogleGeocodingResponse));
                return (GoogleGeocodingResponse)jsonin.ReadObject(webResponse.GetResponseStream());
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
            }
            return null;
        }


        public static GoogleTimeZoneResult GetTimeZone(double lat, double lng)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("location", lat.ToString() + "," + lng.ToString());
            values.Add("timestamp", (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds.ToString());
            values.Add("sensor", "false");
            string URL = TZUrl + URLParams(values);
            return JsonGetTimezone(URL);
        }

        private static GoogleTimeZoneResult JsonGetTimezone(string URL)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(URL);
                webRequest.Method = "GET";
                webRequest.ContentType = "text/json";
                webRequest.KeepAlive = true;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode != HttpStatusCode.OK) throw new Exception(webResponse.StatusCode.ToString() + " response code");

                DataContractJsonSerializer jsonin = new DataContractJsonSerializer(typeof(GoogleTimeZoneResult));
                return (GoogleTimeZoneResult)jsonin.ReadObject(webResponse.GetResponseStream());
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
            }
            return null;
        }

        public static IHtmlString RenderGoogleMap(this HtmlHelper htmlHelper, string Address, int width = 115, int height = 115, int zoom = 15)
        {
            string value = "";
            GoogleGeocodingResponse ggr = null;
            try
            {
                if ((ggr = (GoogleGeocodingResponse)HttpRuntime.Cache.Get("GEOCODE" + Address)) == null)
                {
                    ggr = FindLocation(Address);
                    if (ggr.success)
                    {
                        HttpRuntime.Cache.Insert("GEOCODE" + Address, ggr);
                    }
                }
            }
            catch (Exception err) {
                LogHelper.Log(err);
            }

            if(ggr != null && ggr.success)
            {
                value = GetGoogleMap(ggr.first_result.geometry, width, height, zoom);
            }else{
                LogHelper.Log("Unable to render Google Map");
                if (ggr != null) LogHelper.Log(ggr.status);
            }
            
            return MvcHtmlString.Create(String.Format("<img src=\"{0}\" class=\"google-map\" />", value));
        }

        public static string GetGoogleMap( GoogleSearchGeometry geometry, int width = 115, int height = 115, int zoom = 15)
        {
            return GetGoogleMap(geometry.Coords, width, height, zoom);
        }

        public static string GetGoogleMap(string Coords, int width = 115, int height = 115, int zoom = 15)
        {
            return String.Format("http://maps.googleapis.com/maps/api/staticmap?center={0}&markers=icon:http://www.medlanding.com/content/img/ml-map-icon.png%7C{1}&zoom={2}&size={3}x{4}&sensor=false&style=feature:all|element:all|saturation:-100&key={5}", Coords, Coords, zoom, width, height, GooglePlaces.APIKey);
        }

        public static string GetGoogleMapUrl(GoogleSearchGeometry geometry, int zoom = 15)
        {
            return GetGoogleMapUrl(geometry.Coords, zoom);
        }

        public static string GetGoogleMapUrl(string Coords, int zoom = 15)
        {
            return String.Format("https://maps.google.com/?ll={0}&t=h&z={1}", Coords, zoom);
        }

    }

    /**** GEOCODING RESULTS ****/

    public class GoogleGeocodingResponse
    {
        public bool success { get { return status == GooglePlaceResultStatus.OK && results != null && results.Count > 0; } }
        protected GoogleGeocodingResult _first_result { get; set; }
        public GoogleGeocodingResult first_result
        {
            get
            {
                return (_first_result ?? (results != null ? _first_result = results.FirstOrDefault() : null));
            }
        }

        public List<GoogleGeocodingResult> results { get; set; }
        public string status { get; set; }
    }

    public class GoogleGeocodingResult
    {
        public List<GoogleGeocodingAddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public GoogleSearchGeometry geometry { get; set; }
        public List<string> types { get; set; }

        public string City
        {
            get
            {
                var part = from x in address_components where x.types.Any(e => e == "locality" || e == "administrative_area_level_3") select x.long_name;
                return part.FirstOrDefault();
            }
        }

        public string State
        {
            get
            {
                var part = from x in address_components where x.types.Any(e => e == "administrative_area_level_1") select x.long_name;
                return part.FirstOrDefault();
            }
        }

        public string Zip
        {
            get
            {
                var part = from x in address_components where x.types.Any(e => e == "postal_code") select x.long_name;
                return part.FirstOrDefault();
            }
        }

        public string Address
        {
            get
            {
                return (City ?? "") + " " + (State ?? "") + " " + (Zip ?? "");
            }
        }

    }

    public class GoogleGeocodingAddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }


    /**** SEARCH RESULTS ****/

    public class GoogleSearchResult
    {
        public List<string> html_attributions { get; set; }
        public List<GoogleSearchPlace> results { get; set; }
        public GoogleSearchPlace result { get; set; }
        public string status { get; set; }
        public string next_page_token { get; set; }
    }

    public class GoogleSearchPlace
    {
        public string formatted_address { get; set; }
        public string formatted_phone_number { get; set; }
        public string international_phone_number { get; set; }
        public GoogleSearchGeometry geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public float rating { get; set; }
        public string reference { get; set; }
        public List<string> types { get; set; }
        public List<GoogleSearchReview> reviews { get; set; }
        public string url { get; set; }
        public string website { get; set; }
        public string vicinity { get; set; }
        public string MapUrl
        {
            get
            {
                return GooglePlaces.GetGoogleMap(geometry);
            }
        }

        public string MapLink
        {
            get
            {
                return GooglePlaces.GetGoogleMapUrl(geometry);
            }
        }
    }

    public class GoogleSearchReview
    {
        public List<GoogleSearchReviewAspect> aspects { get; set; }
        public string author_name { get; set; }
        public string author_url { get; set; }
        public string text { get; set; }
        public long time { get; set; }
    }

    public class GoogleSearchReviewAspect
    {
        public int rating { get; set; }
        public string type { get; set; }
    }

    public class GoogleSearchGeometry
    {
        public GoogleSearchCoords location { get; set; }
        public string location_type { get; set; }
        public GoogleSearchViewPort viewport { get; set; }

        public string Coords
        {
            get
            {
                return location.lat.ToString() + "," + location.lng.ToString();
            }
        }
    }

    public class GoogleSearchViewPort
    {
        public GoogleSearchCoords northeast { get; set; }
        public GoogleSearchCoords southwest { get; set; }
    }

    public class GoogleSearchCoords
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    /**** SEARCH RESULTS ****/

    public class GoogleTimeZoneResult
    {
        public int dstOffset { get; set; }
        public int rawOffset { get; set; }
        public string timeZoneId { get; set; }
        public string timeZoneName { get; set; }
        public string status { get; set; }
        public bool success { get { return status == GooglePlaceResultStatus.OK; } }
    }

}