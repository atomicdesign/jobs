﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace CivilianJobs.Helpers
{
    public class AAEmailSender
    {
        public static void Send(MailMessage message, string receiver)
        {
            SmtpClient server = new SmtpClient("smtp.gmail.com");
            server.Port = 587;
            server.EnableSsl = true;
            server.Credentials = new System.Net.NetworkCredential("civilianjobs@gmail.com", "civjobs123");
            message.From = new MailAddress("civilianjobs@gmail.com");
            message.Subject = "CivilianJobs.com";
            message.To.Add(receiver);
            if (message.CC.Count != 0 && message.CC.ToString() != "")
                message.To.Add(message.CC.ToString());
            server.Send(message);
        }
        
    }
}
