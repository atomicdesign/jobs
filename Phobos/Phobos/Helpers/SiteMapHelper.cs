﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using CivilianJobs.Models;
using CivilianJobs.Modules;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CivilianJobs.Helpers
{
    public class SiteMapHelper
    {
        //Move to Settings
        public static string _sitemap_url = "";
        public static string _sitemap_filename = "/sitemap.xml";
        public static string OK = "OK";

        public static string CreateSitemap()
        {
            try
            {
                List<SiteMapListing> urls = new List<SiteMapListing>();

                using (CivilianJobsContext db = new CivilianJobsContext())
                {
                    //Move these to static controller functions. make each controller report its available pages

                    var pages = db.Pages.ToList();
                    foreach (var p in pages) urls.Add(new SiteMapListing(_sitemap_url + "page/" + p.FullSEOURL));

                    var forms = db.Forms.ToList();
                    foreach (var p in forms) urls.Add(new SiteMapListing(_sitemap_url + "form/" + p.SEOURL));

                }

                XDocument doc = new XDocument(new XDeclaration("1.0", "UTF-8", "true"), null);
                XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
                XElement urlset = new XElement(ns + "urlset");

                foreach (var sml in urls) urlset.Add(sml.xelement);

                doc.Add(urlset);

                using (XmlWriter xw = XmlWriter.Create(HttpContext.Current.Server.MapPath(_sitemap_filename)))
                {
                    doc.WriteTo(xw);
                }

                return OK;
            }
            catch (Exception err)
            {
                Helpers.LogHelper.Log(err);
                return err.Message;
            }
        }

    }

    [NotMapped]
    public class SiteMapListing
    {
        public string loc { get; set; }
        public DateTime lastmod { get; set; }
        public string changefreq { get; set; }
        public double priority { get; set; }

        public SiteMapListing() : this("") { }
        public SiteMapListing(string _loc) : this(_loc, DateTime.UtcNow, "daily", 0.5) { }
        public SiteMapListing(string _loc, DateTime _lastmod, string _changefreq, double _priority)
        {
            loc = _loc;
            lastmod = _lastmod;
            changefreq = _changefreq;
            priority = _priority;
        }

        public XElement xelement
        {
            get
            {
                XElement retval = new XElement("url");
                retval.Add(new XElement("loc", loc));
                retval.Add(new XElement("lastmod", lastmod.ToString("yyyy-MM-dd")));
                retval.Add(new XElement("changefreq", changefreq));
                retval.Add(new XElement("priority", priority.ToString("F2")));
                return retval;
            }
        }
    }
}