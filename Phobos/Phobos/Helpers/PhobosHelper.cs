﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CivilianJobs.Models;
using CivilianJobs.Modules;
using MaxMind.GeoIP;
using System.Linq.Expressions;
using System.Web.Caching;
using System.Globalization;
using System.Web.Routing;
using System.Reflection;
using System.ComponentModel;

namespace CivilianJobs.Helpers
{

    public static class CivilianJobsHelper
    {
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        private static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }


        public static List<SelectListItem> CountryList
        {
            get
            {
                List<SelectListItem> CultureList = new List<SelectListItem>();
                CultureInfo[] getCultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

                foreach (CultureInfo getCulture in getCultureInfo)
                {
                    RegionInfo GetRegonInfo = new RegionInfo(getCulture.LCID);
                    SelectListItem item = new SelectListItem()
                    {
                        Text = GetRegonInfo.EnglishName,
                        Value = GetRegonInfo.EnglishName
                    };
                    if (!(CultureList.Contains(item)))
                    {
                        CultureList.Add(item);
                    }
                }

                CultureList.Sort();
                return CultureList;
            }
        }

        public static List<SelectListItem> StateList
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem() {Text = "State", Value = null},
                    new SelectListItem() {Text = "Alabama", Value = "AL"},
                    new SelectListItem() {Text = "Alaska", Value = "AK"},
                    new SelectListItem() {Text = "Arizona", Value = "AZ"},
                    new SelectListItem() {Text = "Arkansas", Value = "AR"},
                    new SelectListItem() {Text = "California", Value = "CA"},
                    new SelectListItem() {Text = "Colorado", Value = "CO"},
                    new SelectListItem() {Text = "Connecticut", Value = "CT"},
                    new SelectListItem() {Text = "District of Columbia", Value = "DC"},
                    new SelectListItem() {Text = "Delaware", Value = "DE"},
                    new SelectListItem() {Text = "Florida", Value = "FL"},
                    new SelectListItem() {Text = "Georgia", Value = "GA"},
                    new SelectListItem() {Text = "Hawaii", Value = "HI"},
                    new SelectListItem() {Text = "Idaho", Value = "ID"},
                    new SelectListItem() {Text = "Illinois", Value = "IL"},
                    new SelectListItem() {Text = "Indiana", Value = "IN"},
                    new SelectListItem() {Text = "Iowa", Value = "IA"},
                    new SelectListItem() {Text = "Kansas", Value = "KS"},
                    new SelectListItem() {Text = "Kentucky", Value = "KY"},
                    new SelectListItem() {Text = "Louisiana", Value = "LA"},
                    new SelectListItem() {Text = "Maine", Value = "ME"},
                    new SelectListItem() {Text = "Maryland", Value = "MD"},
                    new SelectListItem() {Text = "Massachusetts", Value = "MA"},
                    new SelectListItem() {Text = "Michigan", Value = "MI"},
                    new SelectListItem() {Text = "Minnesota", Value = "MN"},
                    new SelectListItem() {Text = "Mississippi", Value = "MS"},
                    new SelectListItem() {Text = "Missouri", Value = "MO"},
                    new SelectListItem() {Text = "Montana", Value = "MT"},
                    new SelectListItem() {Text = "Nebraska", Value = "NE"},
                    new SelectListItem() {Text = "Nevada", Value = "NV"},
                    new SelectListItem() {Text = "New Hampshire", Value = "NH"},
                    new SelectListItem() {Text = "New Jersey", Value = "NJ"},
                    new SelectListItem() {Text = "New Mexico", Value = "NM"},
                    new SelectListItem() {Text = "New York", Value = "NY"},
                    new SelectListItem() {Text = "North Carolina", Value = "NC"},
                    new SelectListItem() {Text = "North Dakota", Value = "ND"},
                    new SelectListItem() {Text = "Ohio", Value = "OH"},
                    new SelectListItem() {Text = "Oklahoma", Value = "OK"},
                    new SelectListItem() {Text = "Oregon", Value = "OR"},
                    new SelectListItem() {Text = "Pennsylvania", Value = "PA"},
                    new SelectListItem() {Text = "Rhode Island", Value = "RI"},
                    new SelectListItem() {Text = "South Carolina", Value = "SC"},
                    new SelectListItem() {Text = "South Dakota", Value = "SD"},
                    new SelectListItem() {Text = "Tennessee", Value = "TN"},
                    new SelectListItem() {Text = "Texas", Value = "TX"},
                    new SelectListItem() {Text = "Utah", Value = "UT"},
                    new SelectListItem() {Text = "Vermont", Value = "VT"},
                    new SelectListItem() {Text = "Virginia", Value = "VA"},
                    new SelectListItem() {Text = "Washington", Value = "WA"},
                    new SelectListItem() {Text = "West Virginia", Value = "WV"},
                    new SelectListItem() {Text = "Wisconsin", Value = "WI"},
                    new SelectListItem() {Text = "Wyoming", Value = "WY"}
                };
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                if (email.Trim() == String.Empty) throw new Exception();
                System.Net.Mail.MailAddress test = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }


        public static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return
                        "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return
                        "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return
                        "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return
                        "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public static string GetMD5Hash(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] hash = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(input));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (byte b in hash) sb.Append(b.ToString("X2"));
                return sb.ToString();
            }
        }

        public static void PushValidationErrorToModelState(
            System.Data.Entity.Validation.DbEntityValidationException err,
            System.Web.Mvc.ModelStateDictionary ModelState)
        {
            foreach (System.Data.Entity.Validation.DbEntityValidationResult everr in err.EntityValidationErrors)
            {
                foreach (System.Data.Entity.Validation.DbValidationError dbverr in everr.ValidationErrors)
                {
                    ModelState.AddModelError(dbverr.PropertyName, dbverr.ErrorMessage);
                }
            }
        }

        public static MembershipUserResetPasswordResult ResetUserPassword(string Username, string NewPassword = "",
            bool SendEmail = true)
        {
            MembershipUserResetPasswordResult retval = new MembershipUserResetPasswordResult() {Username = Username};
            MembershipUser currentUser = null;
            string newPassword = "";
            try
            {
                currentUser = Membership.GetUser(Username, false);
                if (currentUser == null) throw new Exception("Invalid username");

                currentUser.UnlockUser();
                newPassword = currentUser.ResetPassword();

                if (String.IsNullOrWhiteSpace(NewPassword))
                {
                    retval.NewPassword = newPassword;
                }
                else
                {
                    if (currentUser.ChangePassword(newPassword, NewPassword))
                    {
                        retval.NewPassword = NewPassword;
                    }
                    else
                    {
                        throw new Exception("Unable to set password to " + NewPassword);
                    }
                }
                retval.Success = true;
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
                retval.Success = false;
                retval.Message = err.ToString();
            }

            if (retval.Success && SendEmail)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("Your password for enhancedcareMD has been reset.<br/>");
                sb.AppendFormat("Your username is: {0}<br/>\n", retval.Username);
                sb.AppendFormat("Your temporary password is: {0}<br/>\n", retval.NewPassword);

                //TODO: Pull from settings
                EmailMessage em = new EmailMessage("info@enhancedcaremd.com", currentUser.Email, "Password Reset",
                    sb.ToString());
                try
                {
                    using (CivilianJobsContext db = new CivilianJobsContext())
                    {
                        db.EmailQueue.Add(em);
                        db.SaveChanges();
                    }

                    EmailService es = new EmailService();
                    es.SendBatch();

                    retval.EmailSent = true;
                }
                catch (Exception err)
                {
                    LogHelper.Log(err);
                    retval.EmailSent = false;
                    retval.Message = err.Message;
                }
            }
            return retval;
        }

        public static string GetEnumDescription(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof (DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute) attrs[0]).Description;
                }
            }

            return en.ToString();
        }
    }

    public class MembershipUserResetPasswordResult
    {
        public string Username { get; set; }
        public bool Success { get; set; }
        public bool EmailSent { get; set; }
        public string Message { get; set; }
        public string NewPassword { get; set; }

        public MembershipUserResetPasswordResult()
        {
            Success = false;
            Message = String.Empty;
        }

    }

    public class EnumObject
    {
        public string Value { get; set; }
        public int Key { get; set; }

        public static IEnumerable<EnumObject> EnumToclientEnumShort<T>(T enumType) where T : Type
        {
            foreach (var value in Enum.GetValues(enumType))
                yield return new EnumObject { Key = (int)Enum.Parse(enumType, value.ToString()), Value = value.ToString() };
        }

    }

}