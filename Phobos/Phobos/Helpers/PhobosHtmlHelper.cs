﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CivilianJobs.Models;
using CivilianJobs.Modules;
using MaxMind.GeoIP;
using System.Linq.Expressions;

namespace CivilianJobs.Helpers
{
    public static class CivilianJobsHtmlHelper
    {
        public static IHtmlString RenderBlock(this HtmlHelper htmlHelper, string blockname)
        {
            string value = String.Empty;
            using (CivilianJobsContext db = new CivilianJobsContext())
            {
                Block b = db.Blocks.FirstOrDefault(e => e.Name == blockname);
                if (b != null) value = b.HTML;
            }
            return MvcHtmlString.Create(value);
        }

        public static IHtmlString RenderModule(this HtmlHelper htmlHelper, string ModuleName, params object[] p)
        {
            string value = String.Empty;
            Type t = Type.GetType("CivilianJobs.Modules." + ModuleName);
            if (t != null)
            {
                CivilianJobsBaseModule i = (CivilianJobsBaseModule)Activator.CreateInstance(t);
                value = i.RenderModule(htmlHelper, p);
            }

            return MvcHtmlString.Create(value);
        }


        public static IHtmlString RenderPuckScripts(this HtmlHelper htmlHelper)
        {
            CivilianJobsPuck p = (CivilianJobsPuck)htmlHelper.ViewBag.CivilianJobsPuck;
            if (p != null) return p.RenderScripts();
            return MvcHtmlString.Create(String.Empty);
        }


        public static IHtmlString RenderPuckStyles(this HtmlHelper htmlHelper)
        {
            CivilianJobsPuck p = (CivilianJobsPuck)htmlHelper.ViewBag.CivilianJobsPuck;
            if (p != null) return p.RenderStyles();
            return MvcHtmlString.Create(String.Empty);
        }

        public static MvcHtmlString BootstrapTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes, string prepend = "", string append = "")
        {
            var metadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);

            if (htmlAttributes != null && !htmlAttributes.ContainsKey("placeholder")) htmlAttributes.Add("placeholder", metadata.DisplayName);

            StringBuilder retval = new StringBuilder();
            retval.Append(htmlHelper.TextBoxFor(expression, htmlAttributes).ToString());

            bool usesWrapper = false;
            string wrapperClasses = "";

            if (prepend != String.Empty && prepend != null)
            {
                usesWrapper = true;
                wrapperClasses += "input-prepend ";
                retval.Insert(0, String.Format("<span class=\"add-on\">{0}</span>", prepend));
            }

            if (append != null && append != String.Empty)
            {
                usesWrapper = true;
                wrapperClasses += "input-append ";
                retval.Append(String.Format("<span class=\"add-on\">{0}</span>", append));
            }

            if (usesWrapper)
            {
                retval.Insert(0, String.Format("<div class=\"{0}\">", wrapperClasses)).Append("</div>");
            }

            return new MvcHtmlString(retval.ToString());
        }

    }
}