﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CivilianJobs.Models;
using CivilianJobs.Modules;
using MaxMind.GeoIP;
using System.Linq.Expressions;

namespace CivilianJobs.Helpers
{
    public static class MediaHelper
    {
        public static MediaUploadResult AddFile(HttpPostedFileBase upFile, string base_path)
        {
            MediaUploadResult result = new MediaUploadResult();
            result.Success = true;

            if (upFile == null || upFile.ContentLength == 0)
            {
                result.Success = false;
                result.ErrorMessage = "File empty.";
            }

            if (result.Success)
            {
                try
                {
                    var base_url = base_path.Replace("~", "");
                    var mapped_path = HttpContext.Current.Server.MapPath(base_path);
                    System.IO.Directory.CreateDirectory(mapped_path);

                    var filename = System.IO.Path.GetFileName(upFile.FileName);
                    var path = System.IO.Path.Combine(mapped_path, filename);

                    result.Filename = filename;
                    result.FullPathSystem = path;
                    result.FullPathWeb = base_url + filename;

                    FileInfo fi = new FileInfo(path);
                    if (fi.Exists) fi.Delete();

                    upFile.SaveAs(path);
                }
                catch (Exception err)
                {
                    result.Success = false;
                    result.ErrorMessage = err.ToString();
                }
            }

            return result;
        }

        /**** MEDIA ****/
        public static MediaUploadResult AddImage(HttpPostedFileBase Image, string base_path) { return AddImage(Image, base_path, false, 0, 0); }
        public static MediaUploadResult AddImage(HttpPostedFileBase Image, string base_path, bool createThumbnail, int thumbW, int thumbH)
        {
            MediaUploadResult result = new MediaUploadResult();
            result.Success = true;

            if (Image == null || Image.ContentLength == 0)
            {
                result.Success = false;
                result.ErrorMessage = "Image empty.";
            }

            if (result.Success && !System.Text.RegularExpressions.Regex.IsMatch(Image.ContentType, "image"))
            {
                result.Success = false;
                result.ErrorMessage = "Not an image.";
            }

            if (result.Success)
            {
                try
                {
                    var base_url = base_path.Replace("~", "");
                    var mapped_path = HttpContext.Current.Server.MapPath(base_path);
                    System.IO.Directory.CreateDirectory(mapped_path);

                    var filename = System.IO.Path.GetFileName(Image.FileName);
                    var path = System.IO.Path.Combine(mapped_path, filename);

                    result.Filename = filename;
                    result.FullPathSystem = path;
                    result.FullPathWeb = base_url + filename;

                    FileInfo fi = new FileInfo(path);
                    if (fi.Exists) fi.Delete();

                    Image.SaveAs(path);

                    if (createThumbnail)
                    {
                        var thumb_path = System.IO.Path.Combine(mapped_path, "thumb_" + filename);
                        result.ThumbSuccess = true;
                        result.ThumbFilename = "thumb_" + filename;
                        result.ThumbFullPathWeb = base_url + result.ThumbFilename;
                        result.ThumbFullPathSystem = thumb_path;
                        try
                        {
                            using (Bitmap full = new Bitmap(path))
                            {
                                if (full.Height > thumbH || full.Width > thumbW)
                                {
                                    using (Bitmap thumb = new Bitmap(thumbW, thumbH))
                                    {
                                        using (Graphics g = Graphics.FromImage(thumb))
                                        {
                                            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                            g.DrawImage(full, 0, 0, thumbW, thumbH);
                                        }
                                        thumb.Save(thumb_path);
                                    }
                                }
                                else
                                {
                                    Image.SaveAs(thumb_path);
                                }
                            }
                        }
                        catch (Exception err)
                        {
                            result.ThumbSuccess = false;
                            result.ThumbErrorMessage = err.ToString();
                        }
                    }
                }
                catch (Exception err)
                {
                    result.Success = false;
                    result.ErrorMessage = err.ToString();
                }
            }
            return result;

        }
    }

    public class MediaUploadResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string Filename { get; set; }
        public string FullPathWeb { get; set; }
        public string FullPathSystem { get; set; }

        public bool ThumbSuccess { get; set; }
        public string ThumbErrorMessage { get; set; }
        public string ThumbFilename { get; set; }
        public string ThumbFullPathWeb { get; set; }
        public string ThumbFullPathSystem { get; set; }

        public MediaUploadResult() { }
    }
}