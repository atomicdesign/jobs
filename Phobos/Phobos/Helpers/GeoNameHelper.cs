﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CivilianJobs.Models;
using CivilianJobs.Modules;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CivilianJobs.Helpers
{
    public static class GeoNameHelper
    {

        //TODO: Allow it to pull from a URL
        //TODO: Allow it to be pushed from an external source

        public static void LoadCountryFileAsync(string filename)
        {
            Action<object> action = (object f)=>{
                LoadCountryFile((string)f);
            };

            Task t1 = new Task(action, HttpContext.Current.Server.MapPath(filename));

            t1.Start();
        }

        public static void LoadCountryFile(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            if (!fi.Exists) return;
            GeoName temp;
            int count = 0;
            using (CivilianJobsContext db = new CivilianJobsContext())
            {
                using (StreamReader sr = new StreamReader(fi.OpenRead()))
                {
                    while (!sr.EndOfStream)
                    {
                        try
                        {
                            temp = new GeoName { Consume = sr.ReadLine() };
                            if (temp.feature_class == "A" && temp.feature_code == "ADMD")
                            {
                                db.GeoNames.Add(temp);
                                count++;
                            }
                            if (count > 10)
                            {
                                db.SaveChanges();
                                count = 0;
                            }
                        }
                        catch (Exception err)
                        {
                            Helpers.LogHelper.Log(err);
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public static void OutputCountryFileAsync(string filename)
        {
            Action<object> action = (object f) =>
            {
                OutputCountryFile((string)f);
            };

            Task t1 = new Task(action, HttpContext.Current.Server.MapPath(filename));

            t1.Start();
        }

        public static void OutputCountryFile(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists) fi.Delete();
            try
            {
                using (CivilianJobsContext db = new CivilianJobsContext())
                {
                    List<GeoName> geonames = db.GeoNames.ToList();
                    geonames = geonames.Distinct(new GeoNameEqualityComparer()).ToList();
                    using (StreamWriter sw = new StreamWriter(fi.OpenWrite()))
                    {
                        foreach (GeoName geoname in geonames) sw.WriteLine(geoname.Expel);
                    }
                }
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
            }
        }



        public static void LoadCountryInfo(string filename)
        {

        }

        public static void LoadPostalCodes(string filename)
        {

        }
    }
}