/*
 * D I S C L A I M E R          
 * 
 * WARNING: ANY USE BY YOU OF THE SAMPLE CODE PROVIDED IS AT YOUR OWN RISK.
 * 
 * Authorize.Net provides this code "as is" without warranty of any kind, either
 * express or implied, including but not limited to the implied warranties of 
 * merchantability and/or fitness for a particular purpose.   
 * Authorize.Net owns and retains all right, title and interest in and to the 
 * Automated Recurring Billing intellectual property.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.IO;
using CivilianJobs.Models;
using System.Transactions;
using System.Data.Entity;

namespace CivilianJobs.Helpers.AuthorizeNet
{

    public class AuthorizeNetSettings
    {
        private static string _ARB_URL_TEST = "https://apitest.authorize.net/xml/v1/request.api";
        private static string _ARB_URL_LIVE = "https://api.authorize.net/xml/v1/request.api";

        private static string _AIM_URL_TEST = "https://test.authorize.net/gateway/transact.dll";
        private static string _AIM_URL_LIVE = "https://secure.authorize.net/gateway/transact.dll";

        private string _userLoginName { get; set; }
        private string _userTransactionKey { get; set; }

        public bool TestMode { get; set; }

        public AuthorizeNetSettings(){
            TestMode = false;
            _userLoginName = SiteSettingHelper.GetSetting("AuthorizeNet Login Name");
            _userTransactionKey = SiteSettingHelper.GetSetting("AuthorizeNet Transaction Key");
        }

        public AuthorizeNetSettings(bool testMode) : this()
        {
            TestMode = testMode;
        }

        public string API_AIM { get { return (TestMode ? _AIM_URL_TEST : _AIM_URL_LIVE); } }
        public string API_ARB { get { return (TestMode ? _ARB_URL_TEST : _ARB_URL_LIVE); } }

        public string LoginName { get { return _userLoginName; } }
        public string TransactionKey { get { return _userTransactionKey; } }
    }


    public enum AuthorizeNetAimTransactionType
    {
        AUTH_CAPTURE,
        AUTH_ONLY,
        CAPTURE_ONLY,
        CREDIT,
        PRIOR_AUTH_CAPTURE,
        VOID
    }

    public enum AuthorizeNetAimMethod
    {
        CC,
        ECHECK
    }

    public class AuthorizeNetAIM
    {
        //TODO: validate values
        public Dictionary<string, string> _values = new Dictionary<string, string>();

        public bool TestMode
        {
            get
            {
                return AuthorizeNetSettings.TestMode;
            }
            set
            {
                AuthorizeNetSettings.TestMode = value;
                x_test_request = AuthorizeNetSettings.TestMode.ToString().ToUpper();
            }
        }

        protected AuthorizeNetSettings AuthorizeNetSettings { get; set; }

        protected string _FullPostString;
        protected string _FullResponse;
        protected string _ErrorMessage;
        protected AuthorizeNetAimResponse _AuthorizeNetAimResponse;

        public string FullPostString
        {
            get { return _FullPostString ?? (_FullPostString = String.Empty); }
            protected set { _FullPostString = value; }
        }
        public string FullResponse
        {
            get { return _FullResponse ?? (_FullResponse = String.Empty); }
            protected set { _FullResponse = value; }
        }
        public string ErrorMessage
        {
            get { return _ErrorMessage ?? (_ErrorMessage = String.Empty); }
            protected set { _ErrorMessage = value; }
        }
        public AuthorizeNetAimResponse AuthorizeNetAimResponse
        {
            get { return _AuthorizeNetAimResponse; }
            protected set { _AuthorizeNetAimResponse = value; }
        }

        //Store these separately as we need to validate them

        protected string _x_login;
        protected string _x_tran_key;

        public string x_login
        {
            get { return _x_login ?? (_x_login = String.Empty); }
            protected set { _x_login = value; }
        }
        public string x_tran_key
        {
            get { return _x_tran_key ?? (_x_tran_key = String.Empty); }
            protected set { _x_tran_key = value; }
        }

        //The stuff that gets set by the user
        public AuthorizeNetAimMethod Method
        {
            get
            {
                return (AuthorizeNetAimMethod)Enum.Parse(typeof(AuthorizeNetAimMethod), GetValue("x_method"));
            }
            set
            {
                SetValue("x_method", value.ToString());
            }
        }

        public AuthorizeNetAimTransactionType TransactionType
        {
            get
            {
                return (AuthorizeNetAimTransactionType)Enum.Parse(typeof(AuthorizeNetAimTransactionType), GetValue("x_type"));
            }
            set
            {
                SetValue("x_type", value.ToString());
            }
        }

        public decimal Amount
        {
            get
            {
                decimal retval = 0;
                if (decimal.TryParse(GetValue("x_amount"), out retval)) return retval;
                return 0;
            }
            set { SetValue("x_amount", value.ToString("F2")); }
        }

        public string Description { get { return GetValue("x_description"); } set { SetValue("x_description", value); } }

        public string FirstName { get { return GetValue("x_first_name"); } set { SetValue("x_first_name", value); } }
        public string LastName { get { return GetValue("x_last_name"); } set { SetValue("x_last_name", value); } }
        public string Phone { get { return GetValue("x_phone"); } set { SetValue("x_phone", value); } }
        public string Address { get { return GetValue("x_address"); } set { SetValue("x_address", value); } }
        public string City { get { return GetValue("x_city"); } set { SetValue("x_city", value); } }
        public string State { get { return GetValue("x_state"); } set { SetValue("x_state", value); } }
        public string Zip { get { return GetValue("x_zip"); } set { SetValue("x_zip", value); } }
        public string Country { get { return GetValue("x_country"); } set { SetValue("x_country", value); } }
        public string Email { get { return GetValue("x_email"); } set { SetValue("x_email", value); } }

        public string CardNum { get { return GetValue("x_card_num"); } set { SetValue("x_card_num", value); } }
        public string ExpDate { get { return GetValue("x_exp_date"); } set { SetValue("x_exp_date", value); } }
        public string CardCode { get { return GetValue("x_card_code"); } set { SetValue("x_card_code", value); } }

        protected string x_delim_data { get { return GetValue("x_delim_data"); } set { SetValue("x_delim_data", value); } }
        protected string x_delim_char { get { return GetValue("x_delim_char"); } set { SetValue("x_delim_char", value); } }
        protected string x_relay_response { get { return GetValue("x_relay_response"); } set { SetValue("x_relay_response", value); } }
        protected string x_version { get { return GetValue("x_version"); } set { SetValue("x_version", value); } }
        protected string x_email_customer { get { return GetValue("x_email_customer"); } set { SetValue("x_email_customer", value); } }
        protected string x_test_request { get { return GetValue("x_test_request"); } set { SetValue("x_test_request", value); } }

        //Constructors

        public AuthorizeNetAIM()
        {
            x_login = "";
            x_tran_key = "";

            Method = AuthorizeNetAimMethod.CC;
            TransactionType = AuthorizeNetAimTransactionType.AUTH_CAPTURE;

            x_delim_data = "TRUE";
            x_delim_char = "|";
            x_relay_response = "FALSE";
            x_test_request = "FALSE";
            x_version = "3.1";
            x_email_customer = "FALSE";

            AuthorizeNetSettings = new AuthorizeNetSettings();
            x_login = AuthorizeNetSettings.LoginName;
            x_tran_key = AuthorizeNetSettings.TransactionKey;

            TestMode = false;
        }

        //Helper functions

        private void SetValue(string key, string val)
        {
            if (_values.ContainsKey(key))
            {
                _values[key] = val;
            }
            else
            {
                _values.Add(key, val);
            }
        }

        private string GetValue(string key)
        {
            string retval = null;
            if (!_values.TryGetValue(key, out retval)) retval = null;
            return retval;
        }


        protected bool GetQueryString()
        {
            FullPostString = String.Empty;

            if (x_login == null || x_login == String.Empty)
            {
                ErrorMessage = "Login not specified";
                return false;
            }

            if (x_tran_key == null || x_tran_key == String.Empty)
            {
                ErrorMessage = "Transaction Key Not Specified";
                return false;
            }

            try
            {
                string tempValue;
                StringBuilder sb = new StringBuilder();

                sb.Append(String.Format("x_login={0}&x_tran_key={1}", x_login, x_tran_key));

                foreach (string key in _values.Keys)
                {
                    tempValue = GetValue(key);
                    if (tempValue != null && tempValue != String.Empty) sb.Append(String.Format("&{0}={1}", key, HttpUtility.UrlEncode(tempValue)));
                }

                FullPostString = sb.ToString();

                return true;
            }
            catch (Exception err)
            {
                ErrorMessage = err.ToString();
                return false;
            }
        }

        protected bool SendRequest()
        {
            if(!GetQueryString()) return false;

            FullResponse = String.Empty;

            try
            {
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(AuthorizeNetSettings.API_AIM);
                objRequest.Method = "POST";
                objRequest.ContentLength = FullPostString.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";

                using (StreamWriter myWriter = new StreamWriter(objRequest.GetRequestStream())) myWriter.Write(FullPostString);

                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream())) FullResponse = sr.ReadToEnd();

                return true;
            }
            catch (Exception err)
            {
                ErrorMessage = err.ToString();
                return false;
            }
        }

        protected bool ProcessResponse()
        {
            AuthorizeNetAimResponse = new AuthorizeNetAimResponse(FullResponse);

            if (AuthorizeNetAimResponse.ResponseCode == AuthorizeNetAimResponseCode.APPROVED) return true;

            ErrorMessage = AuthorizeNetAimResponse.ResponseReasonText;
            return false;
        }

        public bool ProcessPayment()
        {
            return SendRequest() && ProcessResponse();
        }
    }

    public enum AuthorizeNetAimResponseFields
    {
        RESPONSE_CODE,
        RESPONSE_SUBCODE,
        RESPONSE_REASON_CODE,
        RESPONSE_REASON_TEXT,
        AUTHORIZATION_CODE,
        AVS_RESPONSE,
        TRANSACTION_ID,
        INVOICE_NUMBER,
        DESCRIPTION,
        AMOUNT,
        METHOD,
        TRANSACTION_TYPE,
        CUSTOMER_ID,
        FIRST_NAME,
        LAST_NAME,
        COMPANY,
        ADDRESS,
        CITY,
        STATE,
        ZIP_CODE,
        COUNTRY,
        PHONE,
        FAX,
        EMAIL_ADDRESS,
        SHIP_TO_FIRST_NAME,
        SHIP_TO_LAST_NAME,
        SHIP_TO_COMPANY,
        SHIP_TO_ADDRESS,
        SHIP_TO_CITY,
        SHIP_TO_STATE,
        SHIP_TO_ZIP_CODE,
        SHIP_TO_COUNTRY,
        TAX,
        DUTY,
        FREIGHT,
        TAX_EXEMPT,
        PURCHASE_ORDER_NUMBER,
        MD5_HASH,
        CARD_CODE_RESPONSE,
        CARDHOLDER_AUTHENTICATION_VERIFICATION_RESPONSE,
        BLANK_41,
        BLANK_42,
        BLANK_43,
        BLANK_44,
        BLANK_45,
        BLANK_46,
        BLANK_47,
        BLANK_48,
        BLANK_49,
        BLANK_50,
        ACCOUNT_NUMBER,
        CARD_TYPE,
        SPLIT_TENDER_ID,
        REQUESTED_AMOUNT,
        BALANCE_ON_CARD
    }

    public enum AuthorizeNetAimResponseCode
    {
        UNUSED,
        APPROVED,
        DECLINED,
        ERROR,
        HELD_FOR_REVIEW
    }

    public class AuthorizeNetAimResponse
    {
        private string[] _values;

        public AuthorizeNetAimResponse(string response)
        {
            _values = response.Split(new char[]{ '|' });
        }

        protected int _ResponseCode { get { return int.Parse(_values[(int)AuthorizeNetAimResponseFields.RESPONSE_CODE]); } }
        public AuthorizeNetAimResponseCode ResponseCode
        {
            get
            {
                return (AuthorizeNetAimResponseCode)_ResponseCode;
            }
        }
        protected string ResponseSubcode { get { return _values[(int)AuthorizeNetAimResponseFields.RESPONSE_SUBCODE]; } }

        public int ResponseReasonCode { get { return int.Parse(_values[(int)AuthorizeNetAimResponseFields.RESPONSE_REASON_CODE]); } }
        public string ResponseReasonText { get { return _values[(int)AuthorizeNetAimResponseFields.RESPONSE_REASON_TEXT]; } }
        public string AuthorizationCode { get { return _values[(int)AuthorizeNetAimResponseFields.AUTHORIZATION_CODE]; } }
        
        public string AvsResponseCode { get { return _values[(int)AuthorizeNetAimResponseFields.AVS_RESPONSE]; } }
        public string AvsResponseText
        {
            get
            {
                string retval = "Unknown";
                switch (AvsResponseCode)
                {
                    case "A":
                        retval = "Address (Street) matches, ZIP does not";
                        break;
                    case "B":
                        retval = "Address information not provided for AVS check";
                        break;
                    case "E":
                        retval = "AVS error";
                        break;
                    case "G":
                        retval = "Non-U.S. Card Issuing Bank";
                        break;
                    case "N":
                        retval = "No Match on Address (Street) or ZIP";
                        break;
                    case "P":
                        retval = "AVS not applicable for this transaction";
                        break;
                    case "R":
                        retval = "Retry�System unavailable or timed out";
                        break;
                    case "S":
                        retval = "Service not supported by issuer";
                        break;
                    case "U":
                        retval = "Address information is unavailable";
                        break;
                    case "W":
                        retval = "Nine digit ZIP matches, Address (Street) does not";
                        break;
                    case "X":
                        retval = "Address (Street) and nine digit ZIP match";
                        break;
                    case "Y":
                        retval = "Address (Street) and five digit ZIP match";
                        break;
                    case "Z":
                        retval = "Five digit ZIP matches, Address (Street) does not";
                        break;
                }
                return retval;
            }
        }


        public string TransactionId { get { return _values[(int)AuthorizeNetAimResponseFields.TRANSACTION_ID]; } }

        public string InvoiceNumber { get { return _values[(int)AuthorizeNetAimResponseFields.INVOICE_NUMBER]; } }
        public string Description { get { return _values[(int)AuthorizeNetAimResponseFields.DESCRIPTION]; } }

        public string Amount { get { return _values[(int)AuthorizeNetAimResponseFields.AMOUNT]; } }

        public AuthorizeNetAimMethod Method { get { return (AuthorizeNetAimMethod)Enum.Parse(typeof(AuthorizeNetAimMethod), _values[(int)AuthorizeNetAimResponseFields.METHOD]); } }
        public AuthorizeNetAimTransactionType TransactionType { get { return (AuthorizeNetAimTransactionType)Enum.Parse(typeof(AuthorizeNetAimTransactionType), _values[(int)AuthorizeNetAimResponseFields.TRANSACTION_TYPE]); } }

        public string CustomerId { get { return _values[(int)AuthorizeNetAimResponseFields.CUSTOMER_ID]; } }
        public string FirstName { get { return _values[(int)AuthorizeNetAimResponseFields.FIRST_NAME]; } }
        public string LastName { get { return _values[(int)AuthorizeNetAimResponseFields.LAST_NAME]; } }
        public string Company { get { return _values[(int)AuthorizeNetAimResponseFields.COMPANY]; } }
        public string Address { get { return _values[(int)AuthorizeNetAimResponseFields.ADDRESS]; } }
        public string City { get { return _values[(int)AuthorizeNetAimResponseFields.CITY]; } }
        public string State { get { return _values[(int)AuthorizeNetAimResponseFields.STATE]; } }
        public string ZipCode { get { return _values[(int)AuthorizeNetAimResponseFields.ZIP_CODE]; } }
        public string Country { get { return _values[(int)AuthorizeNetAimResponseFields.COUNTRY]; } }
        public string Phone { get { return _values[(int)AuthorizeNetAimResponseFields.PHONE]; } }
        public string Fax { get { return _values[(int)AuthorizeNetAimResponseFields.FAX]; } }
        public string EmailAddress { get { return _values[(int)AuthorizeNetAimResponseFields.EMAIL_ADDRESS]; } }
        public string ShipToFirstName { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_FIRST_NAME]; } }
        public string ShipToLastName { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_LAST_NAME]; } }
        public string ShipToCompany { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_COMPANY]; } }
        public string ShipToAddress { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_ADDRESS]; } }
        public string ShipToCity { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_CITY]; } }
        public string ShipToState { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_STATE]; } }
        public string ShipToZipCode { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_ZIP_CODE]; } }
        public string ShipToCountry { get { return _values[(int)AuthorizeNetAimResponseFields.SHIP_TO_COUNTRY]; } }
        public string Tax { get { return _values[(int)AuthorizeNetAimResponseFields.TAX]; } }
        public string Duty { get { return _values[(int)AuthorizeNetAimResponseFields.DUTY]; } }
        public string Freight { get { return _values[(int)AuthorizeNetAimResponseFields.FREIGHT]; } }
        public string TaxExempt { get { return _values[(int)AuthorizeNetAimResponseFields.TAX_EXEMPT]; } }
        public string PurchaseOrderNumber { get { return _values[(int)AuthorizeNetAimResponseFields.PURCHASE_ORDER_NUMBER]; } }

        public string MD5Hash { get { return _values[(int)AuthorizeNetAimResponseFields.MD5_HASH]; } }

        public string CardCodeResponse { get { return _values[(int)AuthorizeNetAimResponseFields.CARD_CODE_RESPONSE]; } }
        public string CardCodeResponseText
        {
            get
            {
                string retval = "Unknown";
                switch (CardCodeResponse)
                {
                    case "M":
                        retval = "Match";
                        break;
                    case "N":
                        retval = "No Match";
                        break;
                    case "P":
                        retval = "Not Processed";
                        break;
                    case "S":
                        retval = "Should have been present";
                        break;
                    case "U":
                        retval = "Issuer unable to process request";
                        break;                        
                }
                return retval;
            }
        }

        public string CardholderAuthenticationVerificationResponse { get { return _values[(int)AuthorizeNetAimResponseFields.CARDHOLDER_AUTHENTICATION_VERIFICATION_RESPONSE]; } }

        public string CardholderAuthenticationVerificationResponseText
        {
            get
            {
                string retval = "Unknown";

                switch (CardholderAuthenticationVerificationResponse)
                {
                    case "0":
                        retval = "CAVV not validated because erroneous data was submitted";
                        break;
                    case "1":
                        retval = "CAVV failed validation";
                        break;
                    case "2":
                        retval = "CAVV passed validation";
                        break;
                    case "3":
                        retval = "CAVV validation could not be performed; issuer attempt incomplete";
                        break;
                    case "4":
                        retval = "CAVV validation could not be performed; issuer system error";
                        break;
                    case "5":
                        retval = "Reserved for future use";
                        break;
                    case "6":
                        retval = "Reserved for future use";
                        break;
                    case "7":
                        retval = "CAVV attempt � failed validation � issuer available (U.S.-issued card/non-U.S acquirer)";
                        break;
                    case "8":
                        retval = "CAVV attempt � passed validation � issuer available (U.S.-issued card/non-U.S. acquirer)";
                        break;
                    case "9":
                        retval = "CAVV attempt � failed validation � issuer unavailable (U.S.-issued card/non-U.S. acquirer)";
                        break;
                    case "A":
                        retval = "CAVV attempt � passed validation � issuer unavailable (U.S.-issued card/non-U.S. acquirer)";
                        break;
                    case "B":
                        retval = "CAVV passed validation, information only, no liability shift";
                        break;
                }

                return retval;
            }
        }

        public string Blank41 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_41]; } }
        public string Blank42 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_42]; } }
        public string Blank43 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_43]; } }
        public string Blank44 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_44]; } }
        public string Blank45 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_45]; } }
        public string Blank46 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_46]; } }
        public string Blank47 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_47]; } }
        public string Blank48 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_48]; } }
        public string Blank49 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_49]; } }
        public string Blank50 { get { return _values[(int)AuthorizeNetAimResponseFields.BLANK_50]; } }

        public string AccountNumber { get { return _values[(int)AuthorizeNetAimResponseFields.ACCOUNT_NUMBER]; } }
        public string CardType { get { return _values[(int)AuthorizeNetAimResponseFields.CARD_TYPE]; } }

        public string SplitTenderId { get { return _values[(int)AuthorizeNetAimResponseFields.SPLIT_TENDER_ID]; } }
        
        public string RequestedAmount { get { return _values[(int)AuthorizeNetAimResponseFields.REQUESTED_AMOUNT]; } }

        public string BalanceOnCard { get { return _values[(int)AuthorizeNetAimResponseFields.BALANCE_ON_CARD]; } }
    }

    public partial class AuthorizeNetSubscription
    {
        private XmlDocument xmldoc;
        private object apiResponse;

        protected Subscription _Subscription { get; set; }
        public Subscription Subscription
        {
            get
            {
                return _Subscription;
            }
            set
            {
                _Subscription = value;
                if (_Subscription != null)
                {
                    SubscriptionStartDate = _Subscription.SubscriptionStartDate;
                    SubscriptionAmount = _Subscription.SubscriptionAmount;
                    SubscriptionDescription = _Subscription.SubscriptionDescription;
                    SubscriptionID = _Subscription.GatewaySubscriptionID;
                    SubscriptionInvoiceNumber = _Subscription.SubscriptionInvoiceNumber;
                    SubscriptionMonthInterval = _Subscription.SubscriptionMonthInterval;
                    SubscriptionName = _Subscription.SubscriptionName;
                    SubscriptionNumOccurrences = _Subscription.SubscriptionNumOccurrences;
                    SubscriptionTrialAmount = _Subscription.SubscriptionTrialAmount;
                    SubscriptionTrialOccurrences = _Subscription.SubscriptionTrialOccurrences;

                    AuthNetStarted = false;
                    AuthNetSucceeded = false;
                }
            }
        }

        public AuthorizeNetSubscription()
        {
            AuthorizeNetSettings = new AuthorizeNetSettings();
            
            TestMode = false;

            SubscriptionStartDate = DateTime.Now;
            SubscriptionNoEndDate = true;
            SubscriptionHasTrial = false;
            SubscriptionMonthInterval = 1;
            BillingCountry = "United States";
            SubscriptionAmount = 0.00M;
            AuthNetStarted = false;
            AuthNetSucceeded = false;
            _refID = "CitrusWeb";
        }

        public bool Create()
        {
            PostRequest(ARBCreateSubscriptionRequest);
            return AfterAction("Create");
        }

        public bool Update()
        {
            PostRequest(ARBUpdateSubscriptionRequest);
            return AfterAction("Update");
        }

        public bool Cancel()
        {
            PostRequest(ARBCancelSubscriptionRequest);
            return AfterAction("Cancel");
        }

        public virtual bool AfterAction(string action)
        {
            if (AuthNetSucceeded)
            {
                switch (action)
                {
                    case "Create":
                        CreateSubscriptionRecord();
                        break;

                    case "Update":
                        UpdateSubscriptionRecord();
                        break;

                    case "Cancel":
                        SubscriptionID = null;
                        _Subscription.GatewaySubscriptionID = "";
                        _Subscription.GatewaySubscriptionStatus = "cancelled";
                        UpdateSubscriptionRecord();
                        break;
                }
            }
            return !HasError;
        }

        protected bool CreateSubscriptionRecord()
        {
            try
            {
                _Subscription = new Subscription();

                _Subscription.CustomerId = CustomerID;
                _Subscription.SubscriptionInvoiceNumber = SubscriptionInvoiceNumber;
                _Subscription.SubscriptionName = SubscriptionName;
                _Subscription.SubscriptionDescription = SubscriptionDescription;
                _Subscription.GatewaySubscriptionID = SubscriptionID;
                _Subscription.GatewaySubscriptionStatus = "active";
                _Subscription.GatewaySubscriptionPaymentNumber = 1;
                _Subscription.CreditCardLast4 = (RedactedCardNumber.Length >= 4 ? RedactedCardNumber.Substring(RedactedCardNumber.Length - 4) : "");
                _Subscription.ExpirationDate = CardExpirationDate;
                _Subscription.BillingFirstName = BillingFirstName;
                _Subscription.BillingLastName = BillingLastName;
                _Subscription.BillingAddress = BillingAddress;
                _Subscription.BillingCity = BillingCity;
                _Subscription.BillingState = BillingState;
                _Subscription.BillingZip = BillingZip;
                _Subscription.BillingCountry = BillingCountry;
                _Subscription.SubscriptionAmount = SubscriptionAmount;
                _Subscription.SubscriptionStartDate = SubscriptionStartDate;
                _Subscription.SubscriptionMonthInterval = SubscriptionMonthInterval;
                _Subscription.SubscriptionNumOccurrences = SubscriptionNumOccurrences;
                _Subscription.SubscriptionTrialOccurrences = SubscriptionTrialOccurrences;
                _Subscription.SubscriptionTrialAmount = SubscriptionTrialAmount;

                using (CivilianJobsContext db = new CivilianJobsContext())
                {
                    db.Subscriptions.Add(_Subscription);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
            }
            return false;
        }

        protected bool UpdateSubscriptionRecord()
        {
            if (_Subscription != null)
            {
                try
                {
                    _Subscription.CreditCardLast4 = (RedactedCardNumber.Length >= 4 ? RedactedCardNumber.Substring(RedactedCardNumber.Length - 4) : "");
                    _Subscription.ExpirationDate = CardExpirationDate;
                    
                    _Subscription.BillingFirstName = BillingFirstName;
                    _Subscription.BillingLastName = BillingLastName;
                    _Subscription.BillingAddress = BillingAddress;
                    _Subscription.BillingCity = BillingCity;
                    _Subscription.BillingState = BillingState;
                    _Subscription.BillingZip = BillingZip;
                    _Subscription.BillingCountry = BillingCountry;

                    using (CivilianJobsContext db = new CivilianJobsContext())
                    {
                        db.Entry(_Subscription).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                }
                catch (Exception err)
                {
                    LogHelper.Log(err);
                }
            }
            return false;
        }

        protected bool CancelSubscriptionRecord()
        {
            if (_Subscription != null)
            {
                try
                {
                    using (CivilianJobsContext db = new CivilianJobsContext())
                    {
                        db.Entry(_Subscription).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                }
                catch (Exception err)
                {
                    LogHelper.Log(err);
                }
            }
            return false;
        }

        #region AuthorizeNetInterface

        protected void PostRequest(object apiRequest)
        {
            XmlSerializer serializer;
            xmldoc = null;

            AuthNetStep = "PostRequest";
            AuthNetError = "";
            AuthNetDetailedError = "";
            AuthNetResponseStatus = "";

            AuthNetStarted = true;
            AuthNetSucceeded = false;

            if (apiRequest == null)
            {
                AuthNetError = AuthNetDetailedError = ValidationError;
                return;
            }

            PopulateMerchantAuthentication((ANetApiRequest)apiRequest);

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(AuthorizeNetSettings.API_ARB);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                webRequest.KeepAlive = true;

                // Serialize the request
                serializer = new XmlSerializer(apiRequest.GetType());
                XmlWriter writer = new XmlTextWriter(webRequest.GetRequestStream(), Encoding.UTF8);
                serializer.Serialize(writer, apiRequest);
                writer.Close();

                // Get the response
                WebResponse webResponse = webRequest.GetResponse();

                // Load the response from the API server into an XmlDocument.
                xmldoc = new XmlDocument();
                xmldoc.Load(XmlReader.Create(webResponse.GetResponseStream()));

                ProcessXmlResponse();
            }
            catch (Exception ex)
            {
                AuthNetError = ex.Message;
                AuthNetDetailedError = ex.ToString();
            }
        }

        protected void ProcessXmlResponse()
        {
            XmlSerializer serializer;
            apiResponse = null;
            AuthNetStep = "ProcessXmlResponse";

            try
            {
                // Use the root node to determine the type of response object to create
                switch (xmldoc.DocumentElement.Name)
                {
                    case "ARBCreateSubscriptionResponse":
                        serializer = new XmlSerializer(typeof(ARBCreateSubscriptionResponse));
                        apiResponse = (ARBCreateSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    case "ARBUpdateSubscriptionResponse":
                        serializer = new XmlSerializer(typeof(ARBUpdateSubscriptionResponse));
                        apiResponse = (ARBUpdateSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    case "ARBCancelSubscriptionResponse":
                        serializer = new XmlSerializer(typeof(ARBCancelSubscriptionResponse));
                        apiResponse = (ARBCancelSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    case "ARBGetSubscriptionStatusResponse":
                        serializer = new XmlSerializer(typeof(ARBGetSubscriptionStatusResponse));
                        apiResponse = (ARBGetSubscriptionStatusResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    case "ErrorResponse":
                        serializer = new XmlSerializer(typeof(ANetApiResponse));
                        apiResponse = (ANetApiResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    default:
                        throw new Exception("Unexpected type of object: " + xmldoc.DocumentElement.Name);
                        
                }

                ProcessResponse();
            }
            catch (Exception ex)
            {
                AuthNetError = ex.Message;
                AuthNetDetailedError = ex.ToString();
                apiResponse = null;
            }
        }

        protected void ProcessResponse()
        {
            AuthNetStep = "ProcessResponse";

            try
            {
                // Every response is based on ANetApiResponse so you can always do this sort of type casting.
                ANetApiResponse baseResponse = (ANetApiResponse)apiResponse;
                AuthNetResponseStatus = baseResponse.messages.resultCode.ToString();

                // If the result code is "Ok" then the request was successfully processed.
                if (baseResponse.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (apiResponse.GetType() == typeof(ARBCreateSubscriptionResponse))
                    {
                        ARBCreateSubscriptionResponse createResponse = (ARBCreateSubscriptionResponse)apiResponse;
                        SubscriptionID = createResponse.subscriptionId;
                    }
                    AuthNetSucceeded = true;
                }
                else
                {
                    // Write error messages to console window
                    for (int i = 0; i < baseResponse.messages.message.Length; i++)
                    {
                        AuthNetError += baseResponse.messages.message[i].text;
                        AuthNetDetailedError += "[" + baseResponse.messages.message[i].code + "] " + baseResponse.messages.message[i].text + " ";
                    }
                }
            }
            catch (Exception ex)
            {
                AuthNetError = ex.Message;
                AuthNetDetailedError = ex.ToString();
            }
        }

        protected void PopulateMerchantAuthentication(ANetApiRequest request)
        {
            request.merchantAuthentication = new merchantAuthenticationType();
            request.merchantAuthentication.name = AuthorizeNetSettings.LoginName;
            request.merchantAuthentication.transactionKey = AuthorizeNetSettings.TransactionKey;
            request.refId = _refID;
        }

        #endregion

        #region Validators

        private bool CleanAndValidateAdd()
        {
            //uses the setter validation to confirm
            try
            {
                if (SubscriptionID != null || SubscriptionID == String.Empty) throw new ArgumentException("Subscription ID must be null.");

                CardNumber = CardNumber;
                CardExpirationDate = CardExpirationDate;
                CardCode = CardCode;

                SubscriptionName = SubscriptionName;
                SubscriptionDescription = SubscriptionDescription;
                SubscriptionInvoiceNumber = SubscriptionInvoiceNumber;

                SubscriptionStartDate = SubscriptionStartDate;
                SubscriptionMonthInterval = SubscriptionMonthInterval;
                SubscriptionNumOccurrences = SubscriptionNumOccurrences;

                SubscriptionTrialOccurrences = SubscriptionTrialOccurrences;
                SubscriptionTrialAmount = SubscriptionTrialAmount;

                BillingFirstName = BillingFirstName;
                BillingLastName = BillingLastName;
                BillingAddress = BillingAddress;
                BillingCity = BillingCity;
                BillingState = BillingState;
                BillingZip = BillingZip;
                BillingCountry = BillingCountry;

                CustomerID = CustomerID;
                CustomerEmail = CustomerEmail;
                CustomerPhone = CustomerPhone;

                return true;
            }
            catch (Exception e)
            {
                ValidationError = e.Message;
                return false;
            }
        }

        private bool CleanAndValidateUpdate()
        {
            //uses the setter validation to confirm
            try
            {
                SubscriptionID = SubscriptionID ?? String.Empty;

                if (CardNumber != null)
                {
                    CardNumber = CardNumber;
                    CardExpirationDate = CardExpirationDate;
                    CardCode = CardCode;
                }

                if (BillingAddress != null)
                {
                    BillingFirstName = BillingFirstName;
                    BillingLastName = BillingLastName;
                    BillingAddress = BillingAddress;
                    BillingCity = BillingCity;
                    BillingState = BillingState;
                    BillingZip = BillingZip;
                    BillingCountry = BillingCountry;
                }

                return true;
            }
            catch (Exception e)
            {
                ValidationError = e.Message;
                return false;
            }
        }

        private bool CleanAndValidateCancel()
        {
            //uses the setter validation to confirm
            try
            {
                SubscriptionID = SubscriptionID ?? String.Empty;
                return true;
            }
            catch (Exception e)
            {
                ValidationError = e.Message;
                return false;
            }
        }

        #endregion

        #region AuthorizeNetRequestCreators

        public ARBCreateSubscriptionRequest ARBCreateSubscriptionRequest
        {
            get
            {
                if (!CleanAndValidateAdd()) return null;

                ARBSubscriptionType sub = new ARBSubscriptionType();
                creditCardType creditCard = new creditCardType();

                creditCard.cardNumber = CardNumber;
                creditCard.expirationDate = CardExpirationDate;	// required format for API is YYYY-MM
                creditCard.cardCode = CardCode;

                sub.name = SubscriptionName;
                sub.payment = new paymentType();
                sub.payment.Item = creditCard;

                sub.billTo = new nameAndAddressType();
                sub.billTo.firstName = BillingFirstName;
                sub.billTo.lastName = BillingLastName;
                sub.billTo.address = BillingAddress;
                sub.billTo.city = BillingCity;
                sub.billTo.state = BillingState;
                sub.billTo.zip = BillingZip;
                sub.billTo.country = BillingCountry;

                // Create a subscription that is 999 monthly payments 
                sub.paymentSchedule = new paymentScheduleType();
                sub.paymentSchedule.startDate = SubscriptionStartDate;
                sub.paymentSchedule.startDateSpecified = true;

                sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
                sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                sub.paymentSchedule.interval.length = SubscriptionMonthInterval;

                sub.paymentSchedule.totalOccurrences = SubscriptionNumOccurrences;
                sub.paymentSchedule.totalOccurrencesSpecified = true;

                sub.paymentSchedule.trialOccurrences = SubscriptionTrialOccurrences;
                sub.paymentSchedule.trialOccurrencesSpecified = true;

                sub.trialAmount = SubscriptionTrialAmount;
                sub.trialAmountSpecified = true;

                sub.amount = SubscriptionAmount;
                sub.amountSpecified = true;

                sub.order = new orderType();
                sub.order.description = SubscriptionDescription;
                sub.order.invoiceNumber = SubscriptionInvoiceNumber;

                sub.customer = new customerType();
                sub.customer.id = CustomerID;
                sub.customer.email = CustomerEmail;
                sub.customer.phoneNumber = CustomerPhone;

                ARBCreateSubscriptionRequest request = new ARBCreateSubscriptionRequest();

                request.subscription = sub;

                return request;
            }
        }

        public ARBUpdateSubscriptionRequest ARBUpdateSubscriptionRequest
        {
            get
            {
                if (!CleanAndValidateUpdate()) return null;

                ARBSubscriptionType sub = new ARBSubscriptionType();
                
                if (CardNumber != null)
                {
                    creditCardType creditCard = new creditCardType();
                    creditCard.cardNumber = CardNumber;
                    creditCard.expirationDate = CardExpirationDate;	// required format for API is YYYY-MM
                    creditCard.cardCode = CardCode;
                    sub.payment = new paymentType();
                    sub.payment.Item = creditCard;
                }

                if (BillingAddress != null)
                {
                    sub.billTo = new nameAndAddressType();
                    sub.billTo.firstName = BillingFirstName;
                    sub.billTo.lastName = BillingLastName;
                    sub.billTo.address = BillingAddress;
                    sub.billTo.city = BillingCity;
                    sub.billTo.state = BillingState;
                    sub.billTo.zip = BillingZip;
                    sub.billTo.country = BillingCountry;
                }

                ARBUpdateSubscriptionRequest request = new ARBUpdateSubscriptionRequest();

                request.subscriptionId = SubscriptionID;
                request.subscription = sub;

                return request;
            }
        }

        public ARBCancelSubscriptionRequest ARBCancelSubscriptionRequest
        {
            get
            {
                if (!CleanAndValidateCancel()) return null;

                ARBCancelSubscriptionRequest request = new ARBCancelSubscriptionRequest();

                request.subscriptionId = SubscriptionID;

                return request;
            }
        }

        #endregion

        #region Variables

        protected string _refID;

        private string _SubscriptionID;
        public string SubscriptionID
        {
            get { return _SubscriptionID; }
            set
            {
                if (value != null && value.Trim() == String.Empty) throw new ArgumentException("Subscription ID must be null or non-blank");
                _SubscriptionID = value; 
            }
        }

        private string _CardNumber;
        public string CardNumber
        {
            get { return _CardNumber; }
            set
            {
                string tempValue = Regex.Replace(value, "[^0-9]", "").Trim();
                if (tempValue.Length < 14 || tempValue.Length > 16) throw new ArgumentException("Credit Card Number is Invalid");
                _CardNumber = tempValue;
            }
        }

        public string RedactedCardNumber
        {
            get
            {
                if (CardNumber.Length >= 4) return "************" + CardNumber.Substring(CardNumber.Length - 4);
                return String.Empty;
            }
        }

        private string _CardExpirationDate;
        public string CardExpirationDate
        {
            get { return _CardExpirationDate; }
            set
            {
                if (value == null) throw new ArgumentException("Expiration Date cannot be null.");
                string tempValue = value.Trim();
                if (tempValue == String.Empty) throw new ArgumentException("Expiration Date cannot be blank.");
                if (!Regex.IsMatch(tempValue, @"\d\d\d\d-\d\d")) throw new ArgumentException("Expiration Date is not in the correct format (YYYY-MM).");
                _CardExpirationDate = tempValue;
            }
        }

        private string _CardCode;
        public string CardCode
        {
            get { return _CardCode; }
            set
            {
                if (value == null) throw new ArgumentException("Card Code cannot be null.");
                string tempValue = Regex.Replace(value, "[^0-9]", "").Trim();
                if (tempValue.Length < 3 || tempValue.Length > 4) throw new ArgumentException("Credit Card Code is Invalid");
                _CardCode = tempValue;
            }
        }

        private string _SubscriptionName;
        public string SubscriptionName { get { return _SubscriptionName; } set { _SubscriptionName = value; } }

        private string _SubscriptionDescription;
        public string SubscriptionDescription { get { return _SubscriptionDescription; } set { _SubscriptionDescription = value; } }

        private string _SubscriptionInvoiceNumber;
        public string SubscriptionInvoiceNumber { get { return _SubscriptionInvoiceNumber; } set { _SubscriptionInvoiceNumber = value; } }

        private DateTime _SubscriptionStartDate;
        public DateTime SubscriptionStartDate
        {
            get { return _SubscriptionStartDate; }
            set
            {
                if (value == null) throw new ArgumentException("Subscription Start Date cannot be null");
                if (value.Date < DateTime.Now.Date) throw new ArgumentException("Subscription Start Date cannot be in the past");
                _SubscriptionStartDate = value;
            }
        }

        private short _SubscriptionMonthInterval;
        public short SubscriptionMonthInterval
        {
            get { return _SubscriptionMonthInterval; }
            set
            {
                if (value <= 0) throw new ArgumentException("Subscription Month Interval must be positive");
                _SubscriptionMonthInterval = value;
            }
        }

        private short _SubscriptionNumOccurrences;
        public short SubscriptionNumOccurrences
        {
            get { return _SubscriptionNumOccurrences; }
            set
            {
                if (value <= 0) throw new ArgumentException("Subscription Number of Occurrences must be positive");
                _SubscriptionNumOccurrences = value;
            }
        }

        private decimal _SubscriptionAmount;
        public decimal SubscriptionAmount
        {
            get { return _SubscriptionAmount; }
            set
            {
                if (value < 0) throw new ArgumentException("Subscription Amount cannot be negative");
                _SubscriptionAmount = value;
            }
        }

        public bool SubscriptionNoEndDate
        {
            get { return _SubscriptionNumOccurrences == 9999; }
            set { if (value) _SubscriptionNumOccurrences = 9999; }
        }

        public bool SubscriptionHasTrial
        {
            get { return _SubscriptionTrialOccurrences == 0; }
            set
            {
                if (!value)
                {
                    _SubscriptionTrialOccurrences = 0;
                    _SubscriptionTrialAmount = 0.0M;
                }
            }
        }

        private short _SubscriptionTrialOccurrences;
        public short SubscriptionTrialOccurrences
        {
            get { return _SubscriptionTrialOccurrences; }
            set
            {
                if (value < 0) throw new ArgumentException("Subscription Trial Occurrences cannot be negative");
                _SubscriptionTrialOccurrences = value;
            }
        }

        private decimal _SubscriptionTrialAmount;
        public decimal SubscriptionTrialAmount
        {
            get { return _SubscriptionTrialAmount; }
            set
            {
                if (value < 0) throw new ArgumentException("Subscription Trial Amount cannot be negative");
                _SubscriptionTrialAmount = value;
            }
        }

        private string _BillingFirstName;
        public string BillingFirstName
        {
            get { return _BillingFirstName; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Subscription Trial Amount cannot be negative");
                _BillingFirstName = value.Trim();
            }
        }

        private string _BillingLastName;
        public string BillingLastName
        {
            get { return _BillingLastName; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Billing Last Name is required");
                _BillingLastName = value.Trim();
            }
        }

        private string _BillingAddress;
        public string BillingAddress
        {
            get { return _BillingAddress; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Billing Last Name is required");
                _BillingAddress = value.Trim();
            }
        }

        private string _BillingCity;
        public string BillingCity
        {
            get { return _BillingCity; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Billing Last Name is required");
                _BillingCity = value.Trim();
            }
        }

        private string _BillingState;
        public string BillingState
        {
            get { return _BillingState; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Billing Last Name is required");
                _BillingState = value.Trim();
            }
        }

        private string _BillingZip;
        public string BillingZip
        {
            get { return _BillingZip; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Billing ZIP is required");
                if (value.Trim().Length < 5) throw new ArgumentException("Billing ZIP is required");
                _BillingZip = value.Trim();
            }
        }

        private string _BillingCountry;
        public string BillingCountry
        {
            get { return _BillingCountry; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Billing Country is required");
                _BillingCountry = value.Trim();
            }
        }

        private string _CustomerID;
        public string CustomerID
        {
            get { return _CustomerID; }
            set
            {
                if (value == null || value.Trim() == String.Empty) throw new ArgumentException("Customer ID is required");
                _CustomerID = value.Trim();
            }
        }

        private string _CustomerEmail;
        public string CustomerEmail { get { return _CustomerEmail; } set { _CustomerEmail = (value == null ? null : value.Trim()); } }

        private string _CustomerPhone;
        public string CustomerPhone { get { return _CustomerPhone; } set { _CustomerPhone = (value == null ? null : value.Trim()); } }

        private string _ValidationError;
        public string ValidationError { get { return _ValidationError; } set { _ValidationError = value; } }

        

        protected AuthorizeNetSettings AuthorizeNetSettings { get; set; }
        public bool TestMode
        {
            get
            {
                return AuthorizeNetSettings.TestMode;
            }
            set
            {
                AuthorizeNetSettings.TestMode = value;
            }
        }

        #endregion

        #region ErrorHanding

        protected bool AuthNetStarted = false;
        protected bool AuthNetSucceeded = false;

        protected string AuthNetStep = "";
        protected string AuthNetError = "";
        protected string AuthNetDetailedError = "";
        protected string AuthNetResponseStatus = "";

        public bool HasError
        {
            get
            {
                return AuthNetStarted && !AuthNetSucceeded;
            }
        }

        public string ErrorMessage
        {
            get
            {
                if (!AuthNetStarted || AuthNetSucceeded) return String.Empty;
                return AuthNetError;
            }
        }

        public string DetailedErrorMessage
        {
            get
            {
                if (!AuthNetStarted || AuthNetSucceeded) return String.Empty;
                return AuthNetStep + ": (" + AuthNetResponseStatus + ") " + AuthNetDetailedError;
            }
        }

        #endregion
    }
}
