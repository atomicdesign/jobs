﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using CivilianJobs.Models;

namespace CivilianJobs.Helpers
{
    public class Indeed
    {
        private static string APIUrl = "http://api.indeed.com/ads/apisearch?";
        private static string JobAPIUrl = "http://api.indeed.com/ads/apigetjobs?";
        public static string APIKey = "5094123949579649";

        private static string URLParam(string key, string val)
        {
            if (val == null || val == String.Empty) return "";
            return HttpUtility.UrlPathEncode(key) + "=" + HttpUtility.UrlPathEncode(val);
        }

        private static string URLParams(Dictionary<string, string> values)
        {
            bool first = false;
            StringBuilder sb = new StringBuilder();
            foreach (string key in values.Keys)
            {
                if (first) { first = false; } else { sb.Append("&"); }
                sb.Append(URLParam(key, values[key]));
            }
            return sb.ToString();
        }

        public static IndeedSearchResult GetJobsSearch(string location, string jobtype, string query, string useragent, string userip, int page = 0)
        {
            string jt = System.Text.RegularExpressions.Regex.Replace(jobtype.ToLower(), "[^a-z]", "");

            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("publisher", APIKey);
            values.Add("v", "2");
            values.Add("format", "json");
            values.Add("q", query);
            values.Add("l", location);
            values.Add("jt", jt);
            values.Add("useragent", useragent);
            values.Add("userip", userip);
            values.Add("start", (page * 20).ToString());
            values.Add("limit", "20");

            string URL = APIUrl + URLParams(values);
            return JsonGetResults(URL);
        }

        public static IndeedSearchResult GetJob(string jobkey)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("publisher", APIKey);
            values.Add("v", "2");
            values.Add("format", "json");
            values.Add("jobkeys", jobkey);

            string URL = JobAPIUrl + URLParams(values);
            return JsonGetResults(URL);
        }

        private static IndeedSearchResult JsonGetResults(string URL)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(URL);
                webRequest.Method = "GET";
                webRequest.ContentType = "text/json";
                webRequest.KeepAlive = true;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode != HttpStatusCode.OK) throw new Exception(webResponse.StatusCode.ToString() + " response code");

                DataContractJsonSerializer jsonin = new DataContractJsonSerializer(typeof(IndeedSearchResult));
                return (IndeedSearchResult)jsonin.ReadObject(webResponse.GetResponseStream());
            }
            catch (Exception err)
            {
                LogHelper.Log(err);
            }
            return null;
        }


    }

    public class IndeedSearchResult
    {
        public string query { get; set; }
        public string location { get; set; }
        public bool dupefilter { get; set; }
        public bool highlight { get; set; }
        public int totalResults { get; set; }
        public int start { get; set; }
        public int end { get; set; }
        public int radius { get; set; }
        public int pageNumber { get; set; }
        public List<IndeedSearchResultJob> results { get; set; }
    }

    public class IndeedSearchResultJob
    {
        public string jobtitle { get; set; }
        public string company { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string formattedLocation { get; set; }
        public string source { get; set; }
        public string date { get; set; }
        public string snippet { get; set; }
        public string url { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string jobkey { get; set; }
        public string sponsored { get; set; }
        public string expired { get; set; }
        public string formattedLocationFull { get; set; }
        public string formattedRelativeTime { get; set; }
    }
}