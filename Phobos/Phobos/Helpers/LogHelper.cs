﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CivilianJobs.Models;
using System.IO;

namespace CivilianJobs.Helpers
{

    public static class LogHelper
    {
        /**** LOGGING ****/

        public const int MaxLogSize = (1024 * 1024 * 1);

        public static void SendLogEmail(string message)
        {
            try
            {
                string emailaddr = SiteSettingHelper.GetSetting("Admin Email Address", "info@enhancedcaremd.com");
                EmailMessage msg = new EmailMessage(emailaddr, emailaddr, "ERROR", message);
                EmailService svc = new EmailService();
                svc.SendOne(msg);
            }
            catch (Exception err)
            {
                Log(err);
            }
        }

        public static void Log(Exception err)
        {
            if (err == null) return;
            try
            {
                using (CivilianJobsContext db = new CivilianJobsContext())
                {
                    db.SystemLog.Add(new SystemLog { Exception = err });
                    db.SaveChanges();
                }
            }
            catch (Exception err2) { Log(err2.ToString()); }
            LogToFile(err.ToString());
        }

        public static void Log(string message)
        {
            try
            {
                using (CivilianJobsContext db = new CivilianJobsContext())
                {
                    db.SystemLog.Add(new SystemLog { Message = message, Created = DateTime.UtcNow });
                    db.SaveChanges();
                }
            }
            catch (Exception err) { LogToFile(err.ToString()); }
            LogToFile(message);
        }

        public static void LogToFile(string message)
        {
            LogToFile(message, "error");
        }

        public static void LogToFile(string message, string filename)
        {
            try
            {
                filename = filename.Split('/').LastOrDefault();
                FileInfo fi = new FileInfo(HttpContext.Current.Server.MapPath(String.Format("~/Logs/{0}.log")));
                if (fi.Exists && fi.Length >= MaxLogSize)
                {
                    fi.CopyTo(String.Format("{0}.{1}", fi.FullName, DateTime.Now.ToString("yyyyMMDDHHmm")));
                    fi.Delete();
                }
                using (StreamWriter sw = new StreamWriter(fi.OpenWrite()))
                {
                    sw.WriteLine(DateTime.Now.ToString());
                    sw.WriteLine(message);
                }
            }
            catch (Exception) { }
        }

    }
}