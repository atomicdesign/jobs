namespace CivilianJobs.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CivilianJobs.Models;
    using System.Transactions;
    using System.Web.Security;

    internal sealed class Configuration : DbMigrationsConfiguration<CivilianJobs.Models.CivilianJobsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

    }
}
