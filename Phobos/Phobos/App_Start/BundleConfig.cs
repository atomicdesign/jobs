﻿using System.Web;
using System.Web.Optimization;

namespace CivilianJobs
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/employers-styles").IncludeDirectory("~/content/css/employers", "*.css"));
            bundles.Add(new ScriptBundle("~/bundles/employers-scripts").IncludeDirectory("~/content/js/employers", "*.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.unobtrusive*",
            "~/Scripts/jquery.validate*"));
        }
    }
}