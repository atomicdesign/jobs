﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CivilianJobs
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("content/{*pathInfo}");

			//301s: uncomment this and edit the LegacyUrlRoute class as needed
            //routes.Add(new CivilianJobs.Helpers.LegacyUrlRoute());

            routes.MapRoute(
                name: "Pages",
                url: "page/{*path}",
                defaults: new { controller = "Main", action = "Index", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Forms",
                url: "form/{*path}",
                defaults: new { controller = "Forms", action = "Index", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HireVeterans",
                url: "hire-veterans/",
                defaults: new { controller = "HireVeterans", action = "Index" }
            );

            routes.MapRoute(
                name: "Company",
                url: "company/{query}",
                defaults: new { controller = "Company", action = "Index", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AddToCart",
                url: "ShoppingCart/AddToCart",
                defaults: new { controller = "ShoppingCart", action = "AddToCart" }
            );
            
            routes.MapRoute(
                name: "ShoppingCart",
                url: "ShoppingCart/{path}",
                defaults: new { controller = "ShoppingCart", action = "Index", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Search",
                url: "search/{query}",
                defaults: new { controller = "Search", action = "Index", path = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Main", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}