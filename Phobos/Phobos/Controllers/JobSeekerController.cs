﻿using CivilianJobs.Models;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs.Repositories;
using CivilianJobs.Models.Jobs.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Helpers;
using CivilianJobs.Models.Jobs.Enums;
using System.Web.Security;
using System.Data.Entity;
using CivilianJobs.Models.civjobs;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CivilianJobs.Controllers
{
    [Authorize(Roles = "Job Seeker")]
    public class JobSeekerController : JobsBaseController
    {
        public JobSeekerController()
        {
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            ViewBag.PrimaryNav = db.NavBars.Find(1);

        }

        [HttpGet]
        public JsonResult joinjobfair(int jobFairId)
        {
            var result = (new JobFairRepository()).RegisterUser(CurrentUserProfile.UserProfileID, jobFairId, true);
            if (result == false)
                throw new Exception("Can Not Complete Registration.");
            var jf = (new JobFairRepository()).Get().First(x => x.ID == jobFairId);
            AAEmailSender.Send(new System.Net.Mail.MailMessage
            {
                Body = string.Format(@"Dear {0},

Thank you for preregistering for the CivilianJobs.com Job Fair.
For job fairs on a military base, veterans who do not possess a military ID card must contact the Welcome/Visitor Center to find out about the base?s Installation Access Procedures.

You will receive periodic updates from us about this event in the weeks leading up to the job fair.

If you have any questions, call us toll free at 866-801-4418.

Job Fair Location: {1} / {2}{3} - {4}{5}

{6}


Job Fair Date: {7}/{8}/{9}", CurrentUserProfile.FirstName, jf.locationName, jf.startTime,
           jf.startTimeAMPM, jf.endTime, jf.endTimeAMPM, jf.locationName, jf.day, jf.month, jf.year),
            }, CurrentUserProfile.UserName);
            
            return Json(string.Format("{0}/{1}/{2}: {3} {4}{5} - {6}{7}",
           jf.day, jf.month, jf.year, jf.locationName, jf.startTime,
           jf.startTimeAMPM, jf.endTime, jf.endTimeAMPM), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("invitefriends")]
        public JsonResult inviteFriends(detNotes notes)
        {
            notes.data.ForEach(x =>
            {
                x.senddate = DateTime.Now;
                x.sender = CurrentUserProfile.UserProfileID;
                x.Id = 0;
            });
            foreach (var item in notes.data)
            {
                db.ReferralNotes.Add(item);
                db.SaveChanges();
                AAEmailSender.Send(new System.Net.Mail.MailMessage
                {
                    Body = string.Format(@"You were referred to us CivilianJobs.com by a friend ({0}).
Starting your career search?
Please visit www.CivilianJobs.com.
We are a job board designed by veterans for veterans and we are creating the largest military-specific career site on the world-wide web. This site is totally FREE and SECURE, available only to paying employers that want to hire veterans. Whether you want to market yourself for Administrative, Healthcare, IT, Law Enforcement, Security Clearance Jobs, Sales or many other fields, we have it all. You use other general internet job boards; why not use a board that is exclusively for military men and women? There are thousands of companies that hire military as a strategy for acquiring leadership talent. We have those companies on our site and they are looking for you!

Our company is closely partnered with the largest military placement firm in America and we will work together to ensure that you are exposed to the best opportunities. We recognize the tangible and intangible skills that a military man or woman brings to the civilian sector and we work daily to promote these special qualities to hiring entities throughout the globe. We have the best free career advice page of any job board available on the web. Try us and good luck with your career search.


Bethany Adams
CivilianJobs.com

(t/f) 866-801-4418
(l) 678-819-4170
(f) 678-819-5170
badams@civilianjobs.com
www.CivilianJobs.com", CurrentUserProfile.UserName),
                }, item.receiver);
            }
            return Json(new { result = "sent" }, JsonRequestBehavior.AllowGet);
        }

        [ActionName("refferals")]
        public ActionResult RefferalEmails()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            return View();
        }

        [ActionName("new-job-seeker-forms")]
        public ActionResult NewJobSeekerDataFilling()
        {
            ViewBag.absoluteUrl = "jobseeker";
            return View();
        }
        [AllowAnonymous]
        [ActionName("companies")]
        public JsonResult GetCompanies()
        {
            ViewBag.absoluteUrl = "jobseeker";
            using (var entities = new CivilianJobsContext())
            {
                var temp = entities.JobPostings.Select(x => x.Company).Distinct().ToList();
                return Json(temp, JsonRequestBehavior.AllowGet);
            }
        }
        [AllowAnonymous]
        [ActionName("locations")]
        public JsonResult GetLocations()
        {
            ViewBag.absoluteUrl = "jobseeker";
            using (var entities = new CivilianJobsContext())
            {
                var locs = entities.JobPostings.Select(x => x.JobLocation).Distinct().ToList();
                var retVal = new List<string>();
                foreach (var item in locs)
                {
                    retVal.Add(CivilianJobsHelper.GetEnumDescription((JobLocation)item));
                }
                return Json(retVal, JsonRequestBehavior.AllowGet);
            }
        }
        [AllowAnonymous]
        [ActionName("categories")]
        public List<string> GetCategories()
        {
            ViewBag.absoluteUrl = "jobseeker";
            return new List<string> { "catetgory 1", "category 2", "category 3" };
        }

        [AllowAnonymous]
        [ActionName("index")]
        public ActionResult JobSeekerNotLoggedIn()
        {
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            if (CurrentUserRole != null && CurrentUserRole == "Job Seeker")
            {
                var temp = db.JobSeekersDetails.FirstOrDefault(x => x.UserId == CurrentUserProfile.UserId);
                ViewBag.SavedJobs = db.SavedJobs.Where(x => x.UserProfile.UserProfileID == CurrentUserProfile.UserProfileID).Count();
                ViewBag.ViewCount = temp==null? 0: temp.ViewCount ?? (int?)0;
                ViewBag.JobApplies = db.JobApplications.Where(x => x.UserProfileID == CurrentUserProfile.UserProfileID).Count();
            }
            else
            {
                ViewBag.SavedJobs = "0";
                ViewBag.ViewCount = "0";
                ViewBag.JobApplies = "0";
            }
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.CurrentHome = true;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();

            ViewBag.JobFairs = new JobFairRepository().Get();
            return View();
        }
        public ActionResult PrintResume()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var jobSeekerData = db.JobSeekersDetails.Where(x => x.UserId == CurrentUserProfile.UserId).FirstOrDefault();

            JobSeekersDetail _JobSeekersDetail = new JobSeekersDetail();

            _JobSeekersDetail = jobSeekerData;
            jobSeekerData._userProfile = CurrentUserProfile;
            if (_JobSeekersDetail.Service != null)
                ViewBag.Rank = db.UserDegrees.Where(x => x.Id == _JobSeekersDetail.Rank && x.Type == 4).Select(x => x.Name).FirstOrDefault();
            if (_JobSeekersDetail.HigherSchool_Deg != null)
                ViewBag.HighSchool = db.UserDegrees.Where(x => x.Id == _JobSeekersDetail.HigherSchool_Deg && x.Type == 0).Select(x => x.Name).FirstOrDefault();
            if (_JobSeekersDetail.UnderGrad_Deg != null)
                ViewBag.UnderGradDeg = db.UserDegrees.Where(x => x.Id == _JobSeekersDetail.UnderGrad_Deg && x.Type == 2).Select(x => x.Name).FirstOrDefault();
            if (_JobSeekersDetail.Language != null)
                ViewBag.Language = db.Languages.Where(x => x.LanguageID == _JobSeekersDetail.Language).Select(x => x.LanguageName).FirstOrDefault();
            if (_JobSeekersDetail.SecurityClearence != null)
                ViewBag.SecurityClearence = db.UserDegrees.Where(x => x.Id == _JobSeekersDetail.SecurityClearence && x.Type == 7).Select(x => x.Name).FirstOrDefault();
            if (_JobSeekersDetail.MilitaryRelocation != null)
                ViewBag.MilitaryRelocation = db.UserDegrees.Where(x => x.Id == _JobSeekersDetail.MilitaryRelocation && x.Type == 14).Select(x => x.Name).FirstOrDefault();


            //return new Rotativa.ViewAsPdf("PrintResume", _JobSeekersDetail) { FileName = "TestViewAsPdf.pdf" };
            return View(_JobSeekersDetail);

        }


        public ActionResult DownloadViewPDF()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");

            return new Rotativa.ActionAsPdf("PrintResume");
        }

        [ActionName("profile-info")]
        public ActionResult ProfileInfo(int ids = 0, int rec_id = 0, bool navigationButtonPress = false)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.FormId = ids;
            ViewBag.rec_id = rec_id;
            MembershipUser user = null;



            JobSeekersDetail _JobSeekersDetail = new JobSeekersDetail();

            if (Session["FormId"] == null)
            {
                Session["FormId"] = 0;
                ViewBag.FormId = 0;
            }
            else if (navigationButtonPress)
            {
                Session["FormId"] = ids;
                ViewBag.FormId = ids;
            }
            else
            {

                ViewBag.FormId = Convert.ToInt16(Session["FormId"]);
            }

            var jobSeekerData = db.JobSeekersDetails.Where(x => x.UserId == CurrentUserProfile.UserId).FirstOrDefault();
            if (jobSeekerData != null)
            {
                jobSeekerData._userProfile = CurrentUserProfile;
                var _Awards = db.Awardss.ToList();
                if (_Awards != null)
                {
                    jobSeekerData.awardss = new List<Awards>();
                    jobSeekerData.NewD = new List<NewDesi>();
                    Awards awards;
                    foreach (var item in _Awards)
                    {
                        awards = new Awards();
                        awards.Id = item.Id;
                        awards.Award = item.Award;
                        if (jobSeekerData.MilitaryAwards != null)
                        {
                            string[] values = jobSeekerData.MilitaryAwards.Split(',');

                            for (int i = 0; i < values.Length; i++)
                            {
                                values[i] = values[i].Trim();
                                if (values[i] != "")
                                {
                                    if (item.Id == Convert.ToInt16(values[i]))
                                    {
                                        awards.Checked = true;


                                    }

                                }

                            }
                        }
                        jobSeekerData.awardss.Add(awards);
                    }
                }


                var _New = db.NewDesis.ToList();
                if (_New != null)
                {
                    NewDesi _designation;
                    foreach (var item in _New)
                    {
                        _designation = new NewDesi();
                        _designation.Id = item.Id;
                        _designation.NewDesiField = item.NewDesiField;
                        if (jobSeekerData.Designation != null)
                        {
                            string[] values = jobSeekerData.Designation.Split(',');
                            for (int i = 0; i < values.Length; i++)
                            {
                                values[i] = values[i].Trim();
                                if (values[i] != "")
                                {
                                    if (item.Id == Convert.ToInt16(values[i]))
                                    {
                                        _designation.Checked = true;

                                    }
                                }
                            }
                        }
                        jobSeekerData.NewD.Add(_designation);

                    }
                }

                if (jobSeekerData != null)
                {
                    _JobSeekersDetail = jobSeekerData;
                    ViewBag.rec_id = _JobSeekersDetail.Id;
                }
                else
                {
                    _JobSeekersDetail.NewD = db.NewDesis.ToList();
                    _JobSeekersDetail.awardss = db.Awardss.ToList();
                }
            }
            if(_JobSeekersDetail.NewD ==null || _JobSeekersDetail.NewD.Count==0)
            {
                _JobSeekersDetail.NewD = db.NewDesis.ToList();
            }
            if (_JobSeekersDetail.awardss == null || _JobSeekersDetail.awardss.Count == 0)
            {
                _JobSeekersDetail.awardss = db.Awardss.ToList();
            }
            return View(_JobSeekersDetail);
        }

        [HttpGet]
        public ActionResult saveopInfo(int genderId, int ethnId)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var jsd = db.JobSeekersDetails.First(x => x.UserId == CurrentUserProfile.UserId);
            jsd.Gender = genderId;
            jsd.Ethnicity = ethnId;
            db.Entry(jsd).State = EntityState.Modified;
            db.SaveChanges();
            return null;
        }

        [HttpPost]
        public ActionResult JobSeekerDetail_save(JobSeekersDetail _JobSeekersDetail, string Command)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            MembershipUser user = null;
            if (Session["CurrentUser"] != null)
            {
                user = Membership.GetUser(Session["CurrentUser"].ToString());
            }
            if (Command == "Next")
            {


                if (_JobSeekersDetail.Id == 0)
                {
                    _JobSeekersDetail = new JobSeekersDetail()
                    {
                        UserId = CurrentUserProfile.UserId,
                        Createdon = DateTime.Now,
                        HigherSchool_Deg = _JobSeekersDetail.HigherSchool_Deg != null ? Convert.ToInt32(_JobSeekersDetail.HigherSchool_Deg) : 0,
                        UnderGrad_Deg = _JobSeekersDetail.UnderGrad_Deg != null ? Convert.ToInt32(_JobSeekersDetail.UnderGrad_Deg) : 0,
                        Grad_Deg = _JobSeekersDetail.Grad_Deg != null ? Convert.ToInt32(_JobSeekersDetail.Grad_Deg) : 0,
                        FurtherHelp = _JobSeekersDetail.FurtherHelp != null ? true : false,
                        Language = _JobSeekersDetail.Language != null ? Convert.ToInt32(_JobSeekersDetail.Language) : 0


                    };
                    UserProfile userpersonaldetail = new UserProfile();
                    userpersonaldetail = CurrentUserProfile;
                    userpersonaldetail.RegCompleted = true;
                    _JobSeekersDetail._userProfile = userpersonaldetail;
                    db.JobSeekersDetails.Add(_JobSeekersDetail);
                    db.Entry<UserProfile>(userpersonaldetail).State = EntityState.Modified;
                    db.SaveChanges();
                    int id = _JobSeekersDetail.Id;
                    ViewBag.rec_id = id;
                }
                else
                {
                    var _jobseeker = db.JobSeekersDetails.Where(x => x.Id == _JobSeekersDetail.Id).FirstOrDefault();
                    if (_jobseeker != null)
                    {
                        _jobseeker.UserId = CurrentUserProfile.UserId;
                        _jobseeker.Createdon = DateTime.Now;
                        if (Convert.ToInt16(Session["FormId"]) == 0)
                        {
                            _jobseeker.HigherSchool_Deg = _JobSeekersDetail.HigherSchool_Deg != null ? Convert.ToInt32(_JobSeekersDetail.HigherSchool_Deg) : 0;
                            _jobseeker.UnderGrad_Deg = _JobSeekersDetail.UnderGrad_Deg != null ? Convert.ToInt32(_JobSeekersDetail.UnderGrad_Deg) : 0;
                            _jobseeker.Grad_Deg = _JobSeekersDetail.Grad_Deg != null ? Convert.ToInt32(_JobSeekersDetail.Grad_Deg) : 0;
                            _jobseeker.FurtherHelp = _JobSeekersDetail.FurtherHelp != null ? true : false;
                            _jobseeker.Language = _JobSeekersDetail.Language != null ? Convert.ToInt32(_JobSeekersDetail.Language) : 0;
                        }
                        else if (Convert.ToInt16(Session["FormId"]) == 1)
                        {
                            _jobseeker.Rank = _JobSeekersDetail.Rank != null ? Convert.ToInt32(_JobSeekersDetail.Rank) : 0;
                            _jobseeker.Service = _JobSeekersDetail.Service != null ? Convert.ToInt32(_JobSeekersDetail.Service) : 0;
                            _jobseeker.AvailableSpecialities = _JobSeekersDetail.AvailableSpecialities != null ? _JobSeekersDetail.AvailableSpecialities : "";
                            _jobseeker.Discharged = _JobSeekersDetail.Discharged != null ? _JobSeekersDetail.Discharged : 0;
                            _jobseeker.InstructorDuty = _JobSeekersDetail.InstructorDuty != null ? _JobSeekersDetail.InstructorDuty : 0;
                            _jobseeker.ConvictedFelory = _JobSeekersDetail.ConvictedFelory != null ? _JobSeekersDetail.ConvictedFelory : 0;
                            _jobseeker.SecurityClearence = _JobSeekersDetail.SecurityClearence != null ? _JobSeekersDetail.SecurityClearence : 0;
                            _jobseeker.MOSDD = _JobSeekersDetail.MOSDD != null ? _JobSeekersDetail.MOSDD : 0;

                            _jobseeker.MilitaryRelocation = _JobSeekersDetail.MilitaryRelocation != null ? _JobSeekersDetail.MilitaryRelocation : 0;
                            _jobseeker.YrsOfService = _JobSeekersDetail.YrsOfService != null ? _JobSeekersDetail.YrsOfService : 0;
                            _jobseeker.AvailableSpecialities = _JobSeekersDetail.AvailableSpecialities != null ? _JobSeekersDetail.AvailableSpecialities : "";

                            StringBuilder sb = new StringBuilder();
                            foreach (Awards _Awards in _JobSeekersDetail.awardss)
                            {
                                if (_Awards.Checked)
                                {
                                    sb.Append(_Awards.Id + ", ");

                                }
                            }
                            if (sb.ToString().LastIndexOf(",") != -1)
                            sb.Remove(sb.ToString().LastIndexOf(","), 1);

                            _jobseeker.MilitaryAwards = sb.ToString() != null ? sb.ToString() : "";

                            StringBuilder sbs = new StringBuilder();
                            foreach (NewDesi  designationss in _JobSeekersDetail.NewD)
                            {
                                if (designationss.Checked)
                                {
                                    sbs.Append(designationss.Id + ", ");

                                }
                            }
                            if(sbs.ToString().LastIndexOf(",") != -1)
                            sbs.Remove(sbs.ToString().LastIndexOf(","), 1);

                            _jobseeker.Designation = sbs.ToString() != null ? sbs.ToString() : "";
                            _jobseeker.CommissionDate = _JobSeekersDetail.CommissionDate != null ? _JobSeekersDetail.CommissionDate : DateTime.Now;
                            _jobseeker.SeperationDate = _JobSeekersDetail.SeperationDate != null ? _JobSeekersDetail.SeperationDate : DateTime.Now;
                            _jobseeker.AvailDate = _JobSeekersDetail.AvailDate != null ? _JobSeekersDetail.AvailDate : DateTime.Now;
                        }
                        else if (Convert.ToInt16(Session["FormId"]) == 2)
                        {

                            _jobseeker.location = _JobSeekersDetail.location != null ? _JobSeekersDetail.location : "";
                            var regs = db.UserDegrees.Where(x => x.Type == 999).ToList();
                            for (int i = 0; i < regs.Count; i++)
                            {
                                _JobSeekersDetail.regions[i].Id = regs[i].Id;
                            }
                            _jobseeker.regionso = _JobSeekersDetail.regions != null ?
                                string.Join(",", _JobSeekersDetail.regions.Where(x => x.Checked == true).Select(x => x.Id).ToList()) : "";
                            _jobseeker.SalaryRange = _JobSeekersDetail.SalaryRange != null ? _JobSeekersDetail.SalaryRange : 0;

                            _jobseeker.ShiftWork = _JobSeekersDetail.ShiftWork != null ? _JobSeekersDetail.ShiftWork : 0;
                            _jobseeker.Travel = _JobSeekersDetail.Travel != null ? _JobSeekersDetail.Travel : 0;



                        }

                        else if (Convert.ToInt16(Session["FormId"]) == 3)
                        {
                            _jobseeker.AvailableCategories = _JobSeekersDetail.AvailableCategories != null ? _JobSeekersDetail.AvailableCategories : "";
                        }
                        else if (Convert.ToInt16(Session["FormId"]) == 4)
                        {


                        }
                        else if (Convert.ToInt16(Session["FormId"]) == 5)
                        {
                            _jobseeker.currentstartdate = _JobSeekersDetail.currentstartdate != null ? _JobSeekersDetail.currentstartdate : DateTime.Now;
                            _jobseeker.currentenddate = _JobSeekersDetail.currentenddate != null ? _JobSeekersDetail.currentenddate : DateTime.Now;
                            _jobseeker.PreStartDate = _JobSeekersDetail.PreStartDate != null ? _JobSeekersDetail.PreStartDate : DateTime.Now;
                            _jobseeker.PreEndDate = _JobSeekersDetail.PreEndDate != null ? _JobSeekersDetail.PreEndDate : DateTime.Now;
                            _jobseeker.CompanyUnit = _JobSeekersDetail.CompanyUnit != null ? _JobSeekersDetail.CompanyUnit : "";
                            _jobseeker.MOS = _JobSeekersDetail.MOS != null ? _JobSeekersDetail.MOS : "";
                            _jobseeker.DescriptionJob = _JobSeekersDetail.DescriptionJob != null ? _JobSeekersDetail.DescriptionJob : "";
                            _jobseeker.PreCompanyUnit = _JobSeekersDetail.PreCompanyUnit != null ? _JobSeekersDetail.PreCompanyUnit : "";
                            _jobseeker.PreMOS = _JobSeekersDetail.PreMOS != null ? _JobSeekersDetail.PreMOS : "";
                            _jobseeker.PreDescriptionJob = _JobSeekersDetail.PreDescriptionJob != null ? _JobSeekersDetail.PreDescriptionJob : "";


                        }
                        else if (Convert.ToInt16(Session["FormId"]) == 6)
                        {
                            _jobseeker.ExperienceWorkHistory = _JobSeekersDetail.ExperienceWorkHistory != null ? _JobSeekersDetail.ExperienceWorkHistory : "";

                        }
                        else if (Convert.ToInt16(Session["FormId"]) == 7)
                        {
                          var pid=  CurrentUserProfile.UserProfileID;
                          var res = db.JobFairRegistrations.Where(x => x.UserId == pid).FirstOrDefault();
                            if(res==null)
                            {
                                JobFairRegistration reg = new JobFairRegistration();
                                reg.UserId = pid;
                                reg.JobFairId = _JobSeekersDetail.JobFairs;
                                reg.JobFairRegistrationId = Guid.NewGuid();
                                db.JobFairRegistrations.Add(reg);

                            }
                            else
                            {
                                res.JobFairId = _JobSeekersDetail.JobFairs;
                               
                            }
                            db.SaveChanges();
                           
                        }

                        // db.JobSeekersDetails.Add(_JobSeekersDetail);

                        db.Entry(_jobseeker).State = EntityState.Modified;
                        CurrentUserProfile.RegCompleted = true;
                        db.Entry<UserProfile>(CurrentUserProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        int id = _JobSeekersDetail.Id;
                        ViewBag.rec_id = id;
                        db.SaveChanges();
                    }
                }

                if (ViewBag.FormId == null)
                {
                    if (Session["FormId"] == null)
                    {
                        Session["FormId"] = 1;
                        ViewBag.FormId = 1;

                    }
                    else
                    {

                        Session["FormId"] = Convert.ToInt16(Session["FormId"]) + 1;
                        ViewBag.FormId = Session["FormId"];
                    }

                }
                else
                {
                    Session["FormId"] = Convert.ToInt16(Session["FormId"]) + 1;
                    ViewBag.FormId = Session["FormId"];
                }
            }
            else if (Command == "Back")
            {
                Session["FormId"] = Convert.ToInt16(Session["FormId"]) - 1;
                ViewBag.FormId = Session["FormId"];
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [ActionName("profile-education")]
        public ActionResult ProfileEducation()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            return View();
        }

        [AllowAnonymous]
        [ActionName("getjobpostingsbyfilter")]
        public ActionResult GetJobPostingsByFilter(int index, int count, int sorting, string title, string keyword,
            string location, string company, string postingDate, bool partTime, bool fullTime, bool contract,
            bool internship, bool temp, bool other)
        {
            ViewBag.absoluteUrl = "jobseeker";

            var repoResult = new JobPostingRepository().Get(index, count, sorting, this.Request, this.Url, title, keyword, location, company, postingDate
                 , partTime, fullTime, contract,
                internship, temp, other);

            return Json(repoResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult jobIdForSelection(int id)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            new SavedJobRepository().Delete(User.Identity.Name, id);
            return new JsonResult { Data = "Success", JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }


        [AllowAnonymous]
        [ActionName("getjobpostingsbyfilterSaved")]
        public ActionResult getjobpostingsbyfilterSaved(int index, int count, int sorting, string title, string keyword,
            string location, string company, string postingDate, bool partTime, bool fullTime, bool contract,
            bool internship, bool temp, bool other)
        {
            ViewBag.absoluteUrl = "jobseeker";
            var savedIds = new SavedJobRepository().GetIds(CurrentUserProfile.UserProfileID);
            var repoResult = new JobPostingRepository().Get(index, count, sorting, this.Request, this.Url, title, keyword, location, company, postingDate
                 , partTime, fullTime, contract,
                internship, temp, other, savedIds);
            return Json(repoResult, JsonRequestBehavior.AllowGet);

        }
        [AllowAnonymous]
        [ActionName("job-postings")]
        public ActionResult JobPostings(string keyword, string location)
        {
            ViewBag.Keyword =   string.IsNullOrEmpty(keyword)? "" : keyword;
            ViewBag.Location = string.IsNullOrEmpty(location) ? "" : location; ;
            ViewBag.absoluteUrl = "jobseeker";
            var jobPostings = new List<JobPostings>();
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            return View("job-postings", jobPostings);
        }
       
        [AllowAnonymous]
        public JsonResult redircedCall(string url, int id)
        {
            var job = db.JobPostings.First(x => x.ID == id);
            job.ClickCount = job.ClickCount == null ? 1 : job.ClickCount + 1;
            db.Entry(job).State = EntityState.Modified;
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Job(int id)
        {
            ViewBag.absoluteUrl = "jobseeker";
            var jobPosting = new JobPostingRepository().Get(id);
            jobPosting.ViewCount = jobPosting.ViewCount == null ? 1 : jobPosting.ViewCount += 1;
            jobPosting.ApplicationUrl = "http://" + jobPosting.ApplicationUrl;
            new JobPostingRepository().UpdateViewsCount(id);
            ViewBag.Application = new JobApplicationRepository().Get(User.Identity.Name, id);

            ViewBag.SavedJob = new SavedJobRepository().Get(User.Identity.Name, id);

            return View(jobPosting);
        }
        [ActionName("apply-job")]
        public JsonResult Apply(int id)
        {

            ViewBag.absoluteUrl = "jobseeker";
            new JobApplicationRepository().Add(User.Identity.Name, id);
            var jobApp = new JobPostingRepository().Get(id);
            var employer = db.JobPostings.First(x => x.ID == id).CreatedBy;
            var emp = db.UserProfiles.First(x => x.UserName == employer);
            var task = new Task(() => sendEmail(jobApp, emp));
            task.Start();
            return new JsonResult { Data = "Success", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public void sendEmail(JobPostings jobApp, UserProfile emp)
        {
            AAEmailSender.Send(new System.Net.Mail.MailMessage
            {
                Body = string.Format(@"Dear {0}, 
You have applied for a job - {1}.

Job Description
{2}

Job url - http://localhost:49635/JobSeeker/job/{3}", CurrentUserProfile.FirstName, jobApp.Title, jobApp.Description, jobApp.ID),
            }, CurrentUserProfile.UserName);
            AAEmailSender.Send(new System.Net.Mail.MailMessage
            {
                Body = string.Format(@"Dear {0}, 
You have new applicant for a job.
Job posting - {1}
Applicant - {2}", emp.FullName, jobApp.Title, CurrentUserProfile.FullName),
            }, emp.Email);
        }

        public ActionResult SavedSuccess()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            return View();
        }

        [ActionName("save-job")]
        public JsonResult SaveJob(int id)
        {
            ViewBag.absoluteUrl = "jobseeker";
            new SavedJobRepository().Add(User.Identity.Name, id);

            return new JsonResult { Data = "Success", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [ActionName("remove-job")]
        [HttpGet]
        public JsonResult RemoveJob(int id)
        {
            ViewBag.absoluteUrl = "jobseeker";
            var jId = new SavedJobRepository().Get(CurrentUserProfile.UserName, id).ID;
            new SavedJobRepository().Delete(jId);
            return new JsonResult { Data = "Success", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [ActionName("saved-jobs")]
        public ActionResult SavedJobs()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            return View();
        }

        [ActionName("my-account")]
        public ActionResult MyAccount()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            var model = new JobSeekerAccount()
            {
                User = CurrentUserProfile,
                JobApplications = new JobApplicationRepository().GetByApplicant(User.Identity.Name),
                SavedJobs = new SavedJobRepository().Get(User.Identity.Name),
                Documents = new DocumentRepository().Get(User.Identity.Name)
            };

            return View("my-account", model);
        }

        [ActionName("my-account")]
        [HttpPost]
        public ActionResult MyAccount(HttpPostedFileBase file, string description, string type)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            if (file.ContentLength > 0)
            {
                var uploadsDir = Server.MapPath("~/content/upload/documents");

                if (!Directory.Exists(uploadsDir))
                {
                    Directory.CreateDirectory(uploadsDir);
                }

                var fileName = Path.GetFileName(file.FileName);
                var extent = fileName.Substring(fileName.LastIndexOf('.'));
                fileName = fileName.Substring(0, fileName.LastIndexOf('.'));
                var dt = DateTime.Now.Date;
                var ptt = fileName + "_" + dt.Day + "." + dt.Month + "." + dt.Year + "_" + type+ extent;
                ptt = ptt.Replace(' ', '_');
                var path = Path.Combine(uploadsDir, ptt);
                file.SaveAs(path);

                Document doc = new Document()
                {
                    FileName = ptt,
                    Description = description
                };

                new DocumentRepository().Add(doc, User.Identity.Name);
            }

            return RedirectToAction("my-account");
        }

        [ActionName("my-resume")]
        public ActionResult MyResume()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            var resume = db.JobSeekersDetails.FirstOrDefault(x => x.UserId == CurrentUserProfile.UserId) ?? new JobSeekersDetail();
            resume._userProfile = db.UserProfiles.First(x => x.UserProfileID == CurrentUserProfile.UserProfileID);
            ViewBag.Documents = db.Documents.Where(x => x.UserProfile.UserProfileID == CurrentUserProfile.UserProfileID).ToList();
            return View("my-resume", resume);
        }

        [ActionName("my-resume"), HttpPost]
        public ActionResult MyResume(Resume resume)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            new ResumeRepository().Add(resume, User.Identity.Name);
            ViewBag.absoluteUrl = "jobseeker";
            return View("my-resume", resume);
        }

        [ActionName("job-fairs")]
        public ActionResult JobFairs()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            var model = new JobFairRepository().Get();
            return View("job-fairs", model);
        }


        [ActionName("job-fair-registration")]
        public ActionResult JobFairRegistration()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            return View("job-fair-registration");
        }

        [ActionName("job-fair-registration-2")]
        public ActionResult JobFairRegistration2(string email)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            if (email != null && email.Length > 0)
            {
                ViewBag.Email = email;
            }

            return View("job-fair-registration-2");
        }
        [AllowAnonymous]
        [ActionName("transition-news")]
        public ActionResult TransitionNews()
        {
            ViewBag.absoluteUrl = "jobseeker";
            CivilianJobsContext ctx = new CivilianJobsContext();

            var TransitionNewsArticles = ctx.TransitionNewsArticles.ToList();
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            return View("transition-news", TransitionNewsArticles);
        }
        [AllowAnonymous]
        [ActionName("veterans-associations")]
        public ActionResult VeteransAssociations()
        {
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            return View("veterans-associations");
        }
        [AllowAnonymous]
        [ActionName("article")]
        public ActionResult Article(int id = 0)
        {
            ViewBag.absoluteUrl = "jobseeker";
            CivilianJobsContext ctx = new CivilianJobsContext();
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            var article = ctx.TransitionNewsArticles.Where(m => m.ID == id).ToList();
            return View("article", article);
        }

        [ActionName("transition-news-4-14-2015")]
        public ActionResult TransitionNews2()
        {
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            return View("transition-news-4-14-2015");
        }

        [ActionName("transition-news-4-18-2015")]
        public ActionResult TransitionNews3()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            return View("transition-news-4-18-2015");
        }

        [ActionName("transition-news-4-20-2015")]
        public ActionResult TransitionNews4()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            return View("transition-news-4-20-2015");
        }

        [ActionName("press-releases")]
        public ActionResult PressReleases()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.EmployerNav = db.NavBars.Find(1);
            CivilianJobsContext ctx = new CivilianJobsContext();
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            var PressReleases = ctx.PressReleases.ToList();
            return View("press-releases", PressReleases);
        }

        [ActionName("press-release")]
        public ActionResult PressRelease(int id = 0)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.EmployerNav = db.NavBars.Find(1);
            CivilianJobsContext ctx = new CivilianJobsContext();
            ViewBag.absoluteUrl = "jobseeker";
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker"; ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            var PressRelease = ctx.PressReleases.Where(m => m.ID == id).ToList();
            return View("press-release", PressRelease);
        }

        [ActionName("email-job")]
        public ActionResult EmailJob(List<string> emails)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Job Seeker";
            ViewBag.absoluteUrl = "jobseeker";
            // todo: this can be implemented after the demo
            return null;
        }

        public ActionResult MOSList(int serviceId, int RankId)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            if (RankId <= 0)
            {
                throw new ArgumentNullException("RankId");
            }
            var list = db.SRMs.Where(x => x.ServiceId == serviceId && x.RankId == RankId);
            /**/
            var mossList = list.Where(x => x.MosId != null).Select(x => x.MosId.Value).ToList();
            var IdList = list.Select(x => x.Id).ToList();
            /**/
            var specialityList = db.SrmDegrees.Where(x => IdList.Contains(x.SrmId)).Select(x => x.DegreeId).Distinct();
            var mos = db.UserDegrees.Where(x => mossList.Contains(x.Id)).ToList();
            var spec = db.UserDegrees.Where(x => specialityList.Contains(x.Id)).ToList();

            return Json(new { Mos = mos, Spec = spec }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SpecialtiesList(int RankId, int ServiceId, int MosId)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            if (RankId <= 0)
            {
                throw new ArgumentNullException("RankId");
            }
            var list = db.SRMs.Where(x => x.ServiceId == ServiceId && x.RankId == RankId && (x.MosId == MosId || x.MosId == null)).ToList();
            var IdList = list.Select(x => x.Id).ToList();
            var specialityList = db.SrmDegrees.Where(x => IdList.Contains(x.SrmId)).Select(x => x.DegreeId).Distinct();
            var result = db.UserDegrees.Where(x => specialityList.Contains(x.Id)).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);         
          
        }
    }
}
