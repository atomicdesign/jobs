﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Xml.Linq;

namespace CivilianJobs.Controllers
{
    public class FormsController : CivilianJobsBaseController
    {

        protected readonly Dictionary<string, string> errorMsg = new Dictionary<string, string>{
            {"required", " is required"},
            {"badoption", " contains an invalid option"},
            {"bademail", " is an invalid email"}
        };

        private JsonResult JsonFailure(Dictionary<string, string> errors)
        {
            return this.Json(new { success = false, errors = errors });
        }

        private JsonResult JsonFailure(string errorMessage)
        {
            return this.Json(new { success = false, errormessage = errorMessage });
        }

        //
        // POST: /Forms/[path]
        //@Html.AntiForgeryToken()            
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Index(string path, FormCollection fc)
        {
            if (path == null || fc == null || path == String.Empty || fc.Count == 0) return JsonFailure("Invalid Parameters");
            
            int id;
            if (!int.TryParse(fc["FormID"], out id) || id == 0) return JsonFailure("Invalid Form ID");

            Form f = db.Forms.Include("Fields").Include("Fields.Options").FirstOrDefault(e=>e.FormID == id);
            if (f == null) return JsonFailure("Invalid Form ID");

            Dictionary<string, string> errors = new Dictionary<string,string>();

            StringBuilder formDoc = new StringBuilder();
            string formval;
            bool not_a_spammer = true;

            //loop through form, get values, validate, put into XML Doc, shove into
            foreach (FormField ff in f.Fields)
            {
                formval = (fc[ff.Name] ?? String.Empty);
                if (ff.Required && formval == String.Empty) //just plain not there
                {
                    errors.Add(ff.Name, errorMsg["required"]);
                }
                else
                {
                    formDoc.Append(FormItem(ff.Name, formval));
                    switch(ff.TypeEnum)
                    {
                        case FormFieldType.INPUT:
                        case FormFieldType.TEXTAREA:
                            if (ff.Required && formval.Trim() == String.Empty)
                            {
                                errors.Add(ff.Name, errorMsg["required"]);
                            }
                            break;

                        case FormFieldType.HONEYPOT:
                            if (formval.Trim() != String.Empty)
                            {
                                not_a_spammer = false;
                            }
                            break;

                        case FormFieldType.SELECT:
                        case FormFieldType.RADIO:
                            if (formval != String.Empty && !ff.Options.Any(e => e.Value == formval))
                            {
                                errors.Add(ff.Name, errorMsg["badoption"]);
                            }
                            break;

                        case FormFieldType.CHECKBOX:
                            if (formval != String.Empty)
                            {
                                string[] checkedBoxes = formval.Split(',');
                                
                                foreach (string box in checkedBoxes)
                                {
                                    if (!ff.Options.Any(e => e.Value == box))
                                    {
                                        errors.Add(ff.Name, errorMsg["badoption"]);
                                    }
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }
            }

            bool success = errors.Count == 0;

            if (success)
            {
                //if not a spam bot
                if (not_a_spammer)
                {
                db.FormSubmissions.Add(new FormSubmission { IP = Request.UserHostAddress, XML = formDoc.ToString(), Timestamp = DateTime.Now, FormID = f.FormID });
                db.SaveChanges();

                if (f.EmailContact != null && f.EmailContact != String.Empty)
                {
                    EmailMessage msg = new EmailMessage(GetSetting("Site Admin Email"), f.EmailContact, f.Title + " submission", formDoc.ToString());
                    db.EmailQueue.Add(msg);
                    db.SaveChanges();
                    }
                }
            }

            return this.Json(new { success = success, errors = errors });

        }

        private string FormItem(string key, string val)
        {
            return "<p><label>" + key + ":</label>" + val + "</p>";
        }

        public ActionResult Index(string path = "")
        {
            ViewBag.Completed = false;
            ViewBag.ErrorMessages = new List<string>();

            if (path == String.Empty) return Http404();
            Form f = db.Forms.Include("Fields").Include("Fields.Options").First(e => e.SEOURL == path);
            if (f == null) return Http404();

            ViewBag.Title = f.Title;
            ViewBag.MetaDescription = f.MetaDescription;
            ViewBag.MetaKeywords = f.MetaKeywords;

            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.Find(1);
            }

            Stack<Breadcrumb> bc = new Stack<Breadcrumb>();
            bc.Push(new Breadcrumb { Name = f.Title, URL = "/form/" + f.SEOURL });
            ViewBag.Breadcrumbs = bc;

            string ViewName = "~/Views/Templates/FormPage.cshtml";

            try
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath("~/Views/Forms/" + f.SEOURL + ".cshtml"));
                if (fi.Exists) ViewName = "~/Views/Forms/" + f.SEOURL + ".cshtml";
            }
            catch (Exception) { }

            return View(ViewName, f);
        }

    }
}
