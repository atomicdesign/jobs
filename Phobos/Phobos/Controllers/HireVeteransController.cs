﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs;
using CivilianJobs.Models.Jobs.Repositories;
using CivilianJobs.Models.Jobs.ViewModels;
using CivilianJobs.Models.Jobs.Enums;
using CivilianJobs.Helpers;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace CivilianJobs.Controllers
{
    public class HireVeteransController : JobsBaseController
    {
        [ActionName("Vetern-ocps")]
        public ActionResult Index()
        {
            ViewBag.absoluteUrl = "hireveterans";
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return RedirectToAction("military-job-board");
        }

        /* moved to employers controller
        [ActionName("vets-for-hire")]
        public ActionResult VetsForHire()
        {
            var model = new PostJob()
            {
                JobCategories = new JobCategoryRepository().Get(),
                JobPosting = new JobPosting(),
                EmployeeTypes = base.GetEmployeeTypes()
            };

            return View(model);
        }

        [ActionName("vets-for-hire")]
        [HttpPost]
        public ActionResult VetsForHire(JobPosting jobPosting)
        {
            if (ModelState.IsValid)
            {
                jobPosting.CreatedBy = User.Identity.Name;
                int id = new JobPostingRepository().Add(jobPosting);

                return RedirectToAction("my-job-postings");
            }

            return RedirectToAction("vets-for-hire");
        }

        [ActionName("my-job-postings")]
        public ActionResult MyJobPostings()
        {
            var jobPostings = new JobPostingRepository().Get(User.Identity.Name);

            return View("military-job-board", jobPostings);
        }
        */

        [ActionName("veteran-resumes")]
        public ActionResult VeteranResumes()
        {

            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }

        /* moved to job seekers controller
        [ActionName("military-job-board")]
        public ActionResult AllJobs()
        {
            var jobPostings = new JobPostingRepository().Get();

            return View("military-job-board", jobPostings);
        }
         */

        [ActionName("patriot-package")]
        public ActionResult PatriotPackage()
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }

        [ActionName("liberty-package")]
        public ActionResult LibertyPackage()
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }

        [ActionName("all-events")]
        public ActionResult AllEvents()
        {
            var model = new JobFairRepository().Get();
            ViewBag.absoluteUrl = "hireveterans";
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View("all-events", model);
        }

        [ActionName("press-releases")]
        public ActionResult PressReleases()
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);
            CivilianJobsContext ctx = new CivilianJobsContext();

            var PressReleases = ctx.PressReleases.ToList();
            return View("press-releases", PressReleases);
        }

        [ActionName("press-release")]
        public ActionResult PressRelease(int id = 0)
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);
            CivilianJobsContext ctx = new CivilianJobsContext();

            var PressRelease = ctx.PressReleases.Where(m => m.ID == id).ToList();
            return View("press-release", PressRelease);
        }


        #region checkout


        [ActionName("order-services")]
        public ActionResult OrderServices()
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);

            return View("order-services", new JobFairRepository().Get());
        }

        [HttpPost]
        [ActionName("order-services")]
        public ActionResult OrderServices(Orders model)
        {
            if (model.ResumeAccessViewsPerDay == 0)
            {
                model.ResumeAccessMonthsAccess = 0;
            }
            else if (model.ResumeAccessViewsPerDay == 200)
            {
                model.ResumeAccessMonthsAccess = model.ResumeAccessMonthsAccess200;
            }
            else if (model.ResumeAccessViewsPerDay == 400)
            {
                model.ResumeAccessMonthsAccess = model.ResumeAccessMonthsAccess400;
            }
            else if (model.ResumeAccessViewsPerDay == 600)
            {
                model.ResumeAccessMonthsAccess = model.ResumeAccessMonthsAccess600;
            }

            if (!ModelState.IsValid)
            {
                return View("order-services", new JobFairRepository().Get());
            }

            model.CreateDate = DateTime.Now;
            //model.Active = false;
            //new OrderRepository().Add(model, User.Identity.Name);
            TempData["ServiceOrderModel"] = JsonConvert.SerializeObject(model, Formatting.None);
            return RedirectToAction("basket", "shoppingcart");
        }

        [HttpGet]
        [ActionName("order-checkout")]
        public ActionResult OrderCheckout()
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }
        [ActionName("job-fairs-details")]
        public ActionResult JobFairDetails(int jobFairId)
        {
            ViewBag.EmployerNav = db.NavBars.Find(2);
            ViewBag.absoluteUrl = "hireveterans";
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            var model = new JobFairRepository().Get(jobFairId);
            return View(model);
        }

        [HttpPost]
        [ActionName("order-checkout")]
        public ActionResult OrderCheckout(OrderViewModel model)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://gwapi.demo.securenet.com/api/Payments/Charge");
            ViewBag.EmployerNav = db.NavBars.Find(2);
            string secureNetId = "8004550";
            string secureKey = "7jj5vjExCpQG";

            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(secureNetId + ":" + secureKey)));
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/json";

            var cr = new ChargeRequest
            {
                Amount = 11.00m,
                Card = new Card
                {
                    Number = model.Card.Number,
                    Cvv = model.Card.Cvv,
                    ExpirationDate = model.ExpirationMonth + "/" + model.ExpirationYear
                    //Number = "4111111111111113 ",
                    //Cvv = "929",
                    //ExpirationDate = "04/20"

                },
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = 12345678,
                    Version = "1.2"
                }
            };

            string payLoad = JsonConvert.SerializeObject(cr);
            SecureNetResponse result;

            using (StreamWriter sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write(payLoad);
            }

            HttpWebResponse httpWebResponse = request.GetResponse() as HttpWebResponse;
            using (StreamReader sr = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", httpWebResponse.StatusCode);
                    throw new ApplicationException(message);
                }

                result = JsonConvert.DeserializeObject<SecureNetResponse>(sr.ReadToEnd());
            }

            if (result.Success)
            {
                var or = new OrderRepository();
                var order = or.Get(User.Identity.Name);
                order.Active = true;
                or.Update(order);

                return RedirectToAction("order-success");
            }

            ViewBag.ErrorMessage = result.Message;

            return View(model);
        }

        [ActionName("order-success")]
        public ActionResult OrderSuccess()
        {
            return View();
        }

        #endregion
    }
}
