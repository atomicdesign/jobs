﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using CivilianJobs.Helpers;

namespace CivilianJobs.Controllers
{
    public class CronController : CivilianJobsBaseController
    {
        #region throttling
        private const int MinutesDay = 1440;

        private bool CronThrottle(string key, int minutes)
        {
            if (HttpRuntime.Cache.Get(key) == null)
            {
                HttpRuntime.Cache.Insert(key, 1, null, DateTime.UtcNow.AddMinutes(minutes), System.Web.Caching.Cache.NoSlidingExpiration);
                return true;
            }
            return false;
        }

        private void CancelThrottle(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }
        #endregion

        [HttpGet]
        public ActionResult Index()
        {
            SendEmailQueue();
            return Content(DateTime.Now.ToString());
        }

        private void SendEmailQueue()
        {
            string cronKey = "CRON_EMAIL";
            try
            {
                if (CronThrottle(cronKey, 5))
                {
                    Helpers.EmailService sc = new Helpers.EmailService();
                    if (!sc.SendBatch()) throw new Exception("Mail Send Failed");
                }
                //let it run again
                CancelThrottle(cronKey);
            }
            catch (Exception err)
            {
                Helpers.LogHelper.Log(err);
                CancelThrottle(cronKey);
            }
        }
    }
}