﻿using CivilianJobs.Models.Jobs;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs.Enums;
using CivilianJobs.Models.Jobs.Repositories;
using CivilianJobs.Models.Jobs.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace CivilianJobs.Controllers
{
    [Authorize(Roles = "Employer")]
    public class EmployerController : JobsBaseController
    {

        [ActionName("my-account")]
        public ActionResult myaccount()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            return View();
        }
        [HttpPost]
        [ActionName("post-file")]
        public JsonResult PostFile()
        {
            try
            {
                var httpRequest = this.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var fileName = "";
                    var filePath = "";
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        fileName = postedFile.FileName.Substring(0, postedFile.FileName.LastIndexOf('.')) + postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        filePath = Server.MapPath("~/Files/" + fileName);
                        if (System.IO.File.Exists(filePath))
                            System.IO.File.Delete(filePath);
                        postedFile.SaveAs(filePath);
                    }
                    HSSFWorkbook hssfwb;
                    using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        hssfwb = new HSSFWorkbook(file);
                    }
                    ISheet sheet = hssfwb.GetSheetAt(0);
                    List<List<string>> _content = new List<List<string>>();
                    for (int row = 1; row <= sheet.LastRowNum; row++)
                    {
                        var row_ = new List<string>();
                        for (int j = 0; j < 22; j++)
                        {
                            var cell = sheet.GetRow(row).GetCell(j);
                            if (cell != null)
                            {
                                switch (cell.CellType)
                                {
                                    case CellType.Boolean:
                                        row_.Add(cell.BooleanCellValue.ToString());
                                        break;
                                    case CellType.Numeric:
                                        row_.Add(cell.NumericCellValue.ToString());
                                        break;
                                    case CellType.String:
                                        row_.Add(cell.StringCellValue);
                                        break;
                                    case CellType.Blank:
                                        break;
                                    case CellType.Error:
                                        row_.Add(cell.ErrorCellValue.ToString());
                                        break;
                                    case CellType.Formula:
                                        break;
                                }
                            }
                        }
                        _content.Add(row_);
                    }
                    System.IO.File.Delete(filePath);
                    var categories = (new JobCategoryRepository()).Get();
                    string json = "[";
                    foreach (var item in _content)
                    {
                        var catIds = new List<int>();
                        catIds = item[0].Contains(",")
                            ? item[0].Split(',').Select(int.Parse).ToList()
                            : new List<int> { int.Parse(item[0]) };
                        var loc = ((JobLocation)(int.Parse(item[10]))).ToString();
                        var empType = ((EmployeeType)(int.Parse(item[13]))).ToString();
                        var degree = ((DegreeRequired)(int.Parse(item[14]))).ToString();
                        var security = ((SecurityClearance)(int.Parse(item[15]))).ToString();
                        var experience = ((ExperienceRequired)(int.Parse(item[16]))).ToString();
                        var travel = ((TravelRequired)(int.Parse(item[17]))).ToString();
                        var foreign = ((ForeignLanguage)(int.Parse(item[18]))).ToString();
                        var obj = new
                        {
                            Category = new { Ids = catIds, Name = string.Join(", ", categories.Where(x => catIds.Contains(x.ID)).Select(x => x.Name).ToList()) },
                            Location = new { Id = item[10], Name = loc },
                            EmployeeType = new { Id = item[13], Name = empType },
                            DegreeRequired = new { Id = item[14], Name = degree },
                            SecurityClearance = new { Id = item[15], Name = security },
                            ExperienceRequired = new { Id = item[16], Name = experience },
                            TravelRequired = new { Id = item[17], Name = travel },
                            ForeignLanguage = new { Id = item[18], Name = foreign },
                            Company = item[1],
                            ContactName = item[2],
                            Email = item[3],
                            HiddenEmail = item[4],
                            Phone = item[5],
                            Hiddenphone = item[6],
                            Fax = item[7],
                            HiddenFax = item[8],
                            Notify = item[9],
                            JobTitle = item[11],
                            Description = item[12],
                            Requeirment = item[19],
                            Compensation = item[20],
                            Bonus = item[21]
                        };
                        json += JsonConvert.SerializeObject(obj);
                        json += ",";
                    }
                    json = json.Substring(0, json.Length - 1) + "]";
                    return Json(json);
                }
                throw new Exception("filenotfound");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        [ActionName("import-excel")]
        public ActionResult ImportExcel()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.EmployerNav = db.NavBars.Find(2);
            ViewBag.Title = "Import Job Postings";
            return View();
        }
        public EmployerController()
        {
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            ViewBag.CurrentUserRole = (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0) ? null : "Employer";
            ViewBag.EmployerNav = db.NavBars.Find(2);
            
        }

        [AllowAnonymous]
        [ActionName("index")]
        public ActionResult Index()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            if (CurrentUserProfile == null || CurrentUserProfile.UserProfileID <= 0)
                ViewBag.CurrentUserRole = "";
            ViewBag.EmployerNav = db.NavBars.Find(2);
            ViewBag.absoluteUrl = "employer";
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.CurrentHome = true;
            return View();
        }

        [ActionName("post-job")]
        public ActionResult PostJob()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.Title = "Post Job";
            return View(GetPostJobsModel());
        }



        [ActionName("transition-news-data")]
        public ActionResult transitionnewsdata()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.Title = "Transition News";
            return View();
        }
        [HttpGet]
        [ActionName("changeJobStatus")]
        public JsonResult changeJobStatus(int statusId, string jobIds)
        {
            var ids = jobIds.Split('x').Select(int.Parse).ToList();
            var jobs = db.JobPostings.Where(x => ids.Contains(x.ID)).ToList();
            jobs.ForEach(x =>
            {
                x.Status = statusId;
                db.Entry<JobPostings>(x).State = System.Data.Entity.EntityState.Modified;
            });
            db.SaveChanges();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName("post-jobs")]
        public JsonResult PostJobs(JobPostingsImport import)
        {
            foreach (var item in import.models)
            {
                var jobs = (new JobCategoryRepository()).Get().Where(x => item.CategoryIds.Contains(x.ID)).ToList();
                item.Posting.CreatedBy = User.Identity.Name;
                var id = new JobPostingRepository().Add(item.Posting, jobs);
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [ActionName("post-job")]
        [HttpPost]
        public ActionResult PostJob(JobPostings jobPosting, IEnumerable<JobCategories> selectedJobCategories, string preview)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            if (selectedJobCategories == null || selectedJobCategories.Count() == 0)
            {
                ModelState.AddModelError("categories", "You must select at least one category.");
            }

            if (ModelState.IsValid)
            {
                int id;

                if (jobPosting.ID == 0)
                {
                    jobPosting.CreatedBy = User.Identity.Name;
                    id = new JobPostingRepository().Add(jobPosting, selectedJobCategories);
                }
                else
                {
                    new JobPostingRepository().Update(jobPosting, selectedJobCategories);
                    id = jobPosting.ID;
                }

                if (preview == null)
                {
                    return RedirectToAction("your-jobs");
                }
                else
                {
                    return RedirectToAction("preview-job-posting", new { id = id });
                }
            }

            var model = GetPostJobsModel();
            model.JobPosting = jobPosting;

            ViewBag.Title = "Post Job";

            return View(model);
        }

        [ActionName("edit-job")]
        public ActionResult EditJob(int id)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.Title = "Edit Job";

            var model = GetPostJobsModel();
            model.JobPosting = new JobPostingRepository().Get(id);

            return View("post-job", model);
        }

        [ActionName("your-jobs")]

        public ActionResult YourJobs(string sortOrder)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.Title = "Your Jobs";
            var jobPostings = new JobPostingRepository().Get(User.Identity.Name);
            ViewBag.CompanySortParm = sortOrder == "Company" ? "Company_desc" : "Company_asc";

            switch (sortOrder)
            {
                case "Company_asc":
                    jobPostings = jobPostings.OrderBy(s => s.Company);
                    break;
                case "Company_desc":
                    jobPostings = jobPostings.OrderByDescending(s => s.Company);
                    break;
                default:
                    jobPostings = jobPostings.OrderBy(s => s.Company);
                    break;
            }
            return View("your-jobs", jobPostings.ToList());
        }

        [ActionName("job-applications")]
        public ActionResult JobApplications(int id, bool? showHiddenOnly)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var jobPosting = new JobPostingRepository().GetWithApplicants(id, User.Identity.Name);

            if (showHiddenOnly.HasValue && showHiddenOnly.Value)
            {
                jobPosting.Applications = jobPosting.Applications.Where(x => x.IsHidden).ToList();
            }
            else
            {
                jobPosting.Applications = jobPosting.Applications.Where(x => !x.IsHidden).ToList();
            }

            ViewBag.ShowHiddenOnly = showHiddenOnly.HasValue && showHiddenOnly.Value ? true : false;

            return View(jobPosting);
        }

        [ActionName("get-applicants")]
        public JsonResult GetApplicants(int jobId, bool? starred, int? statusId)
        {
            var jobPostings = new JobPostingRepository().Get(User.Identity.Name).Select(x => x.ID).ToList();
            var result = db.JobApplications.Where(x => jobPostings.Contains(x.JobPostingID.Value) &&
            (starred != true || x.Starred == starred) &&
            (statusId == -1 || x.StatusId == statusId) &&
            (jobId == 0 || x.JobPostingID == jobId)).ToList();
            foreach (var item in result)
            {
                var user = db.UserProfiles.First(x => x.UserProfileID == item.UserProfileID);
                var address = user.Country ?? "" + " " + user.City??"" + " " + user.State??"" + " " + user.Address??""+ " " + user.Address2??"";
                item.UserProfile = new Models.UserProfile {Address = address, FirstName = (user.FirstName ?? "") + " " + (user.LastName ?? "") };
                item.JobPosting = new JobPostings { Title = db.JobPostings.First(x => x.ID == item.JobPostingID.Value).Title };
                item.DateString = item.CreateDate.Date.ToShortDateString();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult updateApplicant(string applicantIds, bool starred, int status)
        {
            var appIds = applicantIds.Split('x').Select(int.Parse).ToList();
            if (!applicantIds.Contains('x'))
            {
                appIds = new List<int>();
                appIds.Add(int.Parse(applicantIds));
            }
            foreach (var applicantId in appIds)
            {
                var result = db.JobApplications.FirstOrDefault(x => x.ID == applicantId);
                result.Starred = starred;
                if (status >= 0)
                    result.StatusId = status;
                db.Entry<JobApplication>(result).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }


        [ActionName("view-applicant")]
        public ActionResult ViewApplicant(string username)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var model = new JobSeekerAccount()
            {
                User = db.LoadUser(username),
                JobApplications = new JobApplicationRepository().GetByApplicant(username),
                SavedJobs = new SavedJobRepository().Get(username),
                Documents = new DocumentRepository().Get(username)
            };

            // TODO: clean this up later
            ViewBag.Resume = new ResumeRepository().Get(username);

            return View("view-applicant", model);
        }

        [ActionName("view-job")]
        public ActionResult JobView(int id)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var jobPosting = new JobPostingRepository().Get(id);
            return View(jobPosting);
        }

        public JsonResult addCondidate(int jobId, int candidateId, int starred)
        {
            var userId = db.JobSeekersDetails.First(x=>x.Id==candidateId).UserId;
            var username = db.UserProfiles.First(x=>x.UserId==userId).UserName;
            new JobApplicationRepository().Add(username, jobId, starred==1);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [ActionName("saved-resumes")]
        public ActionResult savedResumes()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            return View();
        }

        [ActionName("read-saved-resumes")]
        public ActionResult ReadSavedResumes(int index, int count, string keywords)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            if (count > 10) count = 10;
            var repoResult = new ResumeRepository().GetSaved(index, count, 0, keywords, CurrentUserProfile.UserProfileID);
            return Json(repoResult, JsonRequestBehavior.AllowGet);
        }

        [ActionName("applicant")]
        public ActionResult ApplicantResume(int id)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var resume = db.JobSeekersDetails.First(x => x.Id == id);
            resume.ViewCount = resume.ViewCount == null ? 1 : resume.ViewCount + 1;
            db.SaveChanges();
            resume._userProfile = db.UserProfiles.First(x => x.UserId == resume.UserId); 
            var saved = db.SavedResumes.FirstOrDefault(x => x.EmployerId == CurrentUserProfile.UserProfileID && x.ResumeId == resume.Id);
            ViewBag.Saved = saved == null ? "nonsaved" : "Saved";
            
            var list = db.JobApplications.
                Where(x => x.JobPosting.CreatedBy == CurrentUserProfile.UserName &&
           x.UserProfileID == resume._userProfile.UserProfileID).ToList();
            ViewBag.hasjobs = list.Count > 0 ? "yes" : "no";
            var ids = list.Select(x => x.JobPostingID).ToList();
            ViewBag.list = new JobPostingRepository().Get(User.Identity.Name).Where(x => ids.Contains(x.ID)).Distinct();
            ViewBag.postings = new JobPostingRepository().Get(User.Identity.Name).Where(x => !ids.Contains(x.ID)).Distinct();
            return View("applicant", resume);
        }

        [ActionName("save-condidate")]
        public JsonResult SaveOrRemoveCondidate(int id, int operation)
        {
            switch (operation)
            {
                case 0:
                    var temp = new Models.SavedResumes
                    {
                        EmployerId = CurrentUserProfile.UserProfileID,
                        ResumeId = id
                    };
                    db.SavedResumes.Add(temp);
                    db.SaveChanges();
                    break;
                case 1:
                    var result = db.SavedResumes.FirstOrDefault(x => x.EmployerId == CurrentUserProfile.UserProfileID &&
                     x.ResumeId == id);
                    db.Entry(result).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                    break;
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }


        [ActionName("toggle-applicant")]
        public ActionResult HideOrShowApplication(int jobPostingId, int jobApplicationId, bool showHiddenOnly)
        {
            // get the full job posting so we can check if the user calling this is
            // actually the employer who created the posting
            var jobPosting = new JobPostingRepository().GetWithApplicants(jobPostingId, User.Identity.Name);

            if (jobPosting == null)
            {
                return new HttpUnauthorizedResult();
            }

            var jobApplication = jobPosting.Applications.First(x => x.ID == jobApplicationId);
            jobApplication.IsHidden = !jobApplication.IsHidden;
            new JobApplicationRepository().Update(jobApplication);

            return RedirectToAction("job-applications", new { id = jobPostingId, showHiddenOnly });
        }

        [AllowAnonymous]
        [ActionName("search-resumes")]
        public ActionResult SearchResumes()
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            return View();
        }

        [ActionName("getresumesbyfilter")]
        public ActionResult GetResumesByFilter(int index, int count, int sorting)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var repoResult = new ResumeRepository().Get(index, count, sorting, this.Request, this.Url);
            return Json(repoResult, JsonRequestBehavior.AllowGet);

        }

        [AllowAnonymous]
        [ActionName("getresumessearch")]
        public ActionResult GetResumesBySearch(int index, int count, string keywords)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var repoResult = new ResumeRepository().Get(index, count, 0, keywords);
            return Json(repoResult, JsonRequestBehavior.AllowGet);

        }


        [ActionName("preview-job-posting")]
        public ActionResult ViewJob(int id)
        {
            if (CurrentUserProfile != null && !CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var jobPosting = new JobPostingRepository().Get(id);

            return View(jobPosting);
        }

        private PostJob GetPostJobsModel()
        {
            return new PostJob()
            {
                JobPosting = new JobPostings(),
                JobCategories = new JobCategoryRepository().Get(),
                EmployeeTypes = base.GetEmployeeTypes(),
                ExperienceTypes = base.GetExperienceTypes(),
                DegreeTypes = base.GetDegreeTypes(),
                TravelTypes = base.GetTravelTypes(),
                SecurityTypes = base.GetSecurityTypes(),
                ForeignLanguageTypes = base.GetForeignLanguageTypes(),
                LocationTypes = base.GetLocationTypes()
            };
        }


        [ActionName("ResumeSearchlocations")]
        public JsonResult GetResumeSearchlocations()
        {
            var locs = new ResumeRepository().GetLocations();
            var retVal = new List<string>();
            foreach (var item in locs)
            {
                retVal.Add(item.Name);
            }
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }
        [ActionName("ResumeSearchcategories")]
        public JsonResult GetResumeSearchcategories()
        {
            var locs = new ResumeRepository().GetCategories();
            var retVal = new List<string>();
            foreach (var item in locs)
            {
                retVal.Add(item.Name);
            }
            return Json(retVal, JsonRequestBehavior.AllowGet);
        }
    }
}
