﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Controllers
{
    public class SearchController : CivilianJobsBaseController
    {
        //
        // GET: /Search/
        public ActionResult Index(string query = "")
        {
            query = query.ToString().Trim();


            Dictionary<string, List<SearchResult>> results = new Dictionary<string, List<SearchResult>>();
         
            results.Add("Pages", Page.Search(query));
            results.Add("Events", Events.Search(query));
            results.Add("Forms", Form.Search(query));

            return View(results);
        }

    }
}
