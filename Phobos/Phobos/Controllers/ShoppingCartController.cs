﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Web.Security;
using CivilianJobs.Models.Jobs.Repositories;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web.Configuration;
using CivilianJobs.Models.Jobs.ViewModels;
using Newtonsoft.Json;

namespace CivilianJobs.Controllers
{
    //[Authorize]
    public class ShoppingCartController : CivilianJobsBaseController
    {

        


        [AllowAnonymous]
        [ActionName("previous-orders")]
        public ActionResult PreviousOrders()
        {
            ViewBag.absoluteUrl = "shoppingcart";
            ViewBag.PrimaryNav = db.NavBars.Find(2);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }

        [AllowAnonymous]
        [ActionName("all-orders")]
        public ActionResult AllOrders()
        {
            ViewBag.absoluteUrl = "shoppingcart";
            ViewBag.PrimaryNav = db.NavBars.Find(2);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }

        [AllowAnonymous]
        [ActionName("basket")]
        public ActionResult Basket()
        {
            ViewBag.absoluteUrl = "shoppingcart";
            ViewBag.PrimaryNav = db.NavBars.Find(2);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            return View();
        }
        //
        // GET: /ShoppingCart/

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index(string path = "")
        {
            ViewBag.absoluteUrl = "shoppingcart";
            switch (path)
            {
                case "previous-orders":
                    ViewBag.PrimaryNav = db.NavBars.Find(2);
                    ViewBag.EmployerNav = db.NavBars.Find(2);
                    var repox = new ProductDescriptionsRepository();
                    var datax = repox.Get(CurrentUserProfile.UserProfileID).OrderByDescending(x => x.date).Take(10);
                    return View(path);
                case "all-orders":
                    ViewBag.PrimaryNav = db.NavBars.Find(2);
                    ViewBag.EmployerNav = db.NavBars.Find(2);
                    var repo = new ProductDescriptionsRepository();
                    var data = repo.Get(CurrentUserProfile.UserProfileID).OrderByDescending(x => x.date);

                    return View(path, data);
                case "basket":
                    ViewBag.PrimaryNav = db.NavBars.Find(2);
                    ViewBag.EmployerNav = db.NavBars.Find(2);
                    return View(path);
            }
            if (path == String.Empty) path = "staffing-products";

            Page p = db.Pages.FirstOrDefault(e => e.CachedFullSEOURL != String.Empty && e.CachedFullSEOURL == path);

            if (p == null)
            {
                Queue<string> splitpath = new Queue<string>(path.Split('/'));
                if (splitpath.Count > 0)
                {
                    string pathseg = splitpath.Dequeue();
                    p = db.Pages.FirstOrDefault(e => e.SEOURL == pathseg);
                    while (splitpath.Count > 0 && p != null)
                    {
                        pathseg = splitpath.Dequeue();
                        p = p.SubPages.FirstOrDefault(e => e.SEOURL == pathseg);
                    }
                }
            }

            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.FirstOrDefault(e => e.Name == nbname);
            }

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;
            ViewBag.EmployerNav = db.NavBars.Find(2);

            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();
            ViewBag.JobFairs = new JobFairRepository().Get();

            //Page temp = p.Parent;
            //List<Breadcrumb> bc = new List<Breadcrumb>();
            //while(temp != null)
            //{                
            //    bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
            //    temp = temp.Parent;
            //}
            //ViewBag.Breadcrumbs = bc;

            return View(p);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult MtnAdv(string path = "")
        {
            ViewBag.absoluteUrl = "shoppingcart";
            if (path == String.Empty) path = "mtn-adv";

            Page p = db.Pages.FirstOrDefault(e => e.CachedFullSEOURL != String.Empty && e.CachedFullSEOURL == path);

            if (p == null)
            {
                Queue<string> splitpath = new Queue<string>(path.Split('/'));
                if (splitpath.Count > 0)
                {
                    string pathseg = splitpath.Dequeue();
                    p = db.Pages.FirstOrDefault(e => e.SEOURL == pathseg);
                    while (splitpath.Count > 0 && p != null)
                    {
                        pathseg = splitpath.Dequeue();
                        p = p.SubPages.FirstOrDefault(e => e.SEOURL == pathseg);
                    }
                }
            }

            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.FirstOrDefault(e => e.Name == nbname);
            }

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;
            ViewBag.EmployerNav = db.NavBars.Find(2);

            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();
            ViewBag.JobFairs = new JobFairRepository().Get();

            //Page temp = p.Parent;
            //List<Breadcrumb> bc = new List<Breadcrumb>();
            //while(temp != null)
            //{                
            //    bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
            //    temp = temp.Parent;
            //}
            //ViewBag.Breadcrumbs = bc;

            return View(p);
        }
        
        public ActionResult JobBoards(string path = "")
        {
            ViewBag.absoluteUrl = "shoppingcart";
            if (path == String.Empty) path = "job-boards";

            Page p = db.Pages.FirstOrDefault(e => e.CachedFullSEOURL != String.Empty && e.CachedFullSEOURL == path);

            if (p == null)
            {
                Queue<string> splitpath = new Queue<string>(path.Split('/'));
                if (splitpath.Count > 0)
                {
                    string pathseg = splitpath.Dequeue();
                    p = db.Pages.FirstOrDefault(e => e.SEOURL == pathseg);
                    while (splitpath.Count > 0 && p != null)
                    {
                        pathseg = splitpath.Dequeue();
                        p = p.SubPages.FirstOrDefault(e => e.SEOURL == pathseg);
                    }
                }
            }

            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.FirstOrDefault(e => e.Name == nbname);
            }

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;
            ViewBag.EmployerNav = db.NavBars.Find(2);

            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();
            ViewBag.JobFairs = new JobFairRepository().Get();

            //Page temp = p.Parent;
            //List<Breadcrumb> bc = new List<Breadcrumb>();
            //while(temp != null)
            //{                
            //    bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
            //    temp = temp.Parent;
            //}
            //ViewBag.Breadcrumbs = bc;

            return View(p);
        }

        public ActionResult Packages(string path = "")
        {
            ViewBag.absoluteUrl = "shoppingcart";
            if (path == String.Empty) path = "packages";

            Page p = db.Pages.FirstOrDefault(e => e.CachedFullSEOURL != String.Empty && e.CachedFullSEOURL == path);

            if (p == null)
            {
                Queue<string> splitpath = new Queue<string>(path.Split('/'));
                if (splitpath.Count > 0)
                {
                    string pathseg = splitpath.Dequeue();
                    p = db.Pages.FirstOrDefault(e => e.SEOURL == pathseg);
                    while (splitpath.Count > 0 && p != null)
                    {
                        pathseg = splitpath.Dequeue();
                        p = p.SubPages.FirstOrDefault(e => e.SEOURL == pathseg);
                    }
                }
            }

            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.FirstOrDefault(e => e.Name == nbname);
            }

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;
            ViewBag.EmployerNav = db.NavBars.Find(2);

            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();
            ViewBag.JobFairs = new JobFairRepository().Get();

            //Page temp = p.Parent;
            //List<Breadcrumb> bc = new List<Breadcrumb>();
            //while(temp != null)
            //{                
            //    bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
            //    temp = temp.Parent;
            //}
            //ViewBag.Breadcrumbs = bc;

            return View(p);
        }
        
        public ActionResult JobFair(string path = "")
        {
            ViewBag.absoluteUrl = "shoppingcart";
            if (path == String.Empty) path = "job-fair";

            Page p = db.Pages.FirstOrDefault(e => e.CachedFullSEOURL != String.Empty && e.CachedFullSEOURL == path);

            if (p == null)
            {
                Queue<string> splitpath = new Queue<string>(path.Split('/'));
                if (splitpath.Count > 0)
                {
                    string pathseg = splitpath.Dequeue();
                    p = db.Pages.FirstOrDefault(e => e.SEOURL == pathseg);
                    while (splitpath.Count > 0 && p != null)
                    {
                        pathseg = splitpath.Dequeue();
                        p = p.SubPages.FirstOrDefault(e => e.SEOURL == pathseg);
                    }
                }
            }

            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.FirstOrDefault(e => e.Name == nbname);
            }

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;
            ViewBag.EmployerNav = db.NavBars.Find(2);
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();
            ViewBag.JobFairs = new JobFairRepository().Get();

            //Page temp = p.Parent;
            //List<Breadcrumb> bc = new List<Breadcrumb>();
            //while(temp != null)
            //{                
            //    bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
            //    temp = temp.Parent;
            //}
            //ViewBag.Breadcrumbs = bc;

            return View(p);
        }

       [HttpPost]
        public ActionResult AddToCart(CivilianJobs.Models.Jobs.ViewModels.OrderDetail orderDetail)
        {
            ViewBag.absoluteUrl = "shoppingcart";
            Cart cart;
           if(Session["cart"] == null)
           {
               cart = new Cart();
               cart.DateCreated = DateTime.Now;
               cart.OrderDetails.Add(orderDetail);
               cart.Total += orderDetail.SubTotal;
               Session["cart"] = cart;
           }
           else
           {
               cart = Session["cart"] as Cart;
               cart.OrderDetails.Add(orderDetail);
               cart.Total += orderDetail.SubTotal;
           }
           var obj = JsonConvert.SerializeObject(cart);
           return new JsonResult() { Data = cart, JsonRequestBehavior = JsonRequestBehavior.DenyGet };

        }
        public ActionResult RemoveFromCart(int id)
        {
            ViewBag.absoluteUrl = "shoppingcart";
            Cart cart;
            if (Session["cart"] != null)
            {
                cart = Session["cart"] as Cart;
               var item = cart.OrderDetails.Find(x => x.Id==id);
               
                cart.Total -= item.SubTotal;
            }
            else
            {
                cart = new Cart();
            }
            
            return Json(cart,JsonRequestBehavior.DenyGet);
        }

        public ActionResult CartSummary()
        {
            ViewBag.absoluteUrl = "shoppingcart";
            Cart cart;
            if (Session["cart"] != null)
            {
                cart = Session["cart"] as Cart;
            }
            else
            {
                cart = new Cart();
            }

            return Json(cart, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewCart()
        {
            ViewBag.absoluteUrl = "shoppingcart";
            Cart cart;
            if (Session["cart"] == null)
            {
                cart = new Cart();
                
            }
            else
            {
                cart = Session["cart"] as Cart;
                
            }
           return View(cart);
        }

    }
}
