﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Controllers
{
    public class FAQController : CivilianJobsBaseController
    {
        public ActionResult Index()
        {
            List<FAQCategory> retVal =  db.FAQCategories.OrderBy(x => x.Order).ToList();
            return View(retVal);
        }
    }
}