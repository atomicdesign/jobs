﻿using CivilianJobs.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Controllers
{
    public class HomeController :  CivilianJobsBaseController
    {
        [Authorize]
        [ActionName("activation")]
        public ActionResult activate()
        {
            if (CurrentUserProfile.IsActivated.Value == true)
               return Redirect("/");
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            ViewBag.email = CurrentUserProfile.Email;
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult activateMe(string code)
        {
           if(CurrentUserProfile.ActivationCode == code)
            {
                var user = db.UserProfiles.First(x => x.UserProfileID == CurrentUserProfile.UserProfileID);
                user.IsActivated = true;
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json("ok", JsonRequestBehavior.AllowGet);
            }
           else
            {
                return Json("it's bad for you dear user", JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult resendCode()
        {
            var user = db.UserProfiles.First(x => x.UserProfileID == CurrentUserProfile.UserProfileID);
            user.IsActivated = false;
            var code = Helpers.KeyGenerator.GetUniqueKey(35);
            user.ActivationCode = code;
            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            var task = new Task(() =>
            {
                AAEmailSender.Send(new System.Net.Mail.MailMessage
                {
                    Body = string.Format(@"Dear {0}, 
Please use new Activation Code to activate your account.
Activation Code -  {1}", CurrentUserProfile.FirstName, code),
                }, CurrentUserProfile.Email);
            });
            task.Start();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }


        [AllowAnonymous]
        [ActionName("contact-us")]
        public ActionResult contactus()
        {
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.PrimaryNav = db.NavBars.Find(1);
            ViewBag.EmployerNav = db.NavBars.Find(2);
            Random oRandom = new Random();
            int iNumber = oRandom.Next(100000, 999999);
            Session["Captcha"] = iNumber.ToString();
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ActionName("send-mail")]
        public JsonResult sendemail(contactInput input)
        {
            if(Session["Captcha"].ToString() != input.numbers)
            {
                return Json("Captcha error", JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(input.firstname) || string.IsNullOrEmpty(input.lastname)
                || string.IsNullOrEmpty(input.message) || string.IsNullOrEmpty(input.phone)
                   || string.IsNullOrEmpty(input.email))
            {

                return Json("please, fill all fields", JsonRequestBehavior.AllowGet);
            }
            return Json("ok", JsonRequestBehavior.AllowGet);
        }
    }
    public class contactInput
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string numbers { get; set; }
        public string message { get; set; }
    }
}