﻿using CivilianJobs.Helpers;
using CivilianJobs.Models.Jobs.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CivilianJobs.Controllers
{
    public class JobsBaseController : CivilianJobsBaseController
    {
        public SelectList GetEmployeeTypes()
        {
            return new SelectList(Enum.GetValues(typeof(EmployeeType)).Cast<EmployeeType>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

        public SelectList GetExperienceTypes()
        {
            return new SelectList(Enum.GetValues(typeof(ExperienceRequired)).Cast<ExperienceRequired>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

        public SelectList GetDegreeTypes()
        {
            return new SelectList(Enum.GetValues(typeof(DegreeRequired)).Cast<DegreeRequired>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

        public SelectList GetTravelTypes()
        {
            return new SelectList(Enum.GetValues(typeof(TravelRequired)).Cast<TravelRequired>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

        public SelectList GetSecurityTypes()
        {
            return new SelectList(Enum.GetValues(typeof(SecurityClearance)).Cast<SecurityClearance>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

        public SelectList GetForeignLanguageTypes()
        {
            return new SelectList(Enum.GetValues(typeof(ForeignLanguage)).Cast<ForeignLanguage>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

        public SelectList GetLocationTypes()
        {
            return new SelectList(Enum.GetValues(typeof(JobLocation)).Cast<JobLocation>().Select(v => new SelectListItem
            {
                Text = CivilianJobsHelper.GetEnumDescription(v),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
        }

    }
}


