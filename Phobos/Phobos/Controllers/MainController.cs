﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;
using System.Web.Security;
using CivilianJobs.Models.Jobs.Repositories;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web.Configuration;

namespace CivilianJobs.Controllers
{
    public class MainController : CivilianJobsBaseController
    {
        //[OutputCache(Duration=86400, VaryByParam="path")]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index(string path = "")
        {
            if (path == String.Empty) path = "home";

            Page p = db.Pages.FirstOrDefault(e => e.CachedFullSEOURL != String.Empty && e.CachedFullSEOURL == path);

            if (p == null)
            {
                Queue<string> splitpath = new Queue<string>(path.Split('/'));
                if (splitpath.Count > 0)
                {
                    string pathseg = splitpath.Dequeue();
                    p = db.Pages.FirstOrDefault(e => e.SEOURL == pathseg);
                    while (splitpath.Count > 0 && p != null)
                    {
                        pathseg = splitpath.Dequeue();
                        p = p.SubPages.FirstOrDefault(e => e.SEOURL == pathseg);
                    }
                }
            }
            ViewBag.CheckHomePage = "HomePage";
            if (ViewData["Default Primary Nav"] != null)
            {
                string nbname = GetSetting("Default Primary Nav");
                ViewBag.PrimaryNav = db.NavBars.FirstOrDefault(e => e.Name == nbname);
            }

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;
            ViewBag.EmployerNav = db.NavBars.Find(2);
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.CurrentHome = true;
            ViewBag.LatestJobPostings = new JobPostingRepository().GetLatest();
            ViewBag.JobFairs = new JobFairRepository().Get();


            if (CurrentUserRole != null && CurrentUserRole == "Job Seeker")
            {
                var temp = db.JobSeekersDetails.FirstOrDefault(x => x.UserId == CurrentUserProfile.UserId);
                ViewBag.SavedJobs = db.SavedJobs.Where(x => x.UserProfile.UserProfileID == CurrentUserProfile.UserProfileID).Count();
                ViewBag.ViewCount = temp == null ? 0 : temp.ViewCount ?? (int?)0;
                ViewBag.JobApplies = db.JobApplications.Where(x => x.UserProfileID == CurrentUserProfile.UserProfileID).Count();
                ViewBag.absoluteUrl = "jobseeker";
            }


            //Page temp = p.Parent;
            //List<Breadcrumb> bc = new List<Breadcrumb>();
            //while(temp != null)
            //{                
            //    bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
            //    temp = temp.Parent;
            //}
            //ViewBag.Breadcrumbs = bc;

            return View(p.Template.FullPath, p);
        }

        public ActionResult SignUp()
        {

            string CompName = Request.Form["CompName"];
            string ContactName = Request.Form["ContactName"];
            string email = Request.Form["email"];
            string Phone = Request.Form["Phone"];

            //Email
            System.Net.Mail.MailMessage emailApplication = new System.Net.Mail.MailMessage();
            emailApplication.From = new MailAddress(email, "New Sign Up");
            emailApplication.IsBodyHtml = true;



            string htmlBody =
                "<h1>New Employment Application<h1>" +
                "<table><tr><td>Company Name: </td><td>" + CompName + "</td></tr>" +
                "<tr><td>Contant Name: </td><td>" + ContactName + "</td></tr>" +
                "<tr><td>Email: </td><td>" + email + "</td></tr>" +
                "<tr><td>Phone: </td><td>" + Phone + "</td></tr></table>";




            emailApplication.Subject = "New Application Submitted";
            emailApplication.Body = htmlBody;



            emailApplication.To.Add(new MailAddress("nick@atomicdesign.net"));
            SmtpSection cfg = NetSectionGroup.GetSectionGroup(WebConfigurationManager.OpenWebConfiguration("~/web.config")).MailSettings.Smtp;
            SmtpClient client = new SmtpClient(cfg.Network.Host);

            client.Send(emailApplication);

            return RedirectToAction("index");
        }

    }
}


