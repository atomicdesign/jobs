﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Transactions;
using CivilianJobs.Models;
using CivilianJobs.Helpers;
using MaxMind.GeoIP;
using PagedList;
using CivilianJobs.Models.Base.CRM;
using CivilianJobs.Models.Jobs.Enums;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using CivilianJobs.Models.Jobs.Repositories;
using CivilianJobs.Models.Jobs.Entities;
using CivilianJobs.Models.Jobs.ViewModels;
using System.Configuration;
using System.Threading.Tasks;

namespace CivilianJobs.Controllers
{
    public class AccountController : CivilianJobsBaseController
    {

        [ActionName("createOrder")]
        [HttpPost]
        public ActionResult CreateOrder(detOrders orders)
        {
            if (orders.order != null)
            {
                orders.order.Active = false;
                new OrderRepository().Add(orders.order, User.Identity.Name);
            }
            if (orders.data != null)
            {
                var result = orders.data.Select(x => new ProductDescription
                {
                    date = DateTime.Now,
                    package = x.package1,
                    title = x.title1,
                    type = x.type1,
                    state = 1,
                    price = x.price1,
                    quantity = x.quantity1,
                    userId = CurrentUserProfile.UserProfileID
                }).ToList();

                var count = result.Where(x => x.package == "BASIC PACKAGE" || x.package == "MULTIPLE JOB FAIR PACKAGE WITH ADDED SERVICES" ||
                x.package == "DISPLAY ADVERTISING IN MILITARY TRANSITION NEWS"
                || x.package == "SINGLE JOB FAIR WITH ADDED SERVICES: THE PATRIOT’S PACKAGE").Count();
                var temp = new ProductDescriptionsRepository();
                result.ForEach(x =>
                {
                    if (count > 2 && (x.package == "BASIC PACKAGE" || x.package == "MULTIPLE JOB FAIR PACKAGE WITH ADDED SERVICES" ||
                x.package == "DISPLAY ADVERTISING IN MILITARY TRANSITION NEWS"
                || x.package == "SINGLE JOB FAIR WITH ADDED SERVICES: THE PATRIOT’S PACKAGE"))
                        x.price = x.price * 0.9m;
                    temp.create(x);
                });
            }
            return null;
        }
        public ActionResult Register(JobsUserRole type)
        {
            if (type == JobsUserRole.JobSeeker)
            {
                return View("job-seeker-registration");
            }
            else
            {
                return View("employer-registration");
            }
        }

        [HttpPost]
        public ActionResult RegisterJobSeeker(JobSeeker model)
        {

            if (ModelState.IsValid)
            {
                MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                MembershipUser user = null;

                //find user
                if (model.Email != null)
                {
                    user = Membership.GetUser(model.Email);
                }

                //user with specified email DNE
                if (user == null)
                {
                    //create new user
                    user = Membership.CreateUser(model.Email, model.Password, model.Email, null, null, true, out createStatus);
                }
                else
                {
                    ModelState.AddModelError("error", "This e-mail is in use. Please choose another or login.");
                    return PartialView("_RegisterJobSeeker", model);
                }
                var code = Helpers.KeyGenerator.GetUniqueKey(24);
                try
                {
                    //user creation successful
                    if (createStatus == MembershipCreateStatus.Success)
                    {

                        //add user
                        UserProfile userModel = new UserProfile()
                        {
                            UserName = model.Email,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            Address = model.Address,
                            Address2 = model.Address2,
                            City = model.City,
                            State = model.State,
                            Zip = model.Zip,
                            Country = model.Country,
                            PrimaryPhone = model.PrimaryPhone,
                            AlternatePhone = model.AlternatePhone,
                            OCONUSphone = model.OCONUSphone,
                            HowHeardAbout = model.HowHeardAbout,
                            Email = model.Email,
                            Created = DateTime.Now,
                            IsActivated = false,
                            ActivationCode = code
                        };

                        db.UserProfiles.Add(userModel);
                        
                        Roles.AddUserToRole(model.Email, CivilianJobsHelper.GetEnumDescription(JobsUserRole.JobSeeker));
                        db.SaveChanges();
                        var task = new Task(() => {
                            AAEmailSender.Send(new System.Net.Mail.MailMessage
                            {
                                Body = string.Format(@"Dear {0}, 
Thank you for your interest in CivilianJobs.com
Please use Activation Code to activate your account.
Activation Code -  {1}", model.FirstName, code),
                            }, model.Email);
                        });
                        task.Start();
                        if (Membership.ValidateUser(model.Email, model.Password))
                        {
                            FormsAuthentication.SetAuthCookie(model.Email, true);
                            Session["CurrentUser"] = model.Email;
                            //next step
                            return RedirectToAction("activation", "home");
                        }
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException err)
                {
                    Helpers.LogHelper.Log(err);

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Membership.DeleteUser(model.Email, true);
                        db.UserProfiles.Remove(db.UserProfiles.Where(m => m.UserName == model.Email).FirstOrDefault());
                    }

                    ModelState.AddModelError("error", err.Message);
                }
            }

            return PartialView("_RegisterJobSeeker", model);
        }
        public void CheckAjax(string data)
        {
            //company name email phone
            List<string> GEtAttributes = new List<string>();
            StringBuilder sb = new StringBuilder();
            foreach (var item in data)
            {
                if (item == '`')
                {
                    GEtAttributes.Add(sb.ToString());
                    sb.Clear();
                }
                else
                {
                    sb.Append(item);
                }
            }
            
            MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
            MembershipUser user = null;
            if (GEtAttributes[2].ToString() != null)
            {
                user = Membership.GetUser(GEtAttributes[2].ToString());
            }
            if (user == null)
            {
                //create new user
                user = Membership.CreateUser(GEtAttributes[2].ToString(), GEtAttributes[4].ToString(), GEtAttributes[2].ToString(), null, null, true, out createStatus);
            }
            else
            {
                //ModelState.AddModelError("error", "This e-mail is in use. Please choose another or login.");
                //return PartialView("_RegisterEmployer", model);
            }
            if (createStatus == MembershipCreateStatus.Success)
            {
               

                UserProfile userModel = new UserProfile()
                {
                    CompanyName = GEtAttributes[0].ToString(),
                    UserName = GEtAttributes[1].ToString(),
                    Email = GEtAttributes[2].ToString(),
                    PrimaryPhone = GEtAttributes[3].ToString(),
                    
                    Created = DateTime.Now,
                    HowHeardAbout = "Quick Reg Some Source",
                    FirstName = "Quick Reg First Name",
                    LastName = "Quick Reg Last name",
                    Address = "Quick Reg Address",
                    Address2 = "Quick Reg Address2",
                    City = "Quick Reg City",
                    State = "AA",
                    Zip = "00000",
                    Hax = 0,

                    UserId = Guid.Parse(user.ProviderUserKey.ToString()),
                    AlternatePhone = GEtAttributes[3].ToString(),
                    CompanyWebsite = "Quick Reg Company.web.com",
                    WhichAccountExecutive = "Quick Reg Number",
                };
                try
                {
                    db.UserProfiles.Add(userModel);
                    Roles.AddUserToRole(GEtAttributes[2].ToString(), CivilianJobsHelper.GetEnumDescription(JobsUserRole.Employer));
                    db.SaveChanges();
                    
                }
                catch (Exception)
                {
                    
                }
                
            }
            
        }
        public ActionResult RegisterEmployer(Employer model)
        {
            if (ModelState.IsValid)
            {
                MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
                MembershipUser user = null;

                //find user
                if (model.Email != null)
                {
                    user = Membership.GetUser(model.Email);
                }

                //user with specified email DNE
                if (user == null)
                {
                    //create new user
                    user = Membership.CreateUser(model.Email, model.Password, model.Email, null, null, true, out createStatus);
                }
                else
                {
                    ModelState.AddModelError("error", "This e-mail is in use. Please choose another or login.");
                    return PartialView("_RegisterEmployer", model);
                }
                var code = Helpers.KeyGenerator.GetUniqueKey(24);
                try
                {
                    //user creation successful
                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        //add user
                        UserProfile userModel = new UserProfile()
                        {
                            UserName = model.Email,
                            Email = model.Email,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            Address = model.Address,
                            Address2 = model.Address2,
                            City = model.City,
                            State = model.State,
                            Zip = model.Zip,
                            PrimaryPhone = model.PrimaryPhone,
                            AlternatePhone = model.AlternatePhone,
                            HowHeardAbout = model.HowHeardAbout,
                            Created = DateTime.Now,
                            CompanyName = model.CompanyName,
                            CompanyWebsite = model.CompanyWebsite,
                            WhichAccountExecutive = model.WhichAccountExecutive,
                            IsActivated = false,
                            ActivationCode = code
                        };

                        db.UserProfiles.Add(userModel);

                        Roles.AddUserToRole(model.Email, CivilianJobsHelper.GetEnumDescription(JobsUserRole.Employer));
                        db.SaveChanges();
                        var task = new Task(() => {
                            AAEmailSender.Send(new System.Net.Mail.MailMessage
                            {
                                Body = string.Format(@"Dear {0}, 
Thank you for your interest in CivilianJobs.com
Please use Activation Code to activate your account.
Activation Code -  {1}", model.FirstName, code),
                            }, model.Email);
                        });
                        task.Start();
                        if (Membership.ValidateUser(model.Email, model.Password))
                        {
                            FormsAuthentication.SetAuthCookie(model.Email, true);
                            Session["CurrentUser"] = model.Email;
                            //next step
                            return RedirectToAction("activation", "home");
                        }
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException err)
                {
                    Helpers.LogHelper.Log(err);

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Membership.DeleteUser(model.Email, true);
                        db.UserProfiles.Remove(db.UserProfiles.Where(m => m.UserName == model.Email).FirstOrDefault());
                    }

                    ModelState.AddModelError("error", err.Message);
                }
            }

            return PartialView("_RegisterEmployer", model);
        }

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login()
        {
            //new CivilianJobs.Areas.api.Controllers.CivilianJobsInterface().ResetUser();
            return View();
        }

        //
        // POST: /Account/Login

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            
            /* SETTING ADMIN PASSWORD */
            /*
             * Uncomment the following line as per instructions in Services > CivilianJobsInterface.asmx.cs > ResetUser()
            */
            //(new CivilianJobs.Areas.api.Controllers.CivilianJobsInterface()).ResetUser();
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        if (!Request.IsAjaxRequest())
                        {
                            Session["CurrentUser"] = model.UserName;
                            return Redirect(returnUrl);
                        }
                        return Json(new { url = returnUrl });
                    }
                    else
                    {
                        Session["CurrentUser"] = model.UserName;
                        var roles = Roles.GetRolesForUser(model.UserName).ToList();
                        if (roles.Count() > 0 && roles.Contains("Administrator"))
                        {
                            return RedirectToAction("index", "Dashboard", new { Area = "admin" });
                        }
                        return Json(new { url = "/" });
                    }
                    
                }
            }

            return Json("The user name or password provided is incorrect.");
        }

        [Authorize]
        public ActionResult EditProfile()
        {
            if (!CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.EmployerNav = db.NavBars.Find(2);
            var currentUser = CurrentUserProfile;
            var model=   db.UserProfiles.Where(x => x.UserId == currentUser.UserId).FirstOrDefault();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public ActionResult EditProfile(UserProfile modelUserProfile)
        {
            if (!CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.EmployerNav = db.NavBars.Find(2);
            if (ModelState.IsValid)
            {
                var dbUser = db.UserProfiles.Where(x => x.UserId == CurrentUserProfile.UserId).FirstOrDefault();
                dbUser.FirstName = modelUserProfile.FirstName;
                dbUser.LastName = modelUserProfile.LastName;
                dbUser.Address = modelUserProfile.Address;
                dbUser.Address2 = modelUserProfile.Address2;
                dbUser.City = modelUserProfile.City;
                dbUser.State = modelUserProfile.State;
                dbUser.Zip = modelUserProfile.Zip;
                dbUser.CompanyName = modelUserProfile.CompanyName;
                dbUser.CompanyWebsite = modelUserProfile.CompanyWebsite;
                dbUser.Country = modelUserProfile.Country;
                db.SaveChanges();
                return View(dbUser);
            }
            return View(modelUserProfile);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            var temp = new StringBuilder(CurrentUserRole).ToString();
            FormsAuthentication.SignOut();
            Session["FormId"] = null;
            if (temp == "Job Seeker")
                return Redirect("/jobseeker/index");
            else if (temp == "Employer")
                return Redirect("/employer/index");
            return Redirect("/");
        }

        //
        // GET: /Account/ChangePassword
        /*
        public ActionResult ChangePassword()
        {
            return View();
        }
        
        //
        // POST: /Account/ChangePassword

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid && model.NewPassword == model.ConfirmPassword)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, userIsOnline: true);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }
        */

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(FormCollection fc)
        {
            MembershipUser currentUser = null;
            string newPassword = "";
            try
            {
                currentUser = Membership.GetUser(fc["Username"], false);
                currentUser.UnlockUser();
                newPassword = currentUser.ResetPassword();
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Error resetting password.");
            }

            if (newPassword != String.Empty)
            {

                MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["EmailFrom"].ToString(), fc["Username"], "Password Reset", "Your password has been reset to: " + newPassword);
                try
                {
                    SmtpClient smtp = new SmtpClient();
                    smtp.Send(mm);
                    return RedirectToAction("ChangePasswordSuccess");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Error sending email.");
                }
            }

            if (currentUser == null) ModelState.AddModelError("", "Invalid email.");
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion





        #region
        public ActionResult CreateOrder()
        {
            if (!CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            var JobFairList = db.JobFairs.ToList();

                return View(JobFairList);
        }

       
        public ActionResult GetOrderPay()
        {
            if (!CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "shoppingcart";
            ViewBag.EmployerNav = db.NavBars.Find(2);
            OrderViewModel model = new OrderViewModel();
            return View("~/Views/Account/Pay.cshtml",model);
        }

        [HttpPost]
        public ActionResult Pay(OrderViewModel model)
        {

            if (!CurrentUserProfile.IsActivated.Value)
                return RedirectToAction("activation", "home");
            ViewBag.absoluteUrl = "shoppingcart";
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://gwapi.demo.securenet.com/api/Payments/Charge");
        string secureNetId =  ConfigurationManager.AppSettings["secureNetId"].ToString();
        string secureKey = ConfigurationManager.AppSettings["secureKey"].ToString();

        request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(secureNetId + ":" + secureKey)));
        request.Method = WebRequestMethods.Http.Post;
        request.ContentType = "application/json";

        var chargeRequest = new ChargeRequest()
                        {
                            Amount = 11.00m,
                            Card = new Card()
                            {
                                Number = "4444 3333 2222 1111",
                                Cvv = "999",
                                ExpirationDate = "04/2016",
                                Address = new Address()
                                {
                                    Line1 = "123 Main St.",
                                    City = "Austin",
                                    State = "TX",
                                    Zip = "78759"
                                }
                            },
                            DeveloperApplication = new DeveloperApplication()
                            {
                                DeveloperId = Convert.ToInt32(ConfigurationManager.AppSettings["developerId"]),
                                Version = ConfigurationManager.AppSettings["versionId"].ToString()
                            }
                        };

        string payLoad = JsonConvert.SerializeObject(chargeRequest);
        SecureNetResponse result;

        using (StreamWriter sw = new StreamWriter(request.GetRequestStream()))
        {
            sw.Write(payLoad);
        }

        HttpWebResponse httpWebResponse = request.GetResponse() as HttpWebResponse;
        using (StreamReader sr = new StreamReader(httpWebResponse.GetResponseStream()))
        {
            if (httpWebResponse.StatusCode != HttpStatusCode.OK)
            {
                string message = String.Format("POST failed. Received HTTP {0}", httpWebResponse.StatusCode);
                throw new ApplicationException(message);
            }

            result = JsonConvert.DeserializeObject<SecureNetResponse>(sr.ReadToEnd());
        }
            ViewBag.Result = result.Success ? "Success" : "Failed";
        if (result.Success)
        {
            var or = new OrderRepository();
            var order = or.Get(User.Identity.Name);
            order.Active = true;
            or.Update(order);

            return RedirectToAction("order-success");
        }

            ViewBag.ErrorMessage = result.Message;

            return View();
        }
        #endregion


        #region  payment processing
  
    ///https://gwapi.demo.securenet.com/api/Payments/Charge
    ///
       // POST https://gwapi.demo.securenet.com/api/Payments/Verify
                        //        var request = new VerifyRequest()
                        //{
                        //    Amount = 0.00m,
                        //    Card = new Card()
                        //    {
                        //        Number = "4444 3333 2222 1111",
                        //        Cvv = "999",
                        //        ExpirationDate = "04/2016",
                        //        Address = new Address()
                        //        {
                        //            Line1 = "123 Main St.",
                        //            City = "Austin",
                        //            State = "TX",
                        //            Zip = "78759"
                        //        }
                        //    },
                        //    ExtendedInformation = new ExtendedInformation()
                        //    {
                        //        TypeOfGoods = "PHYSICAL"
                        //    },
                        //    DeveloperApplication = new DeveloperApplication()
                        //    {
                        //        DeveloperId = 12345678,
                        //        Version = "1.2"
                        //    }
                        //};
    
        #endregion
    }
}

