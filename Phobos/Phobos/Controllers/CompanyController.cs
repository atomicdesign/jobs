﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Controllers
{
    public class CompanyController : CivilianJobsBaseController
    {
        //
        [Authorize(Roles = "Employer")]  
        public ActionResult Index(string query = "")
        {
            query = query.ToString().Trim();
            ViewBag.EmployerNav = db.NavBars.Find(2);

            Dictionary<string, List<SearchResult>> results = new Dictionary<string, List<SearchResult>>();
         
            results.Add("Pages", Page.Search(query));
            results.Add("Events", Events.Search(query));
            results.Add("Forms", Form.Search(query));

            if (query != ""){
                return View();
            }else{
                return RedirectToAction("add");
            }

            
        }

        public ActionResult add() {
            //Need to check if this user already has a site. If so, redirect to their existing microsite page.
            ViewBag.EmployerNav = db.NavBars.Find(2);

            return View();
        }

    }
}
