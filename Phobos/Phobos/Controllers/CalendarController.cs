﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CivilianJobs.Models;

namespace CivilianJobs.Controllers
{
    public class CalendarController : CivilianJobsBaseController
    {
        //
        // GET: /Calendar/

        public ActionResult Index()
        {

            Page p = null;

            
            p = db.Pages.Find(260);

            if (p == null) return Http404();

            ViewBag.Title = p.Title;
            ViewBag.MetaDescription = p.MetaDescription;
            ViewBag.MetaKeywords = p.MetaKeywords;
            ViewBag.PrimaryNav = p.PrimaryNav;
            ViewBag.SecondaryNav = p.SecondaryNav;

            Page temp = p.Parent;
            List<Breadcrumb> bc = new List<Breadcrumb>();

            while (temp != null)
            {
                bc.Insert(0, new Breadcrumb { Name = temp.Title, URL = temp.FullSEOURL });
                temp = temp.Parent;
            }
            bc.Insert(0, new Breadcrumb { Name = p.Title, URL = p.FullSEOURL });
            ViewBag.Breadcrumbs = bc;



            //ViewBag.PrimaryNav = db.NavBars.Where(x => x.Name == "Primary").FirstOrDefault();            

            //ViewBag.SecondaryNav = db.NavBarSubItems.Where(x => x.ParentID == 10).FirstOrDefault();            

            List<Events> b = db.Events.OrderBy(e => e.EventDate).Where(e => e.EventDate > DateTime.Now).ToList();
            return View(b);
        }

        public ActionResult Date(string id)
        {
            DateTime date;
            if (id != null && id.Trim() != "" && DateTime.TryParse(id, out date))
            {
                List<Events> events = db.Events.Where(e => e.EventDate == date).ToList();
                return View(events);
            }
            return Http404();
            
        }

        public ActionResult Event(int id)
        {
            if (id != null && id > 0)
            {
                Events events = db.Events.Find(id);
                return View(events);
            }
            return Http404();
        }

        public JsonResult GetEvents()
        {
            List<Events> events = db.Events.OrderBy(e => e.EventDate).Where(e => e.EventDate > DateTime.Now).Take(4).ToList();
            var eventList = from e in events
                            select new
                            {
                                title = e.Heading,
                                start = e.EventDate.ToString("o"),
                                eventdesc = e.EventDesc
                            };
            return Json(eventList.ToArray());
        }

    }
}
