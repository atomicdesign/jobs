﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Caching;
using CivilianJobs.Models;
using CivilianJobs.Helpers;
using System.Web.Security;
using CivilianJobs.Models.Jobs.Enums;
using System.Data.Entity;
using System.Configuration;
using System.Web.Providers.Entities;
using System.Web.UI.WebControls;
using Newtonsoft.Json;


namespace CivilianJobs.Controllers
{
    public class CivilianJobsBaseController : Controller
    {
        protected CivilianJobsContext db = new CivilianJobsContext();
        protected UserProfile CurrentUserProfile;
        protected string CurrentUserRole;

        protected UserAlertHandler AlertHandler { get; set; }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            var settings = db.Settings.ToList();
            foreach (Setting s in settings) ViewData[s.Name] = s.Value;

            if (Request.IsAuthenticated)
            {
                CurrentUserProfile = db.LoadUser(User.Identity.Name);
                ViewBag.CurrentUserProfile = CurrentUserProfile;
                List<string> userRoles = Roles.GetRolesForUser(User.Identity.Name).ToList();

                if (userRoles.Contains("Employer"))
                {
                    CurrentUserRole = "Employer";
                }else if (userRoles.Contains("Job Seeker"))
                {
                    CurrentUserRole = "Job Seeker";
                }
                
                ViewBag.CurrentUserRole = CurrentUserRole;

            }
            ViewBag.MainNavbar = db.NavBars.FirstOrDefault(e => e.Name == "MainNavbar").Items;
            ViewBag.CivilianJobsPuck = new CivilianJobsPuck();
        }
       

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        protected string GetSetting(string key)
        {
            return (string)(ViewData[key] ?? String.Empty);
        }

        protected void ShowAlerts()
        {
            if (AlertHandler != null) ViewBag.UserAlerts = AlertHandler.GetAlerts();
        }

        public ActionResult Http404(){
            Response.StatusCode = 404;
            Response.StatusDescription = "HTTP/1.1 404 Not Found";

            if (db.Pages.Any(e => e.SEOURL == "notfound"))
            {
                MainController c = new MainController();
                c.Initialize(ControllerContext.RequestContext);
                return c.Index("notfound");
            }

            return HttpNotFound(); //fallback
        }

        public ActionResult Http301(string InUrl = "")
        {
            var outUrl = db.Redirects.FirstOrDefault(e=>e.InUrl == InUrl);
            if (outUrl != null)
            {
                Response.StatusCode = 301;
                Response.RedirectLocation = outUrl.OutUrl;
                outUrl.TimesUsed++;
                db.Entry(outUrl).State = EntityState.Modified;
                db.SaveChanges();
                return Content("");
            }
            return Http404();
        }

        protected string RenderViewToString(string viewName, object model)
        {
            string retval = String.Empty;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewContext.ViewData.Model = model;
                    viewResult.View.Render(viewContext, sw);
                    retval = sw.ToString();
                }
            }
            catch (Exception err) {
                LogHelper.Log(err);
            }
            return retval;
        }

        protected void RenderEmail(string viewName, object model, string ToEmail, string Subject, string FromEmail = null, string BCC = null)
        {
            try
            {
                string message = RenderViewToString("~/Views/EmailTemplates/" + viewName + ".cshtml", model);
                db.EmailQueue.Add(new EmailMessage(FromEmail ?? GetSetting("Site Admin Email"), ToEmail, Subject, message, BCC));
            }
            catch (Exception err) { LogHelper.Log(err); }
        }

        protected EmailMessage RenderEmailToMessage(string viewName, object model, string ToEmail, string Subject, string FromEmail = null, string BCC = null)
        {
            EmailMessage m = new EmailMessage(FromEmail ?? GetSetting("Site Admin Email"), ToEmail, Subject, "", BCC);
            try
            {
                m.HTML = RenderViewToString("~/Views/EmailTemplates/" + viewName + ".cshtml", model);
            }
            catch (Exception err) { LogHelper.Log(err); }
            return m;
        }

        protected void SendEmails()
        {
            EmailService es = new EmailService();
            es.SendBatch();
        }

        protected string GetCookie(string key)
        {
            if (Request.Cookies.AllKeys.Contains(key)) return Request.Cookies[key].Value.ToString();
            return String.Empty;
        }

        protected string GetFormField(string key)
        {
            if (Request.Form.AllKeys.Contains(key)) return Request.Form[key].ToString();
            return String.Empty;
        }

        protected string GetFormOrCookie(string key)
        {
            string retval = GetFormField(key);
            if (retval == String.Empty)
            {
                retval = GetCookie(key);
            }
            else
            {
                SetCookie(key, retval);
            }
            return retval;
        }

        protected void SetCookie(string key, string val)
        {
            HttpCookie cookie = new HttpCookie(key, val);
            cookie.Expires = DateTime.UtcNow.AddDays(1);
            cookie.Path = "/";
            Response.Cookies.Add(cookie);
        }

        protected void ClearCookie(string key)
        {
            if (Request.Cookies.AllKeys.Contains(key))
            {
                HttpCookie cookie = Request.Cookies[key];
                cookie.Expires = DateTime.UtcNow.AddDays(-1);
                Response.Cookies.Add(cookie);
            }
        }

        protected bool Throttle(string cachekey, int hours = 0, int minutes = 1, int seconds = 0, int repetition = 1)
        {
            int times = (int)(HttpRuntime.Cache.Get(cachekey) ?? 0);
            HttpRuntime.Cache.Insert(cachekey, times + 1, null, Cache.NoAbsoluteExpiration, new TimeSpan(hours, minutes, seconds));
            return times >= repetition;
        }

        protected ActionResult ErrorPage(string message)
        {
            ViewData["ErrorMessage"] = message;
            return View("~/Views/Shared/Error.cshtml");
        }

        protected IEnumerable<ModelError> GetModelErrors(ModelStateDictionary m)
        {
            List<ModelError> retval = new List<ModelError>();
            foreach (ModelState modelState in m.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    retval.Add(error);
                }
            }
            return retval;
        }

        protected string GetModelErrorsAsString(ModelStateDictionary modelState)
        {
            return String.Join("\n ", modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
        }

        protected IEnumerable<string> GetModelErrorMessages(ModelStateDictionary m)
        {
            List<ModelError> errors = (List<ModelError>)GetModelErrors(m);
            List<string> retval = new List<string>();
            foreach (ModelError error in errors) retval.Add(error.ErrorMessage);
            return retval;
        }

        protected string Template(string name)
        {
            return "~/Views/Templates/" + name + ".cshtml";
        }
    }
}
