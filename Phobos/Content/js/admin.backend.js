﻿$(function () {
    $('.alert.auto-dismiss button.close').each(DismissAlert);
    $('[data-dismiss="alert"]').click(DismissAlert);
    InitializeTinyMCE();
});

function DismissAlert() {
    $.ajax({
        url: '/api/adminalert/' + $(this).attr('data-id'),
        type: 'DELETE'
    });
}

function ShowContentBlockFieldEditor()
{
    tinymce.init({
        selector: 'input#Label',
        height: 200,
        menubar: false,
        verify_html: false,
        convert_urls: false,
        forced_root_block: false,
        doctype: "<!DOCTYPE html>",
        plugins: [
                "advlist autolink link lists charmap hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                "table contextmenu paste textcolor"
        ],
        toolbar: "code undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons", content_css: '/Content/css/site.css,/Content/css/StaticPage.css'
    });

    $("label[for=Label]").html("Content");

    $("#ToolTip").parent().hide();
    $("label[for=ToolTip]").parent().hide();

    $("#Required").parent().hide();
    $("label[for=Required]").parent().hide();
}


function InitContentBlockFieldEditor()
{
    if ($("select#TypeVal").val() == "CONTENT_BLOCK") ShowContentBlockFieldEditor();

    $("select#TypeVal").change(function ()
    {
        if ($(this).val() == "CONTENT_BLOCK") ShowContentBlockFieldEditor();
        else if ($("input#Label").siblings(".mce-tinymce").length > 0)
        {
            tinymce.remove('#Label');
            $("label[for=Label]").html("Label");

            $("#ToolTip").parent().show();
            $("label[for=ToolTip]").parent().show();

            $("#Required").parent().show();
            $("label[for=Required]").parent().show();
        }
    });
}



function InitializeTinyMCE(){
    tinymce.init({
        selector: 'textarea.wysiwyg',
        height: 600,
        menubar: false,
        verify_html: false,
        convert_urls: false,
        forced_root_block: false,
        doctype: "<!DOCTYPE html>",
        plugins: [
                "advlist autolink link lists charmap hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                "table contextmenu paste textcolor"
        ],
        toolbar: "code undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons"
        ,content_css: '/Content/css/site.css,/Content/css/StaticPage.css'
    });

    tinymce.init({
        selector: 'textarea.wysiwygsmall',
        height: 200,
        menubar: false,
        verify_html: false,
        convert_urls: false,
        forced_root_block: false,
        doctype: "<!DOCTYPE html>",
        plugins: [
                "advlist autolink link lists charmap hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                "table contextmenu paste textcolor"
        ],
        toolbar: "code undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | forecolor backcolor emoticons"
   , content_css: '/Content/css/site.css,/Content/css/StaticPage.css'
    });

}

var HBTemplates = {};
function LoadHBTemplate(templateID) { HBTemplates[templateID] = Handlebars.compile($('#' + templateID).html()); }

function BindObjectToForm(obj, form) {
    for (variable in obj) {
        $(form).children('[name="' + variable + '"]').val(obj[variable]);
    }
}

//TODO: more validation
function KillEvent(e) { if (e) { try { e.stopPropagation(); e.preventDefault(); } catch (Exception) { } } }