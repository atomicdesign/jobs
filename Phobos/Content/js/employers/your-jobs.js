﻿$(function () {

    $("#emp-job-tab").click(function () {
        if ($("#emp-job-tab").hasClass("jobs-tab-inactive")) {
            $(this).addClass("jobs-tab-active");
            $(this).removeClass("jobs-tab-inactive");
            $("#emp-cand-tab").addClass("candidates-tab-inactive");
            $("#emp-cand-tab").removeClass("candidates-tab-active");
            $("#emp-all-jobs").removeClass("hide");
            $("#emp-all-candidates").addClass("hide");

        }
    });

    $("#emp-cand-tab").click(function () {
        if ($("#emp-cand-tab").hasClass("candidates-tab-inactive")) {
            $(this).addClass("candidates-tab-active");
            $(this).removeClass("candidates-tab-inactive");
            $("#emp-job-tab").addClass("jobs-tab-inactive");
            $("#emp-job-tab").removeClass("jobs-tab-active");
            $("#emp-all-jobs").addClass("hide");
            $("#emp-all-candidates").removeClass("hide");
        }
    });

    $('#btn-post-job').click(function () {
        window.location.href = 'post-job';
    });

});

    $("div.active-menu > a").click(function (e) {
        e.preventDefault();
        var t = $(this).attr("id");
        $(this).addClass("selected");
        $this_div = $(this);
        $div = $("div.box[id=" + t + "]");
        $div.fadeIn();
        $("div.box").not($div).hide();
        $("div.active-menu > a").not($this_div).removeClass("selected");
        return false
    });

    $('div.box#Active').show();
    function filterGlobal() {
        $('#yourJobs').DataTable().search(
            $('#global_filter').val(),
            $('#global_regex').prop('checked'),
            $('#global_smart').prop('checked')
        ).draw();
    }

    $(document).ready(function () {
        $('#yourJobs').DataTable();

        $('input.global_filter').on('keyup click', function () {
            filterGlobal();
        });

        $("div.active-menu > a").on('keyup click', function (e) {
            e.preventDefault();
            var t = $(this).attr("id");
            $('#yourJobs').DataTable().search(t).draw();
            $(this).addClass("selected");
            $("div.active-menu > a").not($(this)).removeClass("selected");
            filterGlobal();
        });

        $('#yourJobs').DataTable().search('Active').draw();
    });

