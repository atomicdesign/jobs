﻿$(function () {

    $('#email-job').click(function (e) {
        var $this = $(this),
            $popup = $('<div id="email-popup">' +
                        '<p>Please enter a comma-separated list of email addresses:</p>' +
                        '<input type="text" id="email-addresses" /><br><br>' +
                        '<input type="submit" id="send-email" /><a style="margin-left: 25px;" href="#" id="cancel-send-email">Cancel</a>' +
                      '</div>');

        e.preventDefault();

        $popup.find('#send-email').click(function () {

            $.post("/jobseeker/email-job", { emails: $('#send-mail').val() })
                .done(function () {
                    $popup
                     .hide()
                     .find('#send-email').val('');
                });
        });

        $popup.find('#cancel-send-email').click(function (e) {
            e.preventDefault();
            $popup.hide();
        });

        $popup.hide();
        $this.parent().append($popup);
        $popup.fadeIn('fast');
    });

    $('#save-job').click(function (e) {
        var $this = $(this),
            count = 0;

        e.preventDefault();

        $this
            .text('.')
            .css('text-decoration', 'none');

        var timer = setInterval(function () {
            var length = $this.text().length;
            count++;

            if (length === 1) {
                $this.text('. .');
            }
            else if (length === 3) {
                $this.text('. . .');
            }
            else {
                $this.text('.');
            }

            if (count === 3) {
                clearInterval(timer);
                $this.hide().text('Job Saved').fadeIn();

                setTimeout(function () {
                    $this.fadeOut();
                }, 100);
            }
        }, 500);
   
        $.post('/jobseeker/save-job', { id: $('#save-job').data('jobId') })
            .done(function () {
                clearInterval(timer);
            });
    });

});