﻿$(function () {
    
    $('[name=JobPostingMonths]').change(function () {
        var $option = $('[name=JobPostingMonths] option:selected');

        $('#JobPostingMonthsFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=ResumeAccessViewsPerDay]').change(function () {
        var $option = $('[name=ResumeAccessViewsPerDay] option:selected');

        $('#resume-access-1, #resume-access-2, #resume-access-3, #resume-access-4').hide();
        $('#ResumeAccessMonthsAccessFee').text('$0.00');

        if ($option.val() === '0') {
            $('#resume-access-1').show();
        }
        else if ($option.val() === '200') {
            $('#resume-access-2').show();
        }
        else if ($option.val() === '400') {
            $('#resume-access-3').show();
        }
        else if ($option.val() === '600') {
            $('#resume-access-4').show();
        }
        
        calculateTotal();
    });
    $('#resume-access-1').change(function () {
        var $option = $('#resume-access-1 option:selected');
        $('#ResumeAccessMonthsAccessFee').text( $option.data('fee'));
        calculateTotal();
    })
    $('#resume-access-2').change(function () {
        var $option = $('#resume-access-2 option:selected');
        $('#ResumeAccessMonthsAccessFee').text( $option.data('fee'));
        calculateTotal();
    })
    $('#resume-access-3').change(function () {
        var $option = $('#resume-access-3 option:selected');
        $('#ResumeAccessMonthsAccessFee').text( $option.data('fee'));
        calculateTotal();
    })

    $('#resume-access-4').change(function () {
        var $option = $('#resume-access-4 option:selected');
        $('#ResumeAccessMonthsAccessFee').text( $option.data('fee'));
        calculateTotal();
    })
    $('[name=ResumeAccessMonthsAccess0]').change(function () {
        var $option = $('[name=ResumeAccessMonthsAccess0]:visible option:selected');

        $('#ResumeAccessMonthsAccessFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=ResumeAccessMonthsAccess200]').change(function () {
        var $option = $('[name=ResumeAccessMonthsAccess200]:visible option:selected');

        $('#ResumeAccessMonthsAccessFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=ResumeAccessMonthsAccess400]').change(function () {
        var $option = $('[name=ResumeAccessMonthsAccess400]:visible option:selected');

        $('#ResumeAccessMonthsAccessFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=ResumeAccessMonthsAccess600]').change(function () {
        var $option = $('[name=ResumeAccessMonthsAccess600]:visible option:selected');

        $('#ResumeAccessMonthsAccessFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=CombinationJobPostingResumeAccess]').change(function () {
        var $option = $('[name=CombinationJobPostingResumeAccess] option:selected');

        $('#CombinationJobPostingResumeAccessFee').text($option.data('fee'));
        
        if ($option.val() === '0') {
            $('[name=ResumeAccessMonthsAccess]').removeProp('disabled');
            $('[name=ResumeAccessViewsPerDay]').removeProp('disabled');
        }
        else {
            $('#ResumeAccessMonthsAccessFee').text('$0.00');
            $('[name=ResumeAccessMonthsAccess]').prop('disabled', true);
            $('[name=ResumeAccessViewsPerDay]').prop('disabled', true);
        }

        calculateTotal();
    });

    $('[name=PrintAdvertising]').change(function () {
        var $option = $('[name=PrintAdvertising] option:selected');

        $('#PrintAdvertisingFee').text($option.data('fee'));

        calculateTotal();
    });
    
    $('[name=BannerAdvertising]').change(function () {
        var $option = $('[name=BannerAdvertising] option:selected');

        $('#BannerAdvertisingFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=PreSelectedResumes]').change(function () {
        var $option = $('[name=PreSelectedResumes] option:selected');

        $('#PreSelectedResumesFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=PostJobsAtMilitaryBaseTransitionOffices]').change(function () {
        var $option = $('[name=PostJobsAtMilitaryBaseTransitionOffices] option:selected');

        $('#PostJobsAtMilitaryBaseTransitionOfficesFee').text($option.data('fee'));

        calculateTotal();
    });

    $('[name=JobFairPackage]').change(function () {
        var $option = $('[name=JobFairPackage] option:selected');

        if ($option.val() !== '') {
            $('#job-fairs-list').show();
        }
        else {
            $('#job-fairs-list').hide();
        }

        if ($option.val() === 'Build-your-own Job Fair Package') {
            $('#job-fairs-packages').show();
        }
        else {
            $('#job-fairs-packages').hide();
        }

        calculateTotal();
    });

    $('.job-fair, #job-fairs-packages input').click(function () {
        calculateTotal();
    });

    $('.job-fair').click(function () {
        var ids = '';

        $('.job-fair:checked').each(function () {
            var $this = $(this);

            if (ids.length) {
                ids += ',';
            }

            ids += $this.data('jobFairId');
        });

        $('[name=JobFairIds]').val(ids);
    });

    $('#btn-next').click(function () {
        var total = calculateTotal();

        if (total < 1) {
            $('#error').show();
            return false;
        }
        else {
            $('#error').hide();
        }
    });

    function calculateTotal() {
        var total = 0,
            jobFairFee = 800;

        $('.fee').each(function () {
            total += parseFloat($(this).text().substring(1).replace(',', ''));
        });

        total += $('.job-fair:checked').length * jobFairFee;

        $('#job-fairs-packages input:checked').each(function () {
            total += parseFloat($(this).data('fee').substring(1).replace(',', ''));
        })

        $('#total-price').text('$' + total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
        $('[name="total"]').val(total);

        return total;
    }


    // basic package
    



//    $.ajax({
//        type: "POST",
//        Accept: "application/json",
//        contentType: "application/json",
//        url: url,
//        data: { name: "John", location: "Boston" },
//        dataType: dataType,
//        success: success,
//        error: printError,
//        statusCode: {
//            404: function () {
//                alert("page not found");
//                             }
//                    }                   
//    });
});

$("#BasicQuantity").on('change keyup paste', function () {
    var qty = 1 * $(this).val();
    if (qty >= 1) {
        var price = 1 * $("#BasicRate").text().replace('$', '');
        var total = qty * price;
        $('#BasicSubTotal').text(total);
        $('#BasicSubTotal').text('$ ' + total);
        $('#BasicSubTotal').attr('data-price', total);
    }
});


function addtocartbasic() {
    var description = "JOB FAIRS BASIC PACKAGE";
    var quantity = 1 * $('#BasicQuantity').val();
    if (quantity <= 0) {
        alert("Please add quantity");
        return false;
    }
    var unitPrice = 1 * $("#BasicRate").text().replace('$', '');
    var subTotal = $('#BasicSubTotal').attr('data-price');
    var url = "/ShoppingCart/AddToCart";
    var dataObject = { 'Description': description, 'Quantity': quantity, 'UnitPrice': unitPrice, 'SubTotal': subTotal };
    var json = JSON.stringify(dataObject);

    $.ajax({
        type: "POST",
        Accept: "application/json",
        contentType: "application/json",
        url: url,
        data: json,
        dataType: JSON,
        success: function () { alert("Add to cart") },
        statusCode: {
            404: function () {
                alert("page not found");
            }
        }
    });
    event.preventDefault();
}


